## Install
1. Install __Sphinx__ e.g. via _pip_. Use the *PREFIX_PATH* variable to install to custom path.
    
    ```
     pip install Sphinx
    ```
    or
    ```
     export PREFIX_PATH=$HOME
     pip install --install-option="--prefix=$PREFIX_PATH" Sphinx
    ```
    
## Setup
1. Go to your project directory and create a  *docs/* folder: `mkdir docs; cd docs`.

2. Go to *docs/* and setup Sphinx by executing `sphinx-quickstart`. Be sure to say yes to the __autodoc__ option.

    ```
    Welcome to the Sphinx 1.3.1 quickstart utility.
    
    Please enter values for the following settings (just press Enter to
    accept a default value, if one is given in brackets).
    
    Enter the root path for documentation.
    > Root path for the documentation [.]: 
    
    You have two options for placing the build directory for Sphinx output.
    Either, you use a directory "_build" within the root path, or you separate
    "source" and "build" directories within the root path.
    > Separate source and build directories (y/n) [n]: y
    
    Inside the root directory, two more directories will be created; "_templates"
    for custom HTML templates and "_static" for custom stylesheets and other static
    files. You can enter another prefix (such as ".") to replace the underscore.
    > Name prefix for templates and static dir [_]: 
    
    The project name will occur in several places in the built documentation.
    > Project name: simple_test_project
    > Author name(s): Christian Strohm
    
    Sphinx has the notion of a "version" and a "release" for the
    software. Each version can have multiple releases. For example, for
    Python the version is something like 2.5 or 3.0, while the release is
    something like 2.5.1 or 3.0a1.  If you don't need this dual structure,
    just set both to the same value.
    > Project version: 0.0.1
    > Project release [0.0.1]: 
    
    If the documents are to be written in a language other than English,
    you can select a language here by its language code. Sphinx will then
    translate text that it generates into that language.
    
    For a list of supported codes, see
    http://sphinx-doc.org/config.html#confval-language.
    > Project language [en]: 
    
    The file name suffix for source files. Commonly, this is either ".txt"
    or ".rst".  Only files with this suffix are considered documents.
    > Source file suffix [.rst]: 
    
    One document is special in that it is considered the top node of the
    "contents tree", that is, it is the root of the hierarchical structure
    of the documents. Normally, this is "index", but if your "index"
    document is a custom template, you can also set this to another filename.
    > Name of your master document (without suffix) [index]: 
    
    Sphinx can also add configuration for epub output:
    > Do you want to use the epub builder (y/n) [n]: 
    
    Please indicate if you want to use one of the following Sphinx extensions:
    > autodoc: automatically insert docstrings from modules (y/n) [n]: y
    > doctest: automatically test code snippets in doctest blocks (y/n) [n]: 
    > intersphinx: link between Sphinx documentation of different projects (y/n) [n]: y
    > todo: write "todo" entries that can be shown or hidden on build (y/n) [n]: 
    > coverage: checks for documentation coverage (y/n) [n]: 
    > pngmath: include math, rendered as PNG images (y/n) [n]: y
    > mathjax: include math, rendered in the browser by MathJax (y/n) [n]: 
    > ifconfig: conditional inclusion of content based on config values (y/n) [n]: 
    > viewcode: include links to the source code of documented Python objects (y/n) [n]: y
    
    A Makefile and a Windows command file can be generated for you so that you
    only have to run e.g. `make html' instead of invoking sphinx-build
    directly.
    > Create Makefile? (y/n) [y]: 
    > Create Windows command file? (y/n) [y]: 
    
    Creating file ./source/conf.py.
    Creating file ./source/index.rst.
    Creating file ./Makefile.
    Creating file ./make.bat.
    ```

3. The Sphinx settings are stored in *source/conf.py*. It is standard _python_ code.

4. In order to use **[Google](https://google.github.io/styleguide/pyguide.html)** or **[Numpy](https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt)** style guidelines in your docstrings you need to add the __napoleon__ extension.
   - In order to install __napoleon__ execute `pip install sphinxcontrib-napoleon`.
   - Then edit *source/conf.py* and look for the _extensions_ variable.
     Add `'sphinx.ext.napoleon'` to that list.

6. For Sphinx to document a _package_ its parent directory needs to be included in __python__'s search path for modules. (see the `PYTHONPATH` variable)
   Uncomment the line `# sys.path.insert(0, os.path.abspath('.'))` and adjust the path `.` to refer to the directory where the package is originated.

   Assuming the following structure of your project directory

   ```
   projectdir
   │   README.md
   │   ...
   │
   └───docs
   │   │   make.bat
   │   │   Makefile
   │   │
   │   └───build
   │   └───source
   │   
   └───src
       │   __init__.py
       │   ...
   ```

   this should be *projectdir* and the line should read `sys.path.insert(0, os.path.abspath('../..'))`.

## Add Content

---
**Warning**
Read the next section before modifying any files.

---

1. The *.rst* files in *source* contain the contents of your documentation in __reStructuredText__ format.
   The root file (*source/index.rst*) serves as a welcome page and contains the root of the "table of contents tree" (or _toctree_).
   The _toctree_ is a way to connect multiple files to a single hierarchy of documents. Initially it is empty and looks like this:

   ```
   .. toctree::
      :maxdepth: 2
   ```

   To add documents, you need to list them in the _content_ of the directive:

   ```
   .. toctree::
      :maxdepth: 2

      intro
      tutorial
      ...
   ```

2. To document __python objects__ like _functions_, _methods_, _classes_ or _modules_, Sphinx provides directives like

   ```
   .. function:: enumerate(sequence[, start=0])

      Return an iterator that yields tuples of an index and an item of the
      *sequence*. (And so on.)
   ```

   The argument of the directive is the signature of the object you describe, the content is the documentation for it. Multiple signatures can be given, each in its own line.

   Since the __autodoc__ extension has been enabled, you can also generate the documentation of `enumerate` automatically with
   ```
   .. autofunction:: enumerate
   ```
   Its signature and docstring are then read directly from the source file.

   [You can also document whole classes or even modules this way.
   Documenting modules this way work with]
   ```
   .. automodule:: io
      :members:
   ```
   The *member* options will include the docstring of all objects in the module `io`.

The provided directives can be used in your *.rst* files as well as in your docstrings.
For further information see http://www.sphinx-doc.org/en/1.5.1/rest.html#rst-primer.

---
**Warning**
__autodoc__ needs to import your modules in order to extract the docstrings. This is why you had to add the appropriate path to sys.path in your conf.py.
If any modules have side effects on import, these will be executed by autodoc when sphinx-build is run.
If you document scripts (as opposed to library modules), make sure their main routine is protected by a `if __name__ == '__main__'` condition.

---

## sphinx-apidoc 
To use the __autodoc__ extension to generate a skeleton of _*.rst_ files documenting your project run
```
sphinx-apidoc -f -o docs/source projectdir
```
This will generate _*.rst_ files in your _docs/source/_ directory (`-o docs/source`), overwriting existing files if necessary (`-f`).

## Compile
1. To generate a html build going in the _docs/_ directory and run the Makefile `cd docs; make html`.
   The results are written to the build directory *docs/build*.
   You can clean the entire build directory by executing `make clean`.

2. You may change the general appearance of your documentation by modifying the corresponding variables in *source/conf.py*.
   ```
   html_logo = 'img/project_logo.png'
   html_theme = 'haiku'
   html_theme_options = {
       "full_logo": True
   }
   pygments_style = 'trac'
   ```

---
**Warning**
Ensure that in the __Makefile__ the links to the executables are set according to your choice.
When, for example, you installed Sphinx locally at `$HOME`, then change the value of `SPHINXBUILD` to *`$HOME`/bin/sphinx-build*.
And `export PYTHONPATH=$HOME/lib/python3.4/site-packages/`.

---
