# -*- coding: utf-8 -*-
import os
import tikzplotlib
import numpy as np
import matplotlib.pyplot as plt


def get_png_file_name(DATA_FILE, appendix, file_type="png"):
    """ Set name for mesh file.
    """
    if file_type[0] != ".":
        file_type = "." + file_type
    file_name = DATA_FILE[DATA_FILE.rindex("/")+1:]
    path = "../img/"
    os.makedirs(path, exist_ok=True)
    assert file_type[0] == "."  # file_type starts with .
    new_file_name = file_name[:file_name.rindex(".")] + appendix + file_type
    return path + new_file_name


def load_level_data(file):
    """ Load level data from file. """
    _data = np.load(file, allow_pickle=True)["lvl_data"].item()
    return _data


def set_legend_upper_left(ax, handles=None):
    """ Add legend to plot on upper left side (outside of plot). """

    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.75, box.height])

    # Put a legend to the right of the current axis
    if handles is None:
        leg = ax.legend(loc='upper left', bbox_to_anchor=(1.05, 1))
    else:
        leg = ax.legend(handles=handles, loc='upper left',
                        bbox_to_anchor=(1.05, 1))
    return leg


def set_legend_lower_left(ax, handles=None):
    """ Add legend to plot on upper left side (outside of plot). """

    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.75, box.height])

    # Put a legend to the right of the current axis
    if handles is None:
        leg = ax.legend(loc='lower left', bbox_to_anchor=(1.05, 0.))
    else:
        leg = ax.legend(handles=handles, loc='lower left',
                    bbox_to_anchor=(1.05, 0.))
    return leg


def color_ref_type(ax, x_axis_vals, lvl_data, colors=None, label=None):
    """ Color convergence plot according to refinement type.

    Parameters
    ----------
    ax : matplotlib.axes._subplots.AxesSubplot
        Axes the coloring is applied to.
    x_axis_vals : list
        Coordinates on x-axis of refinements.
    ref_type : list of int
        Refinement types.
    colors : None or list of int, default = None
        List of colors for the ref_types. If None, default tab20 colors are
        used.
    label : None or list of str, default = None
        List of labels for the refinement types. If None, no labels are set.
    """
    ref_types = lvl_data["ref_type"]
    n_vmcs = lvl_data["n_vmc"]
    sdims = lvl_data["sdim"]
    cmap = plt.cm.tab20
    assert len(ref_types) == len(n_vmcs) == len(sdims)

    assert len(x_axis_vals) == len(ref_types)
    assert len(x_axis_vals) > 1

    unique_ref_types = np.unique(ref_types)
    if colors is None:
        colors = [cmap(2*j) for j, _ in enumerate(unique_ref_types)]
    hatch_ids = ["oo", "//", "\\\\", ".."]  # det, dim, mode, samples

    # get hatching patterns
    hatches = []
    zips = zip(ref_types[:-1], n_vmcs[:-1], sdims[:-1])
    for j, (rt, n, s) in enumerate(zips):
        if rt == 0:  # est_det dominates
            hatches.append(hatch_ids[0])
        elif rt == 1:  # est_sto dominates
            hatch = ""
            if max(s) < max(sdims[j+1]):
                hatch += hatch_ids[1]
            if len(s) < len(sdims[j+1]):
                hatch += hatch_ids[2]
            hatches.append(hatch)
        elif rt == 2:  # est_alg dominates
            hatch = ""
            if n < n_vmcs[j+1]:
                hatch += hatch_ids[3]
            else:
                if max(s) < max(sdims[j+1]):
                    hatch += hatch_ids[1]
                if len(s) < len(sdims[j+1]):
                    hatch += hatch_ids[2]
            hatches.append(hatch)
    assert len(hatches) == len(ref_types) - 1

    assert len(colors) >= len(unique_ref_types)
    for j, ref_type in enumerate(ref_types[:-1]):
        left, right = x_axis_vals[j], x_axis_vals[j+1]
        ax.axvspan(left, right, 1e-5, 1e+2, facecolor="white", alpha=0.5,
                   hatch=hatches[j], edgecolor=colors[ref_type])

    import matplotlib.patches as mpatches
    det_ref = mpatches.Patch(color=cmap(1), label='ref $\eta_{\mathrm{det}}$')
    sto_ref = mpatches.Patch(color=cmap(3), label='ref $\eta_{\mathrm{sto}}$')
    alg_ref = mpatches.Patch(color=cmap(5), label='ref $\eta_{\mathrm{alg}}$')
    T_inc = mpatches.Patch(facecolor="white", hatch=hatch_ids[0],
                           label='$\\mathcal{T}\\nearrow$')
    dim_inc = mpatches.Patch(facecolor="white", hatch=hatch_ids[1],
                             label='max dim $\\nearrow$')
    mode_inc = mpatches.Patch(facecolor="white", hatch=hatch_ids[2],
                              label='modes $\\nearrow$')
    smpl_inc = mpatches.Patch(facecolor="white", hatch=hatch_ids[3],
                              label='$N_{\\mathrm{VMC}}\\nearrow$')
    handles = [det_ref, sto_ref, alg_ref, T_inc, dim_inc, mode_inc, smpl_inc]
    # set_legend_lower_left(ax, handles=handles)

    return handles


def plot_slope(ax, slope, x_axis_vals, y_start, log_type, label=True,
               offset=None, slope_label=None, **kwargs):
    """ Plot slope line in logarithmic scale.

    Parameters
    ----------
    ax : matplotlib.axes._subplots.AxesSubplot
        Axes the coloring is applied to.
    slope : float
        Slope of the loglog line.
    x_axis_vals : list of length 2
        Start and end point on x-axis of slope line.
    y_start : float
        Start value on y-axis (in x_axis_vals[0]) of slope line.
    log_type : str
        Slope for loglog plot or semilogy plot.
    label : bool, default=True
        Add slope label to the middle of the line.
    offset : None or list of length 2, default=None
        Point offset (x, y) of label text.
    """
    if log_type == "loglog":
        def linear_function(x):
            log_x = np.log(x)-np.log(x_axis_vals[0])
            return np.exp(slope*log_x + np.log(y_start))
    elif log_type == "semilogy":
        def linear_function(x):
            log_x = x-x_axis_vals[0]
            return np.exp(slope*log_x + np.log(y_start))
    else:
        raise ValueError(f"Unknown log_type '{log_type}'")

    vals = [linear_function(x_axis_vals[0]), linear_function(x_axis_vals[1])]
    if log_type == "loglog":
        ax.loglog(x_axis_vals, vals, "--k", **kwargs)
    elif log_type == "semilogy":
        ax.semilogy(x_axis_vals, vals, "--k", **kwargs)
    if label is True:
        if log_type == "semilogy":
            x_axis_vals = np.exp(x_axis_vals)
        x_coord = np.exp(np.mean(np.log(x_axis_vals)))
        y_coord = linear_function(x_coord)
        if offset is None:
            offset = (0, 10)
        if slope_label is None:
            ax.annotate(f"{slope:g}",  # this is the text
                        (x_coord, y_coord),  # this is the point to label
                        textcoords="offset points",  # how to position the text
                        xytext=offset,  # distance from text to points (x,y)
                        ha='center')
        else:
            ax.annotate(f"{slope_label}",  # this is the text
                        (x_coord, y_coord),  # this is the point to label
                        textcoords="offset points",  # how to position the text
                        xytext=offset,  # distance from text to points (x,y)
                        ha='center',
                        fontsize=20)


def plot_level_data(ax, _data, x_key, y_key, log_type, ms, c, alpha=1,
                    label=None, **kwargs):
    """ Plot level data against each other.

    Parameters
    ----------
    ax : matplotlib.Axes
        Axes of figure.
    _data : dict
        Level data dictionary.
    x_key : str
        Key for x-axis.
    y_key : str
        Key for y-axis.
    log_type : str
        Type of log plot, i.e. loglog or semilogy.
    ms : str
        Marker style.
    c : str
        Color.
    label : str, optional

    """

    if log_type == "loglog":
        ax.loglog(_data[x_key], _data[y_key], ms, c=c, alpha=alpha,
                  label=label, **kwargs)
    elif log_type == "semilogy":
        ax.semilogy(_data[x_key], _data[y_key], ms, c=c, alpha=alpha,
                    label=label, **kwargs)
    else:
        raise ValueError(f"Unknown log_type '{log_type}'")


def save_as_tikz(path):
    """ Save current figure as LaTex file. """
    os.makedirs(path, exist_ok=True)
    if path[-1] != "/":
        path += "/"
    # tikzplotlib.clean_figure()
    tikzplotlib.save(path+"tikz_image.tex")

    text = "\\documentclass{standalone}\n"
    text += "\\usepackage[utf8]{inputenc}\n"
    text += "\\usepackage{graphicx}\n"
    text += "\\usepackage{pgfplots}\n"
    # text += "\\DeclareUnicodeCharacter{2212}{" + u"\u2212" + "}\n"
    text += "\\usepgfplotslibrary{groupplots,dateplot}\n"
    text += "\\usetikzlibrary{patterns,shapes.arrows}\n"
    text += "\\pgfplotsset{compat=newest}\n"
    text += "\\begin{document}\n"
    text += "\\input{tikz_image.tex}\n"
    text += "\\end{document}"
    with open(path+"main.tex", "w") as file:
        file.write(text)
