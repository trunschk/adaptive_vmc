import os
import shutil
from tqdm import tqdm


def get_dirs(path):
    list_dirs = os.listdir(path)
    if path[-1] != "/":
        path += "/"
    ret = []
    for d in list_dirs:
        if os.path.isdir(path+d) and d != "test":
            ret.append(d)
    return ret


def compile_pdf(subdirs, path):
    os.chdir(path)
    print("compile tex files")
    for subdir in tqdm(subdirs):
        os.chdir(subdir)
        os.system("pdflatex main.tex > .tex_out.log")
        os.chdir("..")


def copy_pdf(subdirs, dest):
    if dest[-1] != "/":
        dest += "/"
    for subdir in subdirs:
        src = "./" + subdir + "/main.pdf"
        if os.path.isfile(dest + subdir + ".pdf"):
            os.remove(dest + subdir + ".pdf")
        if os.path.isfile(src):
            shutil.copy(src, dest)
            os.rename(dest+"main.pdf", dest + subdir + ".pdf")
            print(f"copy {src} -> {dest + subdir + '.pdf'}")


def main():
    source = "../tikz"
    dirs = get_dirs(source)
    compile_pdf(dirs, path=source)
    copy_pdf(dirs, "/home/nando/Dropbox/Apps/Overleaf/adaptive_vmc/img/tikz")


if __name__ == "__main__":
    main()
