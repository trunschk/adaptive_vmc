# -*- coding: utf-8 -*-
import os
import numpy as np
import matplotlib.pyplot as plt

from fenics import plot
import xerus as xe

from context import src
from src.misc import get_y_samples
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
from src.problem.equation.darcy import Problem

from avmc_utils import datetime_id
from avmc_utils import load_level_data
from plot_utils import get_png_file_name


def plot_diffusion_coefficient_random(DATA_FILE, level=-1, plot_mesh=False):
    """ Plot mean of diffusion coefficient from level data file.
    """
    lvl_data = load_level_data(DATA_FILE)
    info = lvl_data["info"][level]
    dist = info["sampling"]["distribution"]
    decay = info["expansion"]["decay rate"]
    if dist == "normal":
        scale = info["expansion"]["vartheta"] * info["expansion"]["rho"]
    else:
        scale = 1
    space = Problem(info).space
    mesh = Problem(info).mesh if plot_mesh else None
    kappa_reco = lvl_data["kappa_reco"][level]

    ys = get_y_samples(info, 1).reshape(1, -1)
    val = tt.eval_approx(kappa_reco, ys, dist, scale=scale, decay=decay)[0]

    file_name = get_png_file_name(DATA_FILE, "_diffCoeff_sample", "png")
    fig, ax = plt.subplots(figsize=(10, 10), nrows=1, ncols=1)
    fem.plot_fenics_function(ax, val, space, mesh=mesh, cbar=False)
    ax.axis("off")
    plt.savefig(file_name, bbox_inches="tight", dpi=600)
    print(f"save mesh to: {file_name}")


def plot_diffusion_coefficient_mean(DATA_FILE, level=-1, plot_mesh=False):
    """ Plot mean of diffusion coefficient from level data file.
    """
    lvl_data = load_level_data(DATA_FILE)
    info = lvl_data["info"][level]
    dist = info["sampling"]["distribution"]
    decay = info["expansion"]["decay rate"]
    if dist == "normal":
        scale = info["expansion"]["vartheta"] * info["expansion"]["rho"]
    else:
        scale = 1
    space = Problem(info).space
    mesh = Problem(info).mesh if plot_mesh else None
    kappa_reco = lvl_data["kappa_reco"][level]

    ys = get_y_samples(info, 1000)
    val = np.mean(tt.eval_approx(
        kappa_reco, ys, dist, scale=scale, decay=decay), axis=0)

    file_name = get_png_file_name(DATA_FILE, "_diffCoeff_mean", "png")
    fig, ax = plt.subplots(figsize=(10, 10), nrows=1, ncols=1)
    fem.plot_fenics_function(ax, val, space, mesh=mesh, cbar=True)
    ax.axis("off")
    plt.savefig(file_name, bbox_inches="tight", dpi=600)
    print(f"save mesh to: {file_name}")


def plot_diffusion_coefficient_var(DATA_FILE, level=-1, plot_mesh=False):
    """ Plot mean of diffusion coefficient from level data file.
    """
    lvl_data = load_level_data(DATA_FILE)
    info = lvl_data["info"][level]
    dist = info["sampling"]["distribution"]
    decay = info["expansion"]["decay rate"]
    if dist == "normal":
        scale = info["expansion"]["vartheta"] * info["expansion"]["rho"]
    else:
        scale = 1
    space = Problem(info).space
    mesh = Problem(info).mesh if plot_mesh else None
    kappa_reco = lvl_data["kappa_reco"][level]

    ys = get_y_samples(info, 1000)
    val = np.var(tt.eval_approx(
        kappa_reco, ys, dist, scale=scale, decay=decay), axis=0)

    file_name = get_png_file_name(DATA_FILE, "_diffCoeff_var", "png")
    fig, ax = plt.subplots(figsize=(10, 10), nrows=1, ncols=1)
    fem.plot_fenics_function(ax, val, space, mesh=mesh, cbar=True)
    ax.axis("off")
    plt.savefig(file_name, bbox_inches="tight", dpi=600)
    print(f"save mesh to: {file_name}")


def plot_solution_mean(DATA_FILE, level=-1, plot_mesh=False):
    """ Plot mean of solution from level data file.
    """
    lvl_data = load_level_data(DATA_FILE)
    info = lvl_data["info"][level]
    dist = info["sampling"]["distribution"]
    decay = info["expansion"]["decay rate"]
    if dist == "normal":
        scale = info["expansion"]["vartheta"] * info["expansion"]["rho"]
    else:
        scale = 1
    space = Problem(info).space
    mesh = Problem(info).mesh if plot_mesh else None
    u_reco = lvl_data["u_reco"][level]

    ys = get_y_samples(info, 1000)
    val = np.mean(tt.eval_approx(
        u_reco, ys, dist, scale=scale, decay=decay), axis=0)

    file_name = get_png_file_name(DATA_FILE, "_solution_mean", "png")
    fig, ax = plt.subplots(figsize=(10, 10), nrows=1, ncols=1)
    fem.plot_fenics_function(ax, val, space, mesh=mesh, cbar=False)
    ax.axis("off")
    plt.savefig(file_name, bbox_inches="tight", dpi=600)
    print(f"save mesh to: {file_name}")


def plot_mesh(DATA_FILE, level=-1):
    """ Plot mesh from level data file.
    """
    lvl_data = load_level_data(DATA_FILE)
    mesh = lvl_data["info"][level]["fe"]["mesh"]

    file_name = get_png_file_name(DATA_FILE, "_mesh", "pdf")
    fig, ax = plt.subplots(figsize=(10, 10), nrows=1, ncols=1)
    plt.sca(ax)
    plot(mesh)
    ax.axis("off")
    plt.savefig(file_name, bbox_inches="tight", dpi=600)
    print(f"save mesh to: {file_name}")


def plot_init_mesh(PATH, FILE):
    """ Plot mesh from level data file. """
    lvl_data = load_level_data(PATH+FILE)
    mesh = lvl_data["info"][0]["fe"]["mesh"]

    file_name = f"../img/" + FILE[:-4] + "_init_mesh.pdf"
    fig, ax = plt.subplots(figsize=(10, 10), nrows=1, ncols=1)
    plt.sca(ax)
    plot(mesh)
    ax.axis("off")
    plt.savefig(file_name, bbox_inches="tight", dpi=600)
    print(f"save mesh to: {file_name}")


def main():
    """ main """

    ids = [
        # ("affine", "UnitSquare", 1, 2, 6),  # done
        # ("affine", "UnitSquare", 3, 2, 13),  # done
        # ("affine", "UnitSquare", 1, 4, 6),  # done
        # ("affine", "UnitSquare", 3, 4, -1),  # done
        # ("affine", "Lshape", 1, 2, 6),  # done
        # ("affine", "Lshape", 3, 2, 13),  # done
        # ("affine", "Lshape", 1, 4, 6),  # done
        # ("affine", "Lshape", 3, 4, 13),  # done
        # ("lognormal", "UnitSquare", 1, 2, -1),  # done
        # ("lognormal", "UnitSquare", 3, 2, 13),  # done
        # ("lognormal", "UnitSquare", 1, 4, 6),  # done
        # ("lognormal", "UnitSquare", 3, 4, 13),  # done
        ("lognormal", "Lshape", 1, 2, 6),  # done
        # ("lognormal", "Lshape", 3, 2, 13),  # done
        # ("lognormal", "Lshape", 1, 4, 6),  # done
        # ("lognormal", "Lshape", 3, 4, 13),  # done
    ]

    for (coeff, geometry, deg, decay, lvl) in ids:
        PATH = f"../data/avmc_FINAL/"
        FILE = f"avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"

        try:
            plot_mesh(PATH+FILE, level=lvl)
            plot_init_mesh(PATH, FILE)
            plot_solution_mean(PATH+FILE, level=lvl, plot_mesh=False)
            plot_diffusion_coefficient_random(
                PATH+FILE, level=lvl, plot_mesh=False)
            plot_diffusion_coefficient_mean(
                PATH+FILE, level=lvl, plot_mesh=False)
            # plot_diffusion_coefficient_var(
            #     PATH+FILE, level=lvl, plot_mesh=False)
        except FileNotFoundError:
            print(f"Unknown file: {PATH+FILE}")
        plt.close("all")


if __name__ == "__main__":
    main()
