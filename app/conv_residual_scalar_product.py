"""
Test positivity of the scalar product of the the residual in different
parts of the inactive set, i.e. for :math:`\\Lambda\\subset\\mathbb{R}^M`
and :math:`\\Delta\\subset\\partial\\Lambda`, check if

.. math::
    0 \\leq \\langle \\sum_{\\mu\\in\\Delta} r_\\mu(w_N)P_\\mu\\zeta_{\\vartheta\\rho}, \\sum_{\\nu\\in\\partial\\Lambda\\setminus\\Delta} r_\\nu(w_N)P_\\nu\\zeta_{\\vartheta\\rho} \\rangle_{\\pi_0, D}

with orthonormal polynomial basis :math:`\\{P_\\mu\\}` of
:math:`L^2(\\Gamma, \\pi_{\\vartheta\\rho})` and residual
:math:`r(w_N) = a_N \\nabla w_N`.
"""
from typing import List, Tuple
import itertools
import numpy as np
import matplotlib.pyplot as plt
import xerus as xe
from fenics import Function, grad, inner, dx, assemble

from context import src
from src.misc import log, timeit
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
import src.estimator.intrusive.intrusive_estimator as EST
from src.utils.darcy_affine_coeff import exact_TT_diff_coeff
from src.problem.equation.darcy import Problem
import avmc_utils as avmc_utils
import conv_utils as conv_utils


def generate_problem() -> Problem:
    """Assemble problem specifications for the Darcy forward problem.

    Returns
    -------
    Problem
        Problem data for the forward problem.
    """
    info = avmc_utils.info_dict(
        M=10, decay=2, vartheta=0.1, rho=1,
        dist="normal", exp=True,
        geometry="Lshape", mesh=10)
    return Problem(info)


def assemble_solution_TT(p: Problem, sdim: List[int]) -> xe.TTTensor:
    """Compute TT for the solution of the Darcy problem.

    Parameters
    ----------
    p : Problem
        Problem data for the Darcy equation.
    sdim : list of int
        Dimensions of the active stochastic set.

    Returns
    -------
    u_tt : xe.TTTensor
        TT coefficient tensor of the solution with respect to the scaled
        Hermite polynomials :math:`H^{\\vartheta\\rho}`.
    """
    # generate training samples
    vartheta = p.info["expansion"]["vartheta"]
    rho = p.info["expansion"]["rho"]
    decay = p.info["expansion"]["decay rate"]
    ys, ws, us, _, _ = avmc_utils.generate_samples(
        1000, p.info, scale=vartheta*rho)

    # VMC regression
    u_tt = tt.reconstruct(dims=sdim, nodes=ys[:, :len(sdim)].T, weights=ws,
                          values=us, dist=p.info["sampling"]["distribution"],
                          scale=rho, decay=decay, init=None,
                          label="", verbose=False)

    # shift coefficient TT to correct (scaled) Hermite basis
    u_tt = femTT.change_basis_to_scaled_hermite(
        u_tt, decay, rho, vartheta, s=rho)
    return u_tt


def assemble_diffCoeff_TT(p: Problem) -> xe.TTTensor:
    """Compute TT for the diffusion coefficient of the Darcy problem.

    Parameters
    ----------
    p : Problem
        Problem data for the Darcy equation.

    Returns
    -------
    kappa_tt : xe.TTTensor
        TT coefficient tensor of the diffusion coefficient with respect to the
        scaled Hermite polynomials :math:`H^{\\vartheta\\rho}`.
    """
    M = p.info["expansion"]["size"]
    decay = p.info["expansion"]["decay rate"]
    dist = p.info["sampling"]["distribution"]
    mean = p.info["expansion"]["mean"]
    vartheta = p.info["expansion"]["vartheta"]
    rho = p.info["expansion"]["rho"]

    # assemble affine exponent
    gamma_tt = exact_TT_diff_coeff(
        p.space_coeff, p.mesh, M, decay, mean, dist, dim=2)
    gamma_tt.round(1e-16)

    # expTT
    kappa_tt = avmc_utils.expTT_kappa(gamma_tt, p)

    # shift coefficient TT to correct (scaled) Hermite basis
    kappa_tt = femTT.change_basis_to_scaled_hermite(
        kappa_tt, decay, rho, vartheta, s=rho)
    return kappa_tt


def compute_estimator(p: Problem,
                      sdim: List[int],
                      u_tt: xe.TTTensor,
                      a_tt: xe.TTTensor
                      ) -> Tuple[np.ndarray, int]:
    """Compute stochastic estimator contribution and refinement type.

    Parameters
    ----------
    p : Problem
        Problem data for the Darcy equation.
    sdim : list of int
        Dimensions of the active stochastic set.
    u_tt : xe.TTTensor
        TT coefficient tensor of the solution with respect to the scaled
        Hermite polynomials :math:`H^{\\vartheta\\rho}`.
    a_tt : xe.TTTensor
        TT coefficient tensor of the diffusion coefficient with respect to the
        scaled Hermite polynomials :math:`H^{\\vartheta\\rho}`.

    Returns
    -------
    est_stoch_loc : np.ndarray
        Stochastic estimator contributions.
    ref_type : int
        Refinement type.
    """
    decay = p.info["expansion"]["decay rate"]
    vartheta = p.info["expansion"]["vartheta"]
    rho = p.info["expansion"]["rho"]
    c_stoch = 5
    c_disc = 10

    # assemble constant RHS
    f_tt = tt.constant(np.ones(p.mesh.num_cells()), sdim)
    f_tt = femTT.change_basis_to_scaled_hermite(
        f_tt, decay, rho, vartheta, s=rho)

    # compute local estimator contributions
    est_det_sq, est_stoch_sq, est_disc_sq = EST.compute_estimator_terms(
        f_tt, a_tt, u_tt, p, t=5)

    # compute stochastic estimator contributions
    est_stoch_loc = c_stoch * np.sqrt(np.abs(est_stoch_sq))

    # compute refinement type
    est_det = np.sqrt(np.sum(np.abs(est_det_sq)))
    est_stoch = np.sum(est_stoch_loc)
    est_disc = c_disc * np.sqrt(np.abs(est_disc_sq))
    ref_type = np.argmax(np.array([est_det, est_stoch, est_disc]))

    return est_stoch_loc, ref_type


def main() -> None:
    """Main file."""

    # assemble TT for solution and diffusion coefficient
    p = generate_problem()
    sdim = [3, 2, 1]
    log("assemble u_TT")
    u_tt = assemble_solution_TT(p, sdim)
    log(f"    dims: {u_tt.dimensions}")
    log(f"    ranks: {u_tt.ranks()}")
    log("assemble a_TT")
    a_tt = assemble_diffCoeff_TT(p)

    # assemble TT for residual
    active_set = u_tt.dimensions[1:] + [1]*(a_tt.order()-u_tt.order())
    boundary_set = [d+q-1 for d, q in zip(active_set, a_tt.dimensions[1:])]
    marked_set = [np.random.randint(d, q)
                  for d, q in zip(active_set, boundary_set)]
    log("active, marked and boundary set dimensions")
    log(f"    FE-dim:   {p.space.dim()}")
    log(f"    active:   {active_set}")
    log(f"    marked:   {marked_set}")
    log(f"    boundary: {boundary_set}")
    log("compute scalar product")
    scalar_product, products = conv_utils.compute_scalar_product(
        p, u_tt, a_tt, active_set, marked_set, boundary_set)
    log(f"->  scalar product: {scalar_product:4.2g}")
    log("->  individual contributions:")
    log(f"->    + <marked, bdry>:   {products[0]:.16f}")
    log(f"->    + <active, marked>: {products[1]:.16f}")
    log(f"->    - <active, bdry>:   {products[2]:.16f}")
    log(f"->    - <marked, marked>: {products[3]:.16f}")

    # compute estimator contributions
    # eta_sto_l, ref_type = compute_estimator(p, sdim, u_tt, a_tt)


if __name__ == "__main__":
    main()
