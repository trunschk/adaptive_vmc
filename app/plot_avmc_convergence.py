# -*- coding: utf-8 -*-
import os
import numpy as np
import matplotlib.pyplot as plt

from fenics import plot
import xerus as xe

from context import src
from src.misc import get_y_samples
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
from src.problem.equation.darcy import Problem

from avmc_utils import datetime_id
from avmc_utils import load_level_data

from plot_utils import load_level_data
from plot_utils import set_legend_upper_left
from plot_utils import set_legend_lower_left
from plot_utils import color_ref_type
from plot_utils import plot_slope
from plot_utils import plot_level_data
from plot_utils import get_png_file_name


def plot_debug(path, file_name, x_axis="level", png_file="../img/debug.png"):
    """ Debug plot for level data.

    Parameters
    ----------
    path : str
        Path to level data file.
    file_name : str
        Name of level data file.
    x_axis : str
        Type of x axis.
    png_file : str
        Name of file the convergence plot is saved to.
    """

    cmap = plt.cm.tab20
    marker_styles = ["o", "s", "D", "P", "v", "^", "X"]

    lvl_data = load_level_data(path+file_name)

    if x_axis == "nnz_dofs_u" and "nnz_dofs_u" not in lvl_data.keys():
        nnz_DoFs = [tt.dofs_nnz(u, EPS=1e-08) for u in lvl_data["u_reco"]]
        lvl_data["nnz_DoFs"] = nnz_DoFs

    fig, ax = plt.subplots(figsize=(18, 5), nrows=1, ncols=2)

    for j, x_axis in enumerate(["level", "tt_dofs_u"]):
        log_type = "semilogy" if x_axis == "level" else "loglog"

        # total estimator
        label = "est_total"
        plot_level_data(ax[j], lvl_data, x_axis, label, log_type,
                        ms=marker_styles[0]+"-", c="k", label=label)
        # label = "est total noDisc"
        # plot_level_data(ax, lvl_data, x_axis, label, log_type,
        #                 ms=marker_styles[0]+"-", c=cmap(2), label=label)

        # estimator parts
        label = "est_det"
        plot_level_data(ax[j], lvl_data, x_axis, label, log_type,
                        ms=marker_styles[0]+"-", c=cmap(0), alpha=0.99,
                        label=label)
        label = "est_stoch"
        plot_level_data(ax[j], lvl_data, x_axis, label, log_type,
                        ms=marker_styles[0]+"-", c=cmap(2), alpha=0.99,
                        label=label)
        label = "est_disc"
        plot_level_data(ax[j], lvl_data, x_axis, label, log_type,
                        ms=marker_styles[0]+"-", c=cmap(4), alpha=0.99,
                        label=label)

        # exact error
        if "exact_err_L2H1_abs" in lvl_data.keys():
            label = "$L^2-H^1$ error"
            plot_level_data(ax[j], lvl_data, x_axis,
                            "exact_err_L2H1_abs", log_type,
                            ms=marker_styles[0]+"-", c=cmap(6),
                            label=label)
        if "exact_err_L2H1_abs_iter" in lvl_data.keys():
            label = "preliminary $L^2-H^1$ error"
            plot_level_data(ax[j], lvl_data, x_axis,
                            "exact_err_L2H1_abs_iter",
                            log_type, ms=marker_styles[0]+"--",
                            c=cmap(7), label=label)

        # approximation errors
        label = "rel_err_energy_u"
        plot_level_data(ax[j], lvl_data, x_axis, label, log_type,
                        ms=marker_styles[0]+"-", c=cmap(8), alpha=0.99,
                        label=label)
        label = "rel_err_L2H1_u"
        plot_level_data(ax[j], lvl_data, x_axis, label, log_type,
                        ms=marker_styles[0]+"-", c=cmap(10), alpha=0.99,
                        label=label)
        # label = "rel_err_L2L2_u"
        # plot_level_data(ax[j], lvl_data, x_axis, label, log_type,
        #                 ms=marker_styles[0]+"-", c=cmap(12), alpha=0.99,
        #                 label=label)
        # label = "abs_err_L2Linf_kappa"
        # plot_level_data(ax[j], lvl_data, x_axis, label, log_type,
        #                 ms=marker_styles[0]+"-", c=cmap(8), alpha=0.99,
        #                 label=label)

        # slope lines
        if x_axis != "level":
            x_coord = [lvl_data[x_axis][0], lvl_data[x_axis][-1]]
            y_coord = 6e-02
            plot_slope(ax[j], -0.33, x_coord, y_coord,
                       log_type, label=True, offset=(0, 0))
            y_coord = 6e-02
            plot_slope(ax[j], -1.0, x_coord, y_coord, log_type,
                       label=True, offset=(0, -15))

        # color in refinement types
        colors = [cmap(1), cmap(3), cmap(5)]
        labels = ["det ref", "stoch ref", "disc ref"]
        handles = color_ref_type(ax[j], lvl_data[x_axis],
                                 lvl_data, colors=colors, label=labels)

        # general plot settings
        ax[j].set_xlabel(x_axis)
        # ax[j].grid()

    l1 = set_legend_upper_left(ax[-1])
    l2 = set_legend_lower_left(ax[-1], handles)
    plt.gca().add_artist(l1)
    plt.gca().add_artist(l2)

    if png_file is None:
        png_file = get_png_file_name(path+file_name, "_convergence")
    fig.suptitle(path+file_name)
    plt.savefig(png_file, bbox_inches='tight', dpi=300)
    print(f"save img to: {png_file}")


def plot_convergence_all_est_parts(path, file_name, x_axis, png_file=None):
    """ Plot convergence of aVMC in loglog plot.

    Parameters
    ----------
    path : str
        Path to level data file.
    file_name : str
        Name of level data file.
    x_axis : str
        Type of x axis.
    png_file : str
        Name of file the convergence plot is saved to.
    """

    cmap = plt.cm.tab20
    marker_styles = ["o", "s", "D", "P", "v", "^", "X"]

    lvl_data = load_level_data(path+file_name)

    if x_axis == "nnz_dofs_u" and "nnz_dofs_u" not in lvl_data.keys():
        nnz_DoFs = [tt.dofs_nnz(u, EPS=1e-08) for u in lvl_data["u_reco"]]
        lvl_data["nnz_DoFs"] = nnz_DoFs

    log_type = "semilogy" if x_axis == "level" else "loglog"

    fig, ax = plt.subplots(figsize=(12, 5), nrows=1, ncols=1)

    # total estimator
    label = "$\\eta$"
    plot_level_data(ax, lvl_data, x_axis, "est_total", log_type,
                    ms=marker_styles[0]+"-", c="k", label=label)
    # label = "est total noDisc"
    # plot_level_data(ax, lvl_data, x_axis, "est_total_noDisc", log_type,
    #                 ms=marker_styles[0]+"-", c=cmap(2), label=label)

    # estimator parts
    label = "$\\eta_{\\mathrm{det}}$"
    plot_level_data(ax, lvl_data, x_axis, "est_det", log_type,
                    ms=marker_styles[0]+"-", c=cmap(0), alpha=0.25,
                    label=label)
    label = "$\\eta_{\\mathrm{sto}}$"
    plot_level_data(ax, lvl_data, x_axis, "est_stoch", log_type,
                    ms=marker_styles[0]+"-", c=cmap(2), alpha=0.25,
                    label=label)
    label = "$\\eta_{\\mathrm{alg}}$"
    plot_level_data(ax, lvl_data, x_axis, "est_disc", log_type,
                    ms=marker_styles[0]+"-", c=cmap(4), alpha=0.25,
                    label=label)

    # exact error
    exact_error_type = "exact_err_L2H1_abs"
    if exact_error_type in lvl_data.keys():
        label = "$L^2-H^1$ error"
        plot_level_data(ax, lvl_data, x_axis, exact_error_type, log_type,
                        ms=marker_styles[0]+"-", c=cmap(6), label=label)
    elif "exact_err_L2H1_abs_iter" in lvl_data.keys():
        label = "preliminary $L^2-H^1$ error"
        plot_level_data(ax, lvl_data, x_axis, "exact_err_L2H1_abs_iter",
                        log_type, ms=marker_styles[0]+"--", c=cmap(6),
                        label=label)

    # slope lines
    if x_axis != "level":
        x_coord = [lvl_data[x_axis][0], lvl_data[x_axis][-1]]
        y_coord = 6e-02
        plot_slope(ax, -0.33, x_coord, y_coord,
                   log_type, label=True, offset=(0, 0))
        y_coord = 6e-02
        plot_slope(ax, -1.0, x_coord, y_coord, log_type,
                   label=True, offset=(0, -15))

    # color in refinement types
    colors = [cmap(1), cmap(3), cmap(5)]
    labels = ["ref $\\mathcal{T}$",
              "ref $\\Lambda_d$", "inc $N_{\\mathrm{VMC}}$"]
    handles = color_ref_type(ax, lvl_data[x_axis], lvl_data,
                             colors=colors, label=labels)

    # general plot settings
    # set_legend_upper_left(ax)
    l1 = set_legend_upper_left(ax)
    l2 = set_legend_lower_left(ax, handles)
    plt.gca().add_artist(l1)
    plt.gca().add_artist(l2)

    ax.set_xlabel(x_axis)
    # ax.grid()

    if file_name[:6] == "clean_":
        file_name = file_name[6:]
    if png_file is None:
        png_file = get_png_file_name(path+file_name, "_convergence")
    # fig.suptitle(path+file_name)
    plt.savefig(png_file, bbox_inches='tight', dpi=300)
    print(f"save img to: {png_file}")


def main():
    """ main """

    ids = [
        ("affine", "UnitSquare", 1, 2),  # done
        ("affine", "UnitSquare", 3, 2),  # done
        ("affine", "UnitSquare", 1, 4),  # done
        ("affine", "UnitSquare", 3, 4),  # done
        ("affine", "Lshape", 1, 2),  # done
        ("affine", "Lshape", 3, 2),  # done
        ("affine", "Lshape", 1, 4),  # done
        ("affine", "Lshape", 3, 4),  # done
        ("lognormal", "UnitSquare", 1, 2),  # done
        ("lognormal", "UnitSquare", 3, 2),  # done
        ("lognormal", "UnitSquare", 1, 4),  # done
        ("lognormal", "UnitSquare", 3, 4),
        ("lognormal", "Lshape", 1, 2),  # done
        ("lognormal", "Lshape", 3, 2),  # done
        ("lognormal", "Lshape", 1, 4),  # done
        ("lognormal", "Lshape", 3, 4),
    ]

    test_plot = False
    for (coeff, geometry, deg, decay) in ids:
        if test_plot is True and len(ids) == 1:
            PATH = f"../data/avmc_{coeff}/"
            # PATH += "server/"
            FILE = f"clean_avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"
            PNG_FILE = f"../img/{coeff}.png"
            # PNG_FILE = None  # uses complete file name
        else:
            PATH = "../data/avmc_FINAL/"
            FILE = f"avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"
            PNG_FILE = None

        # x_axis = "level"
        x_axis = "tt_dofs_u"
        # x_axis = "nnz_dofs_u"
        try:
            plot_convergence_all_est_parts(PATH, FILE, x_axis, PNG_FILE)
            if test_plot is True:
                plot_debug(PATH, FILE, png_file="../img/debug/debug.png")
        except FileNotFoundError:
            print(f"Unknown file: {PATH+FILE}")
        plt.close("all")


if __name__ == "__main__":
    main()
