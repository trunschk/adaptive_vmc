import sys
import os
import numpy as np
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt
import xerus as xe

from context import src
from src.misc import timeit, log, transpose, Logger
import src.utils.tt_tools as tt
import src.utils.fem_tools as fem
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as expTT
from src.problem.equation.darcy import Problem
from src.parallel import Parallel, ParallelizableProblem

# import src.autoPDB


def eval_likelihood(ys, mean, cov):
    """ Evaluate Gaussian Multivariate likelihood. """
    assert mean.ndim == 1
    assert mean.size == cov.shape[0] == cov.shape[1] == ys.shape[1]
    mvn = multivariate_normal(mean, cov)
    if np.allclose(cov, cov[0, 0]*np.eye(cov.shape[0])):
        c = 1
        # c = 1 / normalization_constant(cov)
        c = height_scale(1**2, cov[0, 0], cov.shape[0])
    else:
        c = 1
    return c * mvn.pdf(ys).reshape(-1, 1)


def eval_loglikelihood(ys, mean, cov):
    """ Evaluate Gaussian Multivariate log-likelihood. """
    assert mean.ndim == 1
    assert mean.size == cov.shape[0] == cov.shape[1] == ys.shape[1]
    mvn = multivariate_normal(mean, cov)
    if np.allclose(cov, cov[0, 0]*np.eye(cov.shape[0])):
        c = 1
        # c = 1 / normalization_constant(cov)
        c = height_scale(1**2, cov[0, 0], cov.shape[0])
    else:
        c = 1
    return mvn.logpdf(ys).reshape(-1, 1) + np.log(c)


def normalization_constant(cov):
    """ Compute the normalization constant for multivariate Gaussian."""
    mvn = multivariate_normal(np.zeros(cov.shape[0]), cov)
    c = mvn.pdf(np.zeros(cov.shape[0]))
    return c


def height_scale(var1, var2, dim):
    cov1 = var1 * np.eye(dim)
    cov2 = var2 * np.eye(dim)
    h1 = normalization_constant(cov1)
    h2 = normalization_constant(cov2)
    scale = (h1 * var2) / (h2 * var1)
    return scale


def generate_samples(n, mean, cov, mean_y=None, cov_y=None):
    """ Generate samples of standard Gaussian (log-)likelihood.

    Parameters
    ----------
    n : int
        Number of samples that are generated.
    mean : array_like
        Mean of (log-)likelihood for evaluation.
    cov : array_like
        Covariance of (log-)likelihood for evaluation.
    mean_y : array_like or None
        Mean the y samples are drawn from.
    cov_y : array_like or None
        Covariance the y samples are drawn from.
    """
    assert mean.ndim == 1
    assert mean.size == cov.shape[0] == cov.shape[1]
    mu = np.zeros(mean.size) if mean_y is None else np.array(mean_y)
    sig = np.eye(mean.size) if cov_y is None else np.array(cov_y)
    mvn = multivariate_normal(mu, sig)
    ys = mvn.rvs(n)
    ws = np.ones(ys.shape[0]) / ys.shape[0]
    hs = eval_loglikelihood(ys, mean, cov)
    exphs = eval_likelihood(ys, mean, cov)
    return ys, ws, hs, exphs


def expTT_reconstruction_dupe(dims, h_reco, x0, exph0=None,
                              homogeneous=False, verbose=False, EPS=1e-10):
    """ Duplicate of expTT.expTT_reconstruction() for local debugging. """

    M, ALS_TYPE = len(dims), "simple"

    # build operator and rhs
    log("    build operator W0 and RHS b")
    W, b = expTT.build_system_inexactIC(dims, dims, h_reco, x0, exph0=exph0,
                                        homogeneous=homogeneous)
    log("    round operator W0")
    W.round(EPS)
    log(f"       ranks W: {W.ranks()}")
    if homogeneous:
        b.round(EPS)
    log(f"       ranks b:  {b.ranks()}")

    # random initialization
    log("    set initial guess")
    u_start = xe.TTTensor.random(dims, [11]*(M-1) if M > 1 else [])
    u_start = u_start / xe.frob_norm(u_start)

    # run ALS (simple or xerus built-in)
    log(f"    run {ALS_TYPE} ALS")
    if ALS_TYPE == "simple":
        ret = expTT.run_simpleALS(A=W, u_start=u_start, b=b, verbose=verbose)
    elif ALS_TYPE == "xerus":
        ret = xe.TTTensor(u_start)
        xe.ALS_SPD(W, ret, b)

    # compute residual
    res = wrapper_compute_residual(
        ret, dims, h_reco, x0, exph0, homogeneous=homogeneous)

    log(f"       ranks u: {ret.ranks()}")
    log("    round solution and add first component")
    ret.round(EPS)  # round to get unneccesary large ranks down
    ret = tt.add_dimension_left(ret)  # add deterministic component
    log(f"       ranks u: {ret.ranks()}")

    if homogeneous:
        ret = ret + tt.constant(exph0, ret.dimensions[1:])

    return ret, res


def wrapper_ALS_residual(u, W, b):
    """ Compute ||Wu-b||_2 of ALS residuum."""
    solver = expTT.InternalSolver(W, xe.TTTensor(u), b)
    return solver.calc_residual_norm()


def wrapper_compute_residual(u, dims, h_reco, x0, exph0, homogeneous=False):
    """ Compute ||Bu-f||_2 for homogeneous or inhomogeneous solution."""
    B, f = expTT.build_operator_Bf(dims, dims, h_reco, exph0)
    val = xe.TTTensor(u)
    if val.order() == B.order()//2 + 1 and val.dimensions[0] == 1:
        val.fix_mode(0, 0)
    if not homogeneous:
        # for inhomogeneous case subtract exp h(x_0) from solution and compute
        # residual || B(u-exp h(x_0)) - f||_2 with homogeneous f.
        const = tt.constant(exph0, val.dimensions)
        const.fix_mode(0, 0)
        val = val - const
    return expTT.compute_residual(val, B, f)


def compute_L2_error(ys, us, reco):
    """
    Compute stochastic L2 error of a function with one deterministic component.
    """
    tt_eval = tt.eval_approx(reco, ys, "normal", scale=0)
    abs_err = np.sqrt(np.sum((us-tt_eval)**2)/us.shape[0])
    rel_err = abs_err / np.sqrt(np.sum(us**2)/us.shape[0])
    return abs_err, rel_err


def truncate_samples(ys, domain):
    """ Truncate samples to domain. Only for plotting. """
    dom = np.array(domain)
    if dom.ndim == 1:
        dom.shape = 1, -1
    assert dom.shape[1] == 2
    if dom.shape[0] == 1:
        dom = np.ones((ys.shape[1], 1)) * dom.reshape(1, -1)
    assert ys.shape[1] == np.array(dom).shape[0]
    assert np.array(dom).shape[1] == 2
    idx = []
    for y, d in zip(ys.T, dom):
        idx += list(np.where(y < d[0])[0])
        idx += list(np.where(y > d[1])[0])
    if np.unique(idx).size > 0:
        idx = np.delete(np.arange(ys.shape[0]), np.unique(idx))
    else:
        idx = np.arange(ys.shape[0])
    return ys[idx]


def _meshgrid(ys):
    """ debug only """
    XX, YY = np.meshgrid(ys, ys)
    return np.concatenate([XX.reshape(-1, 1), YY.reshape(-1, 1)], axis=1), ys


if __name__ == "__main__":
    sigma = 0.01
    M = 2
    dims = [21]*M

    # generate measurement
    # NOTE: Posterior is centered around measurement (mean) and has
    # potentially small measurement noise given by cov.
    # mean = np.random.normal(0, 0.75, M)
    mean = np.zeros(M)
    cov = sigma**2 * np.eye(M)
    mean_y, cov_y = np.zeros(M), np.eye(M)

    N_VMC = 1_000 if M < 11 else 10_000
    ys, ws, hs, exphs = generate_samples(
        N_VMC, mean, cov, mean_y=mean_y, cov_y=cov_y)
    ys_test, ws_test, hs_test, exphs_test = generate_samples(
        1000, mean, cov, mean_y=mean, cov_y=cov)

    # _________________________________________________________________________
    # reconstruct log-likelihood
    log("run VMC for log-likelihood")
    h_reco = tt.reconstruct(dims=[3]*M, nodes=ys.T, weights=ws, values=hs,
                            dist="normal")
    h_reco.move_core(1)

    abs_err, rel_err = compute_L2_error(ys_test, hs_test, h_reco)
    log(f"L2 error (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")

    # _________________________________________________________________________
    # reconstruct likelihood with expTT
    log("run expTT for likelihood")
    y0 = np.zeros((1, M))
    exph0 = np.exp(tt.eval_approx(h_reco, y0, dist="normal"))
    # exph0 = eval_likelihood(y0, mean, cov)
    u_reco_expTT, res = expTT_reconstruction_dupe(
        dims, h_reco, y0, exph0=exph0, homogeneous=True, verbose=2,
        EPS=1e-16)
    log(f"    residual ||Bu-f||: {res:4.2e}")
    abs_err, rel_err = compute_L2_error(ys_test, exphs_test, u_reco_expTT)
    log(f"L2 error: {abs_err:4.2e} / {rel_err:4.2e}")

    tmp = tt.eval_approx(h_reco, ys_test, "normal")
    abs_err, rel_err = compute_L2_error(ys_test, np.exp(tmp), u_reco_expTT)
    log(f"L2 error u_TT-exp(h_TT): {abs_err:4.2e} / {rel_err:4.2e}")

    # _________________________________________________________________________
    # reconstruct likelihood with VMC
    log("run VMC for likelihood")
    u_reco_vmc = tt.reconstruct(dims=dims, nodes=ys.T, weights=ws,
                                values=exphs, dist="normal")

    abs_err, rel_err = compute_L2_error(ys_test, exphs_test, u_reco_vmc)
    log(f"L2 error: {abs_err:4.2e} / {rel_err:4.2e}")

    # _________________________________________________________________________
    # plot
    if M == 2:
        dom = [-2, 2]  # [-3*sigma, 3*sigma]
        xx, x = _meshgrid(np.linspace(*dom, 50))
        y1 = eval_likelihood(xx, mean, cov)
        y2 = tt.eval_approx(u_reco_expTT, xx, dist="normal")
        y3 = tt.eval_approx(u_reco_vmc, xx, dist="normal")
        z1 = eval_loglikelihood(xx, mean, cov)
        z2 = tt.eval_approx(h_reco, xx, dist="normal")
        ys_plot = truncate_samples(ys, dom)

        fig, ax = plt.subplots(figsize=(10, 4), nrows=1, ncols=2)
        im = ax[0].contourf(x, x, z1.reshape(x.size, -1))
        ax[0].scatter(ys_plot[:, 0], ys_plot[:, 1], s=0.4, c="r", alpha=0.1)
        ax[0].set_title("target")
        fig.colorbar(im, ax=ax[0])
        im = ax[1].contourf(x, x, z2.reshape(x.size, -1))
        ax[1].set_title("VMC")
        fig.colorbar(im, ax=ax[1])
        plt.savefig("../img/expTT_logLikelihood_gauss_concentrated_logL.png",
                    bbox_inches='tight', dpi=300)

        fig, ax = plt.subplots(figsize=(15, 8), nrows=2, ncols=3)
        im = ax[0, 0].contourf(x, x, y1.reshape(x.size, -1))
        ax[0, 0].scatter(ys_plot[:, 0], ys_plot[:, 1], s=0.4, c="r", alpha=0.3)
        ax[0, 0].set_title("target")
        fig.colorbar(im, ax=ax[0, 0])
        im = ax[0, 1].contourf(x, x, y2.reshape(x.size, -1))
        ax[0, 1].set_title("expTT")
        fig.colorbar(im, ax=ax[0, 1])
        im = ax[0, 2].contourf(x, x, y3.reshape(x.size, -1))
        ax[0, 2].set_title("VMC")
        fig.colorbar(im, ax=ax[0, 2])

        dom = [-3*sigma, 3*sigma]
        xx, x = _meshgrid(np.linspace(*dom, 50))
        y1 = eval_likelihood(xx, mean, cov)
        y2 = tt.eval_approx(u_reco_expTT, xx, dist="normal")
        y3 = tt.eval_approx(u_reco_vmc, xx, dist="normal")
        im = ax[1, 0].contourf(x, x, y1.reshape(x.size, -1))
        ax[1, 0].set_title("target")
        fig.colorbar(im, ax=ax[1, 0])
        im = ax[1, 1].contourf(x, x, y2.reshape(x.size, -1))
        ax[1, 1].set_title("expTT")
        fig.colorbar(im, ax=ax[1, 1])
        im = ax[1, 2].contourf(x, x, y3.reshape(x.size, -1))
        ax[1, 2].set_title("VMC")
        fig.colorbar(im, ax=ax[1, 2])
        plt.savefig("../img/expTT_logLikelihood_gauss_concentrated_L.png",
                    bbox_inches='tight', dpi=300)

    # from IPython import embed; embed(); sys.exit()
