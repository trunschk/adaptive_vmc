from typing import List, Callable
import numpy as np
import matplotlib.pyplot as plt
import conv_utils
from scipy.integrate import quad


def density(ys, sigma, power):
    """Density for the integration."""
    log_zeta = conv_utils.log_zeta(ys, sigma)
    log_pdf = conv_utils.log_gauss_pdf(ys)
    return np.exp(power*log_zeta + log_pdf)


def main() -> None:
    """Main function."""
    M = [1]
    decay = 2
    scales = [1, 0.5, 0.3, 0.1, 0.05, 0.01, 1e-3, 1e-4, 1e-5]
    domain = [-10, 10]

    for scale in scales:
        sigma = conv_utils.sigma_m(m=1, decay=decay, scale=scale)
        basis = conv_utils.hermite_polynomials(
            dim=7, scale=1/sigma, normalized=True)

        def w1(ys): return density(ys, sigma, power=1)
        def w2(ys): return density(ys, sigma, power=2)
        gram_mat1 = conv_utils.compute_gramian(basis, basis, w1, domain)
        gram_mat2 = conv_utils.compute_gramian(basis, basis, w2, domain)
        eye = np.eye(gram_mat1.shape[0])
        print(scale, np.linalg.norm(gram_mat1-eye),
              np.linalg.norm(gram_mat2-eye))

        fig, axes = plt.subplot_mosaic([["one", "two"]])
        val = axes["one"].matshow(gram_mat1, cmap="Blues")
        fig.colorbar(val, ax=axes["one"], orientation="horizontal")
        val = axes["two"].matshow(gram_mat2, cmap="Blues")
        fig.colorbar(val, ax=axes["two"], orientation="horizontal")
        # plt.show()


def test():
    m = 1
    decay = 2
    scale = 0.1

    sigma = conv_utils.sigma_m(m, decay, scale)
    basis = conv_utils.hermite_polynomials(
        dim=12, scale=1/sigma, normalized=True)

    def func1(ys, c=1000):
        val = ((conv_utils.zeta(ys, sigma) + c)
               * basis[1](ys)*basis[3](ys)
               * density(ys, sigma, 1))
        return val

    def func2(ys):
        return basis[8](ys)*basis[10](ys)*density(ys, sigma, 1)

    def func3(ys):
        return basis[1](ys)*basis[3](ys)*density(ys, sigma, 2)

    c = 1
    ys = np.linspace(0, 10, 100)
    val1 = np.zeros_like(ys)
    val2 = np.zeros_like(ys)
    val3 = np.zeros_like(ys)
    for j, y in enumerate(ys):
        val1[j] = quad(lambda x: func1(x, c=c), y, 100)[0]
        val2[j] = quad(func2, -y, y)[0]
        val3[j] = quad(func3, -y, y)[0]
    # val2[np.where(val2 > -1e-16)[0]] = -1e-16
    # val3[np.where(val3 < 1e-16)[0]] = 1e-16
    print(np.max(val2))

    plt.figure()
    # plt.plot(ys, val1, label=f"$\\int_b^\\infty (zeta+{c}) P_\\mu P_\\nu \\zeta$")
    plt.plot(ys, val2, label="2 \\int_0^b P_\\mu P_\\nu \\zeta")
    # plt.plot(ys, val3, label="2 \\int_0^b P_\\mu P_\\nu \\zeta^2")
    # plt.semilogy(ys, np.exp(np.log(val2)-np.log(-val)), label="c(b)")
    plt.legend()

    plt.show()


if __name__ == "__main__":
    main()
    # test()
