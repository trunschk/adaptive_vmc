from typing import List, Callable, Tuple, Optional
import itertools
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import zeta as zeta_function
from scipy.integrate import quad

from fenics import Function, grad, inner, dx, assemble
import xerus as xe

from context import src
from src.misc import log, timeit
import src.utils.tt_tools as tt
from src.problem.equation.darcy import Problem


def mat_str(mat):
    """Pretty print numpy matrix."""
    rows, cols = np.where(np.abs(mat) < 1e-14)
    mat[rows, cols] = 0.0
    _mat_str = "[["
    width = max([len(f"{val:3.0e}") for val in mat.flatten()])
    for row, vector in enumerate(mat):
        for val in vector:
            if val > 1e-14:
                fmt_str = f"{val:3.0e}"
            else:
                fmt_str = f"{val:g}"
            _mat_str += f"{fmt_str:>{width}}  "
        if row < mat.shape[0]-1:
            _mat_str = _mat_str[:-2] + "]\n ["
        else:
            _mat_str = _mat_str[:-2] + "]]"
    return _mat_str


def gauss_pdf(ys: np.ndarray) -> np.ndarray:
    """Standard univariate Gaussian density."""
    factor = 1 / np.sqrt(2*np.pi)
    exp = np.exp(- 1/2 * ys**2)
    return factor*exp


def log_gauss_pdf(ys: np.ndarray) -> np.ndarray:
    """Logarithm of standard univariate Gaussian density."""
    factor = np.log(1 / np.sqrt(2*np.pi))
    exp = - 1/2 * ys**2
    return factor + exp


def zeta(ys: np.ndarray, sigma: float) -> np.ndarray:
    """Compute :math:`\\zeta(y; \\sigma)`.

    Let :math:`\\pi_c` be the PDF of :math:`\\mathcal{N}(0,c^2)` for
    :math:`c>0`. The Radon-Nikodym derivative :math:`\\zeta` from
    :math:`\\mathcal{N}(0,1)` to :math:`\\mathcal{N}(0,\\sigma^2)` such that
    :math:`\\pi_\\sigma = \\zeta\\pi_1` is given by

    .. math::
        \\zeta(y; \\sigma)
        = \\frac{1}{\\sigma} \\exp((\\frac{1}{2} - \\frac{1}{2\\sigma^2}) y^2).

    Parameters
    ----------
    ys : array_like
        Evaluation points.
    sigma : float
        Standard deviation.
    """
    factor = 1 / sigma
    exp = np.exp((1/2 - 1/(2*sigma**2))*ys**2)
    return factor*exp


def log_zeta(ys: np.ndarray, sigma: float) -> np.ndarray:
    """Compute :math:`\\log\\zeta(y; \\sigma)`.

    Let :math:`\\pi_c` be the PDF of :math:`\\mathcal{N}(0,c^2)` for
    :math:`c>0`. The Radon-Nikodym derivative :math:`\\zeta` from
    :math:`\\mathcal{N}(0,1)` to :math:`\\mathcal{N}(0,\\sigma^2)` such that
    :math:`\\pi_\\sigma = \\zeta\\pi_1` is given by

    .. math::
        \\zeta(y; \\sigma)
        = \\frac{1}{\\sigma} \\exp((\\frac{1}{2} - \\frac{1}{2\\sigma^2}) y^2).

    Parameters
    ----------
    ys : array_like
        Evaluation points.
    sigma : float
        Standard deviation.
    """
    factor = np.log(1 / sigma)
    exp = (1/2 - 1/(2*sigma**2))*ys**2
    return factor + exp


def sigma_m(m: int,
            decay: float,
            scale: float,
            gamma: float = 0.9
            ) -> float:
    """Compute :math:`\\sigma_m = \\exp( \\mathrm{scale} * \\beta_m )`.

    In the case of our Darcy diffusion field, we have

    .. math::
          \\beta_m
          = \\Vert a_m \\Vert_{L^\\infty(D)}
          = \\frac{\\gamma}{\\zeta(\\mathrm{decay})} * m**(-\\mathrm{decay}),

    where :math:`\\zeta` denotes the Riemannian zeta function.

    Parameters
    ----------
    m : int
        Mode number (number of stochastic parameter)
    decay : float
        Decay rate for the diffusion coefficient expansion terms. Needs to be
        grater than zero.
    scale : float
        Scaling factor. Needs to be greater or equal to zero.
    gamma : float, default = 0.9
        Problem dependent scaling constant.
    """
    assert decay > 0 and scale >= 0
    beta_m = gamma / zeta_function(decay) * m**(-decay)
    return np.exp(scale*beta_m)


def hermite_polynomials(dim: int,
                        scale: float,
                        normalized: bool
                        ) -> List[Callable[[np.ndarray], np.ndarray]]:
    """Return (scaled) normalized Hermite polynomials up to deg."""
    if normalized is True:
        const = np.array([1/np.sqrt(float(np.math.factorial(j)))
                          for j in range(dim)])
    else:
        const = np.ones(dim)

    def hermite(_d):
        return np.polynomial.hermite_e.HermiteE.basis(_d)

    poly_list = [const[j]*hermite(j)(np.polynomial.Polynomial((0, scale)))
                 for j in range(dim)]
    return poly_list


def inner_product(f: Callable[[np.ndarray], np.ndarray],
                  g: Callable[[np.ndarray], np.ndarray],
                  w: Callable[[np.ndarray], np.ndarray],
                  domain: Optional[List[float]] = None,
                  ) -> float:
    """Compute the inner product w.r.t. a weight on a specified domain.

    The inner product is given by

    .. math::
        \\int_a^b f(y) g(y) w(y) \\mathrm{d}y.

    Parameters
    ----------
    f : callable
        First function of the scalar product.
    g : callable
        Second function of the scalar product.
    w : callable
        Weight for the scalar product.
    domain : list of float, default = None
        Integration domain, defaults to domain (-inf, inf).
    """
    if domain is None:
        domain = [-np.inf, np.inf]

    def integrand(y):
        return f(y) * g(y) * w(y)
    return quad(integrand, domain[0], domain[1])[0]


def compute_gramian(basis1: List[Callable[[np.ndarray], np.ndarray]],
                    basis2: List[Callable[[np.ndarray], np.ndarray]],
                    density: Callable[[np.ndarray], np.ndarray],
                    domain: Optional[List[float]] = None,
                    ) -> np.ndarray:
    """Compute the Gramian.

    The Gramian :math:`G\\in\\mathbb{R}^{N\\times M}` is given by

    .. math::
        G_{i,j} = \\int_a^b p_i p_j \\mathrm{d}\\pi.

    Parameters
    ----------
    basis1 : list(callable)
        List of basis functions :math:`(p_i)_{i=0}^N`.
    basis2 : list(callable)
        List of basis functions :math:`(p_j)_{j=0}^M`.
    density : callable
        PDF of the distribution with respect to Lebesgue measure.
    domain : list of float, default = None
        Integration domain, defaults to domain (-inf, inf).
    """
    gramian = np.zeros((len(basis1), len(basis2)))
    for i, b1 in enumerate(basis1):
        for j, b2 in enumerate(basis2):
            gramian[i, j] = inner_product(b1, b2, density, domain=domain)
    return gramian


def tail_ranks(tail_list: List[np.ndarray]) -> List[int]:
    """Return ranks of TT tail list."""
    left_ranks = [c.shape[0] for c in tail_list]
    right_ranks = [c.shape[-1] for c in tail_list]
    assert all([ell == r for ell, r in zip(left_ranks[1:], right_ranks[:-1])])
    return left_ranks + [right_ranks[-1]]


def compute_K_hat(dims_row: List[int],
                  dims_col: List[int],
                  m: int,
                  s: float,
                  decay: float = 2,
                  ) -> np.ndarray:
    """Compute :math:`\\hat K_m`.

    The stochastic contraction matrix is given by

    .. math::
        \\hat K_m[i,j]
        = \\langle P_i , P_j  \\zeta_{m}(y_m; \\sigma_m(s))^2\\rangle_{\\pi_0}

    for :math:`\\sigma_m(s)=\\exp(s\\Vert\\gamma_m\\Vert_{L^\\infty(D)}`.

    Parameters
    ----------
    dims_row : list(int)
        Dimensions for the row dimension of :math:`\\hat K`
    dims_col : list(int)
        Dimensions for the column dimension of :math:`\\hat K`
    m : int
        Mode number.
    s : float
        Scale for reweighting. This is typically :math:`\\vartheta\\rho`.
    decay : float, default=2
        Decay of the affine diffusion coefficient needed to compute
        :math:`\\sigma_m(s)`.

    Returns
    -------
    hat_K_m : np.ndarray
    """
    max_dim = max(dims_row + dims_col)
    sigma = sigma_m(m, decay, s)
    basis = hermite_polynomials(max_dim+1, 1/sigma, True)

    def density(_y, _sigma=sigma):
        """ Density is :math:`\\zeta(y;\\sigma)^2\\pi_0(y)`."""
        return np.exp(2*log_zeta(_y, _sigma) + log_gauss_pdf(_y))

    hat_K_m = np.zeros((len(dims_row), len(dims_col)))
    for i, deg_row in enumerate(dims_row):
        for j, deg_col in enumerate(dims_col):
            hat_K_m[i, j] = inner_product(
                basis[deg_row], basis[deg_col], density)

    return hat_K_m


def subsum(gramian: np.ndarray, dim: int) -> float:
    """Sum inner product of downward closed subset with its complement.

    For a downward closed set
    :math:`\\Delta = \\{ 0, 1, \\dots, \\mathrm{dim}-1\\}`
    compute

    .. math::
        \\sum_{i\\in\\Delta} \\sum{j\\in\\Lambda\\setminus\\Delta} \\int_{\\mathbb{R}} P_i P_j \\zeta_{\\vartheta\\rho}^2 \\mathrm{d}\\pi_0.

    Parameters
    ----------
    gramian : np.ndarray
        Precomputed Gramian containing the inner products.
    dim : int
        Dimension of the downward closed set :math:`\\Delta`.

    Returns
    -------
    float
    """
    assert gramian.shape[0] > dim
    sum_matrix = gramian[:dim, dim:]
    return 2*np.sum(sum_matrix)


def truncate_tt_tail_dim(TT: List[np.ndarray],
                         dims: List[int]
                         ) -> List[np.ndarray]:
    """Truncate tail components of TT tensor to dim.

    Parameters
    ----------
    TT : list of np.ndarray
        List of tail cores of the TT.
    dims : list of int
        Truncation dimensions for each core.

    Returns
    -------
    ret : list of np.ndarray
        Truncated tail cores of the TT.
    """
    assert len(TT) == len(dims)
    assert all([dim > 0 for dim in dims])
    return [core[:, :dim, :] for core, dim in zip(TT, dims)]


def compute_det_contraction(p: Problem,
                            u_0: np.ndarray,
                            a_0: np.ndarray,
                            ) -> np.ndarray:
    """Compute contraction of deterministic part T of scalar product.

    Parameters
    ----------
    p : Problem
        Problem data for the Darcy equation.
    u_0 : np.ndarray
        Deterministic component of solution TT.
    a_0 : np.ndarray
        Deterministic component of diffusion coefficient TT.

    Returns
    -------
    ret : np.ndarray
        Deterministic contribution T.
    """
    V, W = p.space, p.space_coeff
    u1, u2 = Function(V), Function(V)
    a1, a2 = Function(W), Function(W)

    r_1 = u_0.shape[-1]
    s_1 = a_0.shape[-1]
    ret = np.zeros((r_1, s_1, r_1, s_1))

    for r_l, s_l in itertools.product(range(r_1), range(s_1)):
        u1.vector().set_local(u_0[0, :, r_l])
        a1.vector().set_local(a_0[0, :, s_l])
        for r_r, s_r in itertools.product(range(r_l+1), range(s_l+1)):
            u2.vector().set_local(u_0[0, :, r_r])
            a2.vector().set_local(a_0[0, :, s_r])

            vec = inner(a1*grad(u1), a2*grad(u2)) * dx
            vec = assemble(vec, form_compiler_parameters={
                           'quadrature_degree': -1})
            ret[r_l, s_l, r_r, s_r] = vec
            ret[r_r, s_r, r_l, s_l] = vec
            ret[r_l, s_r, r_r, s_l] = vec
            ret[r_r, s_l, r_l, s_r] = vec

    return ret


def contract_tail(tt1: List[np.ndarray],
                  tt2: List[np.ndarray],
                  hat_Ks: List[np.ndarray]
                  ) -> np.ndarray:
    """Compute the scalar product contraction of the tail of a TT.

    Parameters
    ----------
    tt1 : list of np.ndarray
        List of stochastic TT cores for first tensor.
    tt2 : list of np.ndarray
        List of stochastic TT cores for second tensor.
    hat_Ks : list of np.ndarray
        List of contraction tensors
        :math:`\\langle P_i, P_j \\zeta_{\\vartheta\\rho}^2\\rangle_{\\pi_0}`.

    Returns
    -------
    ret : np.ndarray
        Order-2 tensor of contracted tail with shape (r_1*s_1, r_1*s_1).
    """
    ret = np.array([[1]])
    for core1, K, core2 in reversed(list(zip(tt1, hat_Ks, tt2))):
        assert ret.shape == (core1.shape[-1], core2.shape[-1])
        K_crop = K[:core1.shape[1], :core2.shape[1]]
        # NOTE: einsum is too slow! But the reshaping and dot products below
        #       do the same as the following einsum.
        # ret = np.einsum("rda,dq,sqb,ab->rs", core1, K_crop, core2, ret)

        # contract ret to first core
        r, d, a = core1.shape
        a, b = ret.shape
        core1 = np.reshape(core1, (r*d, a))
        tensor = np.dot(core1, ret)  # shape (r*d, b)
        tensor = np.reshape(tensor, (r, d, b))

        # contract :math:`\\hat{K}` matrix to tensor
        d, q = K_crop.shape
        tensor = np.reshape(np.swapaxes(tensor, 1, 2), (r*b, d))
        tensor = np.dot(tensor, K_crop)  # shape: (r*b, q)
        tensor = np.reshape(tensor, (r, b, q))

        # contract second core to tensor
        s, q, b = core2.shape
        tensor = np.reshape(np.swapaxes(tensor, 1, 2), (r, q*b))
        ret = np.dot(tensor, np.reshape(core2, (s, q*b)).T)

    return np.ascontiguousarray(ret)


def compute_scalar_product(p: Problem,
                           u_tt: xe.TTTensor,
                           a_tt: xe.TTTensor,
                           active_set: List[int],
                           marked_set: List[int],
                           boundary_set: List[int],
                           ) -> Tuple[float, tuple]:
    """Compute the scalar product of the different residual contributions.

    The scalar product is given by

    .. math::
        \\langle
        \\sum_{\\mu\\in\\Delta} r_\\mu(u_N) P_\\mu \\zeta_{\\vartheta\\rho},
        \\sum_{\\nu\\in\\partial\\Lambda\\setminus\\Delta} r_\\nu(u_N) P_\\nu \\zeta_{\\vartheta\\rho}
        \\rangle_{\\pi_0, D}.

    p : Problem
        Problem data for the Darcy equation.
    u_tt : xe.TTTensor
        TT for the solution.
    a_tt : xe.TTTensor
        TT for the diffusion coefficient.
    active_set: List of int
        Mode dimensions of the active set :math:`\\Lambda`.
    marked_set: List of int
        Mode dimensions of the marked tensor set :math:`\\Lambda\\cup\\Delta`.
    boundary_set: List of int
        Mode dimensions of the boundary tensor set
        :math:`\\Lambda\\cup\\partial\\Lambda`.

    Returns
    -------
    float
        Scalar product.
    tuple of float
        Individual components of the scalar product.
    """
    # setup
    dist = p.info["sampling"]["distribution"]
    decay = p.info["expansion"]["decay rate"]
    rho = p.info["expansion"]["rho"]
    vartheta = p.info["expansion"]["vartheta"]

    u_cores = tt.convert(u_tt)
    a_cores = tt.convert(a_tt)

    # check consistency of input
    assert len(active_set) == len(boundary_set) == len(marked_set)
    # check if marked_set is subset of boundary_set
    assert all([a_dim <= m_dim <= b_dim for a_dim, m_dim,
                b_dim in zip(active_set, marked_set, boundary_set)])

    # compute the stochastic components of :math:`r = a \\nabla u`
    log("    assemble residual tail")
    r_tail = tt.compute_product_coeff_tensor(u_cores[1:], a_cores[1:], dist)
    # check that residual has shape of complete boundary
    assert all([core.shape[1] == d for core, d in zip(r_tail, boundary_set)])

    # cut dimensions of residual
    # :math:`r_\\Lambda`
    r_tail_active = truncate_tt_tail_dim(r_tail, active_set)
    # :math:`r_{\\Lambda\\cup\\Delta}`
    r_tail_marked = truncate_tt_tail_dim(r_tail, marked_set)
    # :math:`r_{\\Lambda\\cup\\partial\\Lambda}`
    r_tail_boundary = truncate_tt_tail_dim(r_tail, boundary_set)
    assert all([np.allclose(p, q) for p, q in zip(r_tail, r_tail_boundary)])

    # assemble :math:`\\hat{K}_m` for max possible dimensions
    log("    generate hat_K_m matrices")
    hat_Ks = []
    for m, dim in enumerate(boundary_set):
        hat_Ks.append(compute_K_hat(
            list(range(dim)), list(range(dim)), m+1, vartheta*rho, decay))

    # assemble :math:`\\tau`
    log("    assemble deterministic core contraction")
    T = compute_det_contraction(p, u_cores[0], a_cores[0])
    r_1, s_1 = u_cores[0].shape[-1], a_cores[0].shape[-1]
    T = np.reshape(T, (r_1*s_1, r_1*s_1))

    # contract TT tail of residual
    log("    contract tensor set tails")
    tail_1 = contract_tail(r_tail_marked, r_tail_boundary, hat_Ks)
    tail_2 = contract_tail(r_tail_active, r_tail_marked, hat_Ks)
    tail_3 = contract_tail(r_tail_active, r_tail_boundary, hat_Ks)
    tail_4 = contract_tail(r_tail_marked, r_tail_marked, hat_Ks)

    # contract scalar products
    log("    contract deterministic core with tails")
    product_1 = np.einsum("ij,ij", T, tail_1)
    product_2 = np.einsum("ij,ij", T, tail_2)
    product_3 = np.einsum("ij,ij", T, tail_3)
    product_4 = np.einsum("ij,ij", T, tail_4)

    # compute scalar product
    # NOTE: The residual is computed in the following way:
    #       < Delta, partial_Lambda\Delta >
    #                =   < Lambda + Delta, Lambda + partial_Lambda >
    #                  + < Lambda        , Lambda + Delta          >
    #                  - < Lambda        , Lambda + partial_Lambda >
    #                  - < Lambda + Delta, Lambda + Delta          >
    ret = product_1 + product_2 - product_3 - product_4

    return ret, (product_1, product_2, product_3, product_4)
