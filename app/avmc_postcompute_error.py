# -*- coding: utf-8 -*-
import os
import shutil
import numpy as np
from context import src
import src.utils.tt_tools as tt
from src.misc import log
from avmc_utils import load_level_data
from avmc_utils import update_level_data
from avmc_utils import delete_level_data_items
from avmc_utils import compute_exact_error
from avmc_utils import clean_level_data


def need_update(DATA_FILE, keys):
    """ Check if error data already exist and are complete.

    Parameters
    ----------
    DATA_FILE : string
        Path to level data file.
    """
    lvl_data = load_level_data(DATA_FILE)
    keys_exist = [1 if key in lvl_data.keys() else 0 for key in keys]

    if not np.all(keys_exist):
        # update if at least one error key does not exist
        return True

    number_lvls = len(lvl_data["level"])
    has_full_data = []
    for key in keys:
        val = lvl_data[key]
        has_full_data.append(1 if len(val) == number_lvls else 0)
        assert len(val) <= number_lvls

    if not np.all(has_full_data):
        # update if at least one error key has incomplete data
        return True

    return False


def erase_incomplete_level_data(DATA_FILE):
    """ Delete incomplete levels from data file.

    Parameters
    ----------
    DATA_FILE : string
        Path to level data file.
    """

    _data = load_level_data(DATA_FILE)
    data_length = [len(val) for _, val in _data.items()]
    if min(data_length) == max(data_length) - 1:
        _clean_data = {}
        for key, val in _data.items():
            _clean_data[key] = _data[key][:min(data_length)]
        np.savez(DATA_FILE, lvl_data=_clean_data)
    elif min(data_length) == max(data_length):
        return
    else:
        print("Corrupted data (multiple incomplete levels)")


def postcompute_error(DATA_FILE):
    """ Compute 'exact' error if file has no error data.

    Parameters
    ----------
    DATA_FILE : string
        Path to level data file.
    """

    erase_incomplete_level_data(DATA_FILE)

    if not os.path.isfile(DATA_FILE):
        print(f"No such file '{DATA_FILE}'")
        return

    error_keys = ["exact_err_L2L2_abs", "exact_err_L2L2_rel",
                  "exact_err_L2H1_abs", "exact_err_L2H1_rel"]
    if not need_update(DATA_FILE, error_keys):
        print("File already has complete error data.")
        return

    # delete old error level data
    error_keys = ["exact_err_L2L2_abs", "exact_err_L2L2_rel",
                  "exact_err_L2H1_abs", "exact_err_L2H1_rel"]
    delete_level_data_items(DATA_FILE, error_keys)

    # compute errors
    compute_exact_error(DATA_FILE, N_test=250)
    clean_level_data(DATA_FILE)  # save a cleaned version of level data


def postcompute_nnz_dofs(DATA_FILE):
    """ Compute nnz-DoFs if file has no nnz-DoFs data.

    Parameters
    ----------
    DATA_FILE : string
        Path to level data file.
    """

    erase_incomplete_level_data(DATA_FILE)

    if not os.path.isfile(DATA_FILE):
        print(f"No such file '{DATA_FILE}'")
        return

    nnz_keys = ["nnz_dofs_u", "nnz_dofs_kappa", "nnz_dofs_f"]
    if not need_update(DATA_FILE, nnz_keys):
        print("File already has complete nnz-dofs data.")
        return

    # delete old nnz-DoFs level data
    delete_level_data_items(DATA_FILE, nnz_keys)

    # compute nnz-DoFs
    lvl_data = load_level_data(DATA_FILE)
    EPS = 1e-08
    for lvl in lvl_data["level"]:
        nnz_dofs_u = tt.dofs_nnz(lvl_data["u_reco"][lvl], EPS)
        nnz_dofs_kappa = tt.dofs_nnz(lvl_data["kappa_reco"][lvl], EPS)
        nnz_dofs_f = tt.dofs_nnz(lvl_data["f_reco"][lvl], EPS)
        update_level_data(DATA_FILE, nnz_dofs_u=nnz_dofs_u,
                          nnz_dofs_kappa=nnz_dofs_kappa, nnz_dofs_f=nnz_dofs_f)
    clean_level_data(DATA_FILE)  # save a cleaned version of level data


def main():
    """ main """

    ids = [
        ("affine", "UnitSquare", 1, 2),  # done
        ("affine", "UnitSquare", 3, 2),  # done
        ("affine", "UnitSquare", 1, 4),  # done
        ("affine", "UnitSquare", 3, 4),  # done
        ("affine", "Lshape", 1, 2),  # done
        ("affine", "Lshape", 3, 2),  # done
        ("affine", "Lshape", 1, 4),  # done
        ("affine", "Lshape", 3, 4),  # done
        ("lognormal", "UnitSquare", 1, 2),  # done
        ("lognormal", "UnitSquare", 3, 2),  # done
        ("lognormal", "UnitSquare", 1, 4),  # done
        ("lognormal", "UnitSquare", 3, 4),
        ("lognormal", "Lshape", 1, 2),  # done
        ("lognormal", "Lshape", 3, 2),  # done
        ("lognormal", "Lshape", 1, 4),  # done
        ("lognormal", "Lshape", 3, 4),
    ]

    for (coeff, geometry, deg, decay) in ids:
        # PATH = f"../data/avmc_{coeff}/"
        PATH = "../data/avmc_FINAL/"
        FILE = f"avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"

        log(f"compute nnz-DoFs: {PATH+FILE}")
        postcompute_nnz_dofs(PATH+FILE)

        log(f"compute exact error: {PATH+FILE}")
        postcompute_error(PATH+FILE)


if __name__ == "__main__":
    # coeff = "lognormal"
    # geometry = "UnitSquare"
    # deg = "1"
    # decay = "4"
    # PATH = f"../data/avmc_{coeff}/"
    # FILE = f"avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"
    # postcompute_nnz_dofs(PATH+FILE)
    # postcompute_error(PATH+FILE)

    main()
