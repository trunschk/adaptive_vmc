import numpy as np
from scipy.integrate import quad
import conv_utils as utils


def test_gauss_pdf() -> None:
    """Check if Gaussian pdf is correct."""
    assert np.abs(quad(utils.gauss_pdf, -np.inf, np.inf)[0] - 1) < 1e-10


def test_scaled_gauss_pdf() -> None:
    sigma = 2
    ys = np.random.normal(0, sigma, 1000)
    vals = 1 / (np.sqrt(2*np.pi) * sigma) * np.exp(- 1/2 * ys**2 / sigma**2)
    assert np.linalg.norm(vals - utils.zeta(ys, sigma)
                          * utils.gauss_pdf(ys)) < 1e-14

    sigma = 0.5
    ys = np.random.normal(0, sigma, 1000)
    vals = 1 / (np.sqrt(2*np.pi) * sigma) * np.exp(- 1/2 * ys**2 / sigma**2)
    assert np.linalg.norm(vals - utils.zeta(ys, sigma)
                          * utils.gauss_pdf(ys)) < 1e-14


def test_hermite_orthogonality() -> None:
    """Check if Hermite polynomials are orthonormal."""
    # unscaled unnormalized Hermite polynomials
    basis = utils.hermite_polynomials(10, 1, False)
    gramian = np.diag([np.math.factorial(j) for j in range(len(basis))])
    assert np.allclose(utils.compute_gramian(
        basis, basis, utils.gauss_pdf), gramian)

    # unscaled normalized Hermite polynomials
    basis = utils.hermite_polynomials(10, 1, True)
    gramian = np.eye(len(basis))
    assert np.allclose(utils.compute_gramian(
        basis, basis, utils.gauss_pdf), gramian)

    # scaled normalized Hermite polynomials
    sigma = utils.sigma_m(m=2, decay=2, scale=0.1)
    basis = utils.hermite_polynomials(10, 1/sigma, True)
    gramian = np.eye(len(basis))

    def density(ys):
        return np.exp(utils.log_zeta(ys, sigma) + utils.log_gauss_pdf(ys))
    assert np.allclose(utils.compute_gramian(basis, basis, density), gramian)


def test_compute_K_hat() -> None:
    """Check if computation of :math;`\\hat K_m` is correct."""
    dims = list(range(10))
    hat_K_m = utils.compute_K_hat(dims, dims, m=1, s=0.1, decay=2)

    sigma = utils.sigma_m(m=1, decay=2, scale=0.1)
    basis = utils.hermite_polynomials(max(dims)+1, 1/sigma, True)

    def density(_y, _sigma=sigma, _alpha=2):
        """ Density is :math:`\\zeta^\\alpha(y;\\sigma)\\pi_0(y)`"""
        ret = _alpha*utils.log_zeta(_y, _sigma) + utils.log_gauss_pdf(_y)
        return np.exp(ret)

    gramian = utils.compute_gramian(basis, basis, density)
    assert np.linalg.norm(gramian-hat_K_m) < 1e-14


if __name__ == "__main__":
    pass
