import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import xerus as xe
from fenics import *

from context import src
from src.misc import timeit, log, transpose, Logger
import src.utils.tt_tools as tt
import src.utils.fem_tools as fem
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as expTT
from src.problem.equation.darcy import Problem
from src.parallel import Parallel, ParallelizableProblem

# import src.autoPDB


def eval_log_likelihood(gs, delta, space, scale=1):
    """
    Compute the log-likelihood for sampled evaluations of the forward problem
    for a given measurement.

    Parameter
    ---------
    gs      : array_like of shape (..., fem_deg)
        FE coefficients of forward problem evaluated in multiple samples.
    delta   : array_like of shape (1, fem_deg)
        Measurements for log-likelihood.
    space   : fenics.FunctionSpace
        FE function space of forward problem.
    scale   : int (Default = 1)
        Rescaling of log-likelihood.
    """
    if delta.ndim < 2:
        delta.shape = (1, -1)
    assert delta.shape[0] == 1
    assert gs.ndim == 2 and gs.shape[1] == delta.shape[1] == space.dim()
    diff = delta - gs

    # use basic L2 norm for FE component, i.e. mass matrix
    mass_mat = fem.mass_matrix(space)

    # compute || delta - gs ||^2
    ret = np.sum(diff * (mass_mat@diff.T).T, axis=1)

    return -scale*ret/2


def generate_samples(p, N, samples=None):
    """
    Generate samples of FE forward prblem. Evaluate forward problem in
    optionally given samples.
    """
    ys, ws, gs = fem.sample_solution(p.info, N, samples=samples, get_eta=False)
    return ys, ws, gs


def compute_L2_error(ys, us, reco):
    """
    Compute stochastic L2 error of a function with one deterministic component.
    """
    tt_eval = tt.eval_approx(reco, ys, "normal", scale=0)
    abs_err = np.sqrt(np.sum((us-tt_eval)**2)/us.shape[0])
    rel_err = abs_err / np.sqrt(np.sum(us**2)/us.shape[0])
    return abs_err, rel_err


def compute_absRel_error(ys, us, reco, space):
    """ Compute absolute and relative L2-L2 error.
    """

    ws = np.ones(ys.shape[0])/ys.shape[0]
    abs_err = femTT.compute_L2L2_error(reco, ys, ws, us, space, "normal")
    rel_err = abs_err / femTT.compute_L2L2_norm(ys, us, space, ws=ws)
    return abs_err, rel_err


def log_likelihood(G_TT, delta, space, scale=1):
    """
    Compute the log-likelihood TTTensor for a TTTensor of the forward problem
    and a given measurement.
    """
    if delta.ndim < 2:
        delta.shape = (1, -1)
    assert delta.ndim == 2 and delta.shape[0] == 1
    assert delta.size == G_TT.dimensions[0] == space.dim()
    dims, M = G_TT.dimensions[1:], G_TT.order()-1

    # convert delta to TTTensor of correct shape
    tt1 = xe.TTTensor(xe.Tensor.from_buffer(delta.flatten()))
    tt2 = xe.TTTensor.dirac(dims, [0]*M)
    delta_TT = xe.dyadic_product([tt1, tt2])

    # compute difference: delta-G(y)
    diff = tt.convert(delta_TT - G_TT)
    diff0, diff_tail = diff[0], diff[1:]

    # compute product det component
    diff0 = diff0[0].T  # shape (rank, fem_dim)
    mass_mat = fem.mass_matrix(space)
    ret0 = np.einsum("ij,jk->ik", diff0, mass_mat@diff0.T)
    ret0 = ret0.reshape(1, 1, np.prod(ret0.shape))

    # compute product stoch component
    ret_tail = tt.compute_product_coeff_tensor(diff_tail, diff_tail, "normal")

    assert ret_tail[0].shape[0] == ret0.shape[2]
    ret = tt.convert([ret0] + ret_tail)
    ret.round(1e-16)

    return scale*ret


def expTT_reconstruction_dupe(dims, h_reco, x_0, exph0=None,
                              homogeneous=False, verbose=False, EPS=1e-10):
    """ Duplicate of expTT.expTT_reconstruction() for local debugging. """

    M, ALS_TYPE = len(dims), "simple"

    # build operator and rhs
    log("  build operator W0 and RHS b")
    W0, b = expTT.build_system_inexactIC(dims, dims, h_reco, x_0, exph0=exph0,
                                         homogeneous=homogeneous)
    log("  round operator W0")
    W0.round(EPS)
    log(f"   ranks W0: {W0.ranks()}")
    if homogeneous:
        b.round(EPS)
    log(f"   ranks b:  {b.ranks()}")

    # random initialization
    log("  set initial guess")
    u_start = xe.TTTensor.random(dims, [10]*(M-1) if M > 1 else [])
    u_start = u_start / xe.frob_norm(u_start)

    # run ALS (simple or xerus built-in)
    log(f"  run {ALS_TYPE} ALS")
    if ALS_TYPE == "simple":
        ret = expTT.run_simpleALS(A=W0, u_start=u_start, b=b, verbose=verbose)
    elif ALS_TYPE == "xerus":
        ret = xe.TTTensor(u_start)
        xe.ALS_SPD(W0, ret, b)

    # compute residual
    abs_res, rel_res = wrapper_compute_residual(
        ret, dims, h_reco, exph0, homogeneous=homogeneous)

    log(f"   ranks u: {W0.ranks()}")
    log("  round solution and add first component")
    ret.round(EPS)  # round to get unneccesary large ranks down
    ret = tt.add_dimension_left(ret)  # add deterministic component
    log(f"   ranks u: {W0.ranks()}")

    if homogeneous:
        ret = ret + tt.constant(exph0, ret.dimensions[1:])

    return ret, abs_res, rel_res


def wrapper_compute_residual(u, dims, h_reco, exph0, homogeneous=False):
    """ Compute ||Bu-f||_2 for homogeneous or inhomogeneous solution."""

    val = xe.TTTensor(u)
    if val.order() == len(dims)+1 and val.dimensions[0] == 1:
        val.fix_mode(0, 0)
    if not homogeneous:
        # for inhomogeneous case subtract exp h(x_0) from solution and compute
        # residual || B(u-exp h(x_0)) - f||_2 with homogeneous f.
        const = tt.constant(exph0, val.dimensions)
        const.fix_mode(0, 0)
        val = val - const

    f, g = expTT.build_f_cores(h_reco, normalized=True)
    normalization = []
    for m in range(1, len(f)):
        fTT = tt.convert([g[0]] + [f[j] if j == m else g[j]
                                   for j in range(1, len(f))])
        normalization.append(xe.frob_norm(fTT)**2)

    abs_res = expTT.compute_residual(val, h_reco, exph0)
    rel_res = abs_res / np.sqrt(np.sum(normalization))
    return abs_res, rel_res


if __name__ == "__main__":
    sys.stdout = Logger(
        fileName="app_expTT_logLikelihood.log",
        filePath="../log/",
        overwrite=True)

    mean, decay = 1.0, 2.0
    SETUP = [
        # {"M":   5, "fem_deg": 1, "nT": 50},
        # {"M":  10, "fem_deg": 1, "nT": 50},
        # {"M":  20, "fem_deg": 1, "nT": 50},
        # {"M":  30, "fem_deg": 1, "nT": 50},
        {"M":  40, "fem_deg": 1, "nT": 50},
    ]
    # SETUP = [{"M":  40, "fem_deg": 1, "nT": 50}]

    os.makedirs("../data/logLikelihood_darcy/G_recos_vmc/", exist_ok=True)
    os.makedirs("../data/logLikelihood_darcy/u_recos/", exist_ok=True)
    os.makedirs("../data/logLikelihood_darcy/u_recos_vmc/", exist_ok=True)

    for j, dct in enumerate(SETUP):

        log("__"*40)
        log(f"RUN {j+1}/{len(SETUP)}")
        M, fem_deg, nT = dct["M"], dct['fem_deg'], dct['nT']
        expTT_dims = [4]*M
        FEM_TYPE = f"DG-{fem_deg}" if fem_deg == 0 else f"CG-{fem_deg}"

        # setup
        info = {"problem": {"name": "darcy"},
                "fe": {"degree": dct["fem_deg"], "mesh": dct["nT"]},
                "fe_coeff": {"degree": dct["fem_deg"]-1, "space": "DG"},
                "expansion": {
                    "size": M, "mean": mean, "decay rate": decay,
                    "rho": 1, "vartheta": 0.1, "scale": 6 / np.pi**2},
                "sampling": {
                    "distribution": "normal", "expfield": True,
                    "batch size": 40}
                }
        p = Problem(info)
        log(f"setup: M={dct['M']}, FEM={FEM_TYPE}, nTriangles={p.mesh.num_cells()}")
        log(f"FE-DoFs: {p.space.dim()}")

        element = "DG" if fem_deg == 0 else "CG"
        APPENDIX = f"{element}{fem_deg}-{nT}_decay-{decay}_M{M}"

        # ----------------------------------------------------------------------
        # training and test data forward problem
        N_test = 1000
        log(f"generate {N_test:,} test samples ...")
        with timeit("... ({:4.2f} s)"):
            ys_test, ws_test, gs_test = generate_samples(p, N_test)

        log("generate initial condition samples ...")
        with timeit("... ({:4.2f} s)"):
            y0 = np.zeros([1, M])
            x0 = fem.dof_coords(p.space, p.mesh)  # fem-dofs
            * _, g0 = generate_samples(p, 1, samples=y0)

        # measurement data
        log("generate measurement ...")
        with timeit("... ({:4.2f} s)"):
            ys_true, ws_true, gs_true = generate_samples(p, 1)
            sigma = 1*np.std(gs_test, axis=0)
            sigma = 1e-06*np.eye(gs_test.shape[1])
            noise = np.random.multivariate_normal(
                np.zeros(gs_test.shape[1]), sigma, 1)
            delta = gs_true + noise

        # training and test data (log-)likelihood
        hs_test = eval_log_likelihood(gs_test, delta, p.space, scale=1)
        h0 = eval_log_likelihood(g0, delta, p.space, scale=1)

        us_test, exph0 = np.exp(hs_test), np.exp(h0)

        # ----------------------------------------------------------------------
        # reconstruct solution G(y) with VMC

        log("--"*40)
        log("reconstruct forward map G_TT (VMC) ...")
        file_name = f"../data/logLikelihood_darcy/G_recos_vmc/G_TT_reco_{APPENDIX}.npy"
        # file_name = f"../data/logLikelihood_darcy/G_recos_vmc/G_TT_reco_{APPENDIX}_Run{j}.npy"
        if os.path.isfile(file_name):
            log(f"load G_VMC from: {file_name}")
            G_TT = tt.load(file_name)
        else:
            N = 1000
            log(f"generate {N:,} training samples ...")
            with timeit("... ({:4.2f} s)"):
                ys, ws, gs = generate_samples(p, N)
            hs = eval_log_likelihood(gs, delta, p.space, scale=1)

            log("reconstruct forward problem G(y) ...")
            with timeit("... ({:4.2f} s)"):
                G_TT = tt.reconstruct(
                    dims=[3]*M, nodes=ys.T, weights=ws, values=gs,
                    dist="normal", init=None, label="G_TT", verbose=False
                )
            # G_TT.round(1e-05)
            tt.save(file_name, G_TT)
            log(f"save G_VMC to: {file_name}")

        log(f"ranks G_VMC: {G_TT.ranks()}")
        abs_err, rel_err = compute_absRel_error(
            ys_test, gs_test, G_TT, p.space)
        log(f"error G_VMC: {abs_err:4.2e}/{rel_err:4.2e}")

        # ----------------------------------------------------------------------
        # build log-likelihood TT

        # This is h(y) = - || delta - G(y) ||_s^2
        log("--"*40)
        log("compute log-likelihood TTTensor h_TT ...")
        with timeit("... ({:4.2f} s)"):
            h_TT = log_likelihood(G_TT, delta, p.space, scale=1)
            # h_TT.round(1e-05)

        log(f"ranks h_TT: {h_TT.ranks()}")
        abs_err, rel_err = compute_L2_error(ys_test, hs_test, h_TT)
        log(f"error h_TT: {abs_err:4.2e}/{rel_err:4.2e}")

        # ----------------------------------------------------------------------
        # compute likelihood TT
        log("--"*40)
        log("compute expTT reconstruction u_TT ...")
        with timeit("... ({:4.2f} s)"):
            u_TT, abs_res, rel_res = expTT_reconstruction_dupe(
                expTT_dims, h_TT, y0, exph0=exph0,
                homogeneous=False, verbose=2, EPS=1e-10)
        log(f"residual Bu-f: {abs_res:4.2e}/{rel_res:4.2e}")

        abs_err, rel_err = compute_L2_error(ys_test, us_test, u_TT)
        log(f"error u_TT: {abs_err:4.2e}/{rel_err:4.2e}")

        tmp = tt.eval_approx(h_TT, ys_test, "normal", scale=0)
        abs_err, rel_err = compute_L2_error(ys_test, np.exp(tmp), u_TT)
        log(f"error u_TT-exp(h_TT): {abs_err:4.2e}/{rel_err:4.2e}")

        # ----------------------------------------------------------------------
        # reconstruct likelihood TT with VMC

        log("--"*40)
        log("compute VMC reconstruction u_VMC")

        file_name = f"../data/logLikelihood_darcy/u_recos_vmc/u_reco_vmc_{APPENDIX}.npy"
        if os.path.isfile(file_name):
            log(f"load u_vmc: {file_name} ...")
            u_vmc = tt.load(file_name)
        else:
            N = 1000
            log(f"generate {N} training samples ...")
            with timeit("... ({:4.2f} s)"):
                ys, ws, gs = generate_samples(p, N_test)
                hs = eval_log_likelihood(gs, delta, p.space, scale=1)
                us = np.exp(hs_test).reshape(-1, 1)

            log("run uq_ra_adf ...")
            with timeit("... {:4.2f} s"):
                u_vmc = tt.reconstruct(
                    dims=expTT_dims, nodes=ys.T, weights=ws, values=us,
                    dist="normal", label="naive VMC", verbose=False
                )
            # tt.save(file_name, u_vmc)  # NOTE: this doesn't work here
            # log(f"save to: {file_name}")

        log(f"    ranks: {u_vmc.ranks()}")
        abs_res, rel_res = wrapper_compute_residual(
            u_vmc, expTT_dims, h_TT, exph0, homogeneous=False)
        log(f"residual Bu-f: {abs_res:4.2e}/{rel_res:4.2e}")
        abs_err, rel_err = compute_L2_error(ys_test, us_test, u_vmc)
        log(f"error u_VMC: {abs_err:4.2e}/{rel_err:4.2e}")

    # from IPython import embed; embed(); sys.exit()
