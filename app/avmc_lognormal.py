""" adaptive VMC application with affine diffusion coefficient. """

# -*- coding: utf-8 -*-
import sys
import traceback

from context import src
from src.misc import timeit, logf
from src.problem.equation.darcy import Problem
# import src.autoPDB

from avmc_utils import datetime_id
from avmc_utils import log_file_name
from avmc_utils import initialize_level_data
from avmc_utils import load_level_data
from avmc_utils import update_level_data
from avmc_utils import clean_level_data
from avmc_utils import info_dict
from avmc_utils import solve_step
from avmc_utils import estimate_step
from avmc_utils import mark_and_refine_step
from avmc_utils import check_break_condition
from avmc_utils import compute_exact_error
from conv_utils import compute_scalar_product


def run_avmc_lognormal(DATA_FILE, geometry, deg, decay, mean):
    """ Run adaptive loop stept SOLVE -> ESTIMATE -> MARK -> REFINE.

    Parameters
    ----------
    DATA_FILE : string
        Path to level data file.
    geometry : str
        Computational domain, 'UnitSquare' or 'Lshape'.
    deg : int
        Finite element polynomial degree.
    decay : float
        Decay of stochastic modes of diffusion coefficient kappa.
    mean : float
        Mean value of diffusion coefficient kappa.
    """
    # setup
    sdim = [2]  # initial stochastic dimensions for solution u
    N_VMC = 100  # number of samples for VMC reconstruction
    N_test = 250  # number of samples for error computation
    MAX_TT_DoFs = int(1e+05)  # maximum number of TT DoFs
    MAX_ITER = 50  # maximum number of iterations
    cStoch = 5  # weight for stochastic estimator
    cDisc = 10  # weight for algebraic estimator
    doerfler_threshold = 0.3  # Doerfler marking threshold
    exp = True  # use log-affine diffusion coefficient
    dist = "normal"  # use normal distribution for stochastic variables
    n_refs = 8 if deg == 1 else 5  # number of initial refinements
    look_ahead = 5  # look-ahead of error estimator

    info = info_dict(M=20, degree=deg, coeff_degree=deg, decay=decay,
                     mean=mean, exp=exp, dist=dist, geometry=geometry,
                     mesh=n_refs)
    p = Problem(info)
    assert p.space.dim() == p.space_coeff.dim()  # sanity check
    assert p.info["sampling"]["distribution"] == "normal"
    assert p.info["sampling"]["expfield"] is True

    LOG_FILE = log_file_name(DATA_FILE)
    logf(LOG_FILE, f"Write log to: {LOG_FILE}", overwrite=True)

    # loop
    for loop_iter in range(MAX_ITER):
        logf(LOG_FILE, f"ITERATION {loop_iter} (max. {MAX_ITER-1})")
        logf(LOG_FILE, f"    FE-DoFs: {p.dofs()}")
        logf(LOG_FILE, f"    sdim: {sdim}")
        update_level_data(DATA_FILE, level=loop_iter, info=p.info,
                          max_tt_dofs=MAX_TT_DoFs, fe_dofs_u=p.space.dim(),
                          fe_dofs_kappa=p.space_coeff.dim(), max_iter=MAX_ITER,
                          sdim=sdim, doerfler_threshold=doerfler_threshold,
                          n_vmc=N_VMC, n_test=N_test, cStoch=cStoch,
                          cDisc=cDisc)

        # SOLVE
        u_reco, kappa_reco, f_reco = solve_step(
            p, sdim, N_VMC, N_test, dist=dist, file=DATA_FILE)

        # ESTIMATE
        est_det_loc, est_stoch_loc, ref_type = estimate_step(
            u_reco, kappa_reco, f_reco, p, cStoch, cDisc,
            file=DATA_FILE, look_ahead=look_ahead)

        # convergence test
        logf(LOG_FILE, ":: CONVERGENCE TEST ::")
        active_set = (u_reco.dimensions[1:]
                      + [1]*(kappa_reco.order()-u_reco.order()))
        _, marked_set, _ = mark_and_refine_step(
            est_det_loc, est_stoch_loc, ref_type, p, sdim, N_VMC, look_back=3,
            file=DATA_FILE)
        marked_set = marked_set + [1]*(kappa_reco.order()-1-len(marked_set))
        boundary_set = [d+q-1
                        for d, q in zip(active_set, kappa_reco.dimensions[1:])]
        assert len(active_set) == len(marked_set) == len(boundary_set)
        logf(LOG_FILE, "    active, marked and boundary set dimensions")
        logf(LOG_FILE, f"        FE-dim:   {p.space.dim()}")
        logf(LOG_FILE, f"        active:   {active_set}")
        logf(LOG_FILE, f"        marked:   {marked_set}")
        logf(LOG_FILE, f"        boundary: {boundary_set}")
        scalar_product, products = compute_scalar_product(
            p, u_reco, kappa_reco, active_set, marked_set, boundary_set)
        logf(LOG_FILE, f"    scalar product: {scalar_product:4.2g}")
        logf(LOG_FILE, "    individual contributions:")
        logf(LOG_FILE, f"        + <marked, bdry>:   {products[0]:.16f}")
        logf(LOG_FILE, f"        + <active, marked>: {products[1]:.16f}")
        logf(LOG_FILE, f"        - <active, bdry>:   {products[2]:.16f}")
        logf(LOG_FILE, f"        - <marked, marked>: {products[3]:.16f}")

        # BREAK
        clean_level_data(DATA_FILE)  # save a cleaned version of level data
        do_break, msg = check_break_condition(file=DATA_FILE)
        if do_break:
            logf(LOG_FILE, f":: BREAK :: ({msg})")
            break

        if load_level_data(DATA_FILE)["est_total"][-1] > 1e+01:  # XXX test
            msg = "estimator increase got out of hand"
            logf(LOG_FILE, f":: BREAK :: ({msg})")
            break

        # MARK and REFINE
        p, sdim, N_VMC = mark_and_refine_step(
            est_det_loc, est_stoch_loc, ref_type, p, sdim, N_VMC, look_back=3,
            file=DATA_FILE)

        logf(LOG_FILE, "")

    compute_exact_error(DATA_FILE)
    clean_level_data(DATA_FILE)  # save a cleaned version of level data


def main():
    """ main """

    PATH = "../data/avmc_lognormal/"
    # PATH += f"run_{datetime_id()}" + "/"

    # define runs
    RUNS = [  # (geometry, deg, decay)
        # ("UnitSquare", 1, 2),  # done
        # ("UnitSquare", 1, 4),  # done
        # ("UnitSquare", 3, 2),  # done
        # ("UnitSquare", 3, 4),
        ("Lshape", 1, 2),  # done
        ("Lshape", 1, 4),  # done
        # ("Lshape", 3, 2),  # done
        # ("Lshape", 3, 4),
    ]

    for run in RUNS:
        geometry, deg, decay = run
        FILE = f"avmc_lognormal_{geometry}_deg-{deg}_decay-{decay}.npz"
        initialize_level_data(PATH, FILE)
        try:
            run_avmc_lognormal(PATH+FILE, geometry, deg, decay, mean=0.0)
            print()
        except Exception as e:
            tb = "".join(traceback.format_exception(None, e, e.__traceback__))
            logf(log_file_name(PATH+FILE), tb)
            print()
            continue


if __name__ == "__main__":
    main()
