""" Utility functions for avmc scripts. """

# -*- coding: utf-8 -*-
import os
import shutil
import time
from copy import deepcopy
import numpy as np

from fenics import FunctionSpace
import xerus as xe

import scipy.sparse as sps
from scipy.stats import uniform, norm
import sksparse.cholmod as cholmod

from context import src
from src.misc import Logger, log, logf, timeit
from src.misc import get_y_samples, compute_scaling_constant
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as tt_exp
import src.estimator.intrusive.intrusive_estimator as EST
from src.estimator.intrusive.est_disc import compute_discretization_estimator_terms
from src.estimator.intrusive.est_disc import assemble_operator
from src.estimator.intrusive.est_disc import apply_operator
from src.problem.equation.darcy import Problem
from src.utils.darcy_affine_coeff import exact_TT_diff_coeff
from src.utils.darcy_affine_coeff import eval_diff_coeff

from measurement_utils.bases import LegendrePolynomials, HermitePolynomials
from measurement_utils.samplers import CMDensity
from measurement_utils.samplers import approx_quantiles
from measurement_utils.samplers import CMSampler
from measurement_utils.samplers import CartesianProductSampler
from measurement_utils.samplers import CMWeights
from measurement_utils.samplers import test_CMSamples
from measurement_utils.samplers import test_CMWeights


def get_max_dim(d1, d2):
    """ Get the entry-wise maxima of two lists.

    If one list is shorter than the other, the values of the longer list are
    treated as the maximum.
    """
    if len(d1) > len(d2):
        return get_max_dim(d2, d1)
    dim = [max(i, j) for i, j in zip(d1, d2)]
    dim += d2[len(d1):]
    return dim


def compute_difference(_tt1, _FES1, _tt2, _FES2):
    """ Compute the Difference of two TTTensors in Frobenius norm.

    Parameters
    ----------
    _tt1 : xe.TTTensor
    _FES1 : fenics.FunctionSpace
    _tt2 : xe.TTTensor
    _FES2 : fenics.FunctionSpace
    """
    assert _tt1.dimensions[0] == _FES1.dim()
    assert _tt2.dimensions[0] == _FES2.dim()
    if _FES1.dim() > _FES2.dim():  # prolongate _tt2 to larger FE space
        _tt2 = femTT.prolongate(_tt2, _FES2, _FES1)
    elif _FES1.dim() < _FES2.dim():  # prolongate _tt1 to larger FE space
        _tt1 = femTT.prolongate(_tt1, _FES1, _FES2)
    assert _tt1.dimensions[0] == _tt2.dimensions[0]
    dimensions = get_max_dim(_tt1.dimensions, _tt2.dimensions)
    _tt1 = tt.prolong_tt(_tt1, dimensions)  # padd stochastic dimensions
    _tt2 = tt.prolong_tt(_tt2, dimensions)  # padd stochastic dimensions
    assert _tt1.dimensions == _tt2.dimensions
    return xe.frob_norm(_tt1-_tt2)


def datetime_id():
    """ Time stamp YYYYmmdd_HHMMSS. """
    return time.strftime("%Y%m%d_%H%M%S", time.localtime())


def log_file_name(DATA_FILE):
    """ Generate log-file name from data file.

    File path is '../log/DATA_FILE.log'.
    """
    LOG_FILE = "../log/"
    LOG_FILE += DATA_FILE[DATA_FILE.rfind("/")+1:DATA_FILE.rfind(".")] + ".log"
    return LOG_FILE


def info_dict(dct=None, **kwargs):
    """ Return suitable info dictionary for src.problem.equation.darcy.Problem.

    Return the default info dictionary. Replace values with given keywords.
    If dct is not None, use (copy of) dct as default dictionary.

    Returns
    -------
    info : dict
        Info dictionary for Darcy problem.
    """
    def _set_value(key, dic, default):
        """ Get value for key in dict if exists or return default. """
        return dic[key] if key in dic.keys() else default

    if dct is not None:
        assert isinstance(dct, dict)
        info = deepcopy(dct)
    else:
        info = {"problem": {}, "fe": {}, "fe_coeff": {},
                "expansion": {}, "sampling": {}}
    info["problem"]["name"] = "darcy"
    info["fe"]["degree"] = _set_value("degree", kwargs, 1)
    info["fe"]["mesh"] = _set_value("mesh", kwargs, 8)
    info["fe"]["geometry"] = _set_value("geometry", kwargs, "UnitSquare")
    info["fe_coeff"]["degree"] = _set_value("coeff_degree", kwargs, 1)
    info["fe_coeff"]["space"] = _set_value("coeff_space", kwargs, "CG")
    info["expansion"]["size"] = _set_value("M", kwargs, 20)
    info["expansion"]["mean"] = _set_value("mean", kwargs, 0.0)
    info["expansion"]["decay rate"] = _set_value("decay", kwargs, 2)
    info["expansion"]["rho"] = _set_value("rho", kwargs, 1)
    # NOTE: This vartheta is not the Doerfler marking threshold but the
    #       rescaling parameter for the Hermite basis in the lognormal case.
    info["expansion"]["vartheta"] = _set_value("vartheta", kwargs, 0.1)
    info["expansion"]["scale"] = _set_value("scale", kwargs, 6/np.pi**2)
    info["sampling"]["distribution"] = _set_value("dist", kwargs, "uniform")
    info["sampling"]["expfield"] = _set_value("exp", kwargs, False)
    info["sampling"]["batch size"] = _set_value("batch", kwargs, 40)

    if info["fe_coeff"]["degree"] == 0:
        assert info["fe_coeff"]["space"] != "CG"
    if info["sampling"]["distribution"] == "uniform":
        assert info["sampling"]["expfield"] is False
    elif info["sampling"]["distribution"] == "normal":
        assert info["sampling"]["expfield"] is True

    # generate mesh
    if isinstance(info["fe"]["mesh"], int):
        mesh = fem.generate_mesh(info["fe"]["geometry"], info["fe"]["mesh"])
        info["fe"]["mesh"] = mesh

    return info


def initialize_level_data(path, file):
    """ Initialize path and backup file if it already exists. """
    os.makedirs(path, exist_ok=True)
    if os.path.isfile(path+file):
        shutil.move(path+file, path+file+".backup")
        print(f"Warning: File {path+file} already exists. "
              + f"Move old file to {path+file}.backup")


def update_level_data(file, **kwargs):
    """ Update level data in file.

    Load file (or create new one), append data from kwargs and save file.
    """
    if os.path.isfile(file):
        _data = np.load(file, allow_pickle=True)["lvl_data"].item()
    else:
        _data = {key: [] for key in kwargs}
    for key, value in kwargs.items():
        if key not in _data.keys():
            _data[key] = []
        _data[key].append(value)
    np.savez(file, lvl_data=_data)
    if "verbose" in kwargs.keys() and kwargs["verbose"] > 0:
        logf(log_file_name(file), f"update data file: {file}")


def load_level_data(file):
    """ Load level data from file. """
    _data = np.load(file, allow_pickle=True)["lvl_data"].item()
    return _data


def clean_level_data(file):
    """ Clean level data from xerus and fenics data structures.

    Remove all fenics and xerus data structures from level data to simplify
    requirements on loading the data for post-processing. Clean level data
    file is saved to same path with same filename except it starts with
    'clean_'.

    Parameters
    ----------
    file : string
        Filename of level data file.
    """

    logf(log_file_name(file), f"clean file: {file}")
    clean_file = get_clean_file_name(file)
    keys = ["u_reco", "kappa_reco", "f_reco", "info"]
    delete_level_data_items(file, keys, clean_file)


def delete_level_data_items(file, keys, new_file=None):
    """ Remove items (keys and values) from level data.

    Parameters
    ----------
    file : string
        Filename of level data file.
    keys : list of string
        List of keys to delete.
    new_file : string, default=None
        Filename of new level data file, use same file if None.
    """

    if new_file is None:
        new_file = str(file)
    _data = load_level_data(file)
    for key in keys:
        _ = _data.pop(key, "nothing to pop")
    np.savez(new_file, lvl_data=_data)


def get_clean_file_name(file):
    """ Get filename for clean level data file.

    Parameters
    ----------
    file : string
        Filename of level data file containing all data.

    Returns
    -------
    clean_file : string
        Filename of clean level data file.
    """
    idx = file.rindex("/")  # find last occurence of '/'
    clean_file = file[:idx+1] + "clean_" + file[idx+1:]
    return clean_file


def rescale_samples(ys, s, decay):
    """Rescale normal samples to shifted measure.

    To obtain samples distributed according to the scaled measure, samples
    from the standard normal distribution are rescaled by
            y_m = \sigma_m(s)^{-1} y_m       for m = 1, 2, ...
    where
        \sigma_m(s) = \exp( 0.9 * s * m^{-decay} / \zeta(decay) ),
    with zeta() being the zeta function.

    Parameters
    ----------
    ys : array_like
        Standard normal samples.
    s : float
        Scaling constant. This is e.g. vartheta*rho.
    decay : int
        Decay rate of affine expansion required to compute the scaling
        constant.

    Returns
    -------
    ys_scaled : array_like
        Samples distributed to scaled Gaussian measure.
    """
    # NOTE: scaling constant returns exp( s * || gamma_m ||_{L^\infty(D)} )
    c = np.array([
        compute_scaling_constant(m+1, decay, s) for m in range(ys.shape[1])
    ]).reshape(1, -1)
    return c*ys


def get_wls_samples(info, n, dim):
    """TODO: Docstring for get_wls_samples.

    Parameters
    ----------
    info : TODO
    n : TODO

    Returns
    -------
    TODO

    """
    if isinstance(dim, list):  # if sdims are given for each mode
        dim = int(max(dim))
    order = info["expansion"]["size"]
    dist = info["sampling"]["distribution"]

    # univariate basis
    if dist == "uniform":
        domain = (-1, 1)
        basis_1d = LegendrePolynomials(domain, dim-1)
    elif dist == "normal":
        basis_1d = HermitePolynomials(dim-1)

    # the base density for the 1-dimensional sampler
    def density_1d(x):
        if dist == "uniform":
            return uniform.pdf(x, loc=domain[0], scale=domain[1]-domain[0])
        elif dist == "normal":
            return norm.pdf(x)

    # quantiles for normal distribution
    if dist == "normal":
        q = approx_quantiles(CMDensity(basis_1d, density_1d), 1e-4)
        domain = (-q, q)

    sampler_1d = CMSampler(basis_1d, density_1d, domain=domain)
    sampler = CartesianProductSampler([sampler_1d]*order)

    def density(xs):
        return np.prod(density_1d(xs), axis=1)

    best_error, best_samples, best_weights = 1, None, None
    for _ in range(50):
        samples = sampler.sample(n)
        assert samples.shape == (n, order)
        weights = CMWeights(sampler, density, samples)
        error = test_CMSamples(sampler, samples)
        if error < best_error:
            best_error = error
            best_samples = samples
            best_weights = weights
        if error < 1e-2:
            break

    samples = best_samples
    weights = best_weights
    assert test_CMWeights(sampler, density, samples, weights) < 1e-12

    return samples, weights


def get_random_idx(N, k):
    """
    Get k random disjoint numbers (sorted) in range(N).
    """
    idx = np.unique(np.random.choice(range(N), size=k, replace=False))
    assert idx.size == k
    return idx


def generate_samples(n, info, dims=None, scale=0):
    """ Generate point evaluations of Darcy functions.

    Draw n random points according to distribution of info and compute
    realizations of the solution u, the diffusion coefficient kappa and the
    right-hang side f of the Darcy problem as specified in info.

    Parameters
    ----------
    n : int
        Number of samples.
    info : dict
        Info dictionary that specifies Darcy problem.
    dims : list of int
        Dimensions of the expansion for the wLS samples.
    scale : float
        Draw samples w.r.t. the scaled measure.

    Returns
    -------
    ys : array_like
        Random sample points
    ws : array_like
        Weights for sample points
    us : array_like
        FE coefficients of the solution in each sample point.
    kappas : array_like
        FE coefficients of the diffusion coefficient in each sample point.
    fs : array_like
        FE coefficients of the right-hand side in each sample point.
    """
    decay = info["expansion"]["decay rate"]
    mean = info["expansion"]["mean"]
    if dims is not None:
        ys, ws = get_wls_samples(info, n, dims)
    else:
        ys = get_y_samples(info, n)
        ws = np.ones(ys.shape[0])/ys.shape[0]
    if info["sampling"]["distribution"] == "normal":
        ys = rescale_samples(ys, scale, decay)

    _, _, us = fem.sample_solution(info, n, samples=ys, get_eta=False)
    _, _, fs = fem.sample_rhs(info, n, samples=ys)

    sample_kappa_fenics = False
    if sample_kappa_fenics:
        _, _, kappas = fem.sample_diffusion_coeff(info, n, samples=ys)
    else:
        p = Problem(info)
        dofs = fem.dof_coords(p.space_coeff, p.mesh)
        kappas = eval_diff_coeff(dofs, ys, decay, mean)
        if info["sampling"]["expfield"] is True:
            kappas = np.exp(kappas)

    return ys, ws, us, kappas, fs


def initial_guess_solution(file, p, sdim, EPS=None):
    """ Use last level reconstructions as initial guess.

    Load reconstructions of last level and prolongate them to the current
    FE triangulation.

    Parameters
    ----------
    file : string
        Level data file.
    p : src.problem.equation.darcy.Problem
    sdim : list of int
        Stochastic dimensions initial guess of u.
    EPS : float or None, default=None
        Noise level added to initial guess.

    Returns
    -------
    u_init : xe.TTTensor or None
        Prolongation of solution reconstruction of last level.
    """

    lvl_data = load_level_data(file)
    current_level = lvl_data["level"][-1]
    if current_level > 0:
        q = Problem(lvl_data["info"][current_level-1])

    # initial guess for solution u
    if current_level <= 0:
        u_init = None
    else:
        u_reco_last = lvl_data["u_reco"][current_level-1]
        u_init = tt.get_initial_guess(
            [p.space.dim()]+sdim, u_reco_last, q.space, p.space)
        if EPS is not None:
            _dims, _ranks = u_init.dimensions, [1] * (u_init.order()-1)
            _noise = xe.TTTensor.random(_dims, _ranks)
            u_init += EPS * _noise / _noise.frob_norm()

    return u_init


def need_new_kappa_reco(file, p):
    """ Check if a new VMC reconstruction of kappa is required.

    Parameters
    ----------
    file : string
        Level data file.
    p : src.problem.equation.darcy.Problem

    Returns
    -------
    val : bool
        True, if FE space_coeff dim has changed since last level.
    """

    val = True
    lvl_data = load_level_data(file)
    current_level = lvl_data["level"][-1]
    if current_level > 0:
        q = Problem(lvl_data["info"][current_level-1])
        if p.space_coeff.dim() == q.space_coeff.dim():
            val = False

    return val


def cholesky(S):
    """ Compute Cholesky decomposition of matrix S.

    Sparse version of Cholesky decomposition.
    """
    eps = np.finfo(S.dtype).eps

    m = sps.diags(1/np.sqrt(S.diagonal()))
    mI = sps.diags(np.sqrt(S.diagonal()))
    T = m.dot(S).dot(m)  # T = m.T S m
    T.eliminate_zeros()

    factor = cholmod.cholesky(T.tocsc(), eps)
    PL, DD = factor.L_D()  # LDL′=PAP′
    DD_diag = DD.diagonal()
    DD_diag[DD_diag < 0] = 0
    D = sps.diags(np.sqrt(DD_diag))
    L = mI.dot(factor.apply_Pt(PL.tocsc()).dot(D))
    P = factor.apply_P(sps.eye(S.shape[0], format='csc'))

    # L@L.T = S
    assert np.linalg.norm((L.dot(L.T)-S).data) < 1e-12

    # upper triangular part of P@L is 0
    assert len(sps.triu(P.dot(L), 1).data) == 0

    return P, L


def estimate_sample_validity(ys, TT, dist, scale=0, decay=-1, vals=None):
    """ Check if empirical Gramian is bad approximation of identity.

    Compute the empirical Gramian for each core and return the minimum of
    the smalles eigenvalue of each core as an estimate for the validity of
    the (number of the) samples.
    """
    eigenvalue_list = femTT.compute_gramian_eigenvalues(
        ys[:, :TT.order()-1].T, TT, dist, scale=scale, decay=decay)
    min_eigenvalue = np.min([np.min(evs) for evs in eigenvalue_list])
    return min_eigenvalue


def reconstruct_orthFE(dims, ys, ws, vals, p, scale=0, init=None,
                       verbose=False, space=None):
    """ Wrapper for VMC reconstruction.

    Change FE basis to be orthonormal before running uq_ra_adf, then change
    basis of reconstruction back to standard FE basis.

    Returns
    -------
    reco : xe.TTTensor
        VMC reconstrucion.
    """
    if space is None:
        space = p.space
    S = fem.stiffness_matrix(space)
    P, L = cholesky(S)

    # base change samples
    assert vals.shape == (ys.shape[0], space.dim())
    vals = L.dot(vals.T).T

    if init is not None:
        # base change initial guess
        init.move_core(0)
        core = tt.to_ndarray(init.get_component(0))[0]  # shape (e,r)
        # core = np.einsum('ler,de -> ldr', core, L)
        core = L.dot(core)
        init.set_component(0, xe.Tensor.from_buffer(core[None]))

    # reconstruct in orthonormal basis
    decay = p.info["expansion"]["decay rate"]
    reco = tt.reconstruct(dims=dims, nodes=ys[:, :len(dims)].T, weights=ws,
                          values=vals, dist=p.info["sampling"]["distribution"],
                          scale=scale, decay=decay, init=init,
                          label="", verbose=verbose)

    # base change reconstruction back to standard FE basis
    reco.move_core(0)
    core = tt.to_ndarray(reco.get_component(0))[0]  # shape (e,r)
    # For this we need to compute inv(L)@core = inv(P.T@P@L)@core = inv(P@L)@P@core.
    # Since P and L are sparse and since P@L is lower triangular this can be done very efficiently.
    core = sps.linalg.spsolve_triangular(P.dot(L).tocsr(), P.dot(core))
    reco.set_component(0, xe.Tensor.from_buffer(core[None]))

    return reco


# XXX
def reconstruct_solution_by_update(file, sdim, ys, ws, vals, p, EPS=1e-16):
    """ Reconstruct solution by updte of last step.

    Run VMC for the difference of exact samples and samples from previous step
    and the add the difference to the last reconstruction.

    Parameters
    ----------
    file : str
        Path to level data file.
    sdim : list of int
        Stochatic dimensions.
    ys : array_like
        Paramter samples.
    ws : array_like
        Weights for samples.
    vals : array_like
        Samples of pathwise solution u(ys).
    p : src.problem.equation.darcy.Problem
    EPS : float
        Rounding accuracy for reconstruction.
    """
    # TODO this is currently not correct. Before use, assure eval w.r.t. the
    #      correct basis (scaling) is used.

    lvl_data = load_level_data(file)
    current_level = lvl_data["level"][-1]
    dist = p.info["sampling"]["distribution"]
    decay = p.info["expansion"]["decay rate"]

    u_reco_last = lvl_data["u_reco"][current_level-1]
    # reshape last u_reco to current spatial and stochastic dimensions
    # q = Problem(lvl_data["info"][current_level-1])
    # u_reco_last = femTT.prolongate(u_reco_last, q.space, p.space)
    # u_reco_last = tt.prolong_tt(u_reco_last, u_reco_last.dimensions[:1] + sdim)

    # use VMC for difference of exact samples and last reco samples
    diffs = vals - tt.eval_approx(u_reco_last, ys, dist, scale=scale,
                                  decay=decay)
    # print(norm_L2H1(diffs))  # XXX test
    update_reco = reconstruct_orthFE(sdim, ys, ws, diffs, p, init=None)
    # update_reco = tt.reconstruct(
    #     dims=sdim, nodes=ys[:, :len(sdim)].T, weights=ws, values=diffs,
    #     dist=dist, init=None, label="", verbose=False)

    assert u_reco_last.dimensions == update_reco.dimensions
    u_reco = u_reco_last + update_reco
    u_reco.round(EPS)
    return u_reco


def reconstruct_wrapper(file, sdim, N_VMC, ys, ws, us, p, init=None):
    """ Wrapper for VMC reconstruction of solution u.

    Reconstruct the solution from samples by using VMC. Redo VMC reconstruction
    with new samples, if the absolute L2-H^1 error is not smaller then in last
    step. Also increase ranks of reco such that TT-DoFs of solution increase
    monotonous through the aVMC loop.

    Parameters
    ----------
    file : str
        Path to level data file.
    sdim : list of int
        Stochastic dimensions of solution.
    N_VMC : int
        Number of samples for uq_ra_adf.
    ys : array_like
        Parameter samples for testing.
    ws : array_like
        Sample weights for testing.
    us : array_like
        Samples of solution for testing.
    p : src.problem.equation.darcy.Problem
    init : xe.TTTensor or None
        Initial guess for uq_ra_adf.

    Returns
    -------
    u_reco : xe.TTTensor
        TT-reconstruction of solution.
    """
    LOG_FILE = log_file_name(file)
    lvl_data = load_level_data(file)
    rho = p.info["expansion"]["rho"]
    vartheta = p.info["expansion"]["vartheta"]
    decay = p.info["expansion"]["decay rate"]
    dist = p.info["sampling"]["distribution"]

    u_reco_list, err_list = [], []
    min_ev_list = []

    N_runs = 2
    logf(LOG_FILE, f"reconstruct u -- run VMC {N_runs} times")
    for _run in range(N_runs):
        logf(LOG_FILE, f"    generate {N_VMC} training samples")
        ys_train, ws_train, us_train, _, _ = generate_samples(
            N_VMC, p.info, scale=rho)
        logf(LOG_FILE, "    run VMC")
        u_reco = reconstruct_orthFE(
            sdim, ys_train, ws_train, us_train, p, scale=rho,
            init=init)
        min_ev_list.append(estimate_sample_validity(
            ys_train, u_reco, dist, scale=rho, decay=decay, vals=us_train))
        abs_err, rel_err = error_L2H1_wrapper(
            u_reco, ys, ws, us, p.space, dist, scale=rho, decay=decay)
        u_reco_list.append(u_reco)
        err_list.append(rel_err)
        logf(LOG_FILE, f"        ranks: {u_reco.ranks()}")
        logf(LOG_FILE, f"        rel L2H1 err: {rel_err}")
        logf(LOG_FILE, f"        min Gramian EV: {min_ev_list[-1]}")

    u_reco = u_reco_list[np.argmin(err_list)]
    min_ev = min_ev_list[np.argmin(err_list)]

    current_level = lvl_data["level"][-1]
    if current_level > 0:
        last_tt_dofs = lvl_data["tt_dofs_u"][-1]
        while tt.dofs(u_reco) < last_tt_dofs:
            _dims, _ranks = u_reco.dimensions, [1]*(u_reco.order()-1)
            noise = xe.TTTensor.random(_dims, _ranks)
            u_reco = u_reco + 1e-12 * noise / xe.frob_norm(noise)

    return u_reco, min_ev


# XXX
def reconstruct_wrapper_old(file, sdim, N_VMC, ys, ws, us, p, init=None,
                            kappa_reco=None, f_reco=None):
    """ Wrapper for VMC reconstruction of solution u.

    Reconstruct the solution from samples by using VMC. Redo VMC reconstruction
    with new samples, if the absolute L2-H^1 error is not smaller then in last
    step. Also increase ranks of reco such that TT-DoFs of solution increase
    monotonous through the aVMC loop.

    Parameters
    ----------
    file : str
        Path to level data file.
    sdim : list of int
        Stochastic dimensions of solution.
    N_VMC : int
        Number of samples for uq_ra_adf.
    ys : array_like
        Parameter samples for testing.
    ws : array_like
        Sample weights for testing.
    us : array_like
        Samples of solution for testing.
    p : src.problem.equation.darcy.Problem
    init : xe.TTTensor or None
        Initial guess for uq_ra_adf.

    Returns
    -------
    u_reco : xe.TTTensor
        TT-reconstruction of solution.
    """
    LOG_FILE = log_file_name(file)
    lvl_data = load_level_data(file)
    current_level = lvl_data["level"][-1]
    rho = p.info["expansion"]["rho"]
    vartheta = p.info["expansion"]["vartheta"]
    decay = p.info["expansion"]["decay rate"]
    dist = p.info["sampling"]["distribution"],

    if current_level > 0:
        last_err = lvl_data["rel_err_energy_u"][-1]
        last_tt_dofs = lvl_data["tt_dofs_u"][-1]
    u_reco_list, err_list, tt_dofs_list = [], [], []

    logf(LOG_FILE, "reconstruct u")
    N_tries, N_runs = 3, 3
    for _ in range(N_tries):
        for _run in range(N_runs):
            logf(LOG_FILE, f"    generate {N_VMC} training samples")
            with timeit("    {:4.2f} s"):
                ys_train, ws_train, us_train, _, _ = generate_samples(
                    N_VMC, p.info, scale=rho)

            if current_level > 0 and N_runs > 1:
                logf(LOG_FILE, f"    run VMC for u ({_run+1}/{N_runs})")
            else:
                logf(LOG_FILE, "    run VMC for u")
            with timeit("    {:4.2f} s"):
                u_reco = reconstruct_orthFE(
                    sdim, ys_train, ws_train, us_train, p, scale=rho,
                    init=init)
                # if current_level == 0 or lvl_data["ref_type"][-1] != 2:
                #     u_reco = reconstruct_orthFE(
                #         sdim, ys_train, ws_train, us_train, p, scale=scale,
                #         init=init)
                # else:  # only on est_disc refinement
                #     u_reco = reconstruct_solution_by_update(
                #         file, sdim, ys_train, ws_train,
                #         us_train, p)
                vals = tt.eval_approx(u_reco, ys_train, dist,
                                      scale=rho, decay=decay)
                print(np.max(vals))
                exit()

            if current_level == 0:  # nothing more to do in first iteration
                return u_reco

            while tt.dofs(u_reco) < last_tt_dofs:
                _dims, _ranks = u_reco.dimensions, [1]*(u_reco.order()-1)
                noise = xe.TTTensor.random(_dims, _ranks)
                u_reco = u_reco + 1e-12 * noise / xe.frob_norm(noise)

            abs_err, rel_err = error_L2H1_wrapper(
                u_reco, ys, ws, us, p.space, dist, scale=vartheta*rho,
                decay=decay)
            # abs_err, rel_err = error_energy_wrapper(
            #     u_reco, kappa_reco, f_reco, p)

            tmp = change_base_to_sHerm_wrapper(u_reco, p.info, s=scale)  # XXX
            abs_tmp, rel_tmp = error_L2H1_wrapper(
                tmp, ys, ws, us, p.space, dist, scale=scale*vartheta,
                decay=decay)
            # print(abs_err, rel_err)
            # print(abs_err-abs_tmp, rel_err-rel_tmp)

            u_reco_list.append(u_reco)
            err_list.append(rel_err)
            tt_dofs_list.append(tt.dofs(u_reco))

        # def get_median_idx(_vals):
        #     """ Get index of median element of values. """
        #     assert len(_vals) > 0
        #     assert np.array(_vals).ndim == 1
        #     _idx_list = np.argsort(_vals)
        #     _n = len(_idx_list)
        #     _idx = _idx_list[_n // 2 + _n % 2 - 1]
        #     return _idx

        idx_ret = np.argmin(err_list)
        tt_dofs = tt_dofs_list[idx_ret]

        # find VMC reco with smallest TT-DoFs that is better then before
        for j, err in enumerate(err_list):
            if err < last_err and tt_dofs_list[j] < tt_dofs:
                idx_ret, tt_dofs = j, tt_dofs_list[j]

        if err_list[idx_ret] <= last_err:  # error is small enough
            return u_reco_list[idx_ret]
        logf(LOG_FILE, "    -- no sufficient VMC reconstruction --")

    # idx_ret = np.argmin(abs_err_list)
    # return u_reco_list[idx_ret]  # return median reco
    logf(LOG_FILE, "    use best available VMC reconstruction")
    return u_reco_list[np.argmin(err_list)]


def expTT_kappa(kappa_tt, p, init=None, LF=None):
    """ Compute exponential of affine diffusion coefficient with expTT.

    Parameters
    ----------
    kappa_tt : xe.TTTensor
        TTTensor of affine diffusion coefficient kappa.
    p : src.problem.equation.darcy.Problem
    init : xe.TTTensor
        Initial guess for ALS algorithm.
    LF : str
        Log file path.

    Returns
    -------
    exp_kappa_tt : xe.TTTensor
        TTTensor of exp(kappa).
    """

    M = p.info["expansion"]["size"]
    decay = p.info["expansion"]["decay rate"]
    mean = p.info["expansion"]["mean"]
    scale = 4
    dims_exp_kappa_tt = [11] * M
    dims_scaled = [5] * M
    x0 = fem.dof_coords(p.space_coeff, p.mesh)
    y0 = np.zeros((1, M))
    exp0 = np.exp(eval_diff_coeff(x0, y0, decay, mean) * 2**(-scale)).flatten()
    EPS_W, EPS_kappa = 1e-10, 1e-06

    # scale affine exponent
    kappa_scaled = xe.TTTensor(kappa_tt) * 2**(-scale)

    # build expTT operator and RHS
    logf(LF, "    assemble operator and RHS")
    W, b = tt_exp.build_system_inexactFEMIC(
        dims_scaled, dims_scaled, kappa_scaled, x0, y0, exp0,
        p.space_coeff, mesh=p.mesh, weight=1)
    logf(LF, "    round operator")
    W.round(EPS_W)
    logf(LF, f"    FEM-core W is_sparse: {W.get_component(0).is_sparse()}")
    logf(LF, f"    W ranks: {W.ranks()}")
    # assert W.get_component(0).is_sparse()  # assume FE component is sparse

    # generate random initial guess or prep previous solution
    if init is None:
        init_dims = [p.space_coeff.dim()] + dims_scaled
        init_ranks = [2*r for r in kappa_scaled.ranks()]
        init = xe.TTTensor.random(init_dims, init_ranks)
        init = init / xe.frob_norm(init)
    else:
        init_dims = [p.space_coeff.dim()] + dims_scaled
        init = tt.project(init, init_dims)

    # run ALS
    logf(LF, "    run ALS")
    USE_XERUS_ALS = True
    if USE_XERUS_ALS is True:
        exp_kappa_tt = xe.TTTensor(init)
        xe.ALS_SPD(W, exp_kappa_tt, b)
    else:
        exp_kappa_tt = tt_exp.run_simpleALS(
            A=W, u_start=init, b=b, verbose=2)
    abs_res, rel_res = tt_exp_energy_error_wrapper(
        exp_kappa_tt, kappa_scaled, exp0)
    logf(LF, f"    energy error: {abs_res:4.2e} / {rel_res:4.2e}")
    exp_kappa_tt.round(EPS_kappa)
    logf(LF, f"    exp_kappa_tt ranks: {exp_kappa_tt.ranks()}")

    # rescale exponential TT
    logf(LF, "    rescale solution")
    for _ in range(scale):
        EPS = EPS_kappa
        while EPS > 1e-12:
            _exp_kappa_tt = femTT.power(exp_kappa_tt, 2, eps=EPS,
                                        project_dims=dims_exp_kappa_tt)
            if np.min(_exp_kappa_tt.ranks()) < 2:
                EPS = EPS/10
            else:
                break
        exp_kappa_tt = xe.TTTensor(_exp_kappa_tt)

        logf(LF, f"    exp_kappa_tt ranks: {exp_kappa_tt.ranks()}")

    return exp_kappa_tt


def get_kappa_approximation(file, p, scale=False):
    """ Wrapper to get TT representation of kappa.

    For affine case use exact TT representation of diffusion coefficient.
    For lognormal case either compute the TT representation by expTT with exact
    TT for affine exponent or simply use approximation of previous step if mesh
    did not change. Use prolongation of previous step as initial guess for
    expTT if possible.

    Parameters
    ----------
    file : str
        Level data file.
    p : src.problem.equation.darcy.Problem
    scale : bool
        Reconstruct w.r.t scaled Hermite basis.

    Returns
    -------
    kappa_tt : xe.TTTensor
        TT representation of diffusion coefficient.
    """

    lvl_data = load_level_data(file)
    M = p.info["expansion"]["size"]
    decay = p.info["expansion"]["decay rate"]
    mean = p.info["expansion"]["mean"]
    dist = p.info["sampling"]["distribution"]
    LOG_FILE = log_file_name(file)

    # test samples for error computation
    # NOTE: Do not use generate_samples() as samples for u take long and are
    #       irrelevant.
    dofs = fem.dof_coords(p.space_coeff, p.mesh)
    n_smpl = 1000
    ys = get_y_samples(p.info, n_smpl)
    if p.info["sampling"]["distribution"] == "normal":
        # rho = p.info["expansion"]["rho"]  # XXX
        # vartheta = p.info["expansion"]["vartheta"]  # XXX
        ys = rescale_samples(ys, s=0, decay=decay)
    ws = np.full(n_smpl, 1/n_smpl)  # uniform weights
    hs = eval_diff_coeff(dofs, ys, decay, mean)
    exphs = np.exp(hs)

    # get exact TTTensor for affine diffusion coefficient
    logf(LOG_FILE, "build exact TT for affine kappa")
    affine_kappa = exact_TT_diff_coeff(
        p.space_coeff, p.mesh, M, decay, mean, dist, dim=2)
    affine_kappa.round(1e-16)

    abs_err, rel_err = error_L2Linf_wrapper(affine_kappa, ys, ws, hs, dist)
    logf(LOG_FILE,
         f"    L2-Linf error (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")

    if dist == "uniform":
        return affine_kappa

    # reconstruct exponential of affine kappa
    current_level = lvl_data["level"][-1]
    if current_level <= 0:  # use expTT reconstruction for first step
        logf(LOG_FILE, "compute exponential of kappa with expTT")
        # logf(LOG_FILE, f"    affine kappa ranks: {affine_kappa.ranks()}")
        exp_kappa = expTT_kappa(affine_kappa, p, LF=LOG_FILE)
        # check approximation error
        abs_err, rel_err = error_L2Linf_wrapper(
            exp_kappa, ys, ws, exphs, dist)
        logf(LOG_FILE,
             f"    L2-Linf error (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")
    else:  # load last reconstruction of kappa
        q = Problem(lvl_data["info"][current_level-1])
        exp_kappa_last = lvl_data["kappa_reco"][current_level-1]
        if q.space_coeff.dim() == p.space_coeff.dim():  # use last kappa
            exp_kappa = xe.TTTensor(exp_kappa_last)
        else:  # prolongate kappa to current mesh
            exp_kappa = tt.get_initial_guess(
                [p.space_coeff.dim()] + exp_kappa_last.dimensions[1:],
                exp_kappa_last, q.space_coeff, p.space_coeff)
        # check approximation error
        abs_err, rel_err = error_L2Linf_wrapper(
            exp_kappa, ys, ws, exphs, dist)
        logf(LOG_FILE,
             f"    L2-Linf error (abs/rel) -- previous: {abs_err:4.2e}/{rel_err:4.2e}")

    # compute better approximation, if last kappa is not good enough
    threshold = 5e-04
    if current_level > 0 and rel_err > threshold:
        logf(LOG_FILE, f"rel_err > {threshold:4.2e} --> run expTT")
        exp_kappa = expTT_kappa(affine_kappa, p, LF=LOG_FILE)

    x0 = fem.dof_coords(p.space_coeff, p.mesh)
    y0 = np.zeros((1, M))
    exp0 = np.exp(eval_diff_coeff(x0, y0, decay, mean)).flatten()
    abs_err, rel_err = tt_exp_energy_error_wrapper(
        exp_kappa, affine_kappa, exp0)
    update_level_data(file, abs_err_energy_kappa=abs_err,
                      rel_err_energy_kappa=rel_err)
    logf(LOG_FILE,
         f"    residual (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")

    return exp_kappa


def error_L2L2_wrapper(reco, ys, ws, vals, space, dist, scale=0, decay=-1):
    """ Wrapper to compute the absolute and relative L2-L2 error.

    Parameters
    ----------
    reco : xe.TTTensor
        TTTensor of the reconstruction
    ys : array_like
        Sample points.
    ws : array_like
        Weights for empirical norm.
    vals : array_like
        Function evaluations in samples.
    space : fenis.FunctionSpace
        FE space.
    dist : string, ("uniform" or "normal")
        Distribution of parameters.
    scale : float, default = 0
        Scaling for scaled Hermite basis the coefficients are given in.
        Defaults to no scalining, i.e. standard Hermite basis.
    decay : float, default = -1
        Decay rate of affine diffusion coefficient. If scale != 0, decay has
        to be greater than zero.

    Returns
    -------
    abs_err : float
        Absolute L2-L2 error.
    rel_err : float
        Relative L2-L2 error weighted by the L2-L2 norm of the samples.
    """
    abs_err = femTT.compute_L2L2_error(
        reco, ys, ws, vals, space, dist, scale=scale, decay=decay)
    rel_err = abs_err / femTT.compute_L2L2_norm(ys, vals, space, ws=ws)
    return abs_err, rel_err


def error_L2Linf_wrapper(reco, ys, ws, vals, dist, scale=0, decay=-1):
    """ Wrapper to compute the absolute and relative L2-Linf error.

    Parameters
    ----------
    reco : xe.TTTensor
        TTTensor of the reconstruction
    ys : array_like
        Sample points.
    ws : array_like
        Weights for empirical norm.
    vals : array_like
        Function evaluations in samples.
    dist : string, ("uniform" or "normal")
        Distribution of parameters.
    scale : float, default = 0
        Scaling for scaled Hermite basis the coefficients are given in.
        Defaults to no scalining, i.e. standard Hermite basis.
    decay : float, default = -1
        Decay rate of affine diffusion coefficient. If scale != 0, decay has
        to be greater than zero.

    Returns
    -------
    abs_err : float
        Absolute L2-Linf error.
    rel_err : float
        Relative L2-Linf error weighted by the L2-Linf norm of the samples.
    """
    abs_err = femTT.compute_L2Linf_error(reco, ys, ws, vals, dist,
                                         scale=scale, decay=decay)
    rel_err = abs_err / femTT.compute_L2Linf_norm(ys, vals, ws=ws)
    return abs_err, rel_err


def error_L2H1_wrapper(reco, ys, ws, vals, space, dist, scale=0, decay=-1):
    """ Wrapper to compute the absolute and relative L2-H1 error.

    Parameters
    ----------
    reco : xe.TTTensor
        TTTensor of the reconstruction
    ys : array_like
        Sample points.
    ws : array_like
        Weights for empirical norm.
    vals : array_like
        Function evaluations in samples.
    space : fenis.FunctionSpace
        FE space.
    dist : string, ("uniform" or "normal")
        Distribution of parameters.
    scale : float, default = 0
        Scaling for scaled Hermite basis the coefficients are given in.
        Defaults to no scalining, i.e. standard Hermite basis.
    decay : float, default = -1
        Decay rate of affine diffusion coefficient. If scale != 0, decay has
        to be greater than zero.

    Returns
    -------
    abs_err : float
        Absolute L2-H1 error.
    rel_err : float
        Relative L2-H1 error weighted by the L2-H1 norm of the samples.
    """
    abs_err = femTT.compute_L2H1_error(
        reco, ys, ws, vals, space, dist, scale=scale, decay=decay)
    rel_err = abs_err / femTT.compute_L2H1_norm(ys, vals, space, ws=ws)
    return abs_err, rel_err


def error_energy_wrapper(u, kappa, f, p):
    """ Compute absolute and relative energy error.

    The energy error is given by || AU-F ||_2. The relative error scales with
    || F ||_2^{-1}.
    """
    A, B = assemble_operator(p, kappa, u, f)
    AW_tt = apply_operator(A, u)
    dims = [core.shape[1] for core in B]
    ranks = [core.shape[0] for core in B[1:]]
    F_tt = xe.TTTensor.random(dims, ranks)
    for j, core in enumerate(B):
        F_tt.set_component(j, xe.Tensor.from_buffer(core))
    AWF_tt = F_tt - AW_tt
    abs_err = AWF_tt.frob_norm()
    rel_err = abs_err / F_tt.frob_norm()
    return abs_err, rel_err


def tt_exp_energy_error_wrapper(exp_kappa_tt, kappa_tt, exp0, dims=None):
    """ Compute ||Bu-f||_2."""

    if dims is None:
        dims = exp_kappa_tt.dimensions[1:]

    f, g = tt_exp.build_f_femIC_cores(kappa_tt)
    normalization = []
    for m in range(1, len(f)):
        fTT = tt.convert([g[0]] + [f[j] if j == m else g[j]
                                   for j in range(1, len(f))])
        normalization.append(xe.frob_norm(fTT)**2)

    # compute shift to homogeneous problem
    val = xe.TTTensor(exp_kappa_tt)
    const = tt.constant(exp0, val.dimensions[1:])

    # shift solution to homogeneous problem
    val = val - const

    abs_res = tt_exp.compute_residual_femIC(val, kappa_tt, exp0)
    rel_res = abs_res / np.sqrt(np.sum(normalization))
    return abs_res, rel_res


def change_base_to_sHerm_wrapper(reco, info, s=0):
    """ Wrapper to simplify change TT coefficients to scaled Hermite basis.

    Parameters
    ----------
    reco : xe.TTTensor
        Coefficients of the reconstruction.
    info : dict
        Problem information.
    s : float, default = 0
        Scaling of the Hermite polynomial basis the coefficients reco are
        given in, i.e. H_i(s * y). Default resembles standard Hermite
        polynomial basis.
    """
    if info["sampling"]["distribution"] == "normal":
        decay = info["expansion"]["decay rate"]
        rho = info["expansion"]["rho"]
        vartheta = info["expansion"]["vartheta"]
        reco_new = femTT.change_basis_to_scaled_hermite(
            reco, decay, rho, vartheta, s=s)
    else:
        reco_new = xe.TTTensor(reco)
    return reco_new


def get_ref_type(file, est_det, est_stoch, est_disc, threshold=0.1):
    """ Get the refinement type

    Choose refinement type depending on current estimator contributions and
    past refinement types, i.e. prevent from getting stuck in sample increasing
    without decreasing the overall estimator.

    Parameters
    ----------
    file : string
        File for storage of level data.
    est_det : float
        Estimator contribution of deterministic part.
    est_stoch : float
        Estimator contribution of stochastic part.
    est_disc : float
        Estimator contribution of algebraic part.
    look_back : int
        Number of previous steps to consider.
    """
    # data = load_level_data(file)
    # est_smpl = data["ref_samples_est"][-1]
    current_ref_type = np.argmax(np.array([est_det, est_stoch, est_disc]))
    return current_ref_type


def increase_stoch_dims(sdim, marked_modes):
    """ Increase stochastic dimensions.

    Parameters
    ----------
    sdim : list of int
        Stochastic dimensions.
    marked_modes : list of int
        Indices of marked modes.

    Returns
    -------
    dims : list of int
        Increased stochastic dimensions.
    """
    dims = deepcopy(sdim)
    for mode in marked_modes:
        if mode < len(sdim):
            dims[mode] += 1
        else:
            dims = dims + [2]
    return dims


def increase_vmc_samples(N_VMC, threshold, lvl_data):
    rho = lvl_data["info"][-1]["expansion"]["rho"]
    vartheta = lvl_data["info"][-1]["expansion"]["vartheta"]
    decay = lvl_data["info"][-1]["expansion"]["decay rate"]
    dist = lvl_data["info"][-1]["sampling"]["distribution"]
    u_reco = lvl_data["u_reco"][-1]

    def _smpl_inc(_n):
        return int(np.min((2*_n, 500 + 1.3*_n)))

    n = [N_VMC]
    ys, ws, us, _, _ = generate_samples(
        n[0], lvl_data["info"][-1], scale=vartheta*rho)

    min_ev = 0.0
    while min_ev < threshold:
        n.append(_smpl_inc(n[-1]))
        log(f"        increase samples: {n[-2]} -> {n[-1]}")
        ys_new, ws_new, us_new, _, _ = generate_samples(
            n[-1]-n[-2], lvl_data["info"][-1], scale=vartheta*rho)
        ys = np.concatenate([ys, ys_new], axis=0)
        ws = np.concatenate([ws, ws_new], axis=0)
        us = np.concatenate([us, us_new], axis=0)
        min_ev = estimate_sample_validity(
            ys, u_reco, dist, scale=vartheta*rho, decay=decay, vals=us)
        if min_ev < threshold:
            log(f"        -> min_ev: {min_ev:4.2e} < {threshold:4.2e}")
        else:
            log(f"        -> min_ev: {min_ev:4.2e} > {threshold:4.2e}")

    return int(n[-1])


def increase_vmc_samples_old(N_VMC):
    """ Increase the number of VMC samples.

    Returns
    -------
    N_VMC : int
        New number of VMC samples.
    """
    # n = min(2*N_VMC_old, N_VMC_old + 1000)
    # n = 2 * N_VMC if N_VMC < 1000 else int(1.3*N_VMC + 700)
    n = np.min((2*N_VMC, 500 + 1.3*N_VMC))
    return int(n)


def force_refinement(lvl_data, look_back=3):
    """ Force stochastic refinement if sample increase causes stagnation.

    Check if forced stochastic refinement is required due to stagnation in
    sample increasing loop. Returns True, if the last look_back refinements
    caused sample increase without reducing the relative difference between
    stochastic and algebraic estimator by more then 5 per cent.

    Parameters
    ----------
    lvl_data : dict
        Level data dictionary.
    look_back : int
        Number of preceding levels that are considered.
    """

    assert look_back > 1

    if len(lvl_data["ref_type"]) < look_back:
        # no refinement force if number of iterations is too small
        return False

    N_VMCs = np.array(lvl_data["n_vmc"])[-look_back:]
    eta_sto = np.array(lvl_data["est_stoch"])[-look_back:]
    eta_alg = np.array(lvl_data["est_disc"])[-look_back:]

    diff = np.log(np.abs(eta_alg-eta_sto))

    increased_samples = np.all(N_VMCs[1:] - N_VMCs[:-1] > 0)
    improved = True if (diff[0]-diff[-1])/diff[0] > 0.05 else False
    if increased_samples and not improved:
        return True
    return False


def solve_step(p, sdim, N_VMC, N_test, dist, file):
    """ SOLVE step of adaptive loop.

    Parameters
    ----------
    p : src.problem.equation.darcy.Problem
    sdim : list of int
        Stochastic dimensions of solution u.
    N_VMC : int
        Number of samples for VMC reconstruction.
    N_test : int
        Number of samples for empirical error computation.
    dist : sting, ("uniform" of "normal")
        Sample distribution.
    file : string
        File for storage of level data.

    Returns
    -------
    u_reco : xe.TTTensor
        Reconstruction of solution u.
    kappa_reco : xe.TTTensor
        Reconstruction of diffusion coefficient kappa.
    f_reco : xe.TTTensor
        Reconstruction of right-hand side f.
    """

    LOG_FILE = log_file_name(file)
    logf(LOG_FILE, ":: SOLVE ::")

    vartheta = p.info["expansion"]["vartheta"]
    rho = p.info["expansion"]["rho"]
    decay = p.info["expansion"]["decay rate"]

    logf(LOG_FILE, f"    generate {N_test} test samples")
    # test samples w.r.t. the \pi_{\vartheta*\rho} measure
    ys, ws, us, kappas, fs = generate_samples(N_test, p.info,
                                              scale=vartheta*rho)

    logf(LOG_FILE, "build exact TT for RHS f")
    f_reco = tt.constant(np.ones(p.mesh.num_cells()), sdim)
    f_reco = change_base_to_sHerm_wrapper(f_reco, p.info, s=0)
    logf(LOG_FILE, f"    TT-Dofs f: {tt.dofs(f_reco)}")
    logf(LOG_FILE, f"    nnz-Dofs f: {tt.dofs_nnz(f_reco, EPS=1e-08)}")
    update_level_data(file, f_reco=f_reco, tt_dofs_f=tt.dofs(f_reco),
                      full_dofs_f=np.product(f_reco.dimensions),
                      nnz_dofs_f=tt.dofs_nnz(f_reco, EPS=1e-08))
    logf(LOG_FILE, "compute L2-L2 error for f")
    with timeit("    {:4.2f} s"):
        space = FunctionSpace(p.mesh, "DG", 0)  # P_0(Tcal)
        abs_err, rel_err = error_L2L2_wrapper(
            f_reco, ys, ws, fs, space, dist,
            scale=vartheta*rho, decay=decay)
        update_level_data(file, abs_err_L2L2_f=abs_err, rel_err_L2L2_f=rel_err)
        logf(LOG_FILE,
             f"    L2-L2 error (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")

    kappa_reco = get_kappa_approximation(file, p)
    # NOTE: Always save coeff. of kappa w.r.t. standard Hermite basis, as this
    #       is required for expTT.
    update_level_data(file, kappa_reco=kappa_reco)
    kappa_reco = change_base_to_sHerm_wrapper(kappa_reco, p.info, s=0)
    logf(LOG_FILE, f"    TT-Dofs kappa: {tt.dofs(kappa_reco)}")
    logf(LOG_FILE, f"    nnz-Dofs kappa: {tt.dofs_nnz(kappa_reco, EPS=1e-08)}")
    logf(LOG_FILE, f"    ranks kappa: {kappa_reco.ranks()}")
    logf(LOG_FILE, "compute L2-Linf error for kappa")
    with timeit("    {:4.2f} s"):
        abs_err, rel_err = error_L2Linf_wrapper(
            kappa_reco, ys, ws, kappas, dist, scale=vartheta*rho, decay=decay)
        abs_err_L2H1, rel_err_L2H1 = error_L2H1_wrapper(
            kappa_reco, ys, ws, kappas, p.space, dist,
            scale=vartheta*rho, decay=decay)
        update_level_data(file, abs_err_L2Linf_kappa=abs_err,
                          rel_err_L2Linf_kappa=rel_err,
                          abs_err_L2H1_kappa=abs_err_L2H1,
                          rel_err_L2H1_kappa=rel_err_L2H1,
                          tt_dofs_kappa=tt.dofs(kappa_reco),
                          full_dofs_kappa=np.product(kappa_reco.dimensions),
                          nnz_dofs_kappa=tt.dofs_nnz(kappa_reco, EPS=1e-08))
        logf(LOG_FILE,
             f"    L2-Linf error (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")
        logf(LOG_FILE,
             f"    L2-H1 error (abs/rel): {abs_err_L2H1:4.2e}/{rel_err_L2H1:4.2e}")

    logf(LOG_FILE, "generate initial guess for u")
    with timeit("    {:4.2f} s"):
        # u_init = initial_guess_solution(file, p, sdim, EPS=None)
        u_init = None
    u_reco, min_ev = reconstruct_wrapper(file, sdim, N_VMC, ys, ws, us, p,
                                         init=u_init)
    u_reco = change_base_to_sHerm_wrapper(u_reco, p.info, s=rho)
    logf(LOG_FILE, f"    TT-Dofs u: {tt.dofs(u_reco)}")
    logf(LOG_FILE, f"    nnz-Dofs u: {tt.dofs_nnz(u_reco, EPS=1e-08)}")
    logf(LOG_FILE, f"    ranks u: {u_reco.ranks()}")
    update_level_data(file, u_reco=u_reco, tt_dofs_u=tt.dofs(u_reco),
                      full_dofs_u=np.product(u_reco.dimensions),
                      nnz_dofs_u=tt.dofs_nnz(u_reco, EPS=1e-08),
                      ref_samples_est=min_ev)
    logf(LOG_FILE, "compute L2-L2 and L2-H1 error for u")
    with timeit("    {:4.2f} s"):
        abs_err, rel_err = error_L2L2_wrapper(
            u_reco, ys, ws, us, p.space, dist, scale=vartheta*rho, decay=decay)
        update_level_data(file, abs_err_L2L2_u=abs_err, rel_err_L2L2_u=rel_err)
        logf(LOG_FILE,
             f"    L2-L2 error (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")
        abs_err, rel_err = error_L2H1_wrapper(
            u_reco, ys, ws, us, p.space, dist, scale=vartheta*rho, decay=decay)
        update_level_data(file, abs_err_L2H1_u=abs_err, rel_err_L2H1_u=rel_err)
        logf(LOG_FILE,
             f"    L2-H1 error (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")
        abs_err, rel_err = error_energy_wrapper(u_reco, kappa_reco, f_reco, p)
        update_level_data(file, abs_err_energy_u=abs_err,
                          rel_err_energy_u=rel_err)
        logf(LOG_FILE,
             f"    energy error (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")

        compute_ref_error_iter(file, N_test=N_test)

    return u_reco, kappa_reco, f_reco


def estimate_step(u_reco, kappa_reco, f_reco, p, cStoch, cDisc, file,
                  look_ahead=1):
    """ ESTIMATE step of adaptive loop.

    Parameters
    ----------
    u_reco : xe.TTTensor
        Reconstruction of the solution u.
    kappa_reco : xe.TTTensor
        Reconstruction of the diffusion coefficient kappa.
    f_reco : xe.TTTenso
        Reconstruction of the right-hand side f.
    p : src.problem.equation.darcy.Problem.
    cStoch : float
        Weight for stochastic estimator.
    cDisc : float
        Weight for algebraic estimator.
    file : string
        File for storage of level data.
    look_ahead : int > 0, default = 1
        Look ahead for stochastic estimator (width of margin).

    Returns
    -------
    est_det_loc : array_like
        Local estimator contributions on each mesh triangle.
    est_stoch_loc : array_like
        Local estimator contributions for each mode.
    ref_type : int
        Refinement type.
    """

    LOG_FILE = log_file_name(file)

    logf(LOG_FILE, ":: ESTIMATE ::")

    logf(LOG_FILE, "compute estimator terms")
    with timeit("    {:4.2f} s"):
        estDet_locSq, estStoch_locSq, estDisc_sq = EST.compute_estimator_terms(
            f_reco, kappa_reco, u_reco, p, t=look_ahead)

        # local estimator terms
        est_det_loc = np.sqrt(np.abs(estDet_locSq))
        est_stoch_loc = cStoch * np.sqrt(np.abs(estStoch_locSq))
        update_level_data(file, est_det_loc=est_det_loc,
                          est_stoch_loc=est_stoch_loc)

        # global estimator terms
        est_det = np.sqrt(np.sum(np.abs(estDet_locSq)))
        # TODO why sum(sqrt(abs)) and not sqrt(sum(abs))?
        est_stoch = cStoch * np.sum(np.sqrt(np.abs(estStoch_locSq)))
        est_disc = cDisc * np.sqrt(np.abs(estDisc_sq))

        est_total = np.sqrt((est_det + est_stoch + est_disc)**2 + est_disc**2)
        est_total_noDisc = np.sqrt((est_det + est_stoch)**2)

        ref_type = get_ref_type(file, est_det, est_stoch, est_disc)

    update_level_data(file, est_det=est_det, est_stoch=est_stoch,
                      est_disc=est_disc, est_total=est_total,
                      est_total_noDisc=est_total_noDisc, ref_type=ref_type)

    logf(LOG_FILE, f"    est det: {est_det:4.2e}")
    logf(LOG_FILE, f"    est sto: {est_stoch:4.2e}")
    logf(LOG_FILE, f"    est alg: {est_disc:4.2e}")
    logf(LOG_FILE, f"    est:     {est_total:4.2e}")

    return est_det_loc, est_stoch_loc, ref_type


def mark_and_refine_step(est_det_loc, est_stoch_loc, ref_type, p, sdim, N_VMC,
                         file, look_back=3):
    """ MARK and REFINE step of adaptive loop.

    Parameters
    ----------
    est_det_loc : array_like
        Local estimator contributions on each mesh triangle.
    est_stoch_loc : array_like
        Local estimator contributions for each mode.
    ref_type : int
        Type of refinement.
    p : src.problem.equation.darcy.Problem
    sdim : list of int
        Stochastic dimensions.
    N_VMC : int
        Number of VMC training samples.
    file : string
        File for storage of level data.

    Returns
    -------
    p : src.problem.equation.darcy.Problem
    sdim : list of int
        Stochastic dimensions.
    N_VMC : int
        Number of VMC training samples.
    """

    LOG_FILE = log_file_name(file)
    lvl_data = load_level_data(file)
    est_smpl = lvl_data["ref_samples_est"][-1]
    smpl_threshold = 1e-03

    logf(LOG_FILE, ":: MARK and REFINE ::")
    logf(LOG_FILE, f"    min. Gramian eigenvalue: {est_smpl:4.2g}")
    if ref_type == 0:  # refine FE mesh
        logf(LOG_FILE, "    est_det dominates")
        marked_cells = fem.marking(
            est_det_loc, lvl_data["doerfler_threshold"][-1])
        p = Problem(fem.refine_mesh(marked_cells, lvl_data["info"][-1]))
        logf(LOG_FILE, f"    refine {len(marked_cells)} cells")
    elif ref_type == 1:  # refine sdim
        logf(LOG_FILE, "    est_sto dominates")
        marked_modes = fem.marking(est_stoch_loc, 0.5)
        sdim = increase_stoch_dims(sdim, marked_modes)
        logf(LOG_FILE, f"    refine {len(marked_modes)} modes: {marked_modes}")
    elif ref_type == 2:  # increase number of VMC samples
        logf(LOG_FILE, "    est_alg dominates")
        # if force_refinement(lvl_data, look_back):
        #     marked_modes = fem.marking(est_stoch_loc, 0.5)
        #     sdim = increase_stoch_dims(sdim, marked_modes)
        #     N_VMC_old = int(N_VMC)
        #     N_VMC = lvl_data["n_vmc"][-look_back]
        #     logf(LOG_FILE,
        #          f"    refine {len(marked_modes)} modes: {marked_modes}")
        #     logf(LOG_FILE,
        #          f"    decrease number of VMC samples: {N_VMC_old} -> {N_VMC}")
        if est_smpl > smpl_threshold:
            marked_modes = fem.marking(est_stoch_loc, 0.5)
            sdim = increase_stoch_dims(sdim, marked_modes)
            logf(LOG_FILE,
                 f"    refine {len(marked_modes)} modes: {marked_modes}")
        else:
            logf(LOG_FILE, "    compute required number of samples")
            N_VMC_old = int(N_VMC)
            N_VMC = increase_vmc_samples(N_VMC_old, smpl_threshold, lvl_data)
            logf(LOG_FILE,
                 f"    increase number of VMC samples: {N_VMC_old} -> {N_VMC}")
    else:
        raise ValueError(f"Unknown ref type: {ref_type}.")
    return p, sdim, N_VMC


def check_break_condition(file):
    """ Check break conditions. """
    data = load_level_data(file)

    # break if maximum number of TT-Dofs is reached
    if tt.dofs(data["u_reco"][-1]) > data["max_tt_dofs"][-1]:
        return True, "Max. TT-Dofs reached"

    # break if maximum number of iterations is reached
    if data["level"][-1] >= data["max_iter"][-1] - 1:
        return True, "Max. iterations reached"

    # break if more than 100_000 samples would be needed
    N_VMC_MAX = int(1e+05)
    N_VMC = int(data["n_vmc"][-1])
    if N_VMC > N_VMC_MAX and data["ref_type"][-1] == 2:
        return True, f"Too many samples required ({N_VMC} > {N_VMC_MAX})"
    # N_VMC_NEXT = increase_vmc_samples(N_VMC)
    # if N_VMC_NEXT >= N_VMC_MAX and data["ref_type"][-1] == 2:
    #     return True, f"Too many samples required ({N_VMC_NEXT} > {N_VMC_MAX})"

    # break if estimated error is small enough
    # EPS = 1e-3
    # if data["exact_err_L2H1_abs_iter"][-1] <= EPS:
    #     return True, f"Estimated error below {EPS:e}"

    return False, ""


def compute_exact_error(file, N_test=1000):
    """ Compute the approximation error on a finer mesh.

    Compute the 'exact' L2-L2 and L2-H1 errors of all iterations w.r.t. one
    uniform refinement of the finest available mesh.

    Parameters
    ----------
    file : string
        Path to level data file.
    N_test : int
        Number of samples for empirical L2-norm.
    """
    LOG_FILE = log_file_name(file)
    logf(LOG_FILE, ":: Compute EXACT ERROR ::")
    lvl_data = load_level_data(file)
    dist = lvl_data["info"][-1]["sampling"]["distribution"]
    decay = lvl_data["info"][-1]["expansion"]["decay rate"]
    vartheta = lvl_data["info"][-1]["expansion"]["vartheta"]
    rho = lvl_data["info"][-1]["expansion"]["rho"]

    # uniform refine last mesh
    info = deepcopy(lvl_data["info"][-1])
    info = fem.refine_mesh(range(info["fe"]["mesh"].num_cells()), info)
    p = Problem(info)

    # draw reference samples
    logf(LOG_FILE, f"generate {N_test} test samples")
    with timeit("    {:4.2f} s"):
        ys, ws, us = fem.sample_solution(p.info, N_test)

    logf(LOG_FILE, "compute exact error")
    for lvl in lvl_data["level"]:
        q = Problem(lvl_data["info"][lvl])
        u_reco = xe.TTTensor(lvl_data["u_reco"][lvl])
        u_reco = femTT.prolongate(u_reco, q.space, p.space)
        abs_err_L2L2 = femTT.compute_L2L2_error(
            u_reco, ys, ws, us, p.space, dist, scale=vartheta*rho, decay=decay)
        rel_err_L2L2 = abs_err_L2L2 / \
            femTT.compute_L2L2_norm(ys, us, p.space, ws=ws)
        abs_err_L2H1 = femTT.compute_L2H1_error(
            u_reco, ys, ws, us, p.space, dist, scale=vartheta*rho, decay=decay)
        rel_err_L2H1 = abs_err_L2H1 / \
            femTT.compute_L2H1_norm(ys, us, p.space, ws=ws)

        update_level_data(file,
                          exact_err_L2L2_abs=abs_err_L2L2,
                          exact_err_L2L2_rel=rel_err_L2L2,
                          exact_err_L2H1_abs=abs_err_L2H1,
                          exact_err_L2H1_rel=rel_err_L2H1,
                          )
        logf(LOG_FILE, f"    {100*(lvl+1)/len(lvl_data['level']):3.0f} %")


def compute_ref_error_iter(file, N_test=1000):
    """ Compute the approximation error on a one level refined mesh .

    Compute the 'exact' L2-H1 errors of current iteration w.r.t. one
    uniform refinement of the mesh.

    Parameters
    ----------
    file : string
        Path to level data file.
    N_test : int
        Number of samples for empirical L2-norm.
    """
    LOG_FILE = log_file_name(file)
    logf(LOG_FILE, "compute exact L2H1 error approximation")
    lvl_data = load_level_data(file)
    dist = lvl_data["info"][-1]["sampling"]["distribution"]
    decay = lvl_data["info"][-1]["expansion"]["decay rate"]
    vartheta = lvl_data["info"][-1]["expansion"]["vartheta"]
    rho = lvl_data["info"][-1]["expansion"]["rho"]

    # uniform refine last mesh
    info = deepcopy(lvl_data["info"][-1])
    info = fem.refine_mesh(range(info["fe"]["mesh"].num_cells()), info)
    p = Problem(info)

    # draw reference samples
    logf(LOG_FILE, f"    generate {N_test} test samples")
    with timeit("    {:4.2f} s"):
        # use test samples w.r.t. \pi_{\vartheta\rho} measure in lognormal case
        scale = p.info["expansion"]["vartheta"] * p.info["expansion"]["rho"]
        ys, ws, us, _, _ = generate_samples(N_test, p.info, scale=scale)

    logf(LOG_FILE, "    compute prolonged error")
    q = Problem(lvl_data["info"][-1])
    u_reco = xe.TTTensor(lvl_data["u_reco"][-1])
    u_reco = femTT.prolongate(u_reco, q.space, p.space)
    abs_err_L2H1 = femTT.compute_L2H1_error(
        u_reco, ys, ws, us, p.space, dist, scale=vartheta*rho, decay=decay)
    update_level_data(file, exact_err_L2H1_abs_iter=abs_err_L2H1)


def main():
    """ avmc_utils main(). Nothing is happening here. """
    print("This is just a utility library for the aVMC scripts. "
          + "Please run another script.")


if __name__ == '__main__':
    main()
