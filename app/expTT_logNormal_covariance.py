import sys
import os
import numpy as np
from scipy.sparse.linalg import eigsh
import matplotlib.pyplot as plt

import xerus as xe
from fenics import FunctionSpace

from context import src
from src.misc import timeit, logf, log, Logger
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as expTT

from avmc_utils import info_dict
from src.problem.equation.darcy import Problem


class KernelExponent(object):
    """
    Define lognormal field given via the Gaussian covariance kernel of its
    Gaussian exponent.
    """

    def __init__(self, L, nT, mean, length, scale, fe_deg, geometry):
        """ Initiate affine field.

        Compute KL expansion of Gaussian field with given Gaussian covariance
        kernel on some geometry.

        Parameters
        ----------
        L : int
            Number of expansion terms.
        nT : int
            Number of initial refinements of FE mesh.
        mean : float or array_like
            Mean of Gaussian exponential field gamma. If this is constant,
            assume constant mean function.
        length : float
            Correlation length of Gaussian covariance.
        scale : float
            Scaling of Gaussian covariance.
        fe_deg : int
            Polynomial degree of FE space.
        geometry : ('UnitSquare', 'Lshape')
            Computational domain.
        """

        assert isinstance(L, int) and isinstance(nT, int)
        assert isinstance(mean, float) or isinstance(mean, np.ndarray)
        assert length > 0 and scale > 0
        assert isinstance(fe_deg, int) and fe_deg > 0
        assert geometry in ["UnitSquare", "Lshape"]

        self.L = L
        self.nT = nT
        self.deg = fe_deg
        self.mesh = fem.generate_mesh(geometry, initial_refinement=nT)
        self.space = FunctionSpace(self.mesh, 'CG', fe_deg)
        self.dof_coords = fem.dof_coords(self.space, self.mesh)
        self.cov_length = length
        self.cov_scale = scale
        self.geometry = geometry
        self.ev, self.ef = self._compute_KLE_EVP()
        self.coeffs = np.sqrt(self.ev).reshape(-1, 1) * self.ef

        self._mean = mean
        if isinstance(mean, float):
            self.mean = mean * np.ones(self.space.dim())
        elif isinstance(mean, np.ndarray):
            assert mean.size == self.space.dim()
            self.mean = mean

        self.gammaTT = self._assemble_gamma_TT()

    def covariance_kernel(self, z):
        """ Gaussian covariance kernel with isotropic length l.

        With z = x-y, the Gaussian kernel is given by
            cov(x,y) = scale * exp( - 1/l**2 || x-y ||^2 ).

        Parameters
        ----------
        z : array_like
            Deterministic coordinates to evaluate kernel in.
        """
        exponent = - 1 / self.cov_length**2 * np.linalg.norm(z, axis=1)**2
        return self.cov_scale * np.exp(exponent)

    def _compute_KLE_EVP(self):
        """ Compute the KLE eigenfunctions and eigenvalues of a Gaussian kernel.

        The eigenfunctions are orthogonal and normalized w.r.t. the FE inner
        L^2 product.

        Returns
        -------
        eigenvalues : list
            List of eigenvalues.
        eigenfunctions : list
            List of eigenfunctions evaluated at FE dof coordinates.
        """

        # set covariance kernel
        def _cov(z):
            return self.covariance_kernel(z)

        mass_mat = fem.mass_matrix(self.space).toarray()
        dofs_coords = fem.dof_coords(self.space, self.mesh)

        # vectorized evaluation of cov(x, y)
        c0 = np.repeat(dofs_coords, self.space.dim(), axis=0)  # reshaping
        c1 = np.tile(dofs_coords, [self.space.dim(), 1])  # reshaping
        C = _cov(c0-c1).reshape(self.space.dim(), self.space.dim())

        A = np.dot(mass_mat, np.dot(C, mass_mat))
        # compute the first k eigenpairs
        k = min(max(200, 2*self.L), A.shape[0]-1)
        eigenvalues, eigenfunctions = eigsh(A, k, mass_mat)

        # always choose eigenfunction with positive first component
        # Note: This choice is arbitrary but makes computations consistent.
        idx = np.where(eigenfunctions[0] < 0)[0]
        eigenfunctions[:, idx] *= -1

        # only take most influential eigenfunctions
        idx = np.argsort(eigenvalues)[::-1]  # sort from largest to smallest
        eigenvalues = eigenvalues[idx[:self.L]]
        eigenfunctions = eigenfunctions[:, idx[:self.L]]

        return eigenvalues, eigenfunctions.T

    def eval_gamma(self, ys):
        """ Evaluate KL expansion of Gaussian field.

        The affine field is given by
            gamma(x,y) = sum_{i=1}^{L} sqrt(lambda_i) phi_i(x) y_i,
        where (lambda_i, phi_i) are the eigenpairs of the KL expansion of a
        Gaussian covariance field.

        Parameters
        ----------
        ys : array_like
            Random samples.

        Returns
        -------
        ret : array_like
            Functionvalues gamma(ys) in FE dofs.
        """
        assert ys.ndim == 2 and ys.shape[1] == self.coeffs.shape[0]
        return self.mean.reshape(1, -1) + np.dot(ys, self.coeffs)

    def eval_kappa(self, ys):
        """ Evaluate exponential of KL expansion of Gaussian field.

        The field is given by kappa(x,y) = exp gamma(x,y) for
            gamma(x,y) = sum_{i=1}^{L} sqrt(lambda_i) phi_i(x) y_i,
        where (lambda_i, phi_i) are the eigenpairs of the KL expansion of a
        Gaussian covariance field.

        Parameters
        ----------
        ys : array_like
            Random samples.

        Returns
        -------
        ret : array_like
            Functionvalues kappa(x,y) = exp gamma(ys) in FE dofs.
        """
        return np.exp(self.eval_gamma(ys))

    def _assemble_gamma_TT(self):
        """ Assemble exact TT representation of gamma.

        The TT representation is w.r.t. standard Hermite polynomials.
        """

        cores = []
        sdim = 2

        # assemble spatial core
        core = np.zeros((1, self.space.dim(), self.L+1))
        core[0, :, 0] = self.mean
        core[0, :, 1:] = self.coeffs.T
        cores.append(core)

        # assemble intermediate cores
        for l in range(1, self.L):
            core = np.zeros((self.L+1, sdim, self.L+1))
            for j in range(self.L+1):
                if j == l:
                    core[j, :, j] = np.eye(1, sdim, k=1)[0]
                else:
                    core[j, :, j] = np.eye(1, sdim, k=0)[0]
            cores.append(core)

        # assemble last core
        core = np.zeros((self.L+1, sdim, 1))
        for j in range(self.L+1):
            if j < self.L:
                core[j, :, 0] = np.eye(1, sdim, k=0)[0]
            else:
                core[j, :, 0] = np.eye(1, sdim, k=1)[0]
        cores.append(core)

        return tt.convert(cores)


def compute_exponentialTT(field, h_reco, dims, y0, s):
    """TODO: Docstring for expTT.

    Parameters
    ----------
    arg1 : TODO

    Returns
    -------
    TODO

    """

    tab = "    "
    # scale affine exponent h
    h_reco_scaled = xe.TTTensor(h_reco) * 2**(-s)

    # build operator W0 and rhs b
    dims_scaled = [5] * len(dims)
    exph0_scaled = field.eval_kappa(y0 * 2**(-s)).reshape(1, -1, 1)
    x0 = fem.dof_coords(field.space, field.mesh)
    log(tab+"build operators W, b ...")
    with timeit(tab+"{:4.2f} s"):
        W, b = expTT.build_system_inexactFEMIC(
            dims_scaled, dims_scaled, h_reco_scaled, x0, y0*2**(-s),
            exph0_scaled, field.space, mesh=field.mesh, weight=1)

    log(tab+"round operator W ...")
    with timeit(tab+"{:4.2f} s"):
        W.round(1e-14)
    log(tab+f"    W0 ranks: {W.ranks()}")
    log(tab+f"    FEM-core is_sparse: {W.get_component(0).is_sparse()}")
    assert W.get_component(0).is_sparse()

    # generate random initial guess or prep previous solution
    init_dims = [field.space.dim()] + dims_scaled
    init_ranks = [2*r for r in h_reco_scaled.ranks()]
    init = xe.TTTensor.random(init_dims, init_ranks)
    init = init / xe.frob_norm(init)

    # run ALS
    log(tab+"run ALS")
    USE_XERUS_ALS = True
    if USE_XERUS_ALS is True:
        u_reco = xe.TTTensor(init)
        xe.ALS_SPD(W, u_reco, b)
    else:
        u_reco = expTT.run_simpleALS(
            A=W, u_start=init, b=b, verbose=2)
    abs_res, rel_res = wrapper_energy_error(
        u_reco, h_reco_scaled, exph0_scaled)
    log(tab+f"    residuum: {abs_res:4.2e} / {rel_res:4.2e}")
    u_reco.round(1e-10)

    return u_reco


def rescale_TT(TT, scale, EPS=1e-06):
    """TODO: Docstring for rescale_TT.

    Parameters
    ----------
    TT : TODO
    scale : TODO

    Returns
    -------
    TODO

    """
    ret = xe.TTTensor(TT)
    tab = "    "
    for j in range(scale):
        EPS_tmp = EPS
        while EPS_tmp > 1e-12:
            _tmpTT = femTT.power(ret, 2, eps=EPS_tmp, project_dims=dims)
            if np.min(_tmpTT.ranks()) < 2:
                EPS_tmp /= 10
                log(tab+"    rounded to much -> decrease EPS")
            else:
                break
        ret = xe.TTTensor(_tmpTT)

        log(tab+f"    {j+1}/{scale}: ranks -> {ret.ranks()}")
    return ret


def wrapper_compute_absRel_error(ys, us, reco, space, norm="L2"):
    """ Compute absolute and relative error.

    compute the L2-L2 or L2-Linf error of a tt approximation and the exact h
    (or exp h) over the domain.
    """
    ws = np.ones(ys.shape[0])/ys.shape[0]
    if norm == "L2":
        abs_err = femTT.compute_L2L2_error(reco, ys, ws, us, space, "normal")
        rel_err = abs_err / femTT.compute_L2L2_norm(ys, us, space, ws=ws)
    elif norm == "Linf":
        abs_err = femTT.compute_L2Linf_error(reco, ys, ws, us, "normal")
        rel_err = abs_err / femTT.compute_L2Linf_norm(ys, us, ws=ws)

    return abs_err, rel_err


def wrapper_energy_error(exphTT, hTT, exph0):
    """ Compute ||Bu-f||_2."""

    dims = exphTT.dimensions[1:]
    # Compute B, f for homogeneous problem
    # B, f = expTT.build_operator_Bf_femIC(dims, dims, hTT, exph0)
    f, g = expTT.build_f_femIC_cores(hTT)
    normalization = []
    for m in range(1, len(f)):
        fTT = tt.convert([g[0]] + [f[j] if j == m else g[j]
                                   for j in range(1, len(f))])
        normalization.append(xe.frob_norm(fTT)**2)

    # compute shift to homogeneous problem
    val = xe.TTTensor(exphTT)
    const = tt.constant(exph0, dims)

    # shift solution to homogeneous problem
    val = val - const

    abs_res = expTT.compute_residual_femIC(val, hTT, exph0)
    rel_res = abs_res / np.sqrt(np.sum(normalization))

    return abs_res, rel_res


def test_KLE_approximation_error(field, M, nT=None):
    """ Compare the KLE computed in field with a finer version (larger M).

    Parameters
    ----------
    field : KernelExponent
        Approximative field.
    M : int
        Number of KLE expansion terms to compare against.
    nT : int
        Number of initial triangles in FE mesh.

    Returns
    -------
    ret : dict
        Dictionary containing absolute and relative :math:`L^2-L^2`-errors
        for the approximation of :math:`\\gamma` and :math:`\\kappa`.
    """
    _nT = field.nT if nT is None else nT
    assert _nT >= field.nT
    truth = KernelExponent(M, _nT, field._mean, field.cov_length,
                           field.cov_scale, field.deg, field.geometry)

    ys = np.random.normal(0, 1, (1000, M))
    gammas = field.eval_gamma(ys[:, :field.L])
    if _nT != field.nT:
        gammas = fem.prolongate_x_2(gammas, field.space, truth.space)
    true_gammas = truth.eval_gamma(ys)
    kappas = fem.prolongate_x_2(field.eval_kappa(ys[:, :field.L]),
                                field.space, truth.space)
    true_kappas = truth.eval_kappa(ys)

    ret = {}
    abs_err = femTT.compute_L2L2_norm(ys, true_gammas-gammas, truth.space)
    rel_err = abs_err / femTT.compute_L2L2_norm(ys, true_gammas, truth.space)
    ret["gamma"] = [abs_err, rel_err]
    abs_err = femTT.compute_L2L2_norm(ys, true_kappas-kappas, truth.space)
    rel_err = abs_err / femTT.compute_L2L2_norm(ys, true_kappas, truth.space)
    ret["kappa"] = [abs_err, rel_err]
    return ret


if __name__ == "__main__":
    sys.stdout = Logger(
        fileName="app_expTT_logNormal_covariance.log",
        filePath="../log/",
        overwrite=True)

    # working: NT=50, M=10, IT=5
    # Ls = [10**(j/2) for j in reversed(range(-1, 3))]  # rescaling integer 2**j
    # rescaling integer 2**j
    Ls = list(np.sqrt([10, 5, 3, 1, 0.8, 0.5, 0.3, 0.1]))
    NTs = [20]*len(Ls)  # number of triangles
    Ms = [20]*len(Ls)  # number of Parameters

    assert len(NTs) == len(Ls) == len(Ms)
    for j, (nT, M, length) in enumerate(zip(NTs, Ms, Ls)):
        log("__"*40)
        log("")
        log(f"RUN: {j+1}/{len(Ms)}    --    nT={nT}, ",
            f"length**2={length**2:<4.2g}, M={M}")

        # stoch. stuff
        dims = [11]*M
        ITER = 5
        log(f"dims:    [{max(dims)}]*{M}")

        # exponent info
        log(f"ITER:    2**(-{ITER})")

        # define exponent
        with timeit("setup lognormal field ({:4.2f} s)"):
            mean = 0.0
            # length = 0.01  # 0.3 is from paper
            sigma = 1 / 100
            fe_deg = 1
            domain = "Lshape"
            field = KernelExponent(M, nT, mean, length, sigma, fe_deg, domain)

        with timeit("check lognormal field ({:4.2f} s)"):
            err = test_KLE_approximation_error(field, 100, nT=nT)
        log("    KLE error-L2L2 gamma (abs/rel): ",
            f"{err['gamma'][0]:4.2e} / {err['gamma'][1]:4.2e}")
        log("    KLE error-L2L2 kappa (abs/rel): ",
            f"{err['kappa'][0]:4.2e} / {err['kappa'][1]:4.2e}")

        APPENDIX = f"CG-1{fe_deg}_nT-{nT}_length-{length}_M{M}"
        log(f"FE-dofs: {field.space.dim()}")
        log(f"triangles: {field.mesh.num_cells()}")

        # IC
        # NOTE: evaluate in FE dof coordinates x0
        y0 = np.zeros((1, M))
        kappa0 = field.eval_kappa(y0)

        # plot exponent gamma
        # coeff = field.eval_gamma(np.random.normal(0, 1, (1, M)))
        ys = np.array(
            [[-0.96279913,  1.56836919, -1.40443109,  0.32779539, -1.37815032,
              -0.29188703, -0.06579516, -0.50855701, -2.09816175, -0.48272795,
              -0.89849732, -1.23972382, -0.95969485, -1.60079732, -1.20980928,
              -0.71271558, -1.67689848,  1.00366755, -0.9504321, -2.37058337]])
        coeff = field.eval_gamma(ys)
        fig, ax = plt.subplots(nrows=1, ncols=1)
        # fem.plot_fenics_function(ax, coeff[0], p.field.space, p.field.mesh)
        fem.plot_fenics_function(ax, coeff[0], field.space)
        os.makedirs("../img", exist_ok=True)
        plt.savefig("../img/expTT_logNormal_covariance_gamma_"
                    + f"{j+1}"
                    + ".png", dpi=600)
        continue

        # generate test samples
        ys = np.random.normal(0, 1, (1000, M))
        ws = np.ones(ys.shape[0]) / ys.shape[0]
        gammas = field.eval_gamma(ys)
        kappas = field.eval_kappa(ys)
        # gammasTT = tt.eval_approx(field.gammaTT, ys, dist="normal")

        ys_test = np.random.normal(0, 1, (1000, M)) * 2**(-ITER)
        ws_test = np.ones(ys_test.shape[0]) / ys_test.shape[0]
        kappas_test = field.eval_kappa(ys_test)

        # _____________________________________________________________________
        # get TT representation of affine exponent h
        log("assemble TT for exponent h")
        h_reco = field.gammaTT
        h_reco.round(1e-16)
        log(f"    dims:  {h_reco.dimensions}")
        log(f"    ranks: {h_reco.ranks()}")
        abs_err, rel_err = wrapper_compute_absRel_error(
            ys, gammas, h_reco, field.space)
        log(f"    error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
        abs_err, rel_err = wrapper_compute_absRel_error(
            ys, gammas, h_reco, field.space, norm="Linf")
        log(f"    error-L2Linf (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")

        # _____________________________________________________________________
        # get TT representation of u = exp h with expTT
        log("run expTT to get TT for exp h")
        u_reco_expTT = compute_exponentialTT(field, h_reco, dims, y0, ITER)
        log(f"    dims:  {u_reco_expTT.dimensions}")
        log(f"    ranks: {u_reco_expTT.ranks()}")

        abs_err, rel_err = wrapper_compute_absRel_error(
            ys_test, kappas_test, u_reco_expTT, field.space)
        log(f"    error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
        abs_err, rel_err = wrapper_compute_absRel_error(
            ys_test, kappas_test, u_reco_expTT, field.space, norm="Linf")
        log(f"    error-L2Linf (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")

        # rescale exponential solution
        log("rescale solution")
        u_reco_expTT = rescale_TT(u_reco_expTT, ITER, EPS=1e-06)
        log(f"    dims:  {u_reco_expTT.dimensions}")
        log(f"    ranks: {u_reco_expTT.ranks()}")

        abs_res, rel_res = wrapper_energy_error(u_reco_expTT, h_reco, kappa0)
        log(f"    residuum: {abs_res:4.2e} / {rel_res:4.2e}")
        abs_err, rel_err = wrapper_compute_absRel_error(
            ys, kappas, u_reco_expTT, field.space)
        log(f"    error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
        abs_err, rel_err = wrapper_compute_absRel_error(
            ys, kappas, u_reco_expTT, field.space, norm="Linf")
        log(f"    error-L2Linf (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")

        # _____________________________________________________________________
        # get TT representation of u = exp h with VMC
        log("run VMC to get TT for exp h")
        ys_train = np.random.normal(0, 1, (1000, M))
        ws_train = np.ones(ys_train.shape[0]) / ys_train.shape[0]
        kappas_train = field.eval_kappa(ys_train)
        with timeit("... {:4.2f} s"):
            u_reco_vmc = tt.reconstruct(
                dims=dims, nodes=ys_train.T, weights=ws_train,
                values=kappas_train, dist="normal", label="VMC", verbose=False)
        log(f"    dims:  {u_reco_vmc.dimensions}")
        log(f"    ranks: {u_reco_vmc.ranks()}")
        abs_res, rel_res = wrapper_energy_error(u_reco_vmc, h_reco, kappa0)
        log(f"    residuum: {abs_res:4.2e} / {rel_res:4.2e}")
        abs_err, rel_err = wrapper_compute_absRel_error(
            ys, kappas, u_reco_vmc, field.space)
        log(f"    error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
        abs_err, rel_err = wrapper_compute_absRel_error(
            ys, kappas, u_reco_vmc, field.space, norm="Linf")
        log(f"    error-L2Linf (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")

        # _____________________________________________________________________
        # get TT representation of u = exp h with VMC and rescale
        # log("run VMC to get TT for exp h")
        # ys_train = np.random.normal(0, 1, (1000, M)) * 2**(-ITER)
        # ws_train = np.ones(ys_train.shape[0]) / ys_train.shape[0]
        # kappas_train = field.eval_kappa(ys_train)
        # with timeit("... {:4.2f} s"):
        #     u_reco_vmc = tt.reconstruct(
        #         dims=dims, nodes=ys_train.T, weights=ws_train,
        #         values=kappas_train, dist="normal", label="VMC", verbose=False)
        # log(f"    dims:  {u_reco_vmc.dimensions}")
        # log(f"    ranks: {u_reco_vmc.ranks()}")
        # abs_err, rel_err = wrapper_compute_absRel_error(
        #     ys_test, kappas_test, u_reco_vmc, field.space)
        # log(f"    error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
        # abs_err, rel_err = wrapper_compute_absRel_error(
        #     ys_test, kappas_test, u_reco_vmc, field.space, norm="Linf")
        # log(f"    error-L2Linf (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")

        # log("rescale VMC solution")
        # u_reco_vmc = rescale_TT(u_reco_vmc, ITER, EPS=1e-06)
        # log(f"    dims:  {u_reco_vmc.dimensions}")
        # log(f"    ranks: {u_reco_vmc.ranks()}")
        # abs_err, rel_err = wrapper_compute_absRel_error(
        #     ys, kappas, u_reco_vmc, field.space)
        # log(f"    error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
        # abs_err, rel_err = wrapper_compute_absRel_error(
        #     ys, kappas, u_reco_vmc, field.space, norm="Linf")
        # log(f"    error-L2Linf (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")

    # from IPython import embed; embed(); exit()
