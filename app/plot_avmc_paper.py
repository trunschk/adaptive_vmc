# -*- coding: utf-8 -*-
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from fenics import plot
import xerus as xe

from context import src
from src.misc import get_y_samples
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
from src.problem.equation.darcy import Problem

from avmc_utils import datetime_id
from avmc_utils import load_level_data

from plot_utils import load_level_data
from plot_utils import set_legend_upper_left
from plot_utils import set_legend_lower_left
from plot_utils import plot_slope
from plot_utils import plot_level_data
from plot_utils import get_png_file_name
from plot_utils import save_as_tikz


def _color_ref_type(ax, x_axis_vals, lvl_data, colors=None, label=None):
    """ Color convergence plot according to refinement type.

    Parameters
    ----------
    ax : matplotlib.axes._subplots.AxesSubplot
        Axes the coloring is applied to.
    x_axis_vals : list
        Coordinates on x-axis of refinements.
    ref_type : list of int
        Refinement types.
    colors : None or list of int, default = None
        List of colors for the ref_types. If None, default tab20 colors are
        used.
    label : None or list of str, default = None
        List of labels for the refinement types. If None, no labels are set.
    """
    ref_types = lvl_data["ref_type"]
    n_vmcs = lvl_data["n_vmc"]
    sdims = lvl_data["sdim"]
    cmap = plt.cm.tab20
    assert len(ref_types) == len(n_vmcs) == len(sdims)

    assert len(x_axis_vals) == len(ref_types)
    assert len(x_axis_vals) > 1

    unique_ref_types = np.unique(ref_types)
    if colors is None:
        colors = [cmap(2*j) for j, _ in enumerate(unique_ref_types)]
    hatch_ids = ["-", "/", "\\", "."]  # det, dim, mode, samples

    # get hatching patterns
    hatches = []
    zips = zip(ref_types[:-1], n_vmcs[:-1], sdims[:-1])
    for j, (rt, n, s) in enumerate(zips):
        if rt == 0:  # est_det dominates
            hatches.append(hatch_ids[0])
        elif rt == 1:  # est_sto dominates
            hatch = ""
            if max(s) < max(sdims[j+1]):
                hatch += hatch_ids[1]
            if len(s) < len(sdims[j+1]):
                hatch += hatch_ids[2]
            if hatch == "/\\":
                hatch = "x"
            hatches.append(hatch)
        elif rt == 2:  # est_alg dominates
            hatch = ""
            if n < n_vmcs[j+1]:
                hatch += hatch_ids[3]
            else:
                if max(s) < max(sdims[j+1]):
                    hatch += hatch_ids[1]
                if len(s) < len(sdims[j+1]):
                    hatch += hatch_ids[2]
                if hatch == "/\\":
                    hatch = "x"
            hatches.append(hatch)
    assert len(hatches) == len(ref_types) - 1

    assert len(colors) >= len(unique_ref_types)
    for j, ref_type in enumerate(ref_types[:-1]):
        left, right = x_axis_vals[j], x_axis_vals[j+1]
        ax.axvspan(left, right, 1e-5, 1e+2, facecolor="white", alpha=0.4,
                   hatch=hatches[j], edgecolor=colors[ref_type])

    labels = ["$\\mathcal{T}\\nearrow$", "max dim $\\nearrow$",
              "modes $\\nearrow$", "$N_{\\mathrm{VMC}}\\nearrow$"]
    for label, hatch in zip(labels, hatch_ids):
        ax.axvspan(-3, -2, 1e-5, 1e+2, facecolor="white", alpha=0.5,
                   hatch=hatch, edgecolor="black", label=label)


def plot_affine_convergence(domain, plot_legend=True):
    """ Plot convergence graph for aVMC with affine diffusion coefficient. """
    IDs = [("affine", domain, 1, 2),
           ("affine", domain, 3, 2),
           ("affine", domain, 1, 4),
           ("affine", domain, 3, 4),
           ]
    DATA_PATH = "../data/avmc_FINAL/"
    cmap = plt.cm.Set1
    # cmap = plt.cm.tab20
    marker_styles = ["o", "s", "D", "v", "^", "X", "P"]
    x_axis = "tt_dofs_u"

    fig, ax = plt.subplots(nrows=1, ncols=1)
    for i_iter, (coeff, geometry, deg, decay) in enumerate(IDs):
        DATA_FILE = f"clean_avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"
        DATA = load_level_data(DATA_PATH+DATA_FILE)

        plot_level_data(ax, DATA, x_axis, "est_total", "loglog",
                        ms=marker_styles[i_iter]+"-", c=cmap(i_iter),
                        label=None, markersize=4, linewidth=2,
                        markerfacecolor="lightgray")

        plot_level_data(ax, DATA, x_axis, "exact_err_L2H1_abs", "loglog",
                        ms=marker_styles[i_iter]+"-", c=cmap(i_iter),
                        label=None, markersize=4, linewidth=2,
                        markerfacecolor="white")
        if plot_legend is True:
            ax.loglog([1e+3, 1e+4], [1e+2, 1e+2], color=cmap(i_iter),
                      label=f"$p{deg}$, $\\sigma={decay}$", linewidth=2)

    # plot slope lines
    DATA_FILE = f"clean_avmc_{coeff}_{geometry}_deg-1_decay-2.npz"
    DATA = load_level_data(DATA_PATH+DATA_FILE)
    x_coord = [1e+3, 1e+5]
    plot_slope(ax, -0.33, x_coord, 4e-1, "loglog", label=True, offset=(2, 4),
               linewidth=3, c="gray", slope_label="$-\\frac{1}{3}$")
    x_coord = [1e+3, 3e+4]
    plot_slope(ax, -1.00, x_coord, 6e-3, "loglog", label=True, offset=(-1, -12),
               linewidth=3, c="gray", slope_label="$-1$")

    ax.grid()
    ax.set_xlabel("tt-dofs$(u_{N,r,n})$")

    # build custom legend
    ax.set_ylim(1.5e-4, 5e-1)
    if plot_legend is True:
        ax.loglog([1e+3, 1e+4], [1e+2, 1e+2], "s-", color="black", linewidth=2,
                  markersize=4, markerfacecolor="lightgray",
                  label="$\\eta(u_{N,r,n})$")
        ax.loglog([1e+3, 1e+4], [1e+2, 1e+2], "s-", color="black", linewidth=2,
                  markersize=4, markerfacecolor="white",
                  label="$\\mathcal{{E}}_u(u_{N,r,n})$")
        legend = set_legend_upper_left(ax)
        plt.gca().add_artist(legend)

    TIKZ_PATH = f"../tikz/affine_convergence_{domain}"
    save_as_tikz(TIKZ_PATH)
    print(f"save to: {TIKZ_PATH}")


def plot_lognormal_convergence(domain, plot_legend=True):
    """ Plot convergence graph for aVMC with affine diffusion coefficient. """
    IDs = [("lognormal", domain, 1, 2),
           ("lognormal", domain, 3, 2),
           ("lognormal", domain, 1, 4),
           ("lognormal", domain, 3, 4),
           ]
    DATA_PATH = "../data/avmc_FINAL/"
    cmap = plt.cm.Set1
    # cmap = plt.cm.tab20
    marker_styles = ["o", "s", "D", "v", "^", "X", "P"]
    x_axis = "tt_dofs_u"

    fig, ax = plt.subplots(nrows=1, ncols=1)
    for i_iter, (coeff, geometry, deg, decay) in enumerate(IDs):
        DATA_FILE = f"clean_avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"
        try:
            DATA = load_level_data(DATA_PATH+DATA_FILE)
        except:
            continue

        plot_level_data(ax, DATA, x_axis, "est_total", "loglog",
                        ms=marker_styles[i_iter]+"-", c=cmap(i_iter),
                        label=None, markersize=4, linewidth=2,
                        markerfacecolor="lightgray")

        plot_level_data(ax, DATA, x_axis, "exact_err_L2H1_abs", "loglog",
                        ms=marker_styles[i_iter]+"-", c=cmap(i_iter),
                        label=None, markersize=4, linewidth=2,
                        markerfacecolor="white")
        if plot_legend is True:
            ax.loglog([1e+3, 1e+4], [1e+2, 1e+2], color=cmap(i_iter),
                      label=f"$p{deg}$, $\\sigma={decay}$", linewidth=2)

    # plot slope lines
    DATA_FILE = f"clean_avmc_{coeff}_{geometry}_deg-1_decay-2.npz"
    DATA = load_level_data(DATA_PATH+DATA_FILE)
    x_coord = [1e+3, 1e+5]
    plot_slope(ax, -0.33, x_coord, 1e0, "loglog", label=True, offset=(2, 4),
               linewidth=3, c="gray", slope_label="$-\\frac{1}{3}$")
    x_coord = [3e+2, 6e+3]
    plot_slope(ax, -1.00, x_coord, 2.5e-2, "loglog", label=True,
               offset=(-1, -12), linewidth=3, c="gray", slope_label="$-1$")

    ax.grid()
    ax.set_xlabel("tt-dofs$(u_{N,r,n})$")

    # build custom legend
    ax.set_ylim(7.5e-4, 2.5e0)
    if plot_legend is True:
        ax.loglog([1e+3, 1e+4], [1e+2, 1e+2], "s-", color="black", linewidth=2,
                  markersize=4, markerfacecolor="lightgray",
                  label="$\\eta(u_{N,r,n})$")
        ax.loglog([1e+3, 1e+4], [1e+2, 1e+2], "s-", color="black", linewidth=2,
                  markersize=4, markerfacecolor="white",
                  label="$\\mathcal{{E}}_u(u_{N,r,n})$")
        legend = set_legend_upper_left(ax)
        plt.gca().add_artist(legend)

    TIKZ_PATH = f"../tikz/lognormal_convergence_{domain}"
    save_as_tikz(TIKZ_PATH)
    print(f"save to: {TIKZ_PATH}")


def plot_refinement_level(ID, name_id, plot_legend=True):
    """ Plot convergence graph for aVMC with affine diffusion coefficient. """
    DATA_PATH = "../data/avmc_FINAL/"
    cmap = plt.cm.Set1
    x_axis = "level"

    fig, ax = plt.subplots(nrows=1, ncols=1)
    DATA_FILE = f"clean_avmc_{ID[0]}_{ID[1]}_deg-{ID[2]}_decay-{ID[3]}.npz"
    DATA = load_level_data(DATA_PATH+DATA_FILE)

    label = "$\\eta(u_{N,r,n})$"
    plot_level_data(ax, DATA, x_axis, "est_total", "semilogy",
                    ms="o-", c="black", label=label, markersize=3, linewidth=2,
                    markerfacecolor="lightgray")

    # estimator parts
    label = "$\\eta_{\\mathrm{det}}(u_{N,r,n})$"
    plot_level_data(ax, DATA, x_axis, "est_det", "semilogy",
                    ms="o-", c=cmap(1), label=label, markersize=3, linewidth=2,
                    markerfacecolor="white")
    label = "$\\eta_{\\mathrm{sto}}(u_{N,r,n})$"
    plot_level_data(ax, DATA, x_axis, "est_stoch", "semilogy",
                    ms="o-", c=cmap(3), label=label, markersize=3, linewidth=2,
                    markerfacecolor="white")
    label = "$\\eta_{\\mathrm{alg}}(u_{N,r,n})$"
    plot_level_data(ax, DATA, x_axis, "est_disc", "semilogy",
                    ms="o-", c=cmap(2), label=label, markersize=3, linewidth=2,
                    markerfacecolor="white")

    label = "$\\mathcal{{E}}_u(u_{N,r,n})$"
    plot_level_data(ax, DATA, x_axis, "exact_err_L2H1_abs", "semilogy",
                    ms="o-", c=cmap(0), label=label, markersize=3, linewidth=2,
                    markerfacecolor="lightgray")

    # color in refinement types
    _color_ref_type(ax, DATA[x_axis], DATA, colors=[cmap(1), cmap(3), cmap(2)])
    # _color_ref_type(ax, DATA[x_axis], DATA, colors=["gray", "gray", "gray"])
    if plot_legend is True:
        l1 = set_legend_upper_left(ax)
        plt.gca().add_artist(l1)
    ax.set_xlim(0)
    ax.set_xlabel("refinement level")

    TIKZ_PATH = f"../tikz/refinement_level_{name_id}"
    save_as_tikz(TIKZ_PATH)
    # plt.show()
    print(f"save to: {TIKZ_PATH}")


def plot_ranks_dims_order(coeff_type):
    """ Plot ranks, dims and order for aVMC. """
    IDs = [(coeff_type, "UnitSquare", 1, 2),
           (coeff_type, "Lshape", 1, 2),
           (coeff_type, "UnitSquare", 3, 2),
           (coeff_type, "Lshape", 3, 2),
           (coeff_type, "UnitSquare", 1, 4),
           (coeff_type, "Lshape", 1, 4),
           (coeff_type, "UnitSquare", 3, 4),
           (coeff_type, "Lshape", 3, 4),
           ]
    DATA_PATH = "../data/avmc_FINAL/"
    # cmap = plt.cm.Set1
    cmap = plt.cm.tab20
    marker_styles = ["o", "s"]*4
    plot_range_upper = [1, 1, 1]

    fig, ax = plt.subplots(figsize=(10, 10), nrows=2, ncols=2)
    for i_iter, (coeff, geometry, deg, decay) in enumerate(reversed(IDs)):
        DATA_FILE = f"avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"
        try:
            DATA = load_level_data(DATA_PATH+DATA_FILE)
        except:
            continue

        levels = DATA["level"]
        max_ranks = []
        max_dims = []
        orders = []
        for level in levels:
            u_reco = DATA["u_reco"][level]
            max_ranks.append(max(u_reco.ranks()))
            max_dims.append(max(u_reco.dimensions[1:]))
            orders.append(u_reco.order()-1)
        plot_range_upper[0] = max(plot_range_upper[0], max(max_ranks))
        plot_range_upper[1] = max(plot_range_upper[1], max(max_dims))
        plot_range_upper[2] = max(plot_range_upper[2], max(orders))

        ax[0, 0].plot(levels, max_dims,
                      marker_styles[len(IDs)-i_iter-1]+"-",
                      c=cmap(len(IDs)-i_iter-1), markersize=4, linewidth=2,
                      markerfacecolor="white",
                      # label=f"{geometry} - $p{deg}$ - $\sigma={decay}$")
                      label=None)

        ax[0, 1].plot(levels, orders,
                      marker_styles[len(IDs)-i_iter-1]+"-",
                      c=cmap(len(IDs)-i_iter-1), markersize=4, linewidth=2,
                      markerfacecolor="white",
                      # label=f"{geometry} - $p{deg}$ - $\sigma={decay}$")
                      label=None)

        ax[1, 0].plot(levels, max_ranks,
                      marker_styles[len(IDs)-i_iter-1]+"-",
                      c=cmap(len(IDs)-i_iter-1), markersize=4, linewidth=2,
                      markerfacecolor="white",
                      label=f"{geometry} - $p{deg}$ - $\sigma={decay}$")
                      # label=None)

    # plot legend
    for i_iter, (coeff, geometry, deg, decay) in enumerate(IDs):
        ax[1, 1].plot([1, 10], [-1, -1], marker_styles[i_iter]+"-",
                      c=cmap(i_iter), markersize=4, linewidth=2,
                      markerfacecolor="white",
                      label=f"$p{deg}$ - $\sigma={decay}$ - {geometry}")

    ax[1, 1].axis("off")

    ax[0, 0].grid()
    ax[0, 1].grid()
    ax[1, 0].grid()

    ax[1, 0].set_xlabel("refinement level")
    # ax[0, 0].set_xlabel("refinement level")
    ax[0, 1].set_xlabel("refinement level")

    ax[0, 1].yaxis.tick_right()

    ax[1, 0].set_ylabel("maximum rank $\max(r)$")
    ax[0, 0].set_ylabel("maximum stochastic dimension $\max(d)$")
    ax[0, 1].set_ylabel("number of stochastic modes $M$")

    # build custom legend
    ax[1, 0].set_ylim(0, 1.1*plot_range_upper[0])
    ax[0, 0].set_ylim(1, 1.1*plot_range_upper[1])
    ax[0, 1].set_ylim(0, 1.1*plot_range_upper[2])
    ax[1, 1].set_ylim(0, 1)

    ax[1, 1].legend(loc="center")

    TIKZ_PATH = f"../tikz/{coeff_type}_ranks_dims_order"
    # plt.show()
    save_as_tikz(TIKZ_PATH)
    print(f"save to: {TIKZ_PATH}")


def plot_ranks_dims_order_ttdofs(domain):
    """ Plot ranks, dims and order for aVMC. """
    IDs = [("lognormal", domain, 1, 2),
           ("affine", domain, 1, 2),
           ("lognormal", domain, 3, 2),
           ("affine", domain, 3, 2),
           ("lognormal", domain, 1, 4),
           ("affine", domain, 1, 4),
           ("lognormal", domain, 3, 4),
           ("affine", domain, 3, 4),
           ]
    DATA_PATH = "../data/avmc_FINAL/"
    # cmap = plt.cm.Set1
    cmap = plt.cm.tab20
    marker_styles = ["o-", "s--"]*4
    plot_range_upper = [1, 1, 1]

    fig, ax = plt.subplots(figsize=(10, 10), nrows=2, ncols=2)
    for i_iter, (coeff, geometry, deg, decay) in enumerate(reversed(IDs)):
        DATA_FILE = f"avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"
        try:
            DATA = load_level_data(DATA_PATH+DATA_FILE)
        except:
            continue

        levels = DATA["level"]
        tt_dofs = DATA["tt_dofs_u"]
        max_ranks = []
        max_dims = []
        orders = []
        for level in levels:
            u_reco = DATA["u_reco"][level]
            max_ranks.append(max(u_reco.ranks()))
            max_dims.append(max(u_reco.dimensions[1:]))
            orders.append(u_reco.order()-1)
        plot_range_upper[0] = max(plot_range_upper[0], max(max_ranks))
        plot_range_upper[1] = max(plot_range_upper[1], max(max_dims))
        plot_range_upper[2] = max(plot_range_upper[2], max(orders))

        ax[0, 0].semilogx(tt_dofs, max_dims,
                      marker_styles[len(IDs)-i_iter-1],
                      c=cmap(len(IDs)-i_iter-1), markersize=4, linewidth=2,
                      markerfacecolor="white",
                      # label=f"{geometry} - $p{deg}$ - $\sigma={decay}$")
                      label=None)

        ax[0, 1].semilogx(tt_dofs, orders,
                      marker_styles[len(IDs)-i_iter-1],
                      c=cmap(len(IDs)-i_iter-1), markersize=4, linewidth=2,
                      markerfacecolor="white",
                      # label=f"{geometry} - $p{deg}$ - $\sigma={decay}$")
                      label=None)

        ax[1, 0].loglog(tt_dofs, max_ranks,
                      marker_styles[len(IDs)-i_iter-1],
                      c=cmap(len(IDs)-i_iter-1), markersize=4, linewidth=2,
                      markerfacecolor="white",
                      label=f"{geometry} - $p{deg}$ - $\sigma={decay}$")
                      # label=None)

    # plot legend
    for i_iter, (coeff, geometry, deg, decay) in enumerate(IDs):
        ax[1, 1].plot([1, 10], [-1, -1], marker_styles[i_iter],
                      c=cmap(i_iter), markersize=4, linewidth=2,
                      markerfacecolor="white",
                      label=f"$p{deg}$ - $\sigma={decay}$ - {coeff}")

    ax[1, 1].axis("off")

    ax[0, 0].grid()
    ax[0, 1].grid()
    ax[1, 0].grid()

    ax[1, 0].set_xlabel("tt-dofs$(u_{N,r,n})$")
    # ax[0, 0].set_xlabel("tt-dofs$(u_{N,r,n})$")
    ax[0, 1].set_xlabel("tt-dofs$(u_{N,r,n})$")

    ax[0, 1].yaxis.tick_right()

    ax[1, 0].set_ylabel("maximum rank $\max(r)$")
    ax[0, 0].set_ylabel("maximum stochastic dimension $\max(d)$")
    ax[0, 1].set_ylabel("number of stochastic modes $M$")

    # build custom legend
    # ax[1, 0].set_ylim(1, 1.1*plot_range_upper[0])
    ax[1, 0].set_ylim(1e+0, 1e+02)
    ax[0, 0].set_ylim(1, 1.1*plot_range_upper[1])
    ax[0, 1].set_ylim(0, 1.1*plot_range_upper[2])
    ax[1, 1].set_ylim(0, 1)

    ax[1, 1].legend(loc="center")

    TIKZ_PATH = f"../tikz/{domain}_ranks_dims_order_ttdofs"
    # plt.show()
    save_as_tikz(TIKZ_PATH)
    print(f"save to: {TIKZ_PATH}")


def plot_test(path="../tikz/test"):
    # x = np.array([10**j for j in range(7)])
    cmap = plt.cm.tab10
    x = np.linspace(1, 5, 100)
    fig, ax = plt.subplots(nrows=1, ncols=2)
    ax[0].plot(x, x**(-2), label="$f_1$")
    ax[0].plot(x, np.sin(x**2), label="$f_2$")
    ax[0].plot(x, np.sin(x**2)+5, label="$f_3$")
    ax[0].set_ylim(-1, 1)
    ax[0].legend()

    ax[1].plot(x, x**(-2), label="$f_1$")
    ax[1].plot(x, np.sin(x**2), label="$f_2$")
    ax[1].plot(x, np.sin(x**2)+5, label="$f_3$")
    ax[1].set_ylim(-1, 1)
    ax[1].legend()

    # legend = set_legend_upper_left(ax)
    # plt.gca().add_artist(legend)
    save_as_tikz(path)
    print(f"save to: {path}")
    plt.show()

def print_n_vmc():
    IDs = [("affine", "UnitSquare", 1, 2),
           ("affine", "Lshape", 1, 2),
           ("affine", "UnitSquare", 3, 2),
           ("affine", "Lshape", 3, 2),
           ("affine", "UnitSquare", 1, 4),
           ("affine", "Lshape", 1, 4),
           ("affine", "UnitSquare", 3, 4),
           ("affine", "Lshape", 3, 4),
           ("lognormal", "UnitSquare", 1, 2),
           ("lognormal", "Lshape", 1, 2),
           ("lognormal", "UnitSquare", 3, 2),
           ("lognormal", "Lshape", 3, 2),
           ("lognormal", "UnitSquare", 1, 4),
           ("lognormal", "Lshape", 1, 4),
           ("lognormal", "UnitSquare", 3, 4),
           ("lognormal", "Lshape", 3, 4),
           ]
    DATA_PATH = "../data/avmc_FINAL/"

    def fmttr(coeff, geometry, deg, decay, n_vmc):
        msg = ""
        msg += f"{coeff:<10}  "
        msg += f"{deg:<3}  "
        msg += f"{decay:<5}  "
        msg += f"{geometry:<10}  "
        msg += f"{n_vmc:>7}  "
        return msg

    print()
    print(fmttr("coeff", "domain", "deg", "decay", "N_VMC"))
    print("-"*43)
    for i_iter, (coeff, geometry, deg, decay) in enumerate(IDs):
        DATA_FILE = f"clean_avmc_{coeff}_{geometry}_deg-{deg}_decay-{decay}.npz"
        try:
            DATA = load_level_data(DATA_PATH+DATA_FILE)
        except:
            continue
        print(fmttr(coeff, geometry, deg, decay, DATA["n_vmc"][-1]))


def main():
    """ main """

    # plot_test()

    plot_affine_convergence("UnitSquare", plot_legend=False)
    plot_lognormal_convergence("UnitSquare", plot_legend=True)
    plot_affine_convergence("Lshape", plot_legend=False)
    plot_lognormal_convergence("Lshape", plot_legend=True)

    plot_ranks_dims_order("affine")
    plot_ranks_dims_order("lognormal")
    plot_ranks_dims_order_ttdofs("UnitSquare")
    plot_ranks_dims_order_ttdofs("Lshape")

    ID = ("lognormal", "Lshape", 1, 4)
    plot_refinement_level(ID, "A", plot_legend=False)

    ID = ("lognormal", "Lshape", 3, 2)
    plot_refinement_level(ID, "B", plot_legend=True)

    print_n_vmc()


if __name__ == "__main__":
    main()
