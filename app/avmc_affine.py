""" adaptive VMC application with affine diffusion coefficient. """

# -*- coding: utf-8 -*-
import sys

from context import src
from src.misc import timeit, logf
from src.problem.equation.darcy import Problem
# import src.autoPDB

from fenics import FunctionSpace

from avmc_utils import datetime_id
from avmc_utils import log_file_name
from avmc_utils import initialize_level_data
from avmc_utils import update_level_data
from avmc_utils import clean_level_data
from avmc_utils import info_dict
from avmc_utils import solve_step
from avmc_utils import estimate_step
from avmc_utils import mark_and_refine_step
from avmc_utils import check_break_condition
from avmc_utils import compute_exact_error
from avmc_utils import compute_difference
from avmc_utils import load_level_data


def run_avmc_affine(DATA_FILE, geometry, deg, decay, mean):
    """ Run adaptive loop stept SOLVE -> ESTIMATE -> MARK -> REFINE.

    Parameters
    ----------
    DATA_FILE : string
        Path to level data file.
    geometry : str
        Computational domain, 'UnitSquare' or 'Lshape'.
    deg : int
        Finite element polynomial degree.
    decay : float
        Decay of stochastic modes of diffusion coefficient kappa.
    mean : float
        Mean value of diffusion coefficient kappa.
    """
    # setup
    sdim = [2]  # initial stochastic dimensions for solution u
    N_VMC = 100  # number of samples for VMC reconstruction
    N_test = 250  # number of samples for error computation
    MAX_TT_DoFs = int(1e+05)  # maximum number of TT DoFs
    MAX_ITER = 50  # maximum number of iterations
    cStoch = 5  # weight for stochastic estimator
    cDisc = 10  # weight for algebraic estimator
    doerfler_threshold = 0.3  # Doerfler marking threshold
    n_refs = 8 if deg == 1 else 5  # number of initial refinements

    info = info_dict(degree=deg, coeff_degree=deg, decay=decay, mean=mean,
                     geometry=geometry, mesh=n_refs, rho=0)
    p = Problem(info)
    assert p.space.dim() == p.space_coeff.dim()  # sanity check

    LOG_FILE = log_file_name(DATA_FILE)
    logf(LOG_FILE, f"Write log to: {LOG_FILE}", overwrite=True)

    # loop
    for loop_iter in range(MAX_ITER):
        logf(LOG_FILE, f"ITERATION {loop_iter} (max. {MAX_ITER-1})")
        logf(LOG_FILE, f"    FE-DoFs: {p.dofs()}")
        logf(LOG_FILE, f"    sdim: {sdim}")
        update_level_data(DATA_FILE, level=loop_iter, info=p.info,
                          max_tt_dofs=MAX_TT_DoFs, fe_dofs_u=p.space.dim(),
                          fe_dofs_kappa=p.space_coeff.dim(), max_iter=MAX_ITER,
                          sdim=sdim, doerfler_threshold=doerfler_threshold,
                          n_vmc=N_VMC, n_test=N_test, cStoch=cStoch,
                          cDisc=cDisc)

        # SOLVE
        # if loop_iter == 0:
        #     diff_eps = None
        u_reco, kappa_reco, f_reco = solve_step(
            p, sdim, N_VMC, N_test, dist="uniform", file=DATA_FILE)

        # levelData = load_level_data(DATA_FILE)
        # if loop_iter > 0:
        #     logf(LOG_FILE, "="*80)
        #     old_uReco = levelData['u_reco'][-2]
        #     new_uReco = levelData['u_reco'][-1]
        #     old_FESpace = Problem(levelData['info'][-2]).space
        #     new_FESpace = Problem(levelData['info'][-1]).space
        #     diff = compute_difference(
        #         old_uReco, old_FESpace, new_uReco, new_FESpace)
        #     if diff > 1e-14:
        #         if diff_eps is None:
        #             diff_eps = diff
        #         else:
        #             diff_eps = min(diff_eps, diff)
        #     logf(LOG_FILE, f"\tDifference[u] {diff}")
        #     logf(LOG_FILE, "="*80)

        # ESTIMATE
        est_det_loc, est_stoch_loc, ref_type = estimate_step(
            u_reco, kappa_reco, f_reco, p, cStoch, cDisc,
            file=DATA_FILE, look_ahead=1)

        # BREAK
        clean_level_data(DATA_FILE)  # save a cleaned version of level data
        do_break, msg = check_break_condition(file=DATA_FILE)
        if do_break:
            logf(LOG_FILE, f":: BREAK :: ({msg})")
            break

        # MARK and REFINE
        p, sdim, N_VMC = mark_and_refine_step(
            est_det_loc, est_stoch_loc, ref_type, p, sdim, N_VMC,
            file=DATA_FILE)

        logf(LOG_FILE, "")

    compute_exact_error(DATA_FILE)
    clean_level_data(DATA_FILE)  # save a cleaned version of level data


def main():
    """ main """

    PATH = "../data/avmc_affine/"
    # PATH += f"run_{datetime_id()}" + "/"

    # define runs
    RUNS = [  # (geometry, deg, decay)
        ("UnitSquare", 1, 2),  # done
        # ("UnitSquare", 3, 2),  # done
        # ("UnitSquare", 1, 4),  # done
        # ("UnitSquare", 3, 4),  # done
        ("Lshape", 1, 2),  # done
        # ("Lshape", 3, 2),  # done
        # ("Lshape", 1, 4),  # done
        # ("Lshape", 3, 4),  # done
    ]

    for run in RUNS:
        geometry, deg, decay = run
        FILE = f"avmc_affine_{geometry}_deg-{deg}_decay-{decay}.npz"
        initialize_level_data(PATH, FILE)
        run_avmc_affine(PATH+FILE, geometry, deg, decay, mean=1.0)
        print()


if __name__ == "__main__":
    main()
