import numpy as np
import xerus as xe

from context import src
from src.misc import timeit, log
import src.utils.tt_tools as tt
import src.utils.tt_exp as expTT
import autoPDB

# ==============================================================================
# utiliy functions


def eval_affine_coefficient_explicit(x, h):
    """
    Evaluate 
        h(x) = h_0 P_0(x) + sum_{j=1}^{M} h_{e_j} P_{e_j}(x)
    for given coefficients (h_0, h_{e_1}, ..., h_{e_M}).
    """
    if x.ndim == 1:
        x.shape = (1, -1)
    assert x.shape[1] == h.shape[0]-1
    # normalizing const.
    def c(n): return 1 / np.sqrt(float(np.math.factorial(n)))
    P1 = np.polynomial.hermite_e.hermeval(x, [0, c(1)])
    val = np.ones(x.shape[0])*h[0] + np.sum(h[1:].reshape(1, -1)*P1, axis=1)
    return val.reshape(-1, 1)


def eval_coefficient(x, h):
    """
    Evaluate a function for given expansion coefficients w.r.t. the standard
    normalized HermiteE basis.
    """
    if isinstance(h, xe.TTTensor):
        ret = tt.eval_approx(h, x, "normal")
    else:
        ret = eval_affine_coefficient_explicit(x, h)
    return ret


def compute_L2_error(tt_tensor, xs, ys):
    tt_eval = tt.eval_approx(tt_tensor, xs, "normal")
    abs_err = np.sqrt(np.sum((ys-tt_eval)**2)/ys.shape[0])
    rel_err = abs_err / np.sqrt(np.sum(ys**2)/ys.shape[0])
    return abs_err, rel_err


def compute_ic_error(tt_tensor, x_0, exp_h):
    val = tt.eval_approx(tt_tensor, x_0, "normal")
    assert val.flatten().shape[0] == 1
    err = np.abs(exp_h - val)
    return val[0, 0], err[0, 0]


def pad_dimensions(tt, dimensions):
    assert len(dimensions) == tt.order()
    assert all(tt.dimensions[i] <= dimensions[i] for i in range(tt.order()))
    ret = xe.TTTensor(dimensions)
    ranks = [1] + tt.ranks() + [1]
    for m in range(tt.order()):
        core = np.zeros((ranks[m], dimensions[m], ranks[m+1]))
        core[:, :tt.dimensions[m], :] = tt.get_component(m)
        ret.set_component(m, xe.Tensor.from_buffer(core))
    return ret

# ==============================================================================
# debug functions


def get_random_mdx(dims, size=1):
    """
    Get size many random multiindex within \Lambda_dims.
    """
    mdx = np.array([np.random.randint(d, size=size)
                    for d in dims], dtype=int).T
    return mdx


def compare_tt_cores(tt1, tt2):
    isEqual = True
    tt1.canonicalize_left()
    tt2.canonicalize_left()
    assert tt1.order() == tt2.order()
    for j in range(len(tt1.ranks())+1):
        c1 = np.asarray(tt1.get_component(j))
        c2 = np.asarray(tt2.get_component(j))
        if not c1.shape == c2.shape:
            isEqual = False
        elif not np.allclose(c1, c2):
            isEqual = False
    return isEqual


def compare_tt_entries(tt1, tt2, N=1000):
    assert np.all(np.asarray(tt1.dimensions) == np.asarray(tt2.dimensions))
    mdx = get_random_mdx(tt1.dimensions, N)
    val1, val2 = np.zeros(N), np.zeros(N)
    for j, alpha in enumerate(mdx):
        val1[j] = tt1[alpha]
        val2[j] = tt2[alpha]

    err_max = np.max(np.abs(val1-val2))
    err_L2 = np.linalg.norm(val1-val2)
    err_L2_rel = err_L2 / np.linalg.norm(val1)
    return err_L2, err_L2_rel, err_max

# ==============================================================================
# main test functions


def reconstruct_exponent(h, dh):
    n_samples = 1000
    xs_train = np.random.normal(0, 1, (n_samples, len(dh)))
    ws_train = np.ones(n_samples)/n_samples
    ys_train = eval_coefficient(xs_train, h)

    log("__"*40)
    log("")
    log("reconstruct exponent h")
    with timeit("... {:4.2f} s"):
        reco_h = tt.reconstruct(
            dims=dh,
            nodes=xs_train.T,
            weights=ws_train,
            values=ys_train,
            dist="normal",
            label="h reco",
            verbose=False
        )

    xs = np.random.normal(0, 1, (n_samples, len(dh)))
    ws = np.ones(n_samples)/n_samples
    ys = np.exp(eval_coefficient(xs, h))
    hs = eval_coefficient(xs, h)

    abs_err, rel_err = compute_L2_error(reco_h, xs, hs)
    log(f"error-L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    return reco_h


def expTT_reconstruction_TT(h, da, dt, x_0, h_coeff, verbose=0):
    log("__"*40)
    log("")
    log("reconstruct exp(h) -- expTT TT-case")
    with timeit("... {:4.2f} s"):
        reco_expTT = expTT.expTT_reconstruction(
            da, dt, h, x_0, verbose=verbose
        )

    n_samples = 1000
    xs = np.random.normal(0, 1, (n_samples, M))
    ys = np.exp(eval_coefficient(xs, h_coeff))
    exp_h = expTT.generate_IC_components(x_0, h, da)[1]

    abs_err, rel_err = compute_L2_error(reco_expTT, xs, ys)
    log(f"error-L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    val, err = compute_ic_error(reco_expTT, x_0, exp_h)
    log(f"error IC (val/err): {val:4.2e} / {err:4.2e}")


def expTT_reconstruction_TT_iterDim(h, da, dt, x_0, h_coeff, verbose=0):
    if len(da) < 2:
        return

    def fmttr(s): return log("    "+s)

    log("__"*40)
    log("")
    log("reconstruct exp(h) -- expTT TT-case -- increase da")

    # set initial dimensions
    dai = np.ones(M, dtype=int)
    daM = max(da)
    # set initial guess
    u_start = xe.TTTensor.random(dai, [5]*(M-1))
    u_start = u_start / xe.frob_norm(u_start)
    with timeit("... ({:4.2f} s)"):
        for i in range(1, daM+1):
            # build operator and rhs
            W0, b = expTT.build_system_inexactIC(
                list(dai), dt, h, x_0)
            # run ALS
            log(f"  run ALS for da = [{i}]*{len(da)} ...")
            tmp = expTT.run_simpleALS(
                A=W0, u_start=u_start, b=b, verbose=1, fmttr=fmttr)
            tmp.round(1e-12)
            if i < daM:
                # update initial dimensions for next iteration
                assert np.any(dai < da)
                dai = np.minimum(dai+1, da)
                # update initial guess for next iteration
                u_start = xe.TTTensor.random(dai, [5]*(M-1))
                u_start = u_start / (1e3 * xe.frob_norm(u_start))
                u_start = pad_dimensions(tmp, dai) + u_start
    reco_expTT = tt.add_dimension_left(tmp)

    n_samples = 1000
    xs = np.random.normal(0, 1, (n_samples, M))
    ys = np.exp(eval_coefficient(xs, h_coeff))
    exp_h = expTT.generate_IC_components(x_0, h, da)[1]

    abs_err, rel_err = compute_L2_error(reco_expTT, xs, ys)
    log(f"error-L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    val, err = compute_ic_error(reco_expTT, x_0, exp_h)
    log(f"error IC (val/err): {val:4.2e} / {err:4.2e}")


def naive_reconstruction(h, da, x_0):
    n_samples = 1000
    xs_train = np.random.normal(0, 1, (n_samples, len(da)))
    ws_train = np.ones(n_samples)/n_samples
    ys_train = np.exp(eval_coefficient(xs_train, h))

    log("__"*40)
    log("")
    log("reconstruct exp(h) -- naive")
    with timeit("... {:4.2f} s"):
        reco_naive = tt.reconstruct(
            dims=da,
            nodes=xs_train.T,
            weights=ws_train,
            values=ys_train,
            dist="normal",
            label="naive reco",
            verbose=False
        )

    xs = np.random.normal(0, 1, (n_samples, len(da)))
    ys = np.exp(eval_coefficient(xs, h))
    exp_h = expTT.generate_IC_components(x_0, h, da)[1]

    abs_err, rel_err = compute_L2_error(reco_naive, xs, ys)
    log(f"error-L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    val, err = compute_ic_error(reco_naive, x_0, exp_h)
    log(f"error IC (val/err): {val:4.2e} / {err:4.2e}")


# ==============================================================================
if __name__ == "__main__":
    """ Test expTT reconstuction for affine exponent in any dimension.

    Assume here that the affine exponent h is given by
                h(x) = h_0 + sum_{i=1,...,M} h_{e_i} P_{e_i}(x_i).

    NOTE:
        - This version only tests and supports the expTT reconstuction for
          functions h with one dimensional real output.
        - The dimensions of the test space dt must be larger or equal to the
          dimensions of the ansatz space da to prevent information loss.
    """
    # TODO
    # [ ] use different ansatz and test spaces

    # setup

    M = 2  # number of random variables
    dim_a, dim_t, dim_h = 10, 10, 2
    da, dt, dh = [dim_a]*M, [dim_t]*M, [dim_h]*M
    h_coeff = np.random.normal(0, 1, M+1)
    h_coeff = h_coeff / (3 * np.linalg.norm(h_coeff))
    x_0 = np.zeros((1, M))

    log("da:     ", da)
    log("dt:     ", dt)
    if M < 6:
        log("h_coeff:", h_coeff)
    else:
        minimum, maximum = np.min(np.abs(h_coeff)), np.max(np.abs(h_coeff))
        log(f"h_coeff: min = {minimum:4.2g} -- max = {maximum:4.2g}")

    exp_h = expTT.generate_IC_components(x_0, h_coeff, da)[1]
    if np.all(x_0[0] == x_0[0, 0]):
        log("x_0:               ", x_0[0, 0])
    else:
        log("x_0:               ", x_0[0])
    log(f"exp h(x_0):         {exp_h[0,0]:4.2e}")

    # --------------------------------------------------------------------------
    # reconstruct h
    reco_h = reconstruct_exponent(h_coeff, dh)

    # --------------------------------------------------------------------------
    # expTT reconstruction for TT case
    # NOTE: Here we build the operator to solve the expTT system for a given
    #       TTTensor of the coefficients of the exponent h. The Operator is
    #       build in a way such that is has minimal ranks.
    if M < 6:
        expTT_reconstruction_TT(reco_h, da, dt, x_0, h_coeff, verbose=1)

    # --------------------------------------------------------------------------
    # expTT reconstruction for TT case with increasing dimensions
    # NOTE: Same as the previous one only for TT instead of affin case.
    # expTT_reconstruction_TT_iterDim(reco_h, da, dt, x_0, h_coeff, verbose=1)

    # --------------------------------------------------------------------------
    # naive reconstruction
    # NOTE: Reconstruct exp(h) with VMC.
    naive_reconstruction(h_coeff, da, x_0)

    # --------------------------------------------------------------------------
    # from IPython import embed; embed(); exit() # debug line
