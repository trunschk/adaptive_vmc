import sys
import os
import numpy as np
from scipy.special import zeta
import matplotlib.pyplot as plt
import xerus as xe
from fenics import *

from context import src
from src.misc import timeit, log, transpose, Logger
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as expTT
from src.parallel import Parallel, ParallelizableProblem

# import autoPDB


class TestExponent():
    """Class to construct log-normal Darcy diffusion coefficient.

    This should be the same as src.problem.field.testfield.TestField2 but with
    less structural overhead.
    """

    def __init__(self, decay, M, mean=0.0, scale=1.0, nT=20, fem_deg=0, element="DG", useDarcyCoeff=True):
        """
        Affine function similar to Darcy diffusion coefficient, i.e.
            h(x,y) = scale * ( mean + sum_{m=1}^M h_m(x) y_m )
        where
            h_m(x) = 0.9 / \zeta(decay) * m**(-decay) * cos(2\pi B1(m) x_1) * cos(2\pi B2(m) x_2).
        Here, B1 = B2 = 2 for the test case and
            B1(m) = m - k(m)*( k(m)+1 ) / 2      and     B2(m) = k(m) - B1(m)
        for
            k(m) = floor[ -1/2 + \sqrt{ 1/4 + 2m } ].
        """
        self.decay = decay
        self.M = M
        self.scale = scale
        self.det_coeff = Expression(
            "C * (cos(2*pi*B1*x[0]) * cos(2*pi*B2*x[1]))", C=1, B1=2, B2=2, degree=10)
        self.mean = mean
        self.useDarcyCoeff = useDarcyCoeff

        self.mesh = Mesh(UnitSquareMesh(nT, nT))
        if element == "DG":
            assert fem_deg == 0  # does not work for DG-n wit n > 0
        self.fem_deg = fem_deg
        self.space = FunctionSpace(self.mesh, element, self.fem_deg)

    # NOTE: used to mimic Darcy lognormal coefficient
    def k(self, m): return np.floor(-0.5+np.sqrt(0.25 + 2*m))
    def beta1(self, m): return m - self.k(m) * (self.k(m)+1) / 2
    def beta2(self, m): return self.k(m) - self.beta1(m)

    def realisation(self, y, exp=False):
        """ FEM function for one specific parameter realization. """
        det_coeff = self.det_coeff

        def interpolate_det_coeff(C, B1, B2):
            det_coeff.C, det_coeff.B1, det_coeff.B2 = C, B1, B2
            return interpolate(det_coeff, self.space).vector().get_local()

        def linear(y):
            u = Function(self.space).vector().get_local()
            for m in range(self.M):
                C = 0.9/zeta(self.decay) * (m+1)**(-self.decay)
                if self.useDarcyCoeff == True:
                    B1, B2 = self.beta1(m+1), self.beta2(m+1)
                else:
                    B1, B2 = 2, 2
                u += interpolate_det_coeff(C, B1, B2) * y[m]
            return u
        val = self.scale * (self.mean + linear(y))
        if exp == True:
            val = np.exp(val)
        ret = Function(self.space)
        ret.vector().set_local(val)
        return ret


class Problem(ParallelizableProblem):
    """Mimic of src.problem.equation.darcy.Problem. """

    def __init__(self, info):
        self.info = info
        self.decay = info["decay"]
        self.M = info["M"]
        self.mean = info["mean"]
        self.scale = info["scale"]
        self.nT = info["nT"]
        self.useDarcyCoeff = info["useDarcyCoeff"]
        assert info["element"] in ["CG", "DG"]
        self.element = info["element"]
        self.fem_deg = info["fem_deg"]

        self.field = TestExponent(self.decay, self.M, mean=self.mean,
                                  scale=self.scale, nT=self.nT, element=self.element,
                                  fem_deg=self.fem_deg, useDarcyCoeff=self.useDarcyCoeff)

    def evaluate(self, y):
        """ FEM coefficiens for one specific parameter realization. """
        kappa = self.field.realisation(y, exp=False)
        return kappa.vector().get_local(), y

    def evaluate_exp(self, y):
        """ FEM coefficiens for one specific parameter realization. """
        kappa = self.field.realisation(y, exp=True)
        return kappa.vector().get_local(), y

    def evaluate_in_points(self, x, y, exp=False):
        """ Evaluate h(x,y) / exp(h(x,y)). """
        if x.ndim == 1:
            x.shape = 1, -1
        if y.ndim == 1:
            y.shape = 1, -1
        assert x.ndim == 2 and y.ndim == 2
        assert x.shape[0] == 1 or y.shape[0] == 1
        coeffs = self.get_coefficients(y, exp=exp)
        if x.shape[0] == self.field.space.dim():
            dofs = fem.dof_coords(self.field.space, self.field.mesh)

        # if x are Lagrange points, then evaluation values are the coefficients
        if np.allclose(x, dofs):
            return np.squeeze(coeffs)

        # evaluate coefficients in arbitrary (non-Lagrange) points
        ret = np.zeros((y.shape[0], x.shape[0]))
        for i, coeff in enumerate(coeffs):
            ret[i] = fem.eval_fencis_function(x, coeff, self.field.space)
        ret = np.squeeze(ret)
        if ret.ndim == 0:
            ret = ret.item()  # make 0 dim array to float

        return np.array(ret)

    def get_coefficients(self, y, exp=False):
        """ 
        Compute the FEM coefficients of the affine-normal exponent of the
        diffusion coefficient h for given parameter samples.
        """
        if y.ndim == 1:
            y.shape = 1, -1
        assert y.ndim == 2
        if exp == True:
            coeffs = Parallel(self.evaluate_exp)
        else:
            coeffs = Parallel(self.evaluate)
        ret = coeffs(y)
        c, y = transpose(ret)
        return np.array(c)


def generate_samples(p, N=None, ys=None, hs=None, exphs=None,
                     exphs_scaled=None, ws=None):
    """ Generate samples of log-normal Darcy diffusion coefficient. """
    if N is None:
        N = 1000
    M = p.info["M"]
    if ys is None:
        ys = np.zeros([0, M])
    if hs is None:
        hs = np.zeros([0, p.field.space.dim()])
    if exphs is None:
        exphs = np.zeros([0, p.field.space.dim()])
    if exphs_scaled is None:
        exphs_scaled = np.zeros([0, p.field.space.dim()])
    assert ys.shape[0] == hs.shape[0] == exphs.shape[0] == exphs_scaled.shape[0]

    ys_new = np.random.multivariate_normal(np.zeros(M), np.eye(M), N)
    hs_new = p.get_coefficients(ys_new, exp=False)
    exphs_new = p.get_coefficients(ys_new, exp=True)

    # NOTE this only works if h has mean zero
    assert np.allclose(p.info["mean"], 0.0)
    ys_scaled = ys_new * 2**(-p.info["ITER"])
    exphs_scaled_new = p.get_coefficients(ys_scaled, exp=True)

    ys = np.concatenate([ys, ys_new], axis=0)
    hs = np.concatenate([hs, hs_new], axis=0)
    exphs = np.concatenate([exphs, exphs_new], axis=0)
    exphs_scaled = np.concatenate([exphs_scaled, exphs_scaled_new], axis=0)

    ws = np.full(ys.shape[0], 1/ys.shape[0])

    return ys, hs, exphs, exphs_scaled, ws


def get_sparsity(tensor):
    core = np.asarray(tensor)
    nnz = np.count_nonzero(core)
    return nnz / core.size


def TTGAexp_femIC_global(p, h_reco, dims, x0, y0, init_ranks=None, EPS=1e-12,
                         APPENDIX=None):
    exph0_scaled = p.evaluate_in_points(x0, y0*2**(-p.info["ITER"]), exp=True)

    # scale affine exponent h
    with timeit("scale exponent h: {:4.2f} s"):
        h_reco_scaled = xe.TTTensor(h_reco)*2**(-p.info["ITER"])

    # build global operator and rhs
    log("build operators W0, b ...")
    with timeit("{:4.2f} s"):
        W0, b = expTT.build_system_inexactFEMIC(
            dims, dims, h_reco_scaled, x0, y0*2**(-p.info["ITER"]),
            exph0_scaled, p.field.space, mesh=p.field.mesh, weight=1)
        log(f"    W0 ranks: {W0.ranks()}")
        if APPENDIX is not None and os.path.isfile(f"../data/W0_round/W0_{APPENDIX}.npy"):
            log("  load rounded operator W ...")
            W0 = tt.load(f"../data/W0_round/W0_{APPENDIX}.npy")
        else:
            log("  round operator W ...")
            W0.round(EPS)
            # femTT.round(W0, EPS)
            tt.save(f"../data/W0_round/W0_{APPENDIX}.npy", W0)
        log(f"    W0 ranks: {W0.ranks()}")
        log(f"    FEM-core is_sparse: {W0.get_component(0).is_sparse()}")
        if not W0.get_component(0).is_sparse():
            exit()
        log(f"    FEM-core sparsity:  {get_sparsity(W0.get_component(0))}")

    # random initial guess
    with timeit("generate initial guess: {:4.2f} s"):
        tmp_dims = [p.field.space.dim()] + dims
        if init_ranks is None:
            init_ranks = [10] * p.info["M"]
        assert len(init_ranks) == h_reco.order()-1
        u_start = xe.TTTensor.random(tmp_dims, init_ranks)
        u_start = u_start / xe.frob_norm(u_start)
        log(f"  dims:  {u_start.dimensions}")
        log(f"  ranks: {u_start.ranks()}")

    # run optimization
    log("run ALS ...")
    with timeit("... {:4.2f} s"):
        if True:
            u_reco_expTT_scaled = expTT.run_simpleALS(
                A=W0, u_start=u_start, b=b, verbose=2
            )
        else:
            u_reco_expTT_scaled = xe.TTTensor(u_start)
            xe.ALS_SPD(W0, u_reco_expTT_scaled, b)
    u_reco_expTT_scaled.round(EPS)  # round to get unneccesary large ranks down

    return u_reco_expTT_scaled


def rescale_reco(p, u_reco_scaled, maxDeg, EPS=1e-12):
    log("rescale TTTensor coefficients and project stoch. dims:")
    M = p.info["M"]
    with timeit("... {:4.2f} s"):
        u_reco_expTT = xe.TTTensor(u_reco_expTT_scaled)
        u_reco_expTT.round(EPS)
        for i in range(p.info["ITER"]):
            log(f"  Iteration: {i+1}/{p.info['ITER']}")
            project_dims = [maxDeg]*M
            u_reco_expTT = femTT.power(u_reco_expTT, 2, eps=EPS,
                                       project_dims=project_dims)

            log(f"    dims:  {u_reco_expTT.dimensions}")
            log(f"    ranks: {u_reco_expTT.ranks()}")

            ys_test = np.random.multivariate_normal(
                np.zeros(M), np.eye(M), 250)
            ys_scaled = ys_test * 2**(-p.info["ITER"]+i+1)
            exphs_test = p.get_coefficients(ys_scaled, exp=True)
            abs_err, rel_err = compute_absRel_error(
                ys_test, exphs_test, u_reco_expTT, p.field.space)
            log(f"    error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    return u_reco_expTT


def compute_absRel_error(ys, us, reco, space, norm="L2"):
    """ Compute absolute and relative error.

    compute the L2-L2 or L2-Linf error of a tt approximation and the exact h
    (or exp h) over the domain.
    """
    ws = np.ones(ys.shape[0])/ys.shape[0]
    if norm == "L2":
        abs_err = femTT.compute_L2L2_error(reco, ys, ws, us, space, "normal")
        rel_err = abs_err / femTT.compute_L2L2_norm(ys, us, space, ws=ws)
    elif norm == "Linf":
        abs_err = femTT.compute_L2Linf_error(reco, ys, ws, us, "normal")
        rel_err = abs_err / femTT.compute_L2Linf_norm(ys, us, ws=ws)

    return abs_err, rel_err


if __name__ == "__main__":
    sys.stdout = Logger(fileName="expTT_logNormal_vmc", filePath="../log/")

    log("setup problem")
    # --------------------------------------------------------------------------
    # setup

    NTs = [50, 50, 50, 50, 20, 80, 100]  # number of triangles
    FDs = [1, 1, 1, 1, 1, 1, 1]  # finite element degree
    Ms = [5, 10, 15, 20, 20, 20, 20]  # number of Parameters

    # NTs = [30, 30]  # number of triangles
    # FDs = [1,  1]  # finite element degree
    # Ms = [5,  5]  # number of Parameters

    decay, mean = 2, 0.0
    assert len(NTs) == len(FDs) == len(Ms)
    os.makedirs("../data", exist_ok=True)
    os.makedirs("../data/W0_round/", exist_ok=True)
    os.makedirs("../data/u_recos/", exist_ok=True)
    os.makedirs("../data/u_recos_scaled/", exist_ok=True)
    os.makedirs("../data/u_recos_vmc/", exist_ok=True)

    for j in range(len(Ms)):
        # FEM stuff
        nTriangles = NTs[j]
        fem_deg = FDs[j]
        M = Ms[j]
        element = "DG" if fem_deg == 0 else "CG"

        log("__"*40)
        log("")
        log(f"RUN: {j+1}/{len(Ms)}    --    nT={nTriangles}, fem_deg={fem_deg}, M={M}")

        # stoch. stuff
        dims = [6]*M
        log(f"dims:    [{max(dims)}]*{M}")

        # exponent info
        scale, ITER, useDarcyCoeff = 2**(0), 4, True
        log(f"scale:   {scale}")
        log(f"ITER:    2**(-{ITER})")

        # define exponent
        info = {"M": M, "decay": decay, "mean": mean, "scale": scale,
                "nT": nTriangles, "element": element, "fem_deg": fem_deg,
                "useDarcyCoeff": useDarcyCoeff, "ITER": ITER}
        p = Problem(info)
        APPENDIX = f"{element}{fem_deg}-{nTriangles}_decay-{decay}_M{M}"
        log(f"FE-space: {element}-{fem_deg}")
        log(f"FE-dofs: {p.field.space.dim()}")

        # IC
        x0 = fem.dof_coords(p.field.space, p.field.mesh)  # fem-dofs
        y0 = np.zeros((1, M))
        exph0 = p.evaluate_in_points(x0, y0, exp=True)

        # plot exponent
        coeff = p.evaluate(np.random.normal(0, 1, M))
        fig, ax = plt.subplots(nrows=1, ncols=1)
        # fem.plot_fenics_function(ax, coeff[0], p.field.space, p.field.mesh)
        fem.plot_fenics_function(ax, coeff[0], p.field.space)
        os.makedirs("../img", exist_ok=True)
        plt.savefig("../img/expTT_rescaling_exponent.png")

        N_test = 1000
        log(f"sample {N_test} test data ...")
        with timeit("... {:4.2f} s"):
            ys_test, hs_test, exphs_test, exphs_test_scaled, ws_test = generate_samples(
                p, N=N_test)

        # --------------------------------------------------------------------------
        # reconstruct u = exp h -- naive
        log("reconstruct exp(h) with naive VMC")

        file_name = f"../data/u_recos_vmc/u_reco_vmc_{APPENDIX}.npy"
        if os.path.isfile(file_name):
            log(f"load u_reco: {file_name} ...")
            u_reco_naive = tt.load(file_name)
        else:
            N = 50_000
            log(f"generate {N} samples ...")
            ys, hs, exphs, exphs_scaled, ws = generate_samples(p, N=N)

            log("run uq_ra_adf ...")
            with timeit("... {:4.2f} s"):
                u_reco_naive = tt.reconstruct(
                    dims=dims, nodes=ys.T, weights=ws, values=exphs,
                    dist="normal", label="naive VMC", verbose=True
                )
            tt.save(file_name, u_reco_naive)
            log(f"save to: {file_name}")

        abs_err, rel_err = compute_absRel_error(
            ys_test, exphs_test, u_reco_naive, p.field.space)
        log(f"error-L2L2 (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")
        abs_err, rel_err = compute_absRel_error(
            ys_test, exphs_test, u_reco_naive, p.field.space, norm="Linf")
        log(f"error-L2Linf (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")

    # from IPython import embed; embed()
    # exit()
