# -*- coding: utf-8 -*-
import sys, os, time
import numpy as np
import scipy
from copy import deepcopy
import matplotlib.pyplot as plt
from matplotlib import ticker

from fenics import *
import xerus as xe

from context import src, avmc_intrusive
from src.misc import Logger, log, timeit, herm_triple_product
from src.utils.fem_tools import sample_diffusion_coeff
from src.utils.tt_tools import reconstruct, eval_approx, get_initial_guess
from src.problem.equation.darcy import Problem
from avmc_intrusive import compute_approx_error_coeff

plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}', r'\usepackage{amssymb}']

# ______________________________________________________________________________
# assembling polynomial basis functions / matrix
fac = lambda n: float(np.math.factorial(n))
pdf = lambda x: np.exp(-x**2/2)/np.sqrt(2*np.pi) # density for standard normal distribution
c_pdf = lambda n: 1 / np.sqrt(float(fac(n))) # this is P_n = c(n) * H_n
hermite = lambda deg: np.polynomial.hermite_e.HermiteE.basis(deg)

def get_2d_normalized_hermite_basis(d):
    assert len(d) == 2
    P = [c_pdf(deg)*hermite(deg) for deg in range(max(d)+1)]
    def basis_matrix(y, P=P, d=d):
        P_val_0 = np.array([p(y[:,0]) for p in P[:d[0]+1]]).T          # shape (y.shape[1], d1+1)
        P_val_1 = np.array([p(y[:,1]) for p in P[:d[1]+1]]).T          # shape (y.shape[1], d2+1)
        P_val = np.expand_dims(P_val_0, 2)*np.expand_dims(P_val_1, 1)  # shape (y.shape[1], d1+1, d2+1)
        return P_val
    return basis_matrix

def inner_product_hermite(d1, d2, normalized=True):
    d = max(d1,d2)
    val = np.diag([1 if normalized else 1/c_pdf(j)**2 for j in range(d+1)])
    return val[:d1+1, :d2+1]

# ______________________________________________________________________________
# build K

def assemble_Kappa(A,B,C):
    K = np.zeros([A+1,C+1,B+1])
    for i in range(A+1):
        for j in range(B+1):
            for k in range(C+1):
                K[i,k,j] = herm_triple_product(i, j, k, normalized=True)
    return K

def assemble_tensor_K(A,B,C):
    """
    Order-6 tensor obtained by kappa(A[0],B[0],C[0]) \otimes kappa(A[1],B[1],C[1])
    """
    K1, K2 = assemble_Kappa(A[0],C[0],B[0]), assemble_Kappa(A[1],C[1],B[1])
    s1, s2 = K1.shape, K2.shape
    K1 = np.reshape(K1, (s1[0], 1, s1[1], 1, s1[2], 1))
    K2 = np.reshape(K2, (1, s2[0], 1, s2[1], 1, s2[2]))
    return K1*K2

# ______________________________________________________________________________
# build M

def assemble_matrix_M(d2,d1,normalized=True):
    assert d1 >= 0 and d2 >= 0
    val = np.zeros((d2+1,d1+1))
    if not normalized: 
        norm = [1/norm_rho(j) for j in range(max(d1+1,d2+1))]
    else:
        norm = [1 for j in range(max(d1+1,d2+1))] 
    for i in range(min(d1,d2)+1):
        val[i,i+1] = np.sqrt(i+1)*norm[i]*norm[i+1]
    return val

def assemble_tensor_M(d2, d1, j=0, normalized=True): 
    assert len(d1) == len(d2) == 2
    assert 0 <= j <= 1
    for i in range(2):
        if i == j: assert np.abs(d2[i]-d1[i]) == 1
        else: assert np.abs(d2[i]-d1[i]) == 0
    Ms = []
    for i in range(2):
        if i == j:
            Ms.append(assemble_matrix_M(d2[i],d1[i],normalized=True))
        else:
            Ms.append(inner_product_hermite(d2[i],d1[i],normalized=True))
    M1 = np.reshape(Ms[0], (Ms[0].shape[0],1,Ms[0].shape[1],1))
    M2 = np.reshape(Ms[1], (1,Ms[1].shape[0],1,Ms[1].shape[1]))
    return M1*M2

# ______________________________________________________________________________
# build S

def assemble_tensor_S(da, B, j, normalized=True):
    db = np.array(np.array(B.shape)-1, dtype=int)                               # B shape is (db+1)
    ej = np.array([1 if i==j else 0 for i in range(2)], dtype=int)
    Mb = assemble_tensor_M(db-ej, db, j, normalized=normalized)                 # shape (db+1-ej, db+1)
    K = assemble_tensor_K(db-ej, da+db-ej, da)                                  # shape (db+1-ej, da+eb+2-ej, da+1)
    
    Mb_B = np.einsum("ijkl,kl->ij", Mb, B)                                     # shape (db+1-ej)
    S = np.einsum("ij,ijklmn->klmn", Mb_B, K)                                   # shape (da+db+2-ej, da+1)
    return S

def assemble_tensor_G(d1, d2, normalized=True):
    tmp = inner_product_hermite(d1[0],d2[0], normalized=normalized)
    G1 = np.reshape(
            inner_product_hermite(d1[0],d2[0], normalized=normalized), 
            (d1[0]+1,1,d2[0]+1,1)
            )
    G2 = np.reshape(
            inner_product_hermite(d1[1],d2[1], normalized=normalized), 
            (1,d1[1]+1,1,d2[1]+1)
            )
    return G1*G2

def project_tensor_S(S,G):
    # G shape is (da+1-ej, da+db+2-ej)
    # S shape is (da+db+2-ej, da+1)
    return np.einsum("ijkl,klmn->ijmn", G, S)

# ______________________________________________________________________________
# define functions b, a

def b_coeff(d, random=False):
    """
    Return an (db[0]+1, db[1]+1) coefficient array.
    """
    if random:
        # val = 0.05*np.random.randint(-5,5,d+1)
        val = np.random.uniform(-.5,.5,d+1)
    else:
        assert d[0] <= 4 and d[1] <= 4
        val = 0.1*np.array([[  0,   2,  .1,  .1, -1],
                            [ -1,   1, -.2,  .4, -2],
                            [-.1,   0, -.4,  .4,  0],
                            [-.3, -.5,  .3,  .3, -5],
                            [ -4,  -4,  -4,  -5, -5]]
                           )
        val = 0.1*val
        val = val[:d[0]+1,:d[1]+1]
    return val

def b(y, db, B):
    coeffs = np.expand_dims(B,0)
    P = get_2d_normalized_hermite_basis(db)
    Py = P(y)
    return np.sum(coeffs*Py, axis=(1,2))

def a(y, db, B):
    return np.exp(b(y, db, B))

# ______________________________________________________________________________
if __name__ == "__main__":
    print()
    """
    [-] reconstruct a naiv + compute L2L2-error
    """

    # --------------------------------------------------------------------------
    # setup
    db = np.array([1,1], dtype=int)
    da = np.array([10,10], dtype=int)
    j = 0
    B = b_coeff(db, random=False)
    log("db: ",db," -- ",tuple(db+1))
    log("da: ",da," -- ",tuple(da+1))
    log("j:  ",j)
    
    # --------------------------------------------------------------------------
    # generate samples
    n_samples, n_samples_test = 1000, 1000
    
    y_train = np.random.normal(0,1,(n_samples,2))
    w_train = np.ones(n_samples)/n_samples
    b_train = b(y_train, db, B)
    a_train = a(y_train, db, B)
    
    y_test = np.random.normal(0,1,(n_samples_test,2))
    w_test = np.ones(n_samples_test)/n_samples_test
    b_test = b(y_test, db, B)
    a_test = a(y_test, db, B)

    # --------------------------------------------------------------------------
    # reconstruct a (exp-TT)
    
    # assemble L'
    ej = np.array([1 if i==j else 0 for i in range(2)], dtype=int)
    
    M = assemble_tensor_M(da-ej, da, j=j, normalized=True)
    S = assemble_tensor_S(da, B, j, normalized=True)
    G = assemble_tensor_G(np.array(M.shape[:2])-1, np.array(S.shape[:2])-1)
    GS = project_tensor_S(S,G)
    Lp = M-GS
    
    log("M deg/shape: ",np.array(M.shape)-1," -- ",M.shape)
    log("S deg/shape: ",np.array(S.shape)-1," -- ",S.shape)
    log("G deg/shape: ",np.array(G.shape)-1," -- ",G.shape)
    log("GS deg/shape:",np.array(GS.shape)-1," -- ",GS.shape)
    log("Lp deg/shape:",np.array(Lp.shape)-1," -- ",Lp.shape)

    # assemble L, F
    y_ic = np.random.normal(0,1,(Lp.shape[1],2))
    y_ic = np.random.normal(0,1,(Lp.shape[1],2))
    P = get_2d_normalized_hermite_basis(da)
    IC = np.expand_dims(P(y_ic),0)
    F0 = np.zeros(da+1-ej)
    F1 = np.expand_dims(np.exp(b(y_ic, db, B)),0)
    
    F = np.concatenate([F0, F1],axis=j)
    L = np.concatenate([Lp, IC], axis=j)
    
    log("IC deg/shape:",IC.shape)
    log("F shape:",F.shape)
    log("L shape:",L.shape)

    # solve LA = F
    sL, sF = L.shape, F.shape
    Lm, Fm = L.reshape(sL[0]*sL[1], sL[2]*sL[3]), F.reshape(sF[0]*sF[1])
    Am = np.linalg.solve(Lm, Fm)
    A = np.reshape(Am, sF)
    
    log("A shape:",A.shape)

    # test approximation
    Ay = np.sum(np.expand_dims(A,0)*P(y_test), axis=(1,2))
    log("Ay shape:",Ay.shape)
    
    
    # --------------------------------------------------------------------------
    # reconstruct a (naiv)
    sdim = [int(d)+1 for d in da] 
    a_tt_naiv = reconstruct(
            dims=sdim, nodes=y_train.T, weights=w_train, values=a_train.reshape(-1,1), 
            init=None, dist="normal", label="a naiv", verbose=True
            )
    a_tt_naiv_eval = eval_approx(a_tt_naiv, y_test, dist="normal").reshape(-1)

    # --------------------------------------------------------------------------
    # evaluate errors
    print()
    
    diff = np.reshape((a_test-Ay)**2, [-1])
    err_expTT = np.sum(w_test*diff)
    diff = np.reshape((a_test - a_tt_naiv_eval)**2, [-1])
    err_naiv = np.sum(w_test*diff)
    
    log("error exp-TT:",err_expTT)
    log("error naiv:  ",err_naiv)
    
    # __________________________________________________________________________
    # plot
    shape = 30
    y = np.linspace(-6,6,shape)
    XX,YY = np.meshgrid(y,y)
    yy = np.array([XX.reshape(-1),YY.reshape(-1)]).T
    
    a_yy = a(yy, db, B).reshape(shape,shape)
    a_naiv_yy = eval_approx(a_tt_naiv, yy, dist="normal").reshape(shape,shape)
    a_expTT_yy = np.sum(np.expand_dims(A,0)*P(yy), axis=(1,2)).reshape(shape,shape)
    
    fig, ax = plt.subplots(figsize=(15,5),nrows=1, ncols=3)
    
    im = ax[0].contourf(y, y, a_yy, 10)
    fig.colorbar(im, ax=ax[0])
    ax[0].set_title("exact a")
    
    tmp = np.abs(a_yy-a_naiv_yy)/np.abs(a_yy)
    tmp[tmp < 1e-16] = 1e-16
    im = ax[1].contourf(y, y, tmp, 10, 
            locator=ticker.LogLocator())
    fig.colorbar(im, ax=ax[1])
    ax[1].set_title("relativ pointwise error naiv")
    ax[1].set_xlabel("error L2: {:4.2e}".format(err_naiv))
    
    tmp = np.abs(a_yy-a_expTT_yy)/np.abs(a_yy)
    tmp[tmp < 1e-16] = 1e-16
    im = ax[2].contourf(y, y, tmp, 10,
            locator=ticker.LogLocator())
    fig.colorbar(im, ax=ax[2])
    ax[2].set_title("relative pointwise error exp-TT")
    ax[2].set_xlabel("error L2: {:4.2e}".format(err_expTT))

    fig.suptitle("2D exp-TT reco")
    
    plt.savefig("../img/test_exp_tt_2d.png")
    
    print()
