# -*- coding: utf-8 -*-
import sys, os, time
import numpy as np
import scipy
from copy import deepcopy
import matplotlib.pyplot as plt

from fenics import *
import xerus as xe

from context import src, avmc_intrusive
from src.misc import Logger, log, timeit, herm_triple_product
from src.utils.fem_tools import sample_diffusion_coeff
from src.utils.tt_tools import reconstruct, eval_approx, get_initial_guess
from src.problem.equation.darcy import Problem
from avmc_intrusive import compute_approx_error_coeff

plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}', r'\usepackage{amssymb}']

# ------------------------------------------------------------------------------
fac = lambda n: float(np.math.factorial(n))
rho = lambda x: np.exp(-x**2/2)/np.sqrt(2*np.pi) # density for standard normal distribution
norm_rho = lambda n: 1 / np.sqrt(float(fac(n)))
hermite = lambda deg: np.polynomial.hermite_e.HermiteE.basis(deg)

def compute_matrix_M(d2,d1,normalized=True):
    assert d1 >= 0 and d2 >= 0
    val = np.zeros((d2+1,d1+1))
    if not normalized: 
        norm = [1/norm_rho(j) for j in range(max(d1+1,d2+1))]
    else:
        norm = [1 for j in range(max(d1+1,d2+1))] 
    for i in range(min(d1,d2)+1):
        val[i,i+1] = np.sqrt(i+1)*norm[i]*norm[i+1]
    return val

def compute_matrix_D(d, normalized=True):
    assert d >= 0
    val = np.zeros((d+1,d+1))
    if not normalized: 
        norm = [1/norm_rho(j) for j in range(d+1)]
    else:
        norm = [1 for j in range(d+1)] 
    for i in range(d):
        val[i,i+1] = np.sqrt(i+1)*norm[i]*norm[i+1]
    return val

def compute_matrix_K(A,B,C):
    K = np.zeros([A+1,C+1,B+1])
    for i in range(A+1):
        for j in range(B+1):
            for k in range(C+1):
                K[i,k,j] = herm_triple_product(i, j, k, normalized=True)
    return K
    
def compute_matrix_S(d2,d1):
    assert d1 >= 0 and d2 >= 0
    B = b(0, ret_coef=True)
    d_b = B.size-1
    K = compute_matrix_K(d_b-1, d1, d2)
    M = compute_matrix_M(d_b-1, d_b)
    return np.einsum("jab,ji,i->ab", K, M, B)

def compute_matrix_GH(d2, d1, normalized=True):
    assert d1 >= d2 >= 0
    if normalized: val = np.eye(d1+1)
    else: val = np.diag([1/norm_rho(j)**2 for j in range(d1+1)])
    return val[:d2+1], val[d2+1:]

# ------------------------------------------------------------------------------
def compute_matrix_M_slow(d2,d1,normalized=True):
    assert d1 >= 0 and d2 >= 0
    val = np.zeros((d2+1,d1+1))
    if normalized:
        P = [norm_rho(deg)*hermite(deg) for deg in range(np.max([d1,d2])+1)]
    else:
        P = [hermite(deg) for deg in range(np.max([d1,d2])+1)]
    dP = [p.deriv(1) for p in P]
    for i in range(d2+1):
        for j in range(d1+1):
            f = lambda x: P[i](x)*dP[j](x)*rho(x)
            val[i,j] = scipy.integrate.quad(f, -np.inf, np.inf)[0]
    return val

def compute_matrix_G_slow(d2, d1, normalized=True):
    assert d1 >= 0 and d2 >= 0
    val = np.zeros((d2+1,d1+1))
    if normalized:
        P = [norm_rho(deg)*hermite(deg) for deg in range(np.max([d1,d2])+1)]
    else:
        P = [hermite(deg) for deg in range(np.max([d1,d2])+1)]
    for i in range(d2+1):
        for j in range(d1+1):
            f = lambda x: P[i](x)*P[j](x)*rho(x)
            val[i,j] = scipy.integrate.quad(f, -np.inf, np.inf)[0]
    return val

# ------------------------------------------------------------------------------
def b(y, ret_coef=False):
    d_b = 3
    B = 0.1*np.array([1,2,-1,-.3,4,3,1,2,4,2,1])
    B = B[:d_b+1]
    P = [norm_rho(deg)*hermite(deg) for deg in range(d_b+1)]
    val = np.sum([B[deg]*P[deg](y) for deg in range(d_b+1)], axis=0)
    if ret_coef: 
        return B
    else:
        return val

def a(y): return np.exp(b(y))

def eval_coeff(ys, coeffs, P):
    coeffs = np.reshape(coeffs, (-1,1))
    bs = np.array([p(ys) for p in P[:coeffs.size]])
    return np.einsum("ijk,il->jkl",bs,coeffs)[:,0,:]

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    print()
    
    d_a, d_b = 10, b(0, ret_coef=True).size-1
    P = [norm_rho(deg)*hermite(deg) for deg in range(100)]
    dP = [p.deriv(m=1) for p in P]
    
    # __________________________________________________________________________
    # generate samples
    y_s = np.random.normal(0,1,[1000,1])
    w_s = np.ones(y_s.shape[0])/y_s.shape[0]
    b_s = b(y_s)
    a_s = a(y_s)
    
    y_s_test = np.random.normal(0,1,[1000,1])
    w_s_test = np.ones(y_s_test.shape[0])/y_s_test.shape[0]
    b_s_test = b(y_s_test)
    a_s_test = a(y_s_test)
    
    # __________________________________________________________________________
    # reconstruct b
    b_tt = reconstruct(
            dims=[d_b+1], nodes=y_s.T, weights=w_s, values=b_s, init=None,
            dist="normal", label="b", verbose=True
            )
    b_tt_eval = eval_approx(b_tt, y_s_test, dist="normal")
    diff = np.reshape((b_s_test - b_tt_eval)**2, [-1])
    err_L2_b = np.sum(w_s_test*diff)
    
    cores = [np.array(b_tt.get_component(j)) for j in range(b_tt.order())]
    for j,core in enumerate(cores):
        print(f"b - core {j}:")
        print(core)
    
    # __________________________________________________________________________
    # reconstruct a naively
    a_tt_naiv = reconstruct(
            dims=[d_a+1], nodes=y_s.T, weights=w_s, values=a_s, init=None,
            dist="normal", label="a naiv", verbose=True
            )
    a_tt_naiv_eval = eval_approx(a_tt_naiv, y_s_test, dist="normal")
    diff = np.reshape((a_s_test - a_tt_naiv_eval)**2, [-1])
    err_L2_a_naiv = np.sum(w_s_test*diff)
    
    
    # __________________________________________________________________________
    # reconstruct a by exp_tt
    M = compute_matrix_M(d_a-1,d_a)
    S = compute_matrix_S(d_a+d_b-1, d_a)
    G,H = compute_matrix_GH(d_a-1,d_a+d_b-1)
    dL = M-np.einsum("ij,jk",G,S)
    P0 = np.array([[P[j](0) for j in range(d_a+1)]])
    
    L = np.concatenate([dL, P0], axis=0)
    RHS = np.concatenate([np.zeros(d_a), [np.exp(b(0))]])
    A = np.linalg.solve(L,RHS)
    
    a_tt_expTT_eval = eval_coeff(y_s_test, A, P)
    diff = np.reshape((a_s_test - a_tt_expTT_eval)**2, [-1])
    err_L2_a_expTT = np.sum(w_s_test*diff)
    
    print()
    log("Error-L2 b:       {:4.2e}".format(err_L2_b))
    log("Error-L2 a_naiv:  {:4.2e}".format(err_L2_a_naiv))
    log("Error-L2 a_expTT: {:4.2e}".format(err_L2_a_expTT))
    
    # __________________________________________________________________________
    # XXX TESTING Philipp

    B = b(0, ret_coef=True)
    Db = compute_matrix_D(d_b, normalized=True)
    Da = compute_matrix_D(d_a, normalized=True)
    K = compute_matrix_K(d_b, d_a, d_a+d_b)
    Ma = np.einsum("jab,ji,i->ab", K, Db, B)[:d_a+1,:]
    L_p = Da-Ma
    
    print()
    log("TESTING Philipp")
    log("shape L':",L_p.shape)
    log("rank  L':",np.linalg.matrix_rank(L_p))
    log("cond  L':",np.linalg.cond(L_p))
    # from IPython import embed; embed()
    
    # __________________________________________________________________________
    # plot
    yy = np.linspace(-6,6,200).reshape(-1,1)
    a_tt_naiv_yy = eval_approx(a_tt_naiv, yy, dist="normal").reshape(-1)
    a_tt_expTT_yy = eval_coeff(yy, A, P).reshape(-1)
    a_yy = a(yy).reshape(-1)
    
    fig, ax = plt.subplots(figsize=(12,5), nrows=1, ncols=2)
    ax[0].semilogy(yy, a_yy, "--k", lw=3, label="a exact")
    ax[0].semilogy(yy, a_tt_naiv_yy, label="reco naiv")
    ax[0].semilogy(yy, a_tt_expTT_yy, label="reco expTT")
    ax[0].set_title("reco of a")
    ax[0].set_xlabel("$y$")
    ax[0].set_ylabel("$a(y)$")
    ax[0].legend()
    
    ax[1].semilogy(yy, np.abs(a_yy-a_tt_naiv_yy)/np.abs(a_yy), label="naiv")
    ax[1].semilogy(yy, np.abs(a_yy-a_tt_expTT_yy)/np.abs(a_yy), label="expTT")
    ax[1].set_title("pointwise relative error")
    ax[1].set_xlabel("$y$")
    ax[1].legend()

    title = "1D exp-TT reco -- "
    title += "error L2 naiv:   {:4.2e} -- ".format(err_L2_a_naiv)
    title += "error L2 exp-TT: {:4.2e}".format(err_L2_a_expTT)
    plt.suptitle(title)
    
    plt.savefig("../img/test_exp_tt_1d.png")

    print()
