import numpy as np
import xerus as xe

from context import src, avmc_intrusive
from src.misc import timeit, log
import src.utils.tt_tools as tt
import src.utils.tt_exp as expTT

import autoPDB

# ==============================================================================
# utiliy functions


def compute_L2_error(tt_tensor, xs, ys):
    tt_eval = tt.eval_approx(tt_tensor, xs, "normal")
    abs_err = np.sqrt(np.sum((ys-tt_eval)**2)/ys.shape[0])
    rel_err = abs_err / np.sqrt(np.sum(ys**2)/ys.shape[0])
    return abs_err, rel_err


def log_likelihood_eval(ys, gs, delta, cov=1, scale=1):
    assert cov > 0
    if delta.ndim < 2:
        delta.shape = 1, -1
    if gs.ndim < 2:
        gs.shape = -1, 1
    ret = -np.linalg.norm(np.sqrt(cov)*(delta - gs), axis=1)**2
    return ret.reshape(-1, 1)*scale


def log_likelihood(G_TT, delta, cov=1, scale=1):
    if isinstance(delta, float):
        delta = np.array([delta])
    assert delta.ndim == 2
    # NOTE: only works for integral case
    assert delta.size == G_TT.dimensions[0] == 1
    N, dims, M = G_TT.dimensions[0], G_TT.dimensions[1:], G_TT.order()-1

    # convert delta to TTTensor of correct shape
    tt1 = xe.TTTensor(xe.Tensor.from_buffer(delta.flatten()))
    tt2 = xe.TTTensor.dirac(dims, [0]*M)
    delta_TT = xe.dyadic_product([tt1, tt2])

    # compute difference: delta-G(y)
    diff = tt.convert(delta_TT - G_TT)

    # compute product
    # use something similar to femTT.multiply()
    core0 = diff[0].reshape(1, -1)
    core0sq = np.ascontiguousarray((core0.T * core0).reshape(1, 1, -1))

    # stoch. components
    tail = tt.compute_product_coeff_tensor(diff[1:], diff[1:], "normal",
                                           project_dims=None)

    # join det. + stoch. cmp together
    assert tail[0].shape[0] == core0sq.shape[-1]
    ret = - tt.convert([core0sq] + tail)
    ret.round(1e-16)

    return ret*scale


def expTT_reconstruction(da, dt, h, x_0, method=None, verbose=False):
    """
    see expTT.expTT_reconstruction()
    """
    M = len(da)

    # build operator and rhs
    W0, b = expTT.build_system_inexactIC(da, dt, h, x_0, method=method)
    W0.round(1e-16)

    # random initialization
    u_start = xe.TTTensor.random(da, [10]*(M-1) if M > 1 else [])
    u_start = u_start / xe.frob_norm(u_start)

    # run simple ALS
    ret = expTT.run_simpleALS(A=W0, u_start=u_start, b=b, verbose=verbose)

    # run xerus ALS
    # ret = xe.TTTensor(u_start)
    # xe.ALS_SPD(W0, ret, b)

    ret.round(1e-12)  # round to get unneccesary large ranks down

    ret = tt.add_dimension_left(ret)  # add FEM component

    return ret


def myFunc(ys):
    decay = np.array([1/(j+1) for j in range(ys.shape[1])]).reshape(1, -1)
    decay = np.ones((ys.shape[0], 1)) * decay
    assert np.allclose(decay.shape, ys.shape)
    ret = np.sum(decay*ys, axis=1)
    return ret.reshape(-1, 1)


# ==============================================================================
if __name__ == "__main__":
    """ Test expTT reconstuction for log-likelihood in any dimension.

    Assume here that the exponent h is given by
                h(x) = -|| x - mu ||^2 / (2*sigma^2)

    """

    # setup

    M = 10  # number of random variables
    dim = 4
    da, dt = [dim]*M, [dim]*M
    y0 = np.zeros((1, M))
    scale = 1/2**5

    # --------------------------------------------------------------------------
    # load samples
    # TODO split in training, test and measurement data
    # TODO add noise to measurement data (check noise in Manuel paper)
    # TODO get test data for (log-)likelihood as well

    data = np.load("data/functional_mean.npz")
    samples, values = data["samples"], data["values"].reshape(-1, 1)

    nSamples = 1000
    ys = samples[:min(nSamples, 10000)]
    ys_test = samples[10000:11000]
    ys_true = samples[11000:]

    ws = np.ones(ys.shape[0])/ys.shape[0]

    gs = values[:min(nSamples, 10000)]
    gs_test = values[10000:11000]
    gs_true = values[11000:]

    sigma = 2*np.std(values)
    noise = np.random.normal(0, sigma**2, gs_true.shape)
    delta = gs_true + noise

    hs = log_likelihood_eval(ys, gs, delta, cov=1, scale=1)
    hs_test = log_likelihood_eval(ys_test, gs_test, delta, cov=1, scale=1)

    us = np.exp(hs)
    us_test = np.exp(hs_test)

    # from IPython import embed; embed(); exit() # debug line
    # --------------------------------------------------------------------------
    # reconstruct solution G(y) with VMC

    G_TT = tt.reconstruct(
        dims=[2]*M, nodes=ys.T, weights=ws, values=gs, dist="normal",
        init=None, label="G_TT", verbose=False
    )
    G_TT.move_core(1)

    abs_err, rel_err = compute_L2_error(G_TT, ys_test, gs_test)
    log(f"error G_TT: {abs_err:4.2e}/{rel_err:4.2e}")

    # --------------------------------------------------------------------------
    # build log-likelihood TT

    # This is h(y) = - || delta - G(y) ||_s^2
    h_TT = log_likelihood(G_TT, delta, cov=1, scale=1)
    h_TT.move_core(1)

    abs_err, rel_err = compute_L2_error(h_TT, ys_test, hs_test)
    log(f"error h_TT: {abs_err:4.2e}/{rel_err:4.2e}")

    # --------------------------------------------------------------------------
    # expTT reconstruction for TT case
    # TODO adapt to code above

    u_TT = expTT_reconstruction(
        da, dt, h_TT, y0, method=None, verbose=2
    )

    abs_err, rel_err = compute_L2_error(u_TT, ys_test, us_test)
    log(f"error u_TT: {abs_err:4.2e}/{rel_err:4.2e}")

    # --------------------------------------------------------------------------
    # from IPython import embed; embed(); exit() # debug line
