import sys, os
import numpy as np
from scipy.special import zeta
import matplotlib.pyplot as plt
import xerus as xe
from tqdm import trange
from copy import deepcopy
from fenics import *

from context import src
from src.misc import timeit, log, transpose, factorial
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as expTT
from src.estimator.intrusive.tt_tail_product import compute_product_coeff_tensor
from src.parallel import Parallel, ParallelizableProblem

import autoPDB

# ==============================================================================

class TestExponent():
    def __init__(self, decay, M, mean=0.0, scale=1.0, nT=20, fem_deg=0, element="DG", useDarcyCoeff=True):
        """
        Affine function similar to Darcy diffusion coefficient, i.e.
            h(x,y) = scale * ( mean + sum_{m=1}^M h_m(x) y_m )
        where
            h_m(x) = 0.9 / \zeta(decay) * m**(-decay) * cos(2\pi B1(m) x_1) * cos(2\pi B2(m) x_2).
        Here, B1 = B2 = 2 for the test case and 
            B1(m) = m - k(m)*( k(m)+1 ) / 2      and     B2(m) = k(m) - B1(m)
        for 
            k(m) = floor[ -1/2 + \sqrt{ 1/4 + 2m } ].
        """
        self.decay = decay
        self.M = M
        self.scale = scale
        self.det_coeff = Expression("C * (cos(2*pi*B1*x[0]) * cos(2*pi*B2*x[1]))", C=1, B1=2, B2=2, degree=10)
        self.mean = mean
        self.useDarcyCoeff = useDarcyCoeff
        
        self.mesh = Mesh(UnitSquareMesh(nT,nT))
        if element == "DG": assert fem_deg == 0 # does not work for DG-n wit n > 0
        self.fem_deg = fem_deg
        self.space = FunctionSpace(self.mesh, element, self.fem_deg)
        
    # NOTE: used to mimic Darcy lognormal coefficient
    def k(self, m): return np.floor(-0.5+np.sqrt(0.25 + 2*m))
    def beta1(self, m): return m - self.k(m) * (self.k(m)+1) / 2
    def beta2(self, m): return self.k(m) - self.beta1(m)

    def realisation(self, y, exp=False):
        """ FEM function for one specific parameter realization. """
        det_coeff = self.det_coeff
        def interpolate_det_coeff(C,B1,B2):
            det_coeff.C, det_coeff.B1, det_coeff.B2 = C, B1, B2
            return interpolate(det_coeff, self.space).vector().get_local()
        def linear(y):
            u = Function(self.space).vector().get_local()
            for m in range(self.M):
                C = 0.9/zeta(self.decay) * (m+1)**(-self.decay)
                if self.useDarcyCoeff == True:
                    B1, B2 = self.beta1(m+1), self.beta2(m+1)
                else:
                    B1, B2 = 2, 2
                u += interpolate_det_coeff(C, B1, B2) * y[m]
            return u
        val = self.scale * (self.mean + linear(y))
        if exp == True: val = np.exp(val)
        ret = Function(self.space)
        ret.vector().set_local(val)
        return ret

class Problem(ParallelizableProblem):
    def __init__(self, info):
        self.info = info
        self.decay = info["decay"]
        self.M = info["M"]
        self.mean = info["mean"]
        self.scale = info["scale"]
        self.nT = info["nT"]
        self.useDarcyCoeff= info["useDarcyCoeff"]
        assert element in ["CG", "DG"]
        self.element = info["element"]
        self.fem_deg = info["fem_deg"]

        self.field = TestExponent(self.decay, self.M, mean=self.mean, 
                scale=self.scale, nT=self.nT, element=self.element, 
                fem_deg=self.fem_deg, useDarcyCoeff=self.useDarcyCoeff)
    
    def evaluate(self, y):
        """ FEM coefficiens for one specific parameter realization. """
        kappa = self.field.realisation(y, exp=False)
        return kappa.vector().get_local(), y
        
    def evaluate_exp(self, y):
        """ FEM coefficiens for one specific parameter realization. """
        kappa = self.field.realisation(y, exp=True)
        return kappa.vector().get_local(), y
        
    def evaluate_in_points(self, x, y, exp=False):
        """ Evaluate h(x,y) / exp(h(x,y)). """
        if x.ndim == 1: x.shape = 1, -1
        if y.ndim == 1: y.shape = 1, -1
        assert x.ndim == 2 and y.ndim == 2
        assert x.shape[0] == 1 or y.shape[0] == 1
        coeffs = self.get_coefficients(y, exp=exp)
        if x.shape[0] == self.field.space.dim():
            dofs = fem.dof_coords(self.field.space, self.field.mesh)
            
        # if x are Lagrange points, then evaluation values are the coefficients
        if np.allclose(x, dofs): return np.squeeze(coeffs)
        
        # evaluate coefficients in arbitrary (non-Lagrange) points
        ret = np.zeros((y.shape[0], x.shape[0]))
        for i, coeff in enumerate(coeffs):
            ret[i] = fem.eval_fencis_function(x, coeff, self.field.space)
        ret = np.squeeze(ret)
        if ret.ndim == 0: ret = ret.item() # make 0 dim array to float
        
        return np.array(ret)
    
    def get_coefficients(self, y, exp=False):
        """ 
        Compute the FEM coefficients of the affine-normal exponent of the
        diffusion coefficient h for given parameter samples.
        """
        if y.ndim == 1: y.shape = 1, -1
        assert y.ndim == 2
        if exp == True:
            coeffs = Parallel(self.evaluate_exp)
        else: 
            coeffs = Parallel(self.evaluate)
        ret = coeffs(y)
        c, y = transpose(ret)
        return np.array(c)

def generate_samples(p, N=None, ys=None, hs=None, ws=None):
    if N is None: N = 1000 
    M = p.info["M"]
    if ys is None: ys = np.zeros([0,M])
    if hs is None: hs = np.zeros([0,p.field.space.dim()])
    assert ys.shape[0] == hs.shape[0]

    ys_new = np.random.multivariate_normal(np.zeros(M), np.eye(M), N)
    hs_new = p.get_coefficients(ys_new, exp=False)
    
    ys = np.concatenate([ys, ys_new], axis=0)
    hs = np.concatenate([hs, hs_new], axis=0)
    ws = np.full(ys.shape[0], 1/ys.shape[0])

    return ys, hs, ws

def generate_initial_guess_cmp(h_fix_mode, taylor_order, dims=None, EPS=1e-12):
    """ 
    Get Taylor Expansion initial guess for ALS.
    Approximate exp of function by power series up to [order] terms:
        exp(x) = sum_{k=0}^{order} 1/k! x^k
    """
    if dims is not None: assert len(dims) == h_fix_mode.order()-1
    ret = tt.convert([np.ones((1,1,1))]*h_fix_mode.order()) # k = 0
    xk = xe.TTTensor(h_fix_mode) # k = 1
    xk.round(1)
    for k in range(1, taylor_order):
        xk_scaled = xk / factorial(k)
        ret = tt_add(ret, xk_scaled)
        if dims is not None: ret = tt.project(ret, [ret.dimensions[0]]+dims)
        ret.round(EPS)
        if k < taylor_order-1: 
            xk = femTT.multiply(xk, h_fix_mode)
    ret.round(EPS)
    return ret

def generate_initial_guess(h_reco, fem_dim, dims, taylor_order, EPS=1e-12):
    u_reco_expTT_scaled = xe.TTTensor([p.field.space.dim()]+dims)
    ret = xe.TTTensor([fem_dim] + dims)
    for dof in trange(fem_dim):

        # fix 0th mode of h_reco_scaled
        h_fix_mode = shrink_fem_cmp(h_reco, pos=dof)
        
        # generate 1D initial guess component by Taylor expansion
        tmp = generate_initial_guess_cmp(h_fix_mode, taylor_order, dims=dims, EPS=EPS)
        
        # pad 0th dimension
        tmp = grow_fem_cmp(tmp, pos=dof, dim=fem_dim)
        
        ret = tt_add(ret, tmp)
        ret.round(EPS)
    return ret

def reconstruct_h(q, N=1000, init=None):
    ys, hs, ws = generate_samples(q, N=N)
    h_reco = tt.reconstruct(
            dims=[2]*q.info["M"], nodes=ys.T, weights=ws, values=hs,
            dist="normal", init=init, label="h reco_tmp", verbose=False
            )
    return h_reco

def prolong_to_dim(tt1, dims):
    assert tt1.order()-1 <= len(dims)
    for j, d in enumerate(tt1.dimensions[1:]): assert d == dims[j]
    M, Mnew = tt1.order()-1, len(dims)
    tt1_cmp = tt.convert(tt1)
    for d in dims[M:]: tt1_cmp.append(np.eye(1,d).reshape(1,d,1))
    return tt.convert(tt1_cmp)

def escape_local_minimum(tt1, q, tol=(0.1,0.2), verbose=True):
    """
    Hier muss noch was getan werden... wir waren zu faul...
        if (1+tol[0])*err_start < get_err(tt1, EPS) < (1+tol[1])*err_start: break
    """
    ys, hs, ws = generate_samples(q, N=1000)
    
    def get_err(tt1, EPS):
        ret = xe.TTTensor(tt1)
        ret.round(EPS)
        return compute_L2L2_error(ys, hs, ret, q.field.space)[0], ret.ranks()
    err_start, _ = get_err(tt1, 0.0)

    log_EPS = -12
    while True:
        EPS = 10**log_EPS
        err, rs = get_err(tt1, EPS)
        if verbose: print(log_EPS, err, rs)
        if (1+tol[0])*err_start < err: break
        if np.allclose(rs, 1): break
        log_EPS /= 2

    ret = xe.TTTensor(tt1)
    ret.round(10**log_EPS)
    
    return ret

def compute_L2L2_error(ys, us, reco, space):
    """
    compute the L2-L2 error of a tt approximation and the exact h (or exp h)
    over the domain.
    """
    ws = np.ones(ys.shape[0])/ys.shape[0]
    abs_err = femTT.compute_L2L2_error(reco, ys, ws, us, space, "normal")
    
    mass_mat = tt.mass_matrix(space)
    err_sq = np.einsum('n,ni,in', ws, us, mass_mat@us.T)
    rel_err = abs_err / np.sqrt(err_sq)
    
    return abs_err, rel_err

# ==============================================================================
if __name__ == "__main__":

    log("setup problem")
    log("")
    # --------------------------------------------------------------------------
    # setup 
    n_samples_reco = 1000
    M = 15
    nTriangles = 2
    
    
    fem_deg = 1 # use DG if 0 else CG
    element = "DG" if fem_deg == 0 else "CG"
    
    info = {"M":M, "decay":2, "mean":0.0, "scale":1, "nT":nTriangles, 
            "element":element, "fem_deg":fem_deg, "useDarcyCoeff":True,
            "ITER":0}
    p = Problem(info)
    
    log(f"stoch dim (M): {M}")
    log(f"reco samples:  {n_samples_reco}")
    log(f"FEM dim:       {p.field.space.dim()}")
    log(f"FEM element:   {element}-{fem_deg}")

    # --------------------------------------------------------------------------
    log("__"*40)
    log("")
    log("reconstruct with prolongated initial guess")
    log("")
    
    log("generate initial guess ...")
    Ms = list(np.unique(np.concatenate([np.arange(10,p.info["M"]+1,2), [p.info["M"]]])))
    for j, m in enumerate(Ms):
        info_tmp = deepcopy(p.info)
        info_tmp["M"] = m
        q = Problem(info_tmp)
        log(f"----------   M = {m}   ----------")
        with timeit("prolong initial guess: {:4.2f} s"):
            if j == 0:
                init = xe.TTTensor.random([q.field.space.dim()]+[2]*m, [2]*m)
                init = init / xe.frob_norm(init)
            else: 
                init = prolong_to_dim(h_reco, [2]*m)
                init = escape_local_minimum(init, q, tol=(0.5,"ist egal"), verbose=False)
            
        with timeit("sample test data: {:4.2f} s"):
            ys_test, hs_test, _ = generate_samples(q, N=n_samples_reco)
        with timeit("compute error of initial guess: {:4.2f} s"):
            abs_err, rel_err = compute_L2L2_error(ys_test, hs_test, init, p.field.space)
        log("initial guess:")
        log(f"  ranks: {init.ranks()}")
        log(f"  error: {rel_err:4.2e}")
            
        with timeit("reconstruct h: {:4.2f} s"):
            h_reco = reconstruct_h(q, N=n_samples_reco, init=init)
        
        with timeit("compute error of reconstruction: {:4.2f} s"):
            abs_err, rel_err = compute_L2L2_error(ys_test, hs_test, h_reco, p.field.space)
        log("h_reco:")
        log(f"  ranks: {h_reco.ranks()}")
        log(f"  error: {rel_err:4.2e}")
    
    # --------------------------------------------------------------------------
    log("__"*40)
    log("")
    log("reconstruct with random initial guess")
    log("")
    
    with timeit("generate initial guess: {:4.2f} s"):
        init = xe.TTTensor.random([p.field.space.dim()]+[2]*M, [10]*M)
        init = init / xe.frob_norm(init)
    with timeit("sample test data: {:4.2f} s"):
        ys_test, hs_test, ws_test = generate_samples(p, N=1000)

    with timeit("compute error of initial guess: {:4.2f} s"):
        abs_err, rel_err = compute_L2L2_error(ys_test, hs_test, init, p.field.space)
    log("initial guess:")
    log(f"  ranks: {init.ranks()}")
    log(f"  error: {rel_err:4.2e}")
    
    with timeit("reconstruct h: {:4.2f} s"):
        h_reco = reconstruct_h(p, N=n_samples_reco, init=init)
    
    with timeit("compute error of reconstruction: {:4.2f} s"):
        abs_err, rel_err = compute_L2L2_error(ys_test, hs_test, h_reco, p.field.space)
    log("h_reco:")
    log(f"  ranks: {h_reco.ranks()}")
    log(f"  error: {rel_err:4.2e}")
    
    # --------------------------------------------------------------------------
    # log("__"*40)
    # log("")
    # log("reconstruct with no initial guess")
    # log("")
    # 
    # with timeit("sample test data: {:4.2f} s"):
    #     ys_test, hs_test, ws_test = generate_samples(p, N=1000)

    # with timeit("reconstruct h: {:4.2f} s"):
    #     h_reco = reconstruct_h(p, N=n_samples_reco, init=None)
    # 
    # with timeit("compute error of reconstruction: {:4.2f} s"):
    #     abs_err, rel_err = compute_L2L2_error(ys_test, hs_test, h_reco, p.field.space)
    # log("h_reco:")
    # log(f"  ranks: {h_reco.ranks()}")
    # log(f"  error: {rel_err:4.2e}")
