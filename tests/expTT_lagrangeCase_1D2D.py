import numpy as np
from numpy.polynomial.hermite_e import hermeval, hermeval2d, hermegrid2d
from scipy.special import factorial
from tqdm import tqdm

def D(_da, _dt):
    """
    Discretized differentiation operator

    Parameters
    ----------
    _da : int
        Dimension of the ansatz space.
    _dt : int
        Dimension of the test space.
    """
    # ret[i,j] = (i == (j-1)) * np.sqrt(j)  # P_{j} --> \sqrt{j} P_{j-1}
    ret = np.zeros((_dt, _da))
    # for j in range(min(_dt, _da-1)):
    #     ret[j,j+1] = np.sqrt(j+1)
    j = np.arange(min(_dt, _da-1))
    ret[j,j+1] = np.sqrt(j+1)
    return ret

def Mc(_da, _dt, _coefficient):
    """
    Discretized "multiplication with a constant" operator

    Parameters
    ----------
    _da : int
        Dimension of the ansatz space.
    _dt : int
        Dimension of the test space.
    _coefficient : float
        Coefficient to multiply with.
    """
    return _coefficient*np.eye(_dt, _da)

def S(_da, _dt, _affineCoefficients):
    """
    \partial_1 - M_{\partial_1 b}

    Parameters
    ----------
    _da : int
        Dimension of the ansatz space.
    _dt : int
        Dimension of the test space.
    _affineCoefficient : array-like (shape: (2,))
        Coefficients of the affine map whichs derivative is mutliplied.
    """
    assert np.shape(_affineCoefficients) == (2,)
    return D(_da, _dt) - Mc(_da, _dt, _affineCoefficients[1])

def Sreduced(_dimension, _affineCoefficients):
    sMat = S(_dimension, _dimension-1, _affineCoefficients)
    factors  = 1 / np.sqrt(factorial(np.arange(_dimension)))
    aMat = hermeval([0], np.diag(factors)).T
    mat = np.concatenate([sMat, aMat], axis=0)
    assert mat.shape == (_dimension, _dimension)
    vec = np.zeros(_dimension)
    vec[-1] = np.exp(_affineCoefficients[0])
    return mat, vec

def Sreduced_2d(_dimension, _affineCoefficients):
    sLeft  = S(_dimension, _dimension-1, _affineCoefficients[0])
    sRight = S(_dimension, _dimension-1, _affineCoefficients[:,0])
    assert sLeft.shape == (_dimension-1, _dimension) and sRight.shape == (_dimension-1, _dimension)
    sMat   = np.concatenate([np.kron(sLeft, np.eye(_dimension)),
                             np.kron(np.eye(_dimension), sRight)], axis=0)
    assert sMat.shape == (2*_dimension*(_dimension-1), _dimension**2)
    factors = 1 / np.sqrt(factorial(np.arange(_dimension)))
    factors = np.einsum('ij,kl->ikjl', np.diag(factors), np.diag(factors))
    aMat = hermeval2d([0], [0], factors).reshape(_dimension**2, 1).T
    assert aMat.shape == (1, _dimension**2)
    mat = np.concatenate([sMat, aMat], axis=0)
    assert mat.shape == (2*_dimension*(_dimension-1)+1, _dimension**2)
    vec = np.zeros(mat.shape[0])
    vec[-1] = np.exp(_affineCoefficients[0,0])
    return mat, vec

def SLagrange(_dimension, _affineCoefficients):
    #TODO: This formulation allows for different ansatz and test space dimensions.
    sMat = S(_dimension, _dimension, _affineCoefficients)
    sMat = sMat.T @ sMat
    factors  = 1 / np.sqrt(factorial(np.arange(_dimension)))
    aMat = hermeval([0], np.diag(factors)).T
    mat = np.block([[sMat, aMat.T], [aMat, np.zeros((1,1))]])
    assert mat.shape == (_dimension+1, _dimension+1)
    vec = np.zeros(_dimension+1)
    vec[-1] = np.exp(_affineCoefficients[0])
    return mat, vec

def SLagrange_2d(_dimension, _affineCoefficients):
    #TODO: This formulation allows for different ansatz and test space dimensions.
    sLeft  = S(_dimension, _dimension, _affineCoefficients[0])
    sRight = S(_dimension, _dimension, _affineCoefficients[:,0])
    sMat   = np.concatenate([np.kron(sLeft, np.eye(_dimension)),
                             np.kron(np.eye(_dimension), sRight)], axis=0)
    sMat = sMat.T @ sMat
    factors = 1 / np.sqrt(factorial(np.arange(_dimension)))
    factors = np.einsum('ij,kl->ikjl', np.diag(factors), np.diag(factors))
    aMat = hermeval2d([0], [0], factors).reshape(_dimension**2, 1).T
    mat = np.block([[sMat, aMat.T], [aMat, np.zeros((1,1))]])
    assert mat.shape == (_dimension**2+1, _dimension**2+1)
    vec = np.zeros(_dimension**2+1)
    vec[-1] = np.exp(_affineCoefficients[0,0])
    return mat, vec

def cartesian_product(x, y):
    return np.dstack(np.meshgrid(x, y)).reshape(-1, 2)

def solve(mat, vec):
    # NOTE solve with iterative refinement
    c = np.linalg.solve(mat, vec)
    res = vec - mat @ c
    oldNormRes = np.inf
    while np.linalg.norm(res) < oldNormRes:  # while res decreases
        oldNormRes = np.linalg.norm(res)
        c += np.linalg.solve(mat, res)
        res = vec - mat @ c
    return c
    # return conjugate_gradient(mat, vec, c)


if __name__=="__main__":
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.cm import magma as cmap

    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.preamble'] = r"\usepackage{{amsmath}}"

    def annotate_selection(x, y, ax=None, pos="left", selector=np.argmax, title="max: {0:.0f}"):
        # NOTE only for plot
        assert x.ndim == 1 and x.shape == y.shape
        idx = selector(y)
        xmax = x[idx]
        ymax = y[idx]
        text= title.format(ymax)
        if not ax:
            ax=plt.gca()
        bboxProperties  = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
        # arrowProperties = dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=90,rad=5")
        if pos == "left":
            arrowProperties = dict(arrowstyle="->",connectionstyle="arc3,rad=-0.1")
        elif pos == "right":
            arrowProperties = dict(arrowstyle="->",connectionstyle="arc3,rad=0.1")
        else:
            raise RuntimeError()
        kw = dict(xycoords='data',textcoords="axes fraction",
                arrowprops=arrowProperties, bbox=bboxProperties, ha=pos, va="top")
        if pos == "left":
            ax.annotate(text, xy=(xmax, ymax), xytext=(0.06,0.94), **kw)
        else:
            ax.annotate(text, xy=(xmax, ymax), xytext=(0.94,0.94), **kw)


    # ==========================================================================
    # Exponentiation with 1 parameter
    # ==========================================================================

    affineCoefficients = [0.1, 0.2]
    das = np.arange(2,26)  # da's

    conditionNumbers = np.empty((2,len(das)))
    pbar = tqdm(total=conditionNumbers.size)
    for i,da in enumerate(das):
        mat = Sreduced(da, affineCoefficients)[0]
        conditionNumbers[0,i] = np.linalg.cond(mat)

        mat = SLagrange(da, affineCoefficients)[0]
        conditionNumbers[1,i] = np.linalg.cond(mat)

        pbar.update(2)
    pbar.close()

    # fig,ax = plt.subplots(1, 2, sharey=True)
    fig,ax = plt.subplots(1, 2, sharey=True, dpi=300, figsize=(8,3.5))
    ax = np.array([ax]).reshape(1,2)

    ax[0,0].set_title(f"reduced test space")
    ax[0,0].plot(das, conditionNumbers[0], color='C0')
    ax[0,0].set_xlim(das[0], das[-1])
    ax[0,0].set_yscale('log')
    annotate_selection(das, conditionNumbers[0], ax=ax[0,0])
    ax[0,0].set_xlabel("dimension")

    ax[0,1].set_title(f"Lagrange multiplier")
    ax[0,1].plot(das, conditionNumbers[1], color='C0', label="condition number")
    ax[0,1].set_xlim(das[0], das[-1])
    ax[0,1].set_yscale('log')
    annotate_selection(das, conditionNumbers[1], ax=ax[0,1])
    ax[0,1].set_xlabel("dimension")

    fig.subplots_adjust(top=0.85, bottom=0.225)
    ax[0,1].legend(bbox_to_anchor=(0,0,1,1), loc="lower center", bbox_transform=fig.transFigure, ncol=1)

    fig.suptitle("different discretizations of the $1$D-ODE")
    # plt.show()
    plt.savefig("../img/condition_numbers_1d.png", dpi=300)
    plt.close(fig)


    x = np.linspace(-7, 7, 1001)
    y = np.exp(affineCoefficients[0] + affineCoefficients[1]*x)
    rho = np.exp(-x**2/2)/np.sqrt(2*np.pi)

    errors = np.empty((2,len(das)))
    l2errors = np.empty((2,len(das)))
    residuals = np.empty((2,len(das)))
    pbar = tqdm(total=errors.size)
    factors  = 1 / np.sqrt(factorial(np.arange(das[-1])))
    allMeasures = hermeval(x, np.diag(factors)).T
    for i,da in enumerate(das):
        sMat = S(da, da, affineCoefficients)
        measures = allMeasures[:,:da]
        mat,vec = Sreduced(da, affineCoefficients)
        c = solve(mat, vec)
        yc = measures @ c
        errors[0,i] = np.max(abs(y-yc))
        l2errors[0,i] = np.sqrt(np.trapz(rho*(y-yc)**2, x))
        residuals[0,i] = np.linalg.norm(sMat @ c, ord=2)

        mat,vec = SLagrange(da, affineCoefficients)
        c = solve(mat, vec)[:da]  # The final components of the solution vector are Lagrange multipliers.
        yc = measures @ c
        errors[1,i] = np.max(abs(y-yc))
        l2errors[1,i] = np.sqrt(np.trapz(rho*(y-yc)**2, x))
        residuals[1,i] = np.linalg.norm(sMat @ c, ord=2)

        pbar.update(2)
    pbar.close()

    # fig,ax = plt.subplots(1, 2, sharey=True)
    fig,ax = plt.subplots(1, 2, sharey=True, dpi=300, figsize=(8,3.5))
    ax = np.array([ax]).reshape(1,2)

    ax[0,0].set_title(f"reduced test space")
    ax[0,0].plot(das, errors[0], color='C0')
    ax[0,0].plot(das, l2errors[0], color='C1')
    ax[0,0].plot(das, residuals[0], color='C2')
    ax[0,0].set_xlim(das[0], das[-1])
    ax[0,0].set_yscale('log')
    annotate_selection(das, errors[0], ax=ax[0,0], pos="right", selector=np.argmin, title="min: {0:.1e}")
    ax[0,0].set_xlabel("dimension")

    ax[0,1].set_title(f"Lagrange multiplier")
    ax[0,1].plot(das, errors[1], color='C0', label="pointwise error")
    ax[0,1].plot(das, l2errors[1], color='C1', label=r"$L^2(\rho)$-error")
    ax[0,1].plot(das, residuals[1], color='C2', label="residual error")
    ax[0,1].set_xlim(das[0], das[-1])
    ax[0,1].set_yscale('log')
    annotate_selection(das, errors[1], ax=ax[0,1], pos="right", selector=np.argmin, title="min: {0:.1e}")
    ax[0,1].set_xlabel("dimension")

    fig.subplots_adjust(top=0.85, bottom=0.225)
    ax[0,1].legend(bbox_to_anchor=(0,0,1,1), loc="lower center", bbox_transform=fig.transFigure, ncol=3)

    fig.suptitle("different discretizations of the $1$D-ODE")
    # plt.show()
    plt.savefig("../img/errors_1d.png", dpi=300)
    plt.close(fig)


    # ==========================================================================
    # Exponentiation with 2 parameters
    # ==========================================================================

    affineCoefficients = np.array([[0.1, 0.2],
                                   [0.3, 0.0]])
    das = np.arange(2,26)  # da's

    conditionNumbers = np.empty((2,len(das)))
    pbar = tqdm(total=conditionNumbers.size)
    for i,da in enumerate(das):
        mat = Sreduced_2d(da, affineCoefficients)[0]
        conditionNumbers[0,i] = np.linalg.cond(mat)

        mat = SLagrange_2d(da, affineCoefficients)[0]
        conditionNumbers[1,i] = np.linalg.cond(mat)

        pbar.update(2)
    pbar.close()

    # fig,ax = plt.subplots(1, 2, sharey=True)
    fig,ax = plt.subplots(1, 2, sharey=True, dpi=300, figsize=(8,3.5))
    ax = np.array([ax]).reshape(1,2)

    ax[0,0].set_title(f"reduced test space")
    ax[0,0].plot(das, conditionNumbers[0], color='C0')
    ax[0,0].set_xlim(das[0], das[-1])
    ax[0,0].set_yscale('log')
    annotate_selection(das, conditionNumbers[0], ax=ax[0,0])
    ax[0,0].set_xlabel("dimension")

    ax[0,1].set_title(f"Lagrange multiplier")
    ax[0,1].plot(das, conditionNumbers[1], color='C0', label="condition number")
    ax[0,1].set_xlim(das[0], das[-1])
    ax[0,1].set_yscale('log')
    annotate_selection(das, conditionNumbers[1], ax=ax[0,1])
    ax[0,1].set_xlabel("dimension")

    fig.subplots_adjust(top=0.85, bottom=0.225)
    ax[0,1].legend(bbox_to_anchor=(0,0,1,1), loc="lower center", bbox_transform=fig.transFigure, ncol=1)

    fig.suptitle("different discretizations of the $2$D-ODE")
    # plt.show()
    plt.savefig("../img/condition_numbers_2d.png", dpi=300)
    plt.close(fig)


    x = np.linspace(-7, 7, 101)
    X1,X2 = np.meshgrid(x,x)
    Y = np.exp(affineCoefficients[0,0] + affineCoefficients[0,1]*X1 + affineCoefficients[1,0]*X2)
    y = Y.T.reshape(-1)
    rho = (lambda x1,x2: np.exp(-(x1**2+x2**2)/2)/(2*np.pi))(X1,X2)
    assert rho.shape == (101,101)

    errors = np.empty((2,len(das)))
    l2errors = np.empty((2,len(das)))
    residuals = np.empty((2,len(das)))
    pbar = tqdm(total=errors.size)
    factors  = 1 / np.sqrt(factorial(np.arange(das[-1])))
    factors  = np.einsum('ij,kl->ikjl', np.diag(factors), np.diag(factors))
    allMeasures = hermegrid2d(x, x, factors)
    assert allMeasures.shape == (das[-1], das[-1], len(x), len(x))
    allMeasures = np.transpose(allMeasures.reshape(das[-1], das[-1], len(x)**2), (2,0,1))
    for i,da in enumerate(das):
        sLeft  = S(da, da, affineCoefficients[0])
        sRight = S(da, da, affineCoefficients[:,0])
        sMat   = np.concatenate([np.kron(sLeft, np.eye(da)),
                                np.kron(np.eye(da), sRight)], axis=0)
        measures = allMeasures[:,:da,:da].reshape(-1, da**2)

        mat,vec = Sreduced_2d(da, affineCoefficients)
        assert mat.shape == (2*(da-1)*da+1, da**2)
        c,res,rank,s = np.linalg.lstsq(mat, vec, rcond=None)
        assert rank == da**2 and np.all(res < 1e-12)
        yc = measures @ c
        errors[0,i] = np.max(abs(y-yc))
        l2errors[0,i] = np.sqrt(np.trapz(np.trapz(rho*(y-yc).reshape(101,101)**2, x), x))
        residuals[0,i] = np.linalg.norm(sMat @ c, ord=2)

        mat,vec = SLagrange_2d(da, affineCoefficients)
        assert mat.shape == (da**2+1, da**2+1)
        c = solve(mat, vec)[:-1]  # The last component of the solution vector is the Lagrange multiplier.
        yc = measures @ c
        errors[1,i] = np.max(abs(y-yc))
        l2errors[1,i] = np.sqrt(np.trapz(np.trapz(rho*(y-yc).reshape(101,101)**2, x), x))
        residuals[1,i] = np.linalg.norm(sMat @ c, ord=2)

        pbar.update(2)
    pbar.close()

    # fig,ax = plt.subplots(1, 2, sharey=True)
    fig,ax = plt.subplots(1, 2, sharey=True, dpi=300, figsize=(8,3.5))
    ax = np.array([ax]).reshape(1,2)

    ax[0,0].set_title(f"reduced test space")
    ax[0,0].plot(das, errors[0], color='C0')
    ax[0,0].plot(das, l2errors[0], color='C1')
    ax[0,0].plot(das, residuals[0], color='C2')
    ax[0,0].set_xlim(das[0], das[-1])
    ax[0,0].set_yscale('log')
    annotate_selection(das, errors[0], ax=ax[0,0], pos="right", selector=np.argmin, title="min: {0:.1e}")
    ax[0,0].set_xlabel("dimension")

    ax[0,1].set_title(f"Lagrange multiplier")
    ax[0,1].plot(das, errors[1], color='C0', label="pointwise error")
    ax[0,1].plot(das, l2errors[1], color='C1', label=r"$L^2(\rho)$-error")
    ax[0,1].plot(das, residuals[1], color='C2', label="residual error")
    ax[0,1].set_xlim(das[0], das[-1])
    ax[0,1].set_yscale('log')
    annotate_selection(das, errors[1], ax=ax[0,1], pos="right", selector=np.argmin, title="min: {0:.1e}")
    ax[0,1].set_xlabel("dimension")

    fig.subplots_adjust(top=0.85, bottom=0.225)
    ax[0,1].legend(bbox_to_anchor=(0,0,1,1), loc="lower center", bbox_transform=fig.transFigure, ncol=3)

    fig.suptitle("different discretizations of the $2$D-ODE")
    # plt.show()
    plt.savefig("../img/errors_2d.png", dpi=300)
    plt.close(fig)
