import sys, os
import numpy as np
from scipy.special import zeta
import matplotlib.pyplot as plt
import xerus as xe
from tqdm import trange
from copy import deepcopy
from fenics import *

from context import src
from src.misc import timeit, log, transpose, Logger
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as expTT
from src.parallel import Parallel, ParallelizableProblem

# import autoPDB

# ==============================================================================

class TestExponent():
    def __init__(self, decay, M, mean=0.0, scale=1.0, nT=20, fem_deg=0, element="DG", useDarcyCoeff=True):
        """
        Affine function similar to Darcy diffusion coefficient, i.e.
            h(x,y) = scale * ( mean + sum_{m=1}^M h_m(x) y_m )
        where
            h_m(x) = 0.9 / \zeta(decay) * m**(-decay) * cos(2\pi B1(m) x_1) * cos(2\pi B2(m) x_2).
        Here, B1 = B2 = 2 for the test case and 
            B1(m) = m - k(m)*( k(m)+1 ) / 2      and     B2(m) = k(m) - B1(m)
        for 
            k(m) = floor[ -1/2 + \sqrt{ 1/4 + 2m } ].
        """
        self.decay = decay
        self.M = M
        self.scale = scale
        self.det_coeff = Expression("C * (cos(2*pi*B1*x[0]) * cos(2*pi*B2*x[1]))", C=1, B1=2, B2=2, degree=10)
        self.mean = mean
        self.useDarcyCoeff = useDarcyCoeff

        self.mesh = Mesh(UnitSquareMesh(nT,nT))
        if element == "DG": assert fem_deg == 0 # does not work for DG-n wit n > 0
        self.fem_deg = fem_deg
        self.space = FunctionSpace(self.mesh, element, self.fem_deg)

    # NOTE: used to mimic Darcy lognormal coefficient
    def k(self, m): return np.floor(-0.5+np.sqrt(0.25 + 2*m))
    def beta1(self, m): return m - self.k(m) * (self.k(m)+1) / 2
    def beta2(self, m): return self.k(m) - self.beta1(m)

    def realisation(self, y, exp=False):
        """ FEM function for one specific parameter realization. """
        det_coeff = self.det_coeff
        def interpolate_det_coeff(C,B1,B2):
            det_coeff.C, det_coeff.B1, det_coeff.B2 = C, B1, B2
            return interpolate(det_coeff, self.space).vector().get_local()
        def linear(y):
            u = Function(self.space).vector().get_local()
            for m in range(self.M):
                C = 0.9/zeta(self.decay) * (m+1)**(-self.decay)
                if self.useDarcyCoeff == True:
                    B1, B2 = self.beta1(m+1), self.beta2(m+1)
                else:
                    B1, B2 = 2, 2
                u += interpolate_det_coeff(C, B1, B2) * y[m]
            return u
        val = self.scale * (self.mean + linear(y))
        if exp == True: val = np.exp(val)
        ret = Function(self.space)
        ret.vector().set_local(val)
        return ret

class Problem(ParallelizableProblem):
    def __init__(self, info):
        self.info = info
        self.decay = info["decay"]
        self.M = info["M"]
        self.mean = info["mean"]
        self.scale = info["scale"]
        self.nT = info["nT"]
        self.useDarcyCoeff= info["useDarcyCoeff"]
        assert element in ["CG", "DG"]
        self.element = info["element"]
        self.fem_deg = info["fem_deg"]

        self.field = TestExponent(self.decay, self.M, mean=self.mean, 
                scale=self.scale, nT=self.nT, element=self.element, 
                fem_deg=self.fem_deg, useDarcyCoeff=self.useDarcyCoeff)

    def evaluate(self, y):
        """ FEM coefficiens for one specific parameter realization. """
        kappa = self.field.realisation(y, exp=False)
        return kappa.vector().get_local(), y

    def evaluate_exp(self, y):
        """ FEM coefficiens for one specific parameter realization. """
        kappa = self.field.realisation(y, exp=True)
        return kappa.vector().get_local(), y

    def evaluate_in_points(self, x, y, exp=False):
        """ Evaluate h(x,y) / exp(h(x,y)). """
        if x.ndim == 1: x.shape = 1, -1
        if y.ndim == 1: y.shape = 1, -1
        assert x.ndim == 2 and y.ndim == 2
        assert x.shape[0] == 1 or y.shape[0] == 1
        coeffs = self.get_coefficients(y, exp=exp)
        if x.shape[0] == self.field.space.dim():
            dofs = fem.dof_coords(self.field.space, self.field.mesh)

        # if x are Lagrange points, then evaluation values are the coefficients
        if np.allclose(x, dofs): return np.squeeze(coeffs)

        # evaluate coefficients in arbitrary (non-Lagrange) points
        ret = np.zeros((y.shape[0], x.shape[0]))
        for i, coeff in enumerate(coeffs):
            ret[i] = fem.eval_fencis_function(x, coeff, self.field.space)
        ret = np.squeeze(ret)
        if ret.ndim == 0: ret = ret.item() # make 0 dim array to float

        return np.array(ret)

    def get_coefficients(self, y, exp=False):
        """ 
        Compute the FEM coefficients of the affine-normal exponent of the
        diffusion coefficient h for given parameter samples.
        """
        if y.ndim == 1: y.shape = 1, -1
        assert y.ndim == 2
        if exp == True:
            coeffs = Parallel(self.evaluate_exp)
        else: 
            coeffs = Parallel(self.evaluate)
        ret = coeffs(y)
        c, y = transpose(ret)
        return np.array(c)

    def generate_samples_Darcy_fast(self, ys):
        M = self.info["M"]
        b1 = np.array([self.field.beta1(m+1) for m in range(M)]).reshape(1,-1)
        b2 = np.asarray([self.field.beta2(m+1) for m in range(M)]).reshape(1,-1)
        C = np.array(
                [0.9/zeta(self.info["decay"]) * (m+1)**(-self.info["decay"]) 
                    for m in range(M)]).reshape(1,-1)
        assert ys.ndim == 2 and ys.shape[1] == M
        xs = fem.dof_coords(self.field.space, self.field.mesh)
        cos1 = np.cos(2*np.pi*b1*xs[:,0].reshape(-1,1))
        cos2 = np.cos(2*np.pi*b2*xs[:,1].reshape(-1,1))
        assert cos1.shape[0] == cos2.shape[0] == self.field.space.dim()
        assert cos1.shape[1] == cos2.shape[1] == M
        terms = np.reshape(C*cos1*cos2, (1, self.field.space.dim(), M))
        ys = np.reshape(ys, (ys.shape[0], 1, M))
        # ret = np.sum(terms*ys, axis=2)
        buffers = list(np.unique(list(range(0,ys.shape[0], 1000)) + [ys.shape[0]]))
        ret = np.empty((ys.shape[0],self.field.space.dim()))
        for j in range(len(buffers[:-1])):
            bL, bR = buffers[j], buffers[j+1]
            ret[bL:bR] = np.sum(terms*ys[bL:bR], axis=2)
        return self.info["scale"]*(self.info["mean"]+ret)

def generate_samples(p, N=None, ys=None, hs=None, ws=None):
    if N is None: N = 1000 
    M = p.info["M"]
    if ys is None: ys = np.zeros([0,M])
    if hs is None: hs = np.zeros([0,p.field.space.dim()])
    assert ys.shape[0] == hs.shape[0]

    ys_new = np.random.multivariate_normal(np.zeros(M), np.eye(M), N)
    if p.info["useDarcyCoeff"]:
        hs_new = p.generate_samples_Darcy_fast(ys_new)
    else:
        hs_new = p.get_coefficients(ys_new, exp=False)

    ys = np.concatenate([ys, ys_new], axis=0)
    hs = np.concatenate([hs, hs_new], axis=0)

    ws = np.full(ys.shape[0], 1/ys.shape[0])

    return ys, hs, ws

def compute_L2L2_error(ys, us, reco, space):
    """
    compute the L2-L2 error of a tt approximation and the exact h (or exp h)
    over the domain.
    """
    ws = np.ones(ys.shape[0])/ys.shape[0]
    abs_err = femTT.compute_L2L2_error(reco, ys, ws, us, space, "normal")

    mass_mat = tt.mass_matrix(space)
    err_sq = np.einsum('n,ni,in', ws, us, mass_mat@us.T)
    rel_err = abs_err / np.sqrt(err_sq)

    return abs_err, rel_err

def compute_h_reco(p, N=1000):
    M, nT = p.info["M"], p.info["nT"]
    element, fem_deg, decay = p.info["element"], p.info["fem_deg"], p.info["decay"]
    file_name = f"h_reco_{element}{fem_deg}-{nT}_decay-{decay}_M{M}"

    log(f"sample {1000} test data points ...")
    with timeit("... {:4.2f} s"):
        ys_test, hs_test, ws_test = generate_samples(p, N=1000)

    if os.path.isfile(f"./data/h_recos/{file_name}.npy"):
        log("reco already exists")
        h_reco = tt.load(f"./data/h_recos/{file_name}.npy")
        log("compute error ...")
        with timeit("... {:4.2f} s"):
            abs_err, rel_err = compute_L2L2_error(ys_test, hs_test, h_reco, p.field.space)
        log(f"error: {rel_err:4.2e}")
        return h_reco
    log(f"sample {N} training data points ...")

    with timeit("... {:4.2f} s"):
        ys, hs, ws = generate_samples(p, N=N)

    init = xe.TTTensor.random([p.field.space.dim()]+[2]*M, [10]*M)
    init = init / xe.frob_norm(init)

    log(f"reconstruc h ...")
    rel_err = 1
    while rel_err > 1e-07:
        log(f"  run uq_ra_adf with {ys.shape[0]} samples...")
        with timeit("  ... {:4.2f} s"):
            h_reco = tt.reconstruct(
                    dims=[2]*M, nodes=ys.T, weights=ws, values=hs, dist="normal", 
                    init=init, label="h reco", verbose=1
                    )
        log("  compute error ...")
        with timeit("  ... {:4.2f} s"):
            abs_err, rel_err = compute_L2L2_error(ys_test, hs_test, h_reco, p.field.space)
        log(f"  error: {rel_err:4.2e}")

        if rel_err > 1e-07:
            log(f"  add {N} training data points ...")
            n = N if N < 10_000 else 10_000
            with timeit("  ... {:4.2f} s"):
                ys, hs, ws = generate_samples(p, n, ys, hs, ws)
        else:
            log(f"save to: ./data/h_recos/{file_name}")
            tt.save(f"./data/h_recos/{file_name}.npy", h_reco)

    return h_reco

def compute_h_reco_iter(p, nTs, N=1000):
    M = p.info["M"]
    element, fem_deg, decay = p.info["element"], p.info["fem_deg"], p.info["decay"]
    infos = []
    h_recos = []

    assert max(nTs) <= p.info["nT"]
    nTs = list(np.unique(nTs+[p.info["nT"]]))
    for nT in nTs:
        info = deepcopy(p.info)
        info["nT"] = nT
        infos.append(info)

    for j, nT in enumerate(nTs):
        log("")
        log(f"-----  iter run: {j+1}/{len(nTs)} ----- nTriangles = {nT}")
        p_current = Problem(infos[j])
        file_name = f"h_reco_{element}{fem_deg}-{nT}_decay-{decay}_M{M}"

        log(f"sample {1000} test data points ...")
        with timeit("... {:4.2f} s"):
            ys_test, hs_test, ws_test = generate_samples(p_current, N=1000)

        if os.path.isfile(f"./data/h_recos/{file_name}.npy"):
            log("reco already exists")
            h_reco = tt.load(f"./data/h_recos/{file_name}.npy")
            log("compute error ...")
            with timeit("... {:4.2f} s"):
                abs_err, rel_err = compute_L2L2_error(ys_test, hs_test, h_reco, p_current.field.space)
            log(f"error: {rel_err:4.2e}")
            h_recos.append(h_reco)
        else:
            log(f"sample {N} training data points ...")
            with timeit("... {:4.2f} s"):
                ys, hs, ws = generate_samples(p_current, N=N)

            log(f"generate initial guess")
            if j == 0:
                init = None
            else:
                p_last = Problem(infos[j-1])
                init = h_recos[-1]
                dims = [p_current.field.space.dim()] + [2]*M
                init = tt.get_initial_guess(
                        dims, init=init, 
                        coarse_space=p_last.field.space, 
                        fine_space=p_current.field.space
                        )
                log("  compute error init...")
                with timeit("  ... {:4.2f} s"):
                    abs_err, rel_err = compute_L2L2_error(
                            ys_test, hs_test, init, p_current.field.space)
                log(f"  error init: {rel_err:4.2e}")

            log(f"reconstruc h ...")
            rel_err = 1
            while rel_err > 1e-07:
                log(f"  run uq_ra_adf with {ys.shape[0]} samples ...")
                with timeit("  ... {:4.2f} s"):
                    h_reco = tt.reconstruct(
                            dims=[2]*M, nodes=ys.T, weights=ws, values=hs, dist="normal", 
                            init=init, label="h reco", verbose=False
                            )
                log("  compute error ...")
                with timeit("  ... {:4.2f} s"):
                    abs_err, rel_err = compute_L2L2_error(ys_test, hs_test, h_reco, p_current.field.space)
                log(f"  error: {rel_err:4.2e}")

                if rel_err > 1e-07:
                    n = N if N < 10_000 else 10_000
                    n_smpl = ys.shape[0] + n
                    log(f"  try {n_smpl} new training data points ...")
                    with timeit("  ... {:4.2f} s"):
                        ys, hs, ws = generate_samples(p_current, n_smpl)
                else:
                    h_recos.append(h_reco)
                    log(f"save to: ./data/h_recos/{file_name}")
                    tt.save(f"./data/h_recos/{file_name}.npy", h_reco)
    return h_recos[-1]


# ==============================================================================
if __name__ == "__main__":
    sys.stdout = Logger(fileName="expTT_h_recos", filePath="./log/")
    log("setup problem")
    # --------------------------------------------------------------------------
    # setup 

    # FEM stuff
    decay, mean = 2, 0.0
    NTs = [112, 112, 100, 142,  71, 159, 224] # number of triangles
    FDs = [  0,   2,   0,   1,   2,   0,   1] # finite element degree
    Ms  = [ 20,  20,  20,  20,  20,  20,  20] # number of Parameters
    UP  = [  0,   0,   0,   0,   0,   0,   1] # use prolongation

    NTs = [50] # number of triangles
    FDs = [ 1] # finite element degree
    Ms  = [ 5] # number of Parameters
    UP  = [ 0] # use prolongation

    assert len(NTs) == len(FDs) == len(Ms)

    os.makedirs("./data", exist_ok=True)
    os.makedirs("./data/h_recos/", exist_ok=True)

    for j in range(len(Ms)):
        nT, fem_deg, M = NTs[j], FDs[j], Ms[j]
        log("__"*40)
        log("")
        log(f"RUN: {j+1}/{len(Ms)}    --    nT={nT}, fem_deg={fem_deg}, M={M}")
        dims = [6]*M
        element = "DG" if fem_deg == 0 else "CG"

        info = {"M":M, "decay":decay, "mean":mean, "scale":1, "nT":nT, 
                "element":element, "fem_deg":fem_deg, "useDarcyCoeff":True,
                "ITER":4}
        p = Problem(info)
        log(f"FE-DoFs: {p.field.space.dim()}")

        N = 1000
        if M > 10: N = 10_000
        if M > 15: N = 40_000
        if UP[j]:
            nTs = list(range(2,nT,2))
            if p.info["nT"] == 224: nTs = [112]
            h_reco = compute_h_reco_iter(p, nTs, N=N)
        else:
            h_reco = compute_h_reco(p, N=N)

