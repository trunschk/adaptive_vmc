# coding: utf-8
from  fenics import *
import numpy as np
import scipy.sparse as sps
import xerus as xe
import matplotlib.pyplot as plt
from tqdm import trange


mesh = UnitSquareMesh(100,100)
V = FunctionSpace(mesh, 'CG', 1)

def compute_slice(slc):
    lab = parameters['linear_algebra_backend']
    parameters['linear_algebra_backend'] = "Eigen"
    u = Function(V)
    v = TrialFunction(V)
    w = TestFunction(V)
    u.vector().set_local(np.eye(1, V.dim(), k=slc)[0])
    ret = assemble(u*v*w*dx, form_compiler_parameters={'quadrature_degree': -1})
    ret = as_backend_type(ret).sparray()
    parameters['linear_algebra_backend'] = lab
    return ret

def compute_tensor():
    ret = xe.Tensor([V.dim()]*3)
    for slc in trange(V.dim()):
        S = compute_slice(slc).tocoo()
        n = np.linalg.norm(S.data)
        for d,r,c in zip(S.data, S.row, S.col):
            if abs(d) < 1e-12*n: continue
            ret[[slc, r, c]] = d
    return ret

tpt = compute_tensor()
print(f"Sparsity: {tpt.sparsity()/tpt.size:.2e}")
