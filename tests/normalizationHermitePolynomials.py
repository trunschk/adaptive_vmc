import numpy as np
import scipy.integrate

from context import src  # import src library
from src.misc import timeit, herm_triple_product_slow, herm_triple_product
from measurement_utils.bases import HermitePolynomials
np.set_printoptions(precision=16)

# ______________________________________________________________________________


def is_equal(mat1, mat2):
    return 1 if np.linalg.norm(mat1-mat2) < 1e-10 else 0


def diagnostics(mat, name, print_mat=False):
    I = np.eye(mat.shape[0])
    if is_equal(mat, I):
        print("is identity: {:<20s} [True]".format(name))
    else:
        print("is identity: {:<20s} [False]".format(name))
    if print_mat:
        print(name+":")
        print(mat)


def get_product_matrix(H, w, d, eps):
    mat = np.zeros((d+1, d+1))
    for i in range(d+1):
        for j in range(d+1):
            def integrand(x): return H[i](x)*H[j](x)*w(x)
            val = scipy.integrate.quad(integrand, -np.inf, np.inf)[0]
            if val > eps:
                mat[i, j] = val
    return mat


def get_product_matrix_2(H, w, d, eps):
    mat = np.zeros((d+1, d+1))
    for i in range(d+1):
        for j in range(d+1):
            def integrand(x): return H.eval(x)[i]*H.eval(x)[j]*w(x)
            val = scipy.integrate.quad(integrand, -np.inf, np.inf)[0]
            if val > eps:
                mat[i, j] = val
    return mat


def get_triple_product_slow(H, w, d, eps, normalized=False):
    kappa = np.zeros((d+1, d+1, d+1))
    kappa2 = np.zeros((d+1, d+1, d+1))
    for i in range(d+1):
        for j in range(d+1):
            for k in range(d+1):
                def integrand(x): return H[i](x)*H[j](x)*H[k](x)*w(x)
                val = scipy.integrate.quad(integrand, -np.inf, np.inf)[0]
                val2 = herm_triple_product_slow(i, j, k, normalized=normalized)
                if val > eps:
                    kappa[i, j, k] = val
                if val2 > eps:
                    kappa2[i, j, k] = val2
    return kappa, kappa2


def get_triple_product(H, w, d, eps, normalized=False):
    kappa = np.zeros((d+1, d+1, d+1))
    for i in range(d+1):
        for j in range(d+1):
            for k in range(d+1):
                val = herm_triple_product(i, j, k, normalized=normalized)
                if val > eps:
                    kappa[i, j, k] = val
    return kappa


def fac(n): return float(np.math.factorial(n))
def w(x): return np.exp(-x**2/2)  # weight of hermiteE polynomials
# density for standard normal distribution
def rho(x): return np.exp(-x**2/2)/np.sqrt(2*np.pi)
def hermite(deg): return np.polynomial.hermite_e.HermiteE.basis(deg)


# ______________________________________________________________________________
if __name__ == "__main__":
    # NOTE: Test normalizing of Hermite polynomials and computation of the
    #       triple product tensor.

    d = 8
    eps = 1e-10

    # get hermiteE polynomials (unnormalized)

    H_np = [hermite(deg) for deg in range(d+1)]

    # normalize hermiteE polynomials

    def norm_w(n): return 1 / (np.sqrt(np.sqrt(2*np.pi)*fac(n)))
    H_np_norm = [norm_w(deg)*hermite(deg) for deg in range(d+1)]

    # normalize hermiteE polynomials (w.r.t. density rho)

    def norm_rho(n): return 1 / np.sqrt(fac(n))
    H_np_rho = [norm_rho(deg)*hermite(deg) for deg in range(d+1)]

    # Philipp's HermiteE polynomials (normalized)

    # NOTE: These are the same as H_np_rho
    H_p = HermitePolynomials(d, mean=0, variance=1)

    # --------------------------------------------------------------------------
    print()

    print("assemble H_np ...")
    mat_H_np = get_product_matrix(H_np, w, d, eps)
    diagnostics(mat_H_np, "H_np", print_mat=False)
    print()

    print("assemble H_np_norm ...")
    mat_H_np_norm = get_product_matrix(H_np_norm, w, d, eps)
    diagnostics(mat_H_np_norm, "H_np_norm", print_mat=False)
    print()

    print("assemble H_np_rho ...")
    mat_H_np_rho = get_product_matrix(H_np_rho, rho, d, eps)
    diagnostics(mat_H_np_rho, "H_np_rho", print_mat=False)
    print()

    # NOTE: slow
    # print("assemble H_p ...")
    # mat_H_meas = get_product_matrix_2(H_p, rho, d, eps)
    # diagnostics(mat_H_meas, "H_meas", print_mat=True)

    # --------------------------------------------------------------------------
    # NOTE: This is unnormalized triple product, expect that "is identity" is
    #       False
    print("assemble kappa_H_np ...")
    with timeit("  ... slow triple product unnormalized ({:4.2f} s)"):
        kappa_H_np, kappa2_H_np = get_triple_product_slow(
            H_np, rho, d, eps, normalized=False)
    diagnostics(kappa_H_np[:, :, 0], "kappa_H_np", print_mat=False)
    diagnostics(kappa2_H_np[:, :, 0], "kappa2_H_np", print_mat=False)
    print("is equal:", is_equal(kappa_H_np, kappa2_H_np))
    with timeit("  ... fast triple product unnormalized ({:4.2f} s)"):
        tmp = get_triple_product(H_np, rho, d, eps, normalized=False)
    print("fast-slow L2-norm:", np.linalg.norm(tmp-kappa_H_np))
    print("fast-slow max-norm:", np.max(np.abs(tmp-kappa_H_np)))
    print()

    # NOTE This is wrong normalized triple product,
    # expect that "is identity" is False and "is equal" to misc function is 0
    print("assemble kappa_H_np_norm ...")
    kappa_H_np_norm, kappa2_H_np_norm = get_triple_product_slow(
        H_np_norm, rho, d, eps, normalized=True)
    diagnostics(kappa_H_np_norm[:, :, 0], "kappa_H_np_norm", print_mat=False)
    diagnostics(kappa2_H_np_norm[:, :, 0], "kappa2_H_np_norm", print_mat=False)
    print("is equal:", is_equal(kappa_H_np_norm, kappa2_H_np_norm))
    print("L2-norm:", np.linalg.norm(kappa_H_np_norm-kappa2_H_np_norm))
    print("max-norm:", np.max(np.abs(kappa_H_np_norm-kappa2_H_np_norm)))
    print()

    # NOTE This is normalized triple product, expect that "is identity" is True
    print("assemble kappa_H_np_rho ...")
    with timeit("  ... slow triple product normalized ({:4.2f} s)"):
        kappa_H_np_rho, kappa2_H_np_rho = get_triple_product_slow(
            H_np_rho, rho, d, eps, normalized=True)
    diagnostics(kappa_H_np_rho[:, :, 0], "kappa_H_np_rho", print_mat=False)
    diagnostics(kappa2_H_np_rho[:, :, 0], "kappa2_H_np_rho", print_mat=False)
    print("is equal:", is_equal(kappa_H_np_rho, kappa2_H_np_rho))
    with timeit("  ... fast triple product normalized ({:4.2f} s)"):
        tmp = get_triple_product(H_np_rho, rho, d, eps, normalized=True)
    print("fast-slow L2-norm:", np.linalg.norm(tmp-kappa_H_np_rho))
    print("fast-slow max-norm:", np.max(np.abs(tmp-kappa_H_np_rho)))
    print()
