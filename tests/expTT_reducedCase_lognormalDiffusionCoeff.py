# -*- coding: utf-8 -*-
import sys
import os
import time
import numpy as np
import scipy
from copy import deepcopy
import matplotlib.pyplot as plt

from fenics import *
import xerus as xe

from context import src, avmc_intrusive
from src.misc import log, timeit, herm_triple_product, get_scaled_herm_poly
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
from src.problem.equation.darcy import Problem
from avmc_intrusive import compute_approx_error_coeff

plt.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}', r'\usepackage{amssymb}']

# ______________________________________________________________________________


def fac(n): return np.math.factorial(float(n))
def norm_rho(n): return np.sqrt(float(fac(n)))


def compute_matrix_M(d2, d1, normalized=True):
    assert d1 >= 0 and d2 >= 0
    val = np.zeros((d2+1, d1+1))
    norm = [norm_rho(j) if not normalized else 1 for j in range(
        max(d1+1, d2+1))]
    for i in range(min(d1, d2)+1):
        val[i, i+1] = np.sqrt(i+1)*norm[i]*norm[i+1]
    return val


def compute_matrix_K(A, B, C):
    K = np.zeros([A+1, C+1, B+1])
    for i in range(A+1):
        for j in range(B+1):
            for k in range(C+1):
                K[i, k, j] = herm_triple_product(i, j, k, normalized=True)
    return K


def compute_matrix_S(d2, d1, B):
    assert d1 >= 0 and d2 >= 0
    db = B.size-1
    K = compute_matrix_K(db-1, d1, d2)
    M = compute_matrix_M(db-1, db)
    return np.einsum("jab,ji,i->ab", K, M, B)


def compute_matrix_GH(d2, d1, normalized=True):
    assert d1 >= d2 >= 0
    if normalized:
        val = np.eye(d1+1)
    else:
        val = np.diag([norm_rho(j)**2 for j in range(d1+1)])
    return val[:d2+1], val[d2+1:]

# TODO correct?


def get_initial_condition(bs, dim):
    c = [1/norm_rho(j) for j in range(dim)]
    P, _ = get_scaled_herm_poly(dim-1, c=c)
    P0 = np.array([P[j](0) for j in range(dim)])
    a0 = np.exp(np.dot(bs.T, P0[:bs.shape[0]]))
    return P0, a0

# TODO correct?
# TODO make more efficient (join with IC func)


def compute_core_exp_tt(bs, dim):
    da, db = dim-1, bs.shape[0]-1
    M = compute_matrix_M(da-1, da)
    G, _ = compute_matrix_GH(da-1, da+db-1)
    IC = get_initial_condition(bs, dim)
    val = np.zeros((dim, bs.shape[1]))
    for j, b in enumerate(bs.T):
        S = compute_matrix_S(da+db-1, da, b)
        dL = M-np.einsum("ij,jk->ik", G, S)
        L = np.concatenate([dL, [IC[0]]], axis=0)
        RHS = np.concatenate([np.zeros(da), [IC[1][j]]])
        val[:, j] = np.linalg.solve(L, RHS)
    return val

# TODO correct?


def compute_exp_tt(tt, dims):
    assert len(dims) == tt.order()-1
    tt_cores = [np.asarray(tt.get_component(j)) for j in range(tt.order())]
    exp_tt_cores = [np.ascontiguousarray(tt_cores[0])]
    for j, core in enumerate(tt_cores[1:]):
        # shape (d_m, r_m*r_{m+1})
        bs = np.reshape(np.swapaxes(core, 0, 1), (core.shape[1], -1))
        # shape (q_m, r_m*r_{m+1})
        cmp = compute_core_exp_tt(bs, dims[j])
        cmp = np.swapaxes(np.reshape(                                           # shape (r_m, q_m, r_{m+1})
            cmp, (cmp.shape[0], core.shape[0], core.shape[2])
        ), 0, 1)
        exp_tt_cores.append(cmp)

    new_dims = [core.shape[1] for core in exp_tt_cores]
    exp_tt = xe.TTTensor.random(new_dims, tt.ranks())
    for j, core in enumerate(exp_tt_cores):
        exp_tt.set_component(j, xe.Tensor.from_buffer(
            np.ascontiguousarray(core)))
    return exp_tt


# ______________________________________________________________________________
if __name__ == "__main__":
    print()

    # --------------------------------------------------------------------------
    # setup
    dist = "normal"
    info = {
        "problem": {"name": "darcy"},
        "fe": {"degree": 1, "mesh": 10},
        "fe_coeff": {"degree": 0, "space": "DG"},
        "expansion": {"size": 10, "mean": 0, "decay rate": 2, "rho": 1, "vartheta": 0.1, "scale": 6/np.pi**2},
        "sampling": {"distribution": dist, "expfield": False, "batch size": 40}
    }
    p = Problem(info)

    # --------------------------------------------------------------------------
    # generate samples
    ys_train, ws_train, bs_train = fem.sample_diffusion_coeff(info, 100)
    as_train = np.exp(bs_train)

    ys_test, ws_test, bs_test = fem.sample_diffusion_coeff(info, 1000)
    as_test = np.exp(bs_test)

    # --------------------------------------------------------------------------
    # reconstruct b
    b_reco = tt.reconstruct(
        dims=[2]*info["expansion"]["size"],
        nodes=ys_train.T, weights=ws_train, values=bs_train, dist=dist,
        init=None, label="affine b", verbose=True
    )
    err_b = femTT.compute_L2L2_error(
        b_reco, ys_test, ws_test, bs_test, p.space_coeff, dist
    )

    # --------------------------------------------------------------------------
    # reconstruct a = exp(b) naiv
    # a_reco_naiv = tt.reconstruct(
    #         dims=[10]*info["expansion"]["size"],
    #         nodes=ys_train.T, weights=ws_train, values=as_train, dist=dist,
    #         init=None, label="a naiv", verbose=True
    #         )
    # err_a_naiv = femTT.compute_L2L2_error(
    #         a_reco_naiv, ys_test, ws_test, as_test, p.space_coeff, dist
    #         )

    # --------------------------------------------------------------------------
    # compute a = exp(b) clever
    a_reco_expTT = compute_exp_tt(b_reco, dims=[10]*info["expansion"]["size"])
    err_a_expTT = femTT.compute_L2L2_error(
        a_reco_expTT, ys_test, ws_test, as_test, p.space_coeff, dist
    )

    log("")
    log("  error L2-L2 - b:         {:<1.2e}".format(err_b))
    # log("  error L2-L2 - a naiv:    {:<1.2e}".format(err_a_naiv))
    log("  error L2-L2 - a expTT:   {:<1.2e}".format(err_a_expTT))

    print()
