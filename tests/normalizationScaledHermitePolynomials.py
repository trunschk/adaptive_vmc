from src.misc import timeit, log, factorial
from context import src  # import src library
import time
import matplotlib.pyplot as plt
from matplotlib import ticker
from scipy.special import zeta
import scipy.integrate
import numpy as np
np.set_printoptions(precision=6, suppress=True)


# ------------------------------------------------------------------------------

def get_scaled_herm_poly(d, s=1, c=None):
    """
    Compute standard and scaled Hermite polynomials up to a certain degree. The
    polynomials are
        P_j(x) = c[j] * H_j(x)   and   Q_j(x) = c[j] * H_j(s * x)
    for where H_j are the standard Hermite Polynomials.

    Parameter
    ---------
    d : int
        Maximal polynomial degree of basis functions.
    s : float
        Scaling for Hermite Polynomials.
        (Default 1 represents no scaling, i.e. P_j = Q_j.)
    c : float
        Factor for normalization of Hermite polynomials.
        (Default 1 represents no normalization.)
    """
    if c is None:
        c = np.ones(d+1)

    def hermite(deg): return np.polynomial.hermite_e.HermiteE.basis(deg)
    def sHermite(deg): return hermite(deg)(np.polynomial.Polynomial((0, s)))
    # Hermite polynomials P
    P = [c[deg]*hermite(deg) for deg in range(d+1)]
    # scaled Hermite polynomials Q
    Q = [c[deg]*sHermite(deg) for deg in range(d+1)]
    return P, Q


def compute_base_change_matrix(d, m, rho, decay, gamma=0.9, normalized=True):
    beta_m = gamma / zeta(decay) * m**(-decay)
    sigma = np.exp(rho*beta_m)

    def gamma_rho(x): return np.exp(-x**2/(2*sigma**2)) / \
        (np.sqrt(2*np.pi)*sigma)

    c_norm = np.array([1/np.sqrt(factorial(j)) for j in range(d+1)])
    H, sH = get_scaled_herm_poly(d, s=1/sigma, c=c_norm)

    mat = np.zeros((d+1, d+1))
    for i in range(d+1):
        for j in range(d+1):
            def integrand(x): return sH[i](x)*H[j](x)*gamma_rho(x)
            mat[i, j], _ = scipy.integrate.quad(integrand, -np.inf, np.inf)

    # scale matrix by normalization constants
    # NOTE This is the same as dot( c_norm, dot(mat, c_norm**(-1)) )
    C = np.ones(d+1).reshape(-1, 1) if normalized else c_norm.reshape(-1, 1)
    mat = C * (1/C).reshape(1, -1) * mat
    return mat


# ------------------------------------------------------------------------------
def fac(n): return factorial(n)
def w(x): return np.exp(-x**2/2)  # weight of hermiteE polynomials
# density for standard normal distribution
def gamma(x): return np.exp(-x**2/2)/np.sqrt(2*np.pi)
def hermite(deg): return np.polynomial.hermite_e.HermiteE.basis(deg)


# ------------------------------------------------------------------------------
if __name__ == "__main__":
    print()

    # setup
    d = 10
    m, decay, rho = 2, 4, 2
    normalized = True

    # define Hermite and scaled Hermite basis
    if normalized:
        c_norm = np.array([1/np.sqrt(factorial(j)) for j in range(d+1)])
    else:
        c_norm = np.array([1 for j in range(d+1)])
    sigma = np.exp(rho * 0.9 / zeta(decay) * m**(-decay))
    H, sH = get_scaled_herm_poly(d, s=1/sigma, c=c_norm)

    # define random function in Hermite basis function
    c_h = np.random.randint(-5, 5, d+1)  # coefficients in Hermite basis

    def u_h(x): return np.sum([c_j*H[j](x)
                               for j, c_j in enumerate(c_h)], axis=0)

    # compute base-change matrix
    with timeit("time for BCM: {:4.2f} s"):
        bcm = compute_base_change_matrix(d, m=m, rho=rho, decay=decay,
                                         normalized=normalized)

    # compute function in scaled Hermite basis
    c_sh = np.dot(bcm, c_h)  # coefficients in scaled Hermite basis

    def u_sh(x): return np.sum([c_j*sH[j](x)
                                for j, c_j in enumerate(c_sh)], axis=0)

    # compute error between both expansions
    x = np.linspace(-2, 2, 200)
    val1, val2 = u_h(x), u_sh(x)
    error = np.linalg.norm(val1-val2)
    log("error:", error)

    # plot functions
    plt.figure()
    plt.plot(x, val1, label="Hermite", lw=5)
    plt.plot(x, val2, "--", label="scaled Hermite", lw=5)
    plt.title("normalized: {}, error: {:4.2g}\nc_h: {}".format(
        normalized, error, c_h))
    plt.legend()
    plt.savefig("../img/test_scaledHermiteBasis.png")

    # plot scaling factor
    ms = np.arange(1, 21)
    rhos = np.linspace(-4, 2, 20)
    decays = np.array([2.0, 4.0])
    val = np.zeros((decays.size, ms.size, rhos.size))
    for k, decay in enumerate(decays):
        for i, m in enumerate(ms):
            for j, rho in enumerate(rhos):
                val[k, i, j] = 10.0**(rho) * 0.9 / zeta(decay) * m**(-decay)
    fig, ax = plt.subplots(figsize=(5*decays.size, 5),
                           nrows=1, ncols=decays.size)
    for j in range(decays.size):
        plt.sca(ax[j])
        im = ax[j].contourf(val[j], 20, extent=[rhos[0], rhos[-1], ms[0], ms[-1]],
                            locator=ticker.LogLocator())
        ax[j].set_ylabel("$m$")
        ax[j].set_xlabel("$k$")
        ax[j].set_title("decay: {}".format(decays[j]))
        fig.colorbar(im)
    fig.suptitle("Values for $\log(\sigma) = \\varrho\,\\beta_m$"
                 + " for $\\beta_m =  \\frac{\gamma}{\zeta(decay)}\, m^{-decay}$"
                 + " and $\\varrho = 10^k$"
                 )
    plt.savefig("../img/test_scaledHermiteBasis_sigma.png")

    print()
