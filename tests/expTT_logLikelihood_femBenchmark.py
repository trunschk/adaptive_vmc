import sys
import numpy as np
import matplotlib.pyplot as plt
import xerus as xe
from fenics import *

from context import src
from src.misc import timeit, log, transpose, Logger
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as expTT
from src.problem.equation.darcy import Problem
from src.parallel import Parallel, ParallelizableProblem

# import autoPDB

# ==============================================================================


def eval_log_likelihood(gs, delta, space, scale=1):
    """
    Compute the log-likelihood for sampled evaluations of the forward problem
    for a given measurement.

    Parameter
    ---------
    gs      : array_like of shape (..., fem_deg)
        FE coefficients of forward problem evaluated in multiple samples.
    delta   : array_like of shape (1, fem_deg)
        Measurements for log-likelihood.
    space   : fenics.FunctionSpace
        FE function space of forward problem.
    scale   : int (Default = 1)
        Rescaling of log-likelihood.
    """
    if delta.ndim < 2:
        delta.shape = (1, -1)
    assert delta.shape[0] == 1
    assert gs.ndim == 2 and gs.shape[1] == delta.shape[1] == space.dim()
    diff = delta - gs

    # use basic L2 norm for FE component, i.e. mass matrix
    mass_mat = fem.mass_matrix(space)

    # compute || delta - gs ||^2
    ret = np.sum(diff * (mass_mat@diff.T).T, axis=1)

    return -scale*ret/2


def generate_samples(p, N, samples=None):
    """
    Generate samples of FE forward prblem. Evaluate forward problem in
    optionally given samples.
    """
    ys, ws, gs = fem.sample_solution(p.info, N, samples=samples, get_eta=False)
    return ys, ws, gs


def compute_L2_error(ys, us, reco):
    """
    Compute stochastic L2 error of a function with one deterministic component.
    """
    tt_eval = tt.eval_approx(reco, ys, "normal")
    abs_err = np.sqrt(np.sum((us-tt_eval)**2)/us.shape[0])
    rel_err = abs_err / np.sqrt(np.sum(us**2)/us.shape[0])
    return abs_err, rel_err


def compute_L2L2_error(ys, us, reco, space):
    """
    compute the L2-L2 error of a tt approximation against some reference values
    over the deterministic domain.
    """
    ws = np.ones(ys.shape[0])/ys.shape[0]
    abs_err = femTT.compute_L2L2_error(reco, ys, ws, us, space, "normal")

    mass_mat = tt.mass_matrix(space)
    err_sq = np.einsum('n,ni,in', ws, us, mass_mat@us.T)
    rel_err = abs_err / np.sqrt(err_sq)

    return abs_err, rel_err


# TODO check correctness
def log_likelihood(G_TT, delta, space, scale=1):
    """
    Compute the log-likelihood TTTensor for a TTTensor of the forward problem
    and a given measurement.
    """
    if delta.ndim < 2:
        delta.shape = (1, -1)
    assert delta.ndim == 2 and delta.shape[0] == 1
    assert delta.size == G_TT.dimensions[0] == space.dim()
    dims, M = G_TT.dimensions[1:], G_TT.order()-1

    # convert delta to TTTensor of correct shape
    tt1 = xe.TTTensor(xe.Tensor.from_buffer(delta.flatten()))
    tt2 = xe.TTTensor.dirac(dims, [0]*M)
    delta_TT = xe.dyadic_product([tt1, tt2])

    # compute difference: delta-G(y)
    diff = tt.convert(delta_TT - G_TT)
    diff0, diff_tail = diff[0], diff[1:]

    # compute product det component
    diff0 = diff0[0].T  # shape (rank, fem_dim)
    mass_mat = fem.mass_matrix(space)
    ret0 = np.einsum("ij,jk->ik", diff0, mass_mat@diff0.T)
    ret0 = ret0.reshape(1, 1, np.prod(ret0.shape))

    # compute product stoch component
    ret_tail = tt.compute_product_coeff_tensor(diff_tail, diff_tail, "normal")

    assert ret_tail[0].shape[0] == ret0.shape[2]
    ret = tt.convert([ret0] + ret_tail)
    ret.round(1e-16)

    return scale*ret


# TODO check correctness
def expTT_reconstruction_dupe(dims, h_reco, x_0, exph0=None, verbose=False):
    """ Duplicate of expTT.expTT_reconstruction() for local debugging. """

    M, ALS_TYPE = len(dims), "simple"

    # build operator and rhs
    log("  build operator W0 and RHS b")
    W0, b = expTT.build_system_inexactIC(
        dims, dims, h_reco, x_0, exph0=exph0, method=None)

    log(f"    W0.dimensions: {W0.dimensions}")  # XXX
    log(f"    W0.ranks(): {W0.ranks()}")  # XXX

    log("  round operator W0")
    W0.round(1e-12)
    log(f"    W0.ranks(): {W0.ranks()}")  # XXX

    # random initialization
    log("  set initial guess")
    u_start = xe.TTTensor.random(dims, [10]*(M-1) if M > 1 else [])
    u_start = u_start / xe.frob_norm(u_start)

    # run ALS (simple or xerus built-in)
    log(f"  run {ALS_TYPE} ALS")
    if ALS_TYPE == "simple":
        ret = expTT.run_simpleALS(A=W0, u_start=u_start, b=b, verbose=verbose)
    elif ALS_TYPE == "xerus":
        ret = xe.TTTensor(u_start)
        xe.ALS_SPD(W0, ret, b)

    log("  round solution and add first component")
    ret.round(1e-12)  # round to get unneccesary large ranks down
    ret = tt.add_dimension_left(ret)  # add deterministic component

    return ret


# ==============================================================================
if __name__ == "__main__":
    sys.stdout = Logger(
        fileName="expTT_logLikelihood_fem.log",
        filePath="../log/",
        overwrite=False)

    mean, decay = 1.0, 2.0
    SETUP = [
        # {"M": 20, "fem_deg": 1, "nT": 20},
        # {"M": 20, "fem_deg": 1, "nT": 40},
        # {"M": 20, "fem_deg": 1, "nT": 60},
        # {"M": 20, "fem_deg": 1, "nT": 80},
        {"M": 20, "fem_deg": 1, "nT": 100},
        # {"M": 20, "fem_deg": 1, "nT": 120},
    ]

    for j, dct in enumerate(SETUP):

        print()
        print(f"=== RUN {j+1}/{len(SETUP)} ===")
        M = dct["M"]
        expTT_dims = [5]*M
        log(f"setup: M={dct['M']}, fem-deg={dct['fem_deg']}, nT={dct['nT']}")

        # setup
        info = {"problem": {"name": "darcy"},
                "fe": {"degree": dct["fem_deg"], "mesh": dct["nT"]},
                "fe_coeff": {"degree": dct["fem_deg"]-1, "space": "DG"},
                "expansion": {
                    "size": M, "mean": mean, "decay rate": decay,
                    "rho": 1, "vartheta": 0.1, "scale": 6 / np.pi**2},
                "sampling": {
                    "distribution": "normal", "expfield": True,
                    "batch size": 40}
                }
        p = Problem(info)
        log(f"FE-DoFs: {p.space.dim()}")

        # ----------------------------------------------------------------------
        # training and test data forward problem
        N = 1000

        log(f"generate {N:,} training samples ...")
        with timeit("... ({:4.2f} s)"):
            ys, ws, gs = generate_samples(p, N)

        log(f"generate {1000:,} test samples ...")
        with timeit("... ({:4.2f} s)"):
            ys_test, ws_test, gs_test = generate_samples(p, 1000)

        log("generate initial condition samples ...")
        with timeit("... ({:4.2f} s)"):
            y0 = np.zeros([1, M])
            x0 = fem.dof_coords(p.space, p.mesh)  # fem-dofs
            *_, g0 = generate_samples(p, 1, samples=y0)

        # measurement data
        log("generate measurement ...")
        with timeit("... ({:4.2f} s)"):
            ys_true, ws_true, gs_true = generate_samples(p, 1)
            sigma = 1*np.std(gs, axis=0)
            noise = np.random.multivariate_normal(
                np.zeros(gs.shape[1]), np.diag(sigma**2), 1)
            delta = gs_true + noise

        # training and test data (log-)likelihood
        hs = eval_log_likelihood(gs, delta, p.space, scale=1)
        hs_test = eval_log_likelihood(gs_test, delta, p.space, scale=1)
        h0 = eval_log_likelihood(g0, delta, p.space, scale=1)

        us, us_test, exph0 = np.exp(hs), np.exp(hs_test), np.exp(h0)

        # ----------------------------------------------------------------------
        # reconstruct solution G(y) with VMC

        log("reconstruct forward problem G(y) ...")
        with timeit("... ({:4.2f} s)"):
            G_TT = tt.reconstruct(
                dims=[2]*M, nodes=ys.T, weights=ws, values=gs, dist="normal",
                init=None, label="G_TT", verbose=False
            )

        # from IPython import embed; embed()
        abs_err, rel_err = compute_L2L2_error(ys_test, gs_test, G_TT, p.space)
        log(f"error G_TT: {abs_err:4.2e}/{rel_err:4.2e}")

        # ----------------------------------------------------------------------
        # build log-likelihood TT

        # This is h(y) = - || delta - G(y) ||_s^2
        log("compute log-likelihood TTTensor h_TT ...")
        with timeit("... ({:4.2f} s)"):
            h_TT = log_likelihood(G_TT, delta, p.space, scale=1)

        log(f"ranks h_TT: {h_TT.ranks()}")
        abs_err, rel_err = compute_L2_error(ys_test, hs_test, h_TT)
        log(f"error h_TT: {abs_err:4.2e}/{rel_err:4.2e}")

        # ----------------------------------------------------------------------
        # compute likelihood TT

        log("compute expTT reconstruction u_TT ...")
        with timeit("... ({:4.2f} s)"):
            u_TT = expTT_reconstruction_dupe(
                expTT_dims, h_TT, y0, exph0=exph0, verbose=2)

        abs_err, rel_err = compute_L2_error(ys_test, us_test, u_TT)
        log(f"error u_TT: {abs_err:4.2e}/{rel_err:4.2e}")

    # from IPython import embed; embed(); sys.exit()
