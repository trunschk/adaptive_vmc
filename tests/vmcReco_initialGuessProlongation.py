# -*- coding: utf-8 -*-
import sys, os
from copy import deepcopy
import time, numpy as np
import matplotlib.pyplot as plt

from fenics import *
import xerus as xe

from context import src, avmc_intrusive
from src.misc import timeit, log, Logger
from src.problem.equation.darcy import Problem
from src.utils.tt_tools import reconstruct, get_initial_guess, eval_approx
from src.utils.fem_tools import refine_mesh, sample_diffusion_coeff, prolongate_x_2
from avmc_intrusive import compute_approx_error_coeff

# ------------------------------------------------------------------------------
def generate_random_refinement(info, n_refs=2, theta=0.2):
    info_tmp = deepcopy(info)
    p = Problem(info_tmp)
    for j in range(n_refs):
        nmc = int(theta*p.mesh.num_cells()) # number of marked cells
        marked_cells = np.random.permutation(np.arange(p.mesh.num_cells()))[:nmc]
        info_tmp = refine_mesh(marked_cells, info_tmp)
        p = Problem(info_tmp)
    return info_tmp

# ------------------------------------------------------------------------------
def reconstruct_a(dims, nodes, weights, values, dist=None, init=None, label="", verbose=False):
    """
    Wrapper to time reconstruction.
    """
    tic = time.perf_counter()
    a_reco = reconstruct(dims=dims, nodes=nodes, weights=weights, 
            values=values, init=init, dist=dist, label=label, verbose=verbose)
    toc = time.perf_counter()-tic
    return a_reco, toc
    
# ------------------------------------------------------------------------------
def print_times(times):
    log("   "+"_"*58)
    for k,v in times.items():
        log("  | {:<45s}  {:>6.2f} s  |".format(k,v))
    log("  |"+"_"*58+"|")

# ------------------------------------------------------------------------------
def plot_fenics_function(axis, val, space, mesh=None, cmap=None):
    u = Function(space)
    u.vector().set_local(val)
    
    plt.sca(axis)
    if cmap is not None: plt.set_cmap(cmap)
    im = plot(u)
    if mesh is not None: plot(mesh)
    plt.colorbar(im)
    if cmap is not None: plt.set_cmap("viridis")
    return None

# ------------------------------------------------------------------------------
def plot_prolongation(val_coarse, val_fine, space_coarse, space_fine, 
        mesh_coarse=None, mesh_fine=None, file_name="../img/tmp.png", title=""):

    fig, ax = plt.subplots(figsize=(10,5), nrows=1, ncols=2)
    
    # coarse mesh
    plot_fenics_function(ax[0], val_coarse, space_coarse, mesh_coarse)
    ax[0].set_title("original")

    # fine mesh
    plot_fenics_function(ax[1], val_fine, space_fine, mesh_fine)
    ax[1].set_title("prolongation")
    
    fig.suptitle(title)
    plt.savefig(file_name)

# ------------------------------------------------------------------------------
def plot_reco(val_c, space_c, vals_f, space_f, file_name="../img/tmp.png", title=""):
    fig, ax = plt.subplots(figsize=(15,12), nrows=3, ncols=3)
    
    # (1) sample
    plot_fenics_function(ax[0,0], vals_f[0], space_f)
    ax[0,0].set_title("(1) sample")
    
    # (2) coarse mesh
    plot_fenics_function(ax[0,1], val_c, space_c)
    ax[0,1].set_title("(2) coarse")

    # (3) difference | (1) - (2) |
    u, v = Function(space_c), Function(space_f)
    u.vector().set_local(val_c)
    v = interpolate(u, space_f)
    v = v.vector().get_local()
    plot_fenics_function(ax[0,2], np.abs(v-vals_f[0]), space_f, cmap="inferno")
    ax[0,2].set_title("(3) difference $\\vert\, (1)-(2)\,\\vert$")

    # (4) fine mesh, random init
    plot_fenics_function(ax[1,0], vals_f[1], space_f)
    ax[1,0].set_title("(4) fine, random init")
    
    # (5) initial guess
    plot_fenics_function(ax[1,1], vals_f[2], space_f)
    ax[1,1].set_title("(5) initial guess")
    
    # (6) fine mesh, prolong init
    plot_fenics_function(ax[1,2], vals_f[3], space_f)
    ax[1,2].set_title("(6) fine, prolong init")
    
    # (7) difference | (1) - (4) |
    plot_fenics_function(ax[2,0], np.abs(vals_f[0]-vals_f[1]), space_f, cmap="inferno")
    ax[2,0].set_title("(7) difference $\\vert\, (1)-(4)\,\\vert$")
    
    # (8) difference | (1) - (5) |
    plot_fenics_function(ax[2,1], np.abs(vals_f[0]-vals_f[2]), space_f, cmap="inferno")
    ax[2,1].set_title("(8) difference $\\vert\, (1)-(5)\,\\vert$")
    
    # (9) difference | (1) - (6) |
    plot_fenics_function(ax[2,2], np.abs(vals_f[0]-vals_f[3]), space_f, cmap="inferno")
    ax[2,2].set_title("(9) difference $\\vert\, (1)-(6)\,\\vert$")
    
    fig.suptitle(title)
    plt.savefig(file_name)

# ------------------------------------------------------------------------------
def plot_time_deviation(times, errors, file_name="../img/tmp.png", title=""):
    t, e = np.array(times).T, np.array(errors).T
    labels = [ "coarse reco", "fine reco: \n random init", "generate init", 
            "fine reco: \n prolong init"]
    
    fig, ax = plt.subplots(figsize=(15,7), nrows=1, ncols=2)
    
    ax[0].boxplot(t)
    ax[0].set_ylabel("time [s]")
    ax[0].set_xticklabels(labels) 
    ax[0].get_xaxis().tick_bottom()
    ax[0].get_yaxis().tick_left()
    ax[0].set_title("reconstruction time")
    ax[0].grid(axis="y")
    
    ax[1].boxplot(e)
    ax[1].set_yscale('log')    
    ax[1].set_ylabel("error")
    ax[1].set_xticklabels(labels) 
    ax[1].get_xaxis().tick_bottom()
    ax[1].get_yaxis().tick_left()
    ax[1].set_title("reconstruction error L2-L2")
    ax[1].grid(axis="y")
    
    fig.suptitle(title)
    plt.savefig(file_name)
    
# ------------------------------------------------------------------------------
if __name__ == "__main__":
    sys.stdout = Logger(fileName = "output.log", filePath = "./log/")
    print()
    
    # initial setup
    # plot prolongation of random function -------------------------------------
    if True:
        log("TEST 1: Compute prolongation of random DG-0 function")
        log("-"*60)
        
        info = {
            "problem": {"name": "darcy"},
            "fe": { "degree": 1, "mesh": 4 },
            "fe_coeff": { "degree": 0, "space":"DG" },
            "expansion": { "size": 20, "mean": 1.0, "decay rate": 2.0,
                "scale": 6 / np.pi**2  # scale <= mean * 6 / np.pi**2 for decay == 2
            },
            "sampling": { "distribution": "uniform", "batch size": 40, "expfield": False }
        }

        # generate coarse mesh
        info_coarse = generate_random_refinement(info, n_refs=1, theta=0.2)
        p_coarse = Problem(info_coarse) # coarse problem
        # generate fine mesh
        info_fine = generate_random_refinement(info_coarse, n_refs=1, theta=0.2)
        p_fine = Problem(info_fine) # fine problem
        
        log("  generate random function")
        s = np.random.uniform(0.5,1.5,2)
        g = Expression("sin(s1*2*pi*x[0])*cos(s2*2*pi*x[1])", 
                s1=s[0], s2=s[1], degree=5)
        g = interpolate(g, p_coarse.space_coeff)
        u_coarse = g.vector().get_local()
        coarse_space = p_coarse.space_coeff
        fine_space = p_fine.space_coeff
        
        log("  prolong function")
        u_fine = prolongate_x_2(u_coarse.reshape(1,-1), coarse_space, fine_space)[0]
        
        log("  plot result")
        file_name = "../img/test_prolongation_FEprolong.png"
        title = "Prolongation of arbitrary {}-{} function".format(
                info["fe_coeff"]["space"], info["fe_coeff"]["degree"]
                )
        plot_prolongation(u_coarse, u_fine, coarse_space, fine_space, 
                p_coarse.mesh, p_fine.mesh, file_name, title)
        log(f"    save path: {file_name}")
        print()

    # generate initial guess for fine mesh -------------------------------------
    if False:
        log("TEST 2: Generate initial guess for reconstruction")
        log("-"*60)
        
        info = {
            "problem": {"name": "darcy"},
            "fe": { "degree": 3, "mesh": 5 },
            "fe_coeff": { "degree": 2, "space":"DG" },
            "expansion": { "size": 20, "mean": 1.0, "decay rate": 2.0,
                "scale": 6 / np.pi**2  # scale <= mean * 6 / np.pi**2 for decay == 2
            },
            "sampling": { "distribution": "uniform", "batch size": 40, "expfield": False }
        }
        n_samples = 100
        
        # generate coarse mesh
        info_coarse = generate_random_refinement(info, n_refs=0, theta=0.3)
        p_coarse = Problem(info_coarse) # coarse problem
        # generate fine mesh
        info_fine = generate_random_refinement(info_coarse, n_refs=2, theta=0.3)
        p_fine = Problem(info_fine) # fine problem
        
        times = {}
        
        # generate samples
        log("GENERATE SAMPLES")
        log(f"  generate {n_samples} samples on coarse mesh ...")
        with timeit("  ... ({:4.2f}) s"):
            ys_c, ws_c, cs_c = sample_diffusion_coeff(info_coarse, n_samples)
        log(f"  generate {n_samples} samples on fine mesh ...")
        with timeit("  ... ({:4.2f}) s"):
            ys_f, ws_f, cs_f = sample_diffusion_coeff(info_fine, n_samples)
        sdim = [2]*info_coarse["expansion"]["size"]
        
        # reconsttruct on coarse mesh
        log("RECONSTRUCT a on coarse level")
        a_reco_coarse, t = reconstruct_a(
                dims=sdim, nodes=ys_c.T, weights=ws_c, values=cs_c, 
                dist=info["sampling"]["distribution"],
                init=None, label="a", verbose=True
                )
        times["(1) reco coarse"] = t
        _ = compute_approx_error_coeff(a_reco_coarse, info_coarse, verbose=True)
        
        # reconsttruct on fine mesh (no init guess)
        log("RECONSTRUCT a on fine level -- no initial guess")
        a_reco_fine, t = reconstruct_a(
                dims=sdim, nodes=ys_f.T, weights=ws_f, values=cs_f, 
                dist=info["sampling"]["distribution"],
                init=None, label="a", verbose=True
                )
        times["(2) reco fine, random init"] = t
        _ = compute_approx_error_coeff(a_reco_fine, info_fine, verbose=True)

        # generate prolongation of a_reco_coarse and compare to a_reco_fine
        log("GENERATE INITIAL GUESS")
        dims = [p_fine.space_coeff.dim()] + sdim
        init = get_initial_guess(
                dims, a_reco_coarse, p_coarse.space_coeff, p_fine.space_coeff
                )
        _ = compute_approx_error_coeff(init, info_fine, verbose=True)

        # reconstruct on fine mesh (with init guess)
        log("RECONSTRUCT a on fine level -- with initial guess")
        a_reco_fine_init, t = reconstruct_a(
                dims=sdim, nodes=ys_f.T, weights=ws_f, values=cs_f, 
                dist=info["sampling"]["distribution"],
                init=init, label="a", verbose=True
                )
        times["(3) reco fine, prolong init"] = t
        _ = compute_approx_error_coeff(a_reco_fine_init, info_fine, verbose=True)
        
        # plot a for test sample
        log("PLOT RECO APPROXIMATION in sample ...")
        ys_t, ws_t, cs_t = sample_diffusion_coeff(info_fine, 1)
        val_coarse = eval_approx(
                a_reco_coarse, ys_t, info_coarse["sampling"]["distribution"]
                )[0]
        vals_fine = []
        vals_fine += [cs_t[0]]
        vals_fine += [eval_approx(a_reco_fine, ys_t, info_fine["sampling"]["distribution"])[0]]
        vals_fine += [eval_approx(init, ys_t, info_fine["sampling"]["distribution"])[0]]
        vals_fine += [eval_approx(a_reco_fine_init, ys_t, info_fine["sampling"]["distribution"])[0]]
        
        title = "Reco in random sample"
        file_name = "../img/test_prolongation_reco_smpl.png"
        plot_reco( val_coarse, p_coarse.space_coeff, vals_fine, p_fine.space_coeff,
                file_name=file_name, title=title)
        log(f"    save path: {file_name}")
        
        # print times for reconstructions
        log("Computation times:")
        print_times(times)
        
        print()
        
    # generate initial guess for fine mesh -------------------------------------
    if True:
        log("TEST 3: Test reco time with initial guess")
        log("-"*60)
        
        info = {
            "problem": {"name": "darcy"},
            "fe": { "degree": 3, "mesh": 2 },
            "fe_coeff": { "degree": 2, "space":"DG" },
            "expansion": { "size": 20, "mean": 1.0, "decay rate": 2.0,
                "scale": 6 / np.pi**2  # scale <= mean * 6 / np.pi**2 for decay == 2
            },
            "sampling": { "distribution": "uniform", "batch size": 40, "expfield": False }
        }
        n_samples = 1000
        N_RUNS = 2
        times = [[],[],[],[]]
        errors = [[],[],[],[]]
        
        for i_run in range(N_RUNS):
            if N_RUNS > 1: log(f"RUN {i_run+1} / {N_RUNS}")
            # generate coarse mesh
            info_coarse = generate_random_refinement(info, n_refs=1, theta=0.3)
            p_coarse = Problem(info_coarse) # coarse problem
            # generate fine mesh
            info_fine = generate_random_refinement(info_coarse, n_refs=1, theta=0.3)
            p_fine = Problem(info_fine) # fine problem
            
            info_coarse["expansion"]["size"] = 10
        
            # generate samples
            log(f"  generate {n_samples} samples on coarse mesh ...")
            ys_c, ws_c, cs_c = sample_diffusion_coeff(info_coarse, n_samples)
            log(f"  generate {n_samples} samples on fine mesh ...")
            ys_f, ws_f, cs_f = sample_diffusion_coeff(info_fine, n_samples)
            sdim_coarse = [2]*info_coarse["expansion"]["size"]
            sdim_fine = [2]*info_fine["expansion"]["size"]
        
            # reconsttruct on coarse mesh
            log("  reconstruct a on coarse level")
            a_reco_coarse, t = reconstruct_a(
                    dims=sdim_coarse, nodes=ys_c.T, weights=ws_c, values=cs_c, 
                    dist=info["sampling"]["distribution"],
                    init=None, label="", verbose=True
                    )
            e = compute_approx_error_coeff(a_reco_coarse, info_coarse, verbose=False)
            times[0] += [t]
            errors[0] += [e]
        
            # reconsttruct on fine mesh (no init guess)
            log("  reconstruct a on fine level -- no initial guess")
            a_reco_fine, t = reconstruct_a(
                    dims=sdim_fine, nodes=ys_f.T, weights=ws_f, values=cs_f, 
                    dist=info["sampling"]["distribution"],
                    init=None, label="", verbose=True
                    )
            e = compute_approx_error_coeff(a_reco_fine, info_fine, verbose=False)
            times[1] += [t]
            errors[1] += [e]

            # generate prolongation of a_reco_coarse and compare to a_reco_fine
            log("  generate initial guess")
            dims = [p_fine.space_coeff.dim()] + sdim_fine
            tic = time.perf_counter()
            init = get_initial_guess(
                    dims, a_reco_coarse, p_coarse.space_coeff, p_fine.space_coeff
                    )
            times[2] += [time.perf_counter()-tic]
            e = compute_approx_error_coeff(init, info_fine, verbose=False)
            errors[2] += [e]

            # reconstruct on fine mesh (with init guess)
            log("  reconstruct a on fine level -- with initial guess")
            a_reco_fine_init, t = reconstruct_a(
                    dims=sdim_fine, nodes=ys_f.T, weights=ws_f, values=cs_f, 
                    dist=info["sampling"]["distribution"],
                    init=init, label="", verbose=True
                    )
            e = compute_approx_error_coeff(a_reco_fine_init, info_fine, verbose=False)
            times[3] += [t]
            errors[3] += [e]

            # plot times
            log("  plot average times")
            file_name = "../img/test_prolongation_reco_times.png"
            title = ( f"expansion length L = {info['expansion']['size']}, "
                    + f" decay = {info['expansion']['decay rate']}, "
                    + f" runs = {i_run+1}, "
                    + f" n_samples = {n_samples}, \n"
                    + f" FEM space = {info['fe_coeff']['space']}-{info['fe_coeff']['degree']}, "
                    + f" FEM dofs coarse = {p_coarse.space_coeff.dim()}, "
                    + f" FEM dofs fine = {p_fine.space_coeff.dim()}"
                    )
            
            plot_time_deviation(times, errors, file_name, title)
            log(f"    save path: {file_name}")
        
        print()
    print("\n"*3)
