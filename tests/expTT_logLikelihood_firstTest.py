import numpy as np
import xerus as xe

from context import src, avmc_intrusive
from src.misc import timeit, log
import src.utils.tt_tools as tt
import src.utils.tt_exp as expTT
import autoPDB

# ==============================================================================
# utiliy functions


def compute_L2_error(tt_tensor, xs, ys):
    tt_eval = tt.eval_approx(tt_tensor, xs, "normal")
    abs_err = np.sqrt(np.sum((ys-tt_eval)**2)/ys.shape[0])
    rel_err = abs_err / np.sqrt(np.sum(ys**2)/ys.shape[0])
    return abs_err, rel_err


def log_likelihood(x, mu=0, sigma=10):
    """
    l(x) = - || x - mu ||^2 / (2*sigma^2)

    Parameter
    ---------
    x     : shape (..., M)
    mu    : float (mean)
    sigma : float (std)
    """
    # return np.ones(x.shape[0])
    return -np.linalg.norm(x-mu, axis=-1)**2 / (2*sigma**2)


def expTT_reconstruction(da, dt, h, x_0, method=None, verbose=False):
    """
    see expTT.expTT_reconstruction()
    """
    M = len(da)

    # build operator and rhs
    W0, b = expTT.build_system_inexactIC(da, dt, h, x_0, method=method)
    W0.round(1e-16)

    # random initialization
    u_start = xe.TTTensor.random(da, [2]*(M-1) if M > 1 else [])
    u_start = u_start / xe.frob_norm(u_start)

    # run simple ALS
    ret = expTT.run_simpleALS(A=W0, u_start=u_start, b=b, verbose=verbose)

    # run xerus ALS
    # ret = xe.TTTensor(u_start)
    # xe.ALS_SPD(W0, ret, b)

    ret.round(1e-12)  # round to get unneccesary large ranks down

    ret = tt.add_dimension_left(ret)  # add FEM component

    return ret


# ==============================================================================
if __name__ == "__main__":
    # TODO write more detailed how the setting looks
    """ Test expTT reconstuction for log-likelihood in any dimension.

    Assume here that the exponent h is given by
                h(x) = -|| x - mu ||^2 / (2*sigma^2)

    NOTE:
        - This version only tests and supports the expTT reconstuction for
          functions h with one dimensional real output.
        - The dimensions of the test space dt must be larger or equal to the
          dimensions of the ansatz space da to prevent information loss.
    """

    # setup

    M = 20  # number of random variables
    dim = 4
    da, dt = [dim]*M, [dim]*M
    y0 = np.zeros((1, M))

    # --------------------------------------------------------------------------
    # construct log-likelihood

    ys = np.random.normal(0, 1, (10000, M))
    ws = np.ones(ys.shape[0])/ys.shape[0]
    hs = log_likelihood(ys).reshape(-1, 1)

    log("reconstruct h ...")
    h_reco = tt.reconstruct(
        dims=[3]*M, nodes=ys.T, weights=ws,
        values=hs, dist="normal", label="h reco",
        verbose=False
    )

    ys_test = np.random.normal(0, 1, (1000, M))
    hs_test = log_likelihood(ys_test).reshape(-1, 1)

    abs_err, rel_err = compute_L2_error(h_reco, ys_test, hs_test)
    log(f"rel_err h_reco: {rel_err:4.2e}")

    # --------------------------------------------------------------------------
    # expTT reconstruction for TT case

    u_reco_expTT = expTT_reconstruction(
        da, dt, h_reco, y0, method=None, verbose=2
    )

    ys_test = np.random.normal(0, 1, (1000, M))
    us_test = np.exp(log_likelihood(ys_test).reshape(-1, 1))

    abs_err, rel_err = compute_L2_error(u_reco_expTT, ys_test, us_test)
    log(f"rel_err u_reco_expTT: {rel_err:4.2e}")

    # from IPython import embed; embed()
    exit()

    # --------------------------------------------------------------------------
    # naive reconstruction
    # NOTE: Reconstruct exp(h) with VMC.
    # naive_reconstruction(h_coeff, da, x_0)

    # --------------------------------------------------------------------------
    # from IPython import embed; embed(); exit() # debug line
