import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate, scipy.special
import itertools

# ______________________________________________________________________________
# problem dependent stuff

def eval(c,P,x):
    if np.array(x).ndim == 0: x = np.array(x).reshape(1)
    assert len(c) <= len(P)
    val = np.zeros_like(x)
    for j in range(len(c)):
        val = val + c[j]*P[j](x)
    return val

def get_coeff_b(s):
    B = np.array([-(s**2+1), 2*s, -np.sqrt(2)])                                 # b w.r.t. normal Hermite poly.
    sB = np.array((-1,0,-np.sqrt(2)))                                           # b w.r.t. shifted Hermite poly.
    DB = np.array([B[j+1]*np.sqrt(j+1) for j in range(sB.size-1)])              # derivative of b w.r.t. normal Hermite poly.
    sDB = np.array([sB[j+1]*np.sqrt(j+1) for j in range(sB.size-1)])            # derivative of b w.r.t. shifted Hermite poly.
    return B, sB, DB, sDB

def b(x,B,s):
    # NOTE: Needs coefficients of b w.r.t. shifted Hermite poly.
    P = shifted_hermite(2,s)
    return eval(B,P,x)

def a(x,B,s): 
    # NOTE: Needs coefficients of b w.r.t. shifted Hermite poly.
    return np.exp(b(x,B,s=s))

def rho_s(x,s=3): return np.exp(-(x-s)**2/2)/np.sqrt(2*np.pi) 
def rho(x): return rho_s(x,0)

fac = lambda n: np.math.factorial(n)
norm = lambda n: 1 / np.sqrt(float(fac(n)))
hermite_unnormalized = lambda deg: np.polynomial.hermite_e.HermiteE.basis(deg)

hermite = lambda deg: [norm(d)*hermite_unnormalized(d) for d in range(deg+1)]
def shifted_hermite(deg, s):
    return [norm(d)*hermite_unnormalized(d)(np.polynomial.Polynomial((-s,1))) 
            for d in range(deg+1)]

def inner_prod(P1, P2, pdf):
    f = lambda x: P1(x)*P2(x)*pdf(x)
    return scipy.integrate.quad(f, -np.inf, np.inf)[0]

def bcm_slow(P1, P2, pdf, s):
    """
    Change basis from P2 to P1.
    """
    mat = np.zeros((len(P2),len(P1)))
    for i,p in enumerate(P1):
        for j,q in enumerate(P2):
            mat[i,j] = inner_prod(p,q,pdf)
    return mat

def bcm_shift(dim_Phi, dim_P, s, pdf_type="rho_s"):
    """
    The base-change matrix from {P_0, P_1, ...} to {P^s_0, P^s_1, ...} is
    given by
        < P^s_i, P_j >_rho_s = sqrt(i!)/sqrt(j!) binom(j,i) s**(j-i),
    where binom(j,i)=0 for i > j.
    """
    # NOTE: special case for base_change_matrix for faster computation. 
    if np.linalg.norm(s) < 1e-16: 
        return np.eye(dim_Phi, dim_P)
    
    ret = np.zeros((dim_Phi, dim_P))
    for i in range(dim_Phi):
        for j in range(dim_P):
            c = np.sqrt(float(fac(i))) / np.sqrt(float(fac(j)))
            if pdf_type == "rho_s":
                ret[i,j] = c * scipy.special.binom(j,i) * s**(j-i)
            elif pdf_type == "rho":
                ret[i,j] = (-1)**(i-j) * 1/c * scipy.special.binom(i,j) * s**(i-j)
    return ret

def herm_triple_product(a, b, c, normalized=False):
    """
    Compute int_{-1}^{1} P_a P_b P_c dx for 1D probabilist Hermite Polynomials
    P by formula. If normalized is true, use normalized probabilist Hermite
    polynomials in the triple product.
    """
    if not (a+b+c) % 2 == 0: return 0
    if (a+b-c < 0) or (a-b+c < 0) or (-a+b+c < 0): return 0
    
    if normalized == False: normalized = [False]*3
    elif normalized == True: normalized = [True]*3
    assert len(normalized) == 3
    
    A = lambda i,j,k: fac((i+j+k)/2)
    # NOTE - Normalization constant is 1/sqrt(n!)
    #      - measure is d\rho = exp(-x**2/2) / sqrt(2*pi) dx
    fs = lambda n: 1/np.sqrt(fac(n)) # normalization constant
    norm_const = np.prod([
        fs(n) if normalized[i] else 1 for i,n in enumerate([a,b,c])
        ])
    val = fac(a)*fac(b)*fac(c) / A(a,b,-c) / A(a,-b,c) / A(-a,b,c) * norm_const
    return float(val)

# ______________________________________________________________________________
# stuff for computation of a' = a b' + IC

def Dmat(Phi, P, pdf, integrate=False):
    """
    D_{ij} = < Phi_i, P'_j >_pdf = sqrt(j) < Phi_i, P_{j-1} >_pdf

    If integrate is true, use numerical quadrature to compute the inner product.
    """
    dim_P, dim_Phi = len(P), len(Phi)
    ret = np.zeros((dim_Phi, dim_P))
    if integrate:
        for j in range(min(dim_Phi,dim_P-1)):
            ret[j,j+1] = np.sqrt(j+1)*inner_prod(Phi[j], P[j], pdf)
    else:
        j = np.arange(np.min(ret.shape-np.array([0,1])))
        ret[j,j+1] = np.sqrt(j+1)
    return ret

def Mmat(Phi, P, B, pdf):
    """
    M_{ij} = < Phi_i, b' P_j >_pdf

    This assumes coefficients of b w.r.t. basis {P_j}.
    """
    dim_P, dim_Phi = len(P), len(Phi)
    Q = P[:len(B)]
    DB = Dmat(Q, Q, pdf) @ B
    K = kappa(Phi, Q, P, pdf)

    return np.einsum("ilj,l->ij",K,DB)

def kappa(Phi, Q, P, pdf, normalized=True, integrate=False):
    d1, d2, d3 = len(Phi), len(Q), len(P)
    K= np.zeros([d1,d2,d3])
    for i,l,j in itertools.product(range(d1), range(d2), range(d3)): 
        if integrate:
            K[i,l,j] = inner_prod(Phi[i],lambda x: Q[l](x)*P[j](x), pdf)
        else:
            K[i,l,j] = herm_triple_product(i,l,j,normalized=normalized)
    return K

def Smat(Phi, P, B, pdf): 
    return Dmat(Phi, P, pdf) - Mmat(Phi, P, B, pdf)

def build_system_reduced(Phi, P, B, pdf, s, x_0):
    assert len(Phi) == len(P)-1
    sMat = Smat(Phi, P, B, pdf)
    icMat =  np.array([p(x_0) for p in P]).reshape(1,-1) # this is P(0)
    mat = np.concatenate([sMat, icMat], axis=0)
    assert mat.shape == (len(P), len(P))
    rhs = np.zeros(len(P))
    rhs[-1] = a(x_0, B, s)
    return mat, rhs

def solve(mat, vec):
    c = np.linalg.solve(mat, vec)
    res = vec - mat @ c
    oldNormRes = np.inf
    while np.linalg.norm(res) < oldNormRes:  # while res decreases
        oldNormRes = np.linalg.norm(res)
        c += np.linalg.solve(mat, res)
        res = vec - mat @ c
    return c

def compute_error(c, P, pdf, s, sB):
    N = 1000
    x = np.random.normal(0,1,N)+s # shift by tau^{-1}(x) = x + s
    y_true = a(x, sB, s).flatten()
    y_approx = eval(c, P, x).flatten()
    diff = y_true - y_approx
    return np.sqrt(np.sum(diff**2)/N)

def get_a_reduced_std(deg, B, x_0, s):
    """
    Standard reconstruction case. Ansatz and Test space are standard Hermite
    polynomials. Compute the approximation of a = exp b for b given as
    coefficients w.r.t. standard Hermite basis.

    Parameter
    ---------
    deg : int
        Maximum degree of expansion for a.
    B   : array_like of size 3
        Coefficient of b w.r.t. standard (normalized) Hermite basis.
    s   : float
        Shift of b(x) = -(x-s)**2.
    x_0 : float
        Point to evaluate initial conditions is.
    """
    P, Phi = hermite(deg), hermite(deg-1)
    sMat, rhs = build_system_reduced(Phi, P, B, rho, s, x_0)
    A = solve(sMat, rhs)
    return A, P, rho

def get_a_reduced_shift(deg, sB, x_0, s):
    """
    Standard reconstruction case only for pdf and polynomials shifted. Ansatz
    and Test space are shifted Hermite polynomials, pdf is shifted Gaussian.
    Compute the approximation of a = exp b for b given as coefficients w.r.t.
    shifted Hermite basis.

    Parameter
    ---------
    deg : int
        Maximum degree of expansion for a.
    sB  : array_like of size 3
        Coefficient of b w.r.t. shifted (normalized) Hermite basis.
    s   : float
        Shift of b(x) = -(x-s)**2.
    x_0 : float
        Point to evaluate initial conditions is.
    """
    P, Phi = shifted_hermite(deg, s), shifted_hermite(deg-1, s)
    pdf = lambda x: rho_s(x,s)
    sMat, rhs = build_system_reduced(Phi, P, sB, pdf, s, x_0)
    A = solve(sMat, rhs)
    return A, P, pdf

def get_a_reduced_mixed(deg, B, x_0, s):
    """
    Mixed reconstruction case for different ansatz and test space. Ansatz
    space is standard Hermite and test space is shifted Hermite polynomials,
    pdf is shifted Gaussian. Compute the approximation of a = exp b for b
    given as coefficients w.r.t. standard Hermite basis.

    Parameter
    ---------
    deg : int
        Maximum degree of expansion for a.
    B   : array_like of size 3
        Coefficient of b w.r.t. standard (normalized) Hermite basis.
    s   : float
        Shift of b(x) = -(x-s)**2.
    x_0 : float
        Point to evaluate initial conditions is.
    """
    P, Phi = hermite(deg), shifted_hermite(deg, s)
    pdf = lambda x: rho_s(x,s)
    G = bcm_shift(len(Phi), len(P), s, pdf_type="rho_s")                        # NOTE: same as bcm_slow(Phi, P, pdf, s)
    sMat = np.dot( Smat(Phi[:-1], Phi, np.dot(G[:len(B),:len(B)],B), pdf), G)
    icMat =  np.array([p(x_0) for p in P]).reshape(1,-1) # this is P(0)
    mat = np.concatenate([sMat, icMat], axis=0)
    assert mat.shape == (len(P), len(P))
    rhs = np.zeros(len(P))
    rhs[-1] = a(x_0, B, 0)
    
    A = solve(mat, rhs)
    return A, P, pdf

# NOTE: This is not doing what we want due to different integration densities
#       rho vs. rho_s.
def get_a_reduced_naiv(deg, B, x_0, s):
    """
    Naiv mixed reconstruction case for different ansatz and test space. Ansatz
    space is standard Hermite and test space is shifted Hermite polynomials,
    pdf is shifted Gaussian. Compute the approximation of a = exp b for b
    given as coefficients w.r.t. standard Hermite basis.

    Parameter
    ---------
    deg : int
        Maximum degree of expansion for a.
    B   : array_like of size 3
        Coefficient of b w.r.t. standard (normalized) Hermite basis.
    s   : float
        Shift of b(x) = -(x-s)**2.
    x_0 : float
        Point to evaluate initial conditions is.
    """
    P, Phi = hermite(deg), shifted_hermite(deg, s)
    pdf = lambda x: rho_s(x,s)
    G = bcm_shift(len(Phi), len(P), s, pdf_type="rho_s")                        # NOTE: same as bcm_slow(Phi, P, pdf, s)
    sMat, rhs = build_system_reduced(P[:-1], P, B, rho, s, x_0)
    A = solve(np.dot(G,sMat), rhs)
    return A, P, rho

# ______________________________________________________________________________
if __name__ == "__main__":
    
    deg = 10
    shift = [3]
    shift = np.arange(0, 8, 0.20)

    # # XXX
    # t = 2
    # H, sH = hermite(4), shifted_hermite(4, t)
    # BCM = bcm_shift(len(sH), len(H), t, pdf_type="rho_s")
    # print(np.dot(BCM.T,BCM))
    # raise AssertionError
    # # XXX

    err_std, err_shift, err_mixed, err_naiv = [], [], [], []
    fmttr = "{:<15}" + "{:<13.2e}"*3
    print("{:14} {:12} {:12} {:12}".format("iter","err_std", "err_mixed", "err_naiv"))
    print("_"*50)
    
    for j, s in enumerate(shift):

        B, sB, DB, sDB = get_coeff_b(s)
        
        # ----------------------------------------------------------------------
        # solve problem with reduced ansatz and standard Hermite polynomials as
        # ansatz and test space and standard normal pdf.
        x_0_std = 0
        A_std, P_std, pdf_std = get_a_reduced_std(deg, B, s, x_0_std)
        err = compute_error(A_std, P_std, pdf_std, s, sB)
        err_std.append(err)
        
        # ----------------------------------------------------------------------
        # NOTE this is only for debug/reference
        # solve problem with reduced ansatz and shifted Hermite polynomials as
        # ansatz and test space and shifted pdf.
        
        # x_0_shift = s
        # A_shift, P_shift, pdf_shift = get_a_reduced_shift(deg, sB, s, x_0_shift)
        # err_shift = compute_error(A_shift, P_shift, pdf_shift, s, sB)
        # err = 0
        # err_shift.append(err)

        # ----------------------------------------------------------------------
        # solve problem with reduced ansatz and different ansatz and test space
        # and shifted pdf.
        x_0_mixed = s
        A_mixed, P_mixed, pdf_mixed = get_a_reduced_mixed(deg, B, s, x_0_mixed)
        err = compute_error(A_mixed, P_mixed, pdf_mixed, s, sB)
        err_mixed.append(err)
        
        # ----------------------------------------------------------------------
        # solve problem with reduced ansatz and different ansatz and test space
        # and shifted pdf naively. ( BS A = 0)
        x_0_naiv = 0
        A_naiv, P_naiv, pdf_naiv = get_a_reduced_naiv(deg, B, s, x_0_naiv)
        err = compute_error(A_naiv, P_naiv, pdf_naiv, s, sB)
        err_naiv.append(err)
        
        # ----------------------------------------------------------------------
        # print
        iter_str = "{:>2d}/{:d}".format(j+1,len(shift))
        print(fmttr.format(iter_str, err_std[-1], err_mixed[-1], err_naiv[-1]))
        
        # ----------------------------------------------------------------------
        # compute
        xx = np.linspace(-4,12,200)
        a_true = a(xx, sB, s)
        a_approx_std = eval(A_std, P_std, xx)
        # a_approx_shift = eval(A_shift, P_shift, xx)
        a_approx_mixed = eval(A_mixed, P_mixed, xx)
        
        # ----------------------------------------------------------------------
        # plot
        fig, ax = plt.subplots(figsize=(13,4.5), nrows=1, ncols=3)
        
        ax[0].axvline(x=x_0_std, c="k", ls="--", label="$x_0$")
        ax[0].plot(xx, rho(xx), "k", label="$\\rho$")
        ax[0].plot(xx, a_true, label="$u$", lw=3)
        ax[0].plot(xx, a_approx_std, label="$u_N$", lw=3)
        box = ax[0].get_position()
        ax[0].set_position([box.x0, box.y0+0.15*box.height, box.width, box.height * 0.9])
        ax[0].legend(loc='upper center', bbox_to_anchor=(0.5, -0.10), fancybox=True, shadow=True, ncol=5)
        ax[0].set_ylim([-0.3,1.5])
        ax[0].grid(True)
        ax[0].set_title("STD: $\mathcal{X}_d=\mathcal{Y}_d=\operatorname{span}\{P_j\}$")
        
        ax[1].axvline(x=x_0_mixed, c="k", ls="--", label="$x_0$")
        ax[1].plot(xx, rho(xx), "k", label="$\\rho$")
        ax[1].plot(xx, a_true, label="$u$", lw=3)
        ax[1].plot(xx, a_approx_mixed, label="$u_N$", lw=3)
        ax[1].plot(xx, pdf_mixed(xx), "--", label="$\\pi$")
        box = ax[1].get_position()
        ax[1].set_position([box.x0, box.y0+0.15*box.height, box.width, box.height * 0.9])
        ax[1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.10), fancybox=True, shadow=True, ncol=5)
        ax[1].set_ylim([-0.3,1.5])
        ax[1].grid(True)
        ax[1].set_title("MIXED: $\mathcal{X}_d=\operatorname{span}\{P_j\}$, "
                        + "$\mathcal{Y}_d=\operatorname{span}\{\Phi_j\}$")

        ax[2].semilogy(shift[:j+1], err_std, label="error STD")
        ax[2].semilogy(shift[:j+1], err_mixed, label="error MIXED")
        box = ax[2].get_position()
        ax[2].set_position([box.x0, box.y0+0.15*box.height, box.width, box.height * 0.9])
        ax[2].legend(loc='upper center', bbox_to_anchor=(0.5, -0.10), fancybox=True, shadow=True, ncol=5)
        ax[2].set_xlim([shift[0]-0.2,shift[-1]+0.2])
        ax[2].set_ylim([1e-02,1e+02])
        ax[2].grid(True)
        ax[2].set_title("$L^2$-error vs. shift s")
        
        if len(shift) == 1:
            plt.savefig("../img/exp_tt_base_change_tmp.png")
        else:
            plt.savefig("../img/gif/exp_tt_base_change_gif_{:03d}.png".format(j))
        plt.close()

    # TODO: check case S = G*S
