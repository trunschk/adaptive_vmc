import sys
import os
import numpy as np
import xerus as xe

from context import src
from src.misc import timeit, log, transpose, Logger
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT


def copy_TT(TTO):
    """docstring for copy_TT"""
    return tt.convert(tt.convert(TTO))


def generate_test_TTOperator(dims, ranks):
    """docstring for generate_test_TTOperator"""
    ret = []
    for j, d in enumerate(dims):
        l = 1 if j == 0 else ranks[j-1]
        r = 1 if j == len(dims)-1 else ranks[j]
        core = np.asarray(xe.Tensor.dirac(
            [l, dims[j], dims[j], r], [0, 0, 0, 0]))
        ret.append(core)
    return tt.convert(ret)


def generate_test_TTTensor(dims, ranks):
    """docstring for generate_test_TTTensor"""
    ret = []
    for j, d in enumerate(dims):
        l = 1 if j == 0 else ranks[j-1]
        r = 1 if j == len(dims)-1 else ranks[j]
        core = np.asarray(xe.Tensor.dirac(
            [l, dims[j], r], [0, 0, 0]))
        ret.append(core)
    return tt.convert(ret)


def increase_rank(TT):
    """docstring for increase_rank"""
    tmp = copy_TT(TT)
    scale = xe.frob_norm(tmp) * 1e-10
    ranks = [1]*(len(tmp.ranks()))
    ranks[3] = 2
    if isinstance(TT, xe.TTOperator):
        eps = xe.TTOperator.random(tmp.dimensions, ranks)
    else:
        eps = xe.TTTensor.random(tmp.dimensions, ranks)
    tmp = tmp + scale*eps
    return tmp


if __name__ == "__main__":
    dims = [200] + [5]*10
    ranks = [10]*10
    EPS = 1e-01
    TTO = generate_test_TTOperator(dims, ranks)
    TTO = increase_rank(TTO)
    TTO2 = copy_TT(TTO)
    print(f"|| TTO - TTO2 ||_2 = {xe.frob_norm(TTO-TTO2)}")

    print("TTO")
    print(f"  dims: {TTO.dimensions}")
    print(f"  rank: {TTO.ranks()}")

    print("TTO.round")
    TTO.round(EPS)
    print(f"  dims: {TTO.dimensions}")
    print(f"  rank: {TTO.ranks()}")

    print("femTT.round(TTO2)")
    TTO2 = femTT.round(TTO2, EPS)
    print(f"  dims: {TTO2.dimensions}")
    print(f"  rank: {TTO2.ranks()}")
    print(f"|| TTO - TTO2 ||_2 = {xe.frob_norm(TTO-TTO2)}")

    u = generate_test_TTTensor(dims, ranks)
    u = increase_rank(u)

    print("femTT.round(u)")
    u2 = femTT.round(u, EPS)
    print(f"  dims: {u2.dimensions}")
    print(f"  rank: {u2.ranks()}")

    print("u.round")
    u.round(EPS)
    print(f"  dims: {u.dimensions}")
    print(f"  rank: {u.ranks()}")

    print(f"|| u - u2 ||_2 = {xe.frob_norm(u-u2)}")
