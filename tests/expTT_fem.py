import numpy as np
import matplotlib.pyplot as plt
import xerus as xe
from fenics import *

import scipy.sparse as sps
from tqdm import tqdm

from context import src
from src.misc import timeit, log, get_y_samples, transpose
from src.problem.equation.darcy import Problem
from src.parallel import Parallel
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as expTT

import autoPDB

# ==============================================================================
# setup

def setup_problem(deg, M, decay, mesh, space="DG"):
    """ Initialize fenics problem. """
    info = {
        "problem":{"name": "darcy"},
        "fe": { "degree": 3, "mesh": mesh },
        "fe_coeff": {"degree": deg, "space":space},
        "expansion": {"size": M, "mean": 1.0, "decay rate": decay, 
            "rho": 1, "vartheta":0.1,  "scale": 6 / np.pi**2},
        "sampling": { "distribution": "normal", "expfield": False,
            "batch size": 40 }
        }
    p = Problem(info)
    return p, info

# ==============================================================================
# evaluate h (eval of original fenics problem)

def h_fenics_coeff(func, y):
    """ 
    Compute the FEM coefficients of the affine-normal exponent of the
    diffusion coefficient h for given parameter samples.
    """
    if y.ndim == 1: y.shape = 1, -1
    assert y.ndim == 2
    coeffs = Parallel(func)
    ret = coeffs(y)
    c, y = transpose(ret)
    return np.array(c)

# ------------------------------------------------------------------------------
def eval_h(p, x, y):
    """
    Evaluate h(x,y).
    """
    if x.ndim == 1: x.shape = 1, -1
    if y.ndim == 1: y.shape = 1, -1
    assert x.ndim == 2 and y.ndim == 2
    assert x.shape[0] == 1 or y.shape[0] == 1
    coeffs = h_fenics_coeff(p.diffusion_coefficient, y)
    ret = np.zeros((y.shape[0], x.shape[0]))
    for i, coeff in enumerate(coeffs):
        ret[i] = fem.eval_fencis_function(x, coeff, p.space_coeff)
    ret = np.squeeze(ret)
    if ret.ndim == 0: ret = ret.item() # make 0 dim array to float
    return ret

# ------------------------------------------------------------------------------
def eval_exph(p, x, y):
    """
    Evaluate exp(h(x,y)).
    """
    return np.exp(eval_h(p,x,y))
    
# ==============================================================================
# reconstruction functions

def reconstruct_h(p, x0, y0):
    """ Reconstruct affine exponent h with VMC """
    n_samples = 1000
    ys_train, ws_train, hs_train = fem.sample_diffusion_coeff(p.info, n_samples)
    M = p.info["expansion"]["size"]
    
    log("__"*40)
    log("")
    log("reconstruct exponent h")
    with timeit("... {:4.2f} s"):
        reco = tt.reconstruct(
                dims = [2]*M,
                nodes = ys_train.T,
                weights = ws_train,
                values = hs_train,
                dist = "normal",
                label = "h reco",
                verbose = False
                )
        
    ys, ws, hs = fem.sample_diffusion_coeff(p.info, n_samples)
    abs_err, rel_err = compute_approx_error(p, ys, hs, reco)
    log(f"error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    abs_err, rel_err = compute_error_in_point(p, reco, x0, y0, exp=False)
    log(f"error IC (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    
    return reco
    
# ------------------------------------------------------------------------------
def reconstruct_expTT(p, da, dt, h, x0, y0, u_start, method, verbose=0):
    """ Reconstruct affine exponent h with expTT. """
    log("__"*40)
    log("")
    log("reconstruct u expTT")
    with timeit("... {:4.2f} s"):
        exph0 = eval_exph(p, x0, y0)
        W0, b = expTT.build_system_inexactFEMIC(
                da, dt, h, x0, y0, exph0, p.space_coeff, weight=1)
        
        dims, ranks = [p.space_coeff.dim()] + da, [5] * (M)
        if u_start is None:
            u_start = xe.TTTensor.random(dims, ranks)
            u_start = u_start / xe.frob_norm(u_start)
        
        reco = expTT.run_simpleALS(A=W0, u_start=u_start, b=b, verbose=verbose)
        reco.round(1e-12) # round to get unneccesary large ranks down
        
    n_samples = 1000
    ys, ws, hs = fem.sample_diffusion_coeff(p.info, n_samples)
    us = np.exp(hs)
    abs_err, rel_err = compute_approx_error(p, ys, us, reco)
    log(f"error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    abs_err, rel_err = compute_error_in_point(p, reco, x0, y0, exp=True)
    log(f"error IC (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    
    return reco
    
# ------------------------------------------------------------------------------
def pad_dimensions(tt, dimensions):
    assert len(dimensions) == tt.order()-1
    assert all(tt.dimensions[i] <= dimensions[i-1] for i in range(1,tt.order()))
    dims = [tt.dimensions[0]] + list(dimensions)
    ret = xe.TTTensor(dims)
    ranks = [1] + tt.ranks() + [1]
    for m in range(tt.order()):
        core = np.zeros((ranks[m], dims[m], ranks[m+1]))
        core[:,:tt.dimensions[m],:] = tt.get_component(m)
        ret.set_component(m, xe.Tensor.from_buffer(core))
    return ret

def reconstruct_expTT_iterDim(p, da, dt, h, x0, y0, u_start, method, verbose=0):
    if len(da) < 1: return
    fmttr = lambda s: log("    "+s)
    
    log("__"*40)
    log("")
    log("reconstruct u expTT -- increase da")
    
    ### set initial dimensions
    dai = np.ones(len(da), dtype=int)
    daM = max(da)
    
    ### set initial guess
    dims, ranks = [p.space_coeff.dim()] + list(dai), [5] * len(da)
    if u_start is None:
        u_start = xe.TTTensor.random(dims, ranks)
        u_start = u_start / xe.frob_norm(u_start)
        
    with timeit("... ({:4.2f} s)"):
        for i in range(1, daM+1):
            ### build operator and rhs
            W0, b = expTT.build_system_inexactFEMIC(
                    p, list(dai), list(dai), h, x0, y0, weight=1)
            ### run ALS
            log(f"  run ALS for da = [{i}]*{len(da)} ...")
            reco = expTT.run_simpleALS(
                    A=W0, u_start=u_start, b=b, verbose=1, fmttr=fmttr)
            reco.round(1e-12)
            if i < daM:
                ### update initial dimensions for next iteration
                assert np.any(dai < da)
                dai = np.minimum(dai+1, da)
                ### update initial guess for next iteration
                u_start = xe.TTTensor.random(
                        [p.space_coeff.dim()] + list(dai), [5]*len(da))
                u_start = u_start / (1e3 * xe.frob_norm(u_start))
                u_start = pad_dimensions(reco, dai) + u_start
                
    n_samples = 1000
    ys, ws, hs = fem.sample_diffusion_coeff(p.info, n_samples)
    us = np.exp(hs)
    abs_err, rel_err = compute_approx_error(p, ys, us, reco)
    log(f"error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    abs_err, rel_err = compute_error_in_point(p, reco, x0, y0, exp=True)
    log(f"error IC (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    
    return reco
    
# ------------------------------------------------------------------------------
def reconstruct_naive(p, da):
    """ Reconstruct affine exponent h with VMC. """
    n_samples = 1000
    ys_train, ws_train, hs_train = fem.sample_diffusion_coeff(p.info, n_samples)
    us_train = np.exp(hs_train)
    
    log("__"*40)
    log("")
    log("reconstruct u naive")
    with timeit("... {:4.2f} s"):
        reco = tt.reconstruct(
                dims = da,
                nodes = ys_train.T,
                weights = ws_train,
                values = us_train,
                dist = "normal",
                label = "u reco naive",
                verbose = False
                )
        
    ys, ws, hs = fem.sample_diffusion_coeff(p.info, n_samples)
    us = np.exp(hs)
    abs_err, rel_err = compute_approx_error(p, ys, us, reco)
    log(f"error-L2L2 (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    abs_err, rel_err = compute_error_in_point(p, reco, x0, y0, exp=True)
    log(f"error IC (abs/rel): {abs_err:4.2e} / {rel_err:4.2e}")
    
    return reco

# ------------------------------------------------------------------------------
def eval_tt_fenics(p, reco, x, y):
    """
    Evaluate a TTTensor in point (x,y).
    """
    coeffs = tt.eval_approx(reco, y, dist="normal")
    ret = np.zeros((y.shape[0], x.shape[0]))
    for i, coeff in enumerate(coeffs):
        ret[i] = fem.eval_fencis_function(x, coeff, p.space_coeff)
    ret = np.squeeze(ret)
    if ret.ndim == 0: ret = ret.item() # make 0 dim array to float
    return ret
    
# ==============================================================================
# error computation

def compute_L2L2_norm(p, us, ws=None):
    if ws is None: ws = np.ones(us.shape[0]) / us.shape[0]
    mass_mat = tt.mass_matrix(space=p.space_coeff)
    err_sq = np.einsum('n,ni,in', ws, us, mass_mat@us.T)
    return np.sqrt(err_sq)

# ------------------------------------------------------------------------------
def compute_error_in_point(p, reco, x, y, exp=False, norm=True):
    """
    compute the difference between a tt approximation and the exact value of
    h (or exp h) at in point (x,y).
    """
    approx_coeff = tt.eval_approx(reco, y, "normal").flatten()
    approx = fem.eval_fencis_function(x, approx_coeff, p.space_coeff)
    exact = eval_exph(p, x, y) if exp else eval_h(p, x, y)
    if norm:
        abs_err = np.linalg.norm(exact-approx)
        rel_err = abs_err / np.linalg.norm(exact)
    else:
        abs_err = np.abs(exact - approx)
        rel_err = abs_err / np.abs(exact)
    return abs_err, rel_err

# ------------------------------------------------------------------------------
def compute_approx_error(p, ys, us, reco):
    """
    compute the L2-L2 error of a tt approximation and the exact h (or exp h)
    over the domain.
    """
    ws = np.ones(ys.shape[0])/ys.shape[0]
    abs_err = femTT.compute_L2L2_error(
            reco, ys, ws, us, p.space_coeff, "normal")
    rel_err = abs_err / compute_L2L2_norm(p, us, ws=ws)
    return abs_err, rel_err

# ==============================================================================
# plot

# XXX
def plot_test(p, x0, c, titles=None, path=".", show_mesh=False, suptitle=None):
    nrows, ncols = 1, c.shape[0]
    mesh = p.mesh if show_mesh else None
    assert c.shape[1] == p.space_coeff.dim()
    if titles is not None: assert len(titles) == c.shape[0]
    fig, ax = plt.subplots(figsize=(5*ncols, 4*nrows), nrows=nrows, ncols=ncols)
    
    for j, coeff in enumerate(c):
        fem.plot_fenics_function(ax[j], c[j], p.space_coeff, mesh=mesh, cmap=None)
        ax[j].set_title(titles[j])
    ax[0].scatter(x0[0,0], x0[0,1], color="red")
    
    plt.suptitle(suptitle)
    plt.savefig(path)
    
def plot_approximation_error(xx, exact, approx, x0, title, filename, **kwargs):
    exact.shape = xx.size, xx.size
    approx.shape = xx.size, xx.size
    rel_err = np.abs(exact-approx)/np.abs(exact)
    
    nrows, ncols = 1, 3
    fig, ax = plt.subplots(figsize=(5*ncols, 4*nrows), nrows=nrows, ncols=ncols)
    
    im = ax[0].contourf(xx, xx, exact, **kwargs)
    ax[0].scatter(x0[:,0], x0[:,1], color="black")
    ax[0].set_title("exact")
    fig.colorbar(im, ax=ax[0])
    
    im = ax[1].contourf(xx, xx, approx, **kwargs)
    ax[1].set_title("approx")
    fig.colorbar(im, ax=ax[1])
    
    im = ax[2].contourf(xx, xx, rel_err, **kwargs)
    ax[2].set_title("relative (ptws) error")
    fig.colorbar(im, ax=ax[2])
    
    plt.suptitle(title)
    plt.savefig(filename)
    
# ==============================================================================
# debug functions

def plot_OPcore(core):
    if isinstance(core, xe.Tensor): core = np.asarray(core)
    core = np.abs(core)
    assert core.ndim == 4
    l, r = core.shape[0], core.shape[-1]
    fig, ax = plt.subplots(figsize=(7*r,5*l), nrows=l, ncols=r)
    for i in range(l):
        for j in range(r):
            if l == 1 and r == 1:
                im = ax.matshow(core[i,:,:,j], cmap="Blues")
                fig.colorbar(im, ax=ax)
            elif l == 1:
                im = ax[j].matshow(core[i,:,:,j], cmap="Blues")
                fig.colorbar(im, ax=ax[j])
            elif r == 1:
                im = ax[i].matshow(core[i,:,:,j], cmap="Blues")
                fig.colorbar(im, ax=ax[i])
            else:
                im = ax[i,j].matshow(core[i,:,:,j], cmap="Blues")
                fig.colorbar(im, ax=ax[i,j])
    plt.savefig("../img/expTT_fem_plot_OPcore.png")

# ==============================================================================
# expTT-FEM-diff functions
# NOTE: This is experimental code that is not working yet

def compute_FE_normalizing_factor(space):
    f = TrialFunction(space)
    g = TestFunction(space)

    lab = parameters['linear_algebra_backend']
    parameters['linear_algebra_backend'] = "Eigen"
    const = assemble(f * g * dx, 
            form_compiler_parameters={'quadrature_degree': -1}
            )
    const = sps.csr_matrix(as_backend_type(const).sparray())
    parameters['linear_algebra_backend'] = lab
    return np.sqrt(const.diagonal())

def compute_tensor_K_fem(space, space_h=None):
    if space_h is None: space_h = space
    phi_i = TrialFunction(space)
    phi_j = TestFunction(space)
    psi_k = Function(space_h)
    N = space_h.dim()
    
    # rescale constant for K to be normalized
    const = compute_FE_normalizing_factor(space)
    # const2 = fem.mass_matrix(space).todense()
    # print(np.allclose(np.diag(const**2),const2))
    # exit()
    
    lab = parameters['linear_algebra_backend']
    parameters['linear_algebra_backend'] = "Eigen"
    ret = xe.Tensor([space.dim(), space.dim(), N])
    for j in range(N): # NOTE: here we compute N (NxN) sparse matrices
        psi_k.vector()[:] = np.eye(1,N,k=j).flatten()
        kappa = assemble(phi_i * phi_j * psi_k * dx, 
                form_compiler_parameters={'quadrature_degree': -1}
                )
        kappa = sparsify_matrix(
                sps.csr_matrix(as_backend_type(kappa).sparray())
                ).tocoo()

        for r,c,d in zip(kappa.row, kappa.col, kappa.data): 
            ret[r,c,j] = d / const[j]**2 # rescale kappa to normalized
    parameters['linear_algebra_backend'] = lab
            
    # # XXX ->
    # lab = parameters['linear_algebra_backend']
    # parameters['linear_algebra_backend'] = "Eigen"
    # K = []
    # for j in range(N):
    #     psi_k.vector()[:] = np.eye(1,N,k=j).flatten()
    #     kappa = assemble(phi_i * phi_j * psi_k * dx, form_compiler_parameters={'quadrature_degree': -1})
    #     kappa = sps.csr_matrix(as_backend_type(kappa).sparray())
    #     K.append(sparsify_matrix(kappa))
    # parameters['linear_algebra_backend'] = lab
    # # XXX <-
            
    ret.use_sparse_representation()
    return ret

def compute_D_fem(space, use_diff=True):
    lab = parameters['linear_algebra_backend']
    parameters['linear_algebra_backend'] = "Eigen"
    
    phi_i = TrialFunction(space)
    phi_j = TestFunction(space)
    if use_diff:
        D = assemble(phi_i * phi_j.dx(0) * dx,
                form_compiler_parameters={'quadrature_degree': -1}
                )
    else:
        D = assemble(phi_i * phi_j * dx,
                form_compiler_parameters={'quadrature_degree': -1}
                )
    D = sparsify_matrix(
            sps.csr_matrix(as_backend_type(D).sparray())
            )
    parameters['linear_algebra_backend'] = lab
    
    # rescale D to be normalized
    const = compute_FE_normalizing_factor(space)
    D = np.dot(D, sps.diags(1/const**2)).tocoo()
    
    ret = xe.Tensor([1,space.dim(),space.dim(),1])
    for r,c,d in zip(D.row, D.col, D.data): 
        ret[0,r,c,0] = d

    return ret

def compute_H_fem(space, h, use_diff=True):
    if isinstance(h, np.ndarray): h = xe.Tensor.from_buffer(h)
    if h.order() == 1: h.reinterpret_dimensions([1,h.order(),1])
    assert h.order() == 3  # assure h is given as a TTTensor core
    assert h.dimensions[1] == space.dim() # assure h is in the correct space
    
    ret = xe.Tensor()
    i,j,k,l,r = xe.indices(5)
    
    D = compute_D_fem(space)
    D.reinterpret_dimensions([space.dim(), space.dim()])
    K = fem.compute_interpolation_tensor(space.dim())
    
    del_h = xe.Tensor(h)
    if use_diff: 
        del_h(l,i,r) << D(i,j) * del_h(l,j,r)
    ret(l,i,j,r) << K(i,j,k) * del_h(l,k,r)

    return ret

def sparsify_matrix(mat):
    """
    Given a (sparse) matrix, check nonzero elements on assure that only those
    are stored.

    Parameter
    ---------
    mat : np.ndarray or sps.csr.csr_matrix or np.matrix
        Matrix that should be sparsified.

    Return
    ------
    ret : sps.csr.csr_matrix
        Sparsified matrix.
    """
    row, col = mat.nonzero()
    if row.size > 0:
        data = np.asarray(mat[row,col]).flatten()
        ret = sps.csr_matrix((data, (row, col)), shape=mat.shape)
    else:
        ret = sps.csr_matrix(mat.shape)
    return ret

# ==============================================================================
if __name__ == "__main__":
    """ Test expTT reconstuction for function with deterministic dimensions.
    
    Assume here that the exponent h is given by
            h(x,y) = sum_{j=1}^N sum_{alpha in Lambda_h} h[j,alpha] psi_j(x) P_alpha(y)
    for some multi-index set Lambda_h and FE basis functions {psi_j}.

    NOTE:
    """
    
    # TODO
    # [ ] fem IC version not yet (good) working for
    #       - init_mesh > 2
    #       - M >> 5
    #       - space > DG-0
    # [ ] iter version is super slow in deg, why?
    # [ ] maybe think about type handling in expTT_reconstruction
    #       Right now, functions expect np.ndarrays (some also handle first
    #       component as xe.Tensor. Maybe use xe.Tensors for everything and do
    #       the necessary casting for np.ndarrays?
    
    # --------------------------------------------------------------------------
    # setup 

    dim_a, dim_t = 6, 6
    fem_deg = 0
    space = "DG"
    M = 20
    decay = 2
    init_mesh = 2
    
    da, dt = [dim_a]*M, [dim_t]*M
    p, info = setup_problem(fem_deg, M, decay, init_mesh, space=space)
    
    N = p.space_coeff.dim()
    # x0, y0 = np.random.uniform(0,1,(1,2)), np.random.normal(0,1,(1,M))
    # x0, y0 = np.random.uniform(0,1,(N,2)), np.random.normal(0,1,(1,M))
    x0, y0 = fem.triangle_centers(p.mesh), np.random.normal(0,1,(1,M))
    exph0 = eval_exph(p, x0, y0)
    
    log(f"FEM space:     {space}-{fem_deg}")
    log(f"FEM space dim: {N}")
    log(f"mesh cells:    {p.mesh.num_cells()}")
    log(f"# params:      {M}")
    log(f"da:            {da}")
    log(f"dt:            {dt}")
    log(f"x0 shape:      {x0.shape}")
    log(f"y0 shape:      {y0.shape}")
    
    # --------------------------------------------------------------------------
    # reconstruct affine exponent h
    h_reco = reconstruct_h(p, x0, y0)
    
    # --------------------------------------------------------------------------
    # reconstruct u = exp h -- naive
    u_reco_naive = reconstruct_naive(p, da)
    
    # --------------------------------------------------------------------------
    # reconstruct u = exp h -- expTT -- fem IC
    u_reco_expTT = reconstruct_expTT(
            p, da, dt, h_reco, x0, y0, u_start=None, method="IC", verbose=1)
    
    # --------------------------------------------------------------------------
    # reconstruct u = exp h -- expTT iter -- fem IC
    # u_reco_expTT = reconstruct_expTT_iterDim(
    #         p, da, dt, h_reco, x0, y0, u_start=None, method="IC", verbose=1)

    # --------------------------------------------------------------------------
    # plot

    use_rnd_param_value = False
    y_rnd = np.random.normal(0,1,(1,M))
    ys = y_rnd if use_rnd_param_value else y0

    xx = np.linspace(0,1,30)
    XX, YY = np.meshgrid(xx,xx)
    XX = np.concatenate([XX.reshape(-1,1),YY.reshape(-1,1)], axis=1)
    
    # h reco
    exact = eval_h(p, XX, ys)
    approx = eval_tt_fenics(p, h_reco, XX, ys)
    title = "Reconstruction of exponent h - VMC"
    plot_approximation_error(
            xx, exact, approx, x0, title, 
            filename="../img/expTT_fem_hReco.png",
            cmap="Greens"
            )
    
    # u reco expTT
    exact = eval_exph(p, XX, ys)
    approx = eval_tt_fenics(p, u_reco_expTT, XX, ys)
    title = "Reconstruction of u -- expTT"
    
    plot_approximation_error(
            xx, exact, approx, x0, title, 
            filename="../img/expTT_fem_uReco_expTT.png",
            cmap="Reds"
            )
    
    # u reco naive
    exact = eval_exph(p, XX, ys)
    approx = eval_tt_fenics(p, u_reco_naive, XX, ys)
    title = "Reconstruction of u -- naive"
    
    plot_approximation_error(
            xx, exact, approx, x0, title, 
            filename="../img/expTT_fem_uReco_naive.png",
            cmap="Blues"
            )
    
    # --------------------------------------------------------------------------
    # XXX test
    # log("__"*40)
    # log("")
    # 
    # x_rnd = np.array([[0.2,0.8]])
    # log("x0:   ",x0)
    # log("x_rnd:",x_rnd)
    # log("y0:   ",y0)
    # log("y_rnd:",y_rnd)
    # log("exp h(x0,y0):",eval_exph(p,x0,y0))
    # log()
    # 
    # abs_err, rel_err = compute_error_in_point(p, u_reco_expTT, x0, y0, exp=True)
    # log("(x0,y0):       {:4.2e} / {:4.2e}".format(abs_err, rel_err))
    # 
    # abs_err, rel_err = compute_error_in_point(p, u_reco_expTT, x0, y_rnd, exp=True)
    # log("(x0,y_rnd):    {:4.2e} / {:4.2e}".format(abs_err, rel_err))
    # 
    # abs_err, rel_err = compute_error_in_point(p, u_reco_expTT, x_rnd, y0, exp=True)
    # log("(x_rnd,y0):    {:4.2e} / {:4.2e}".format(abs_err, rel_err))
    # 
    # abs_err, rel_err = compute_error_in_point(p, u_reco_expTT, x_rnd, y_rnd, exp=True)
    # log("(x_rnd,y_rnd): {:4.2e} / {:4.2e}".format(abs_err, rel_err))

    # --------------------------------------------------------------------------
    # from IPython import embed; embed()
    # exit()

