# coding: utf-8
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def D(_da, _dt):
    ret = np.zeros((_dt, _da))
    # The i-th polynomial in V_a is of order i+1.
    idcs = np.arange(min(_da, _dt))
    ret[idcs, idcs] = np.sqrt(idcs+1)
    return ret

def M(_da, _dt, _c):
    return _c*np.eye(_dt, _da, k=-1)

def S(_da, _dt, _affineCoefficients):
    return D(_da, _dt) - M(_da, _dt, _affineCoefficients[1])

dh = 2
affineCoefficients = [0.1, 0.2]
cond = lambda da: np.linalg.cond(S(da, da+dh-1, affineCoefficients))

das = np.arange(2,26)
conds = [cond(da) for da in das]
plt.plot(das, conds)
plt.show()
