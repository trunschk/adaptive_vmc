from __future__ import division
from dolfin import *
import scipy.sparse as sp
import scipy.sparse.linalg as spla
from scipy.special import kv, gamma
import numpy as np
from functools import partial
import argparse
import os
from random import uniform

# import os


def cov_exp(p, a, sigma2):
    """
    :param p: point might given as difference of coordinates p = x-y for radial dependence
    :param a: isotropic correlation length
    :param sigma2: scaling constant
    :return:
    """
    return sigma2 * np.exp(-1/a * np.abs(np.linalg.norm(p, axis=1)))


def cov_gauss(p, a, sigma2):
    """
    :param p: point might given as difference of coordinates p = x-y for radial dependence
    :param a: isotropic correlation length
    :param sigma2: scaling constant
    :return:
    """
    return sigma2 * np.exp(-1/a**2 * np.linalg.norm(p, axis=1)**2)


def cov_matern(p, a, sigma2, nu):
    assert nu >= 0.5
    sq2nu = np.sqrt(2) * nu
    C = sigma2 * np.prod([np.power(sq2nu*np.abs(di)/a, nu) * kv(nu, sq2nu *
                         np.abs(di)/a) / (gamma(nu) * np.power(2, nu-1)) for di in p.T], axis=0)
    C[np.isnan(C)] = 0
    return C


# FEM based KL evaluation
# NOTE: rho is covariation length
def compute_EVP(rho, k, spc):
    """
    Approximates k-th largest eigenvalues and corresponding eigenfunctions
    solving

        int_{mesh} cov(x,y) phi(y) dy  = lambda phi(x)

    for given mercy kernel cov on H1 conforming space.

    :type degree: object
    :param cov: covariance kernel
    :param k: number of apprixmated eigenfunctions to be computed
    :param spc: predefined fenics FunctionSpace
    :param EVPtype:
    :return:
    """

    # set cov(x) = cov_gauss(x, a=rho, sigma2=1.0)
    cov = partial(cov_gauss, a=rho, sigma2=1.0)

    def setup_FEM():
        """
        Fem setup, possible with more details provided.
        :return:
        """
        u = TrialFunction(spc)
        v = TestFunction(spc)

        # assemble mass matrix and convert to numpy array
        M = assemble(u * v * dx(spc.mesh()))
        M = M.array()

        return spc, u, v, M

    # construct FEM space
    spc, u, v, M = setup_FEM()

    print("dim = {dim}".format(dim=spc.dim()))

    # obtain covariance (interpolation) matrix
    C = interpolate_covariance(cov, spc, spc)  # !!!

    # solve eigenvalue problem
    # M is MassMat
    A = np.dot(M, np.dot(C, M))  # A = M.T@C@M
    w, v = spla.eigsh(A, k, M)  # Ax = \lambda Mx

    # return eigenpairs
    # print "===== EVP size =====", A.shape, w.shape, v.shape
    return w, v  # , spc, M


def interpolate_covariance(cov, space1, space2):
    """
    @pre The approximation input space is built on a Lagrangian basis (i.e. has partition of Unity property)

    This method builds a covariance matrix C, with C(i,j) = cov( x_i, x_j),
    where x_i, and x_j are coordinates of the degrees of freedoms associated with Lagrangian basis
    defined on the input spaces.

    The covariance kernel

                cov : mesh1 x mesh2 -> IR

    is assumed to be isotrop, i.e. cov(x,y) = cov(x-y).
    Here mesh1 and mesh2 are underlying meshes of Lagragian spaces space1 and space2.

    :param cov: (isotropic) Covariance kernel
    :param space1: Lagrangian space on mesh1
    :param space2: Lagrangian space on mesh2
    :return: interpolation Covariance Matrix
    """
    # dof related coordinates w.r.t. to input spaces
    dofcoord_1 = space1.tabulate_dof_coordinates().reshape(
        space1.dim(), space1.mesh().geometry().dim())
    dofcoord_2 = space2.tabulate_dof_coordinates().reshape(
        space2.dim(), space2.mesh().geometry().dim())

    # evaluate covariance matrix
    L1 = dofcoord_1.shape[0]
    L2 = dofcoord_2.shape[0]

    # vectorised evaluation of cov( coords_i, coords_j )
    c0 = np.repeat(dofcoord_1, L2, axis=0)  # this makes code efficient
    c1 = np.tile(dofcoord_2, [L1, 1])  # this makes code efficient
    # C = cov(r)
    C = cov(c0 - c1)  # !!!
    # reshape to rectangular matrix
    C.shape = [L1, L2]

    return C
