import os
import numpy as np
from copy import deepcopy

from context import src
from src.misc import log, timeit
from src.misc import get_y_samples
from src.utils.darcy_affine_coeff import eval_diff_coeff
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
from src.problem.equation.darcy import Problem
from measurement_utils.measures import MeasurementList, BasisMeasure
from measurement_utils.bases import HermitePolynomials


def generate_samples(n, p):
    decay = p.info["expansion"]["decay rate"]
    mean = p.info["expansion"]["mean"]
    dofs = fem.dof_coords(p.space_coeff, p.mesh)
    ys = get_y_samples(p.info, n)
    ws = np.ones(ys.shape[0])/ys.shape[0]
    kappas = eval_diff_coeff(dofs, ys, decay, mean)
    kappas = np.exp(kappas)
    _, _, fs = fem.sample_rhs(p.info, n, samples=ys)
    _, _, us = fem.sample_solution(p.info, n, samples=ys, get_eta=False)
    return ys, ws, us, kappas, fs


def ls_residual(y_dim, nodes, values, weights):
    log(
        f"Performing 1D least squares estimation. Polynomial dimension: {y_dim}.")
    N = len(nodes)
    dims = [values.shape[1], y_dim]
    assert nodes.shape == (
        N, len(dims)-1) and values.shape == (N, dims[0]) and weights.shape == (N,)

    def bm(dim): return BasisMeasure(
        HermitePolynomials(dim-1, mean=0, variance=1))
    ml = MeasurementList([bm(dim) for dim in dims[1:]])
    assert ml.dimensions == tuple(
        dims[1:]), f"{ml.dimensions} != {tuple(dims[1:])}"
    meas = ml(nodes.T)  # meas.shape == (numModes, numSamples, numDimensions)
    assert len(meas)+1 == len(dims)

    meas = meas[0]
    assert meas.shape == (N, dims[1])
    coefs = np.linalg.lstsq(meas, values, rcond=None)[0]

    res = np.linalg.norm(meas@coefs - values) / np.linalg.norm(values)
    log(f"Total least squares residual: {res:.2e}")
    if res > 1e-8:
        res = np.linalg.norm(meas@coefs - values, axis=0)
        vns = np.linalg.norm(values, axis=0)
        res[vns != 0] /= vns[vns != 0]
        with np.printoptions(formatter={'float': lambda x: f"{x:.2e}" if x != 0 else "0"}):
            log(f"Least squares residual for each FE-dof: {res}")


def residuum(solution, nodes, values, weights):
    N = len(nodes)
    dims = solution.dimensions
    assert nodes.shape == (
        N, len(dims)-1) and values.shape == (N, dims[0]) and weights.shape == (N,)

    def bm(dim): return BasisMeasure(
        HermitePolynomials(dim-1, mean=0, variance=1))
    ml = MeasurementList([bm(dim) for dim in dims[1:]])
    assert ml.dimensions == tuple(
        dims[1:]), f"{ml.dimensions} != {tuple(dims[1:])}"
    meas = ml(nodes.T)  # meas.shape == (numModes, numSamples, numDimensions)
    assert len(meas)+1 == len(dims)

    prediction = np.ones((1, N))
    for pos in reversed(range(1, len(dims))):
        assert meas[pos-1].shape == (N, dims[pos])
        core = np.array(solution.get_component(pos))
        prediction = np.einsum(
            'ne,ler,rn -> ln', meas[pos-1], core, prediction)
    core = np.array(solution.get_component(0))
    prediction = np.einsum('lsr,rn -> lsn', core, prediction)
    assert prediction.shape == (1, dims[0], N)
    prediction = prediction[0].T

    return np.sqrt(np.sum(weights[:, None]*(values - prediction)**2) / np.sum(weights[:, None]*values**2))


def info_dict(dct=None, **kwargs):
    """ Return suitable info dictionary for src.problem.equation.darcy.Problem.

    Return the default info dictionary. Replace values with given keywords.
    If dct is not None, use (copy of) dct as default dictionary.

    Returns
    -------
    info : dict
        Info dictionary for Darcy problem.
    """
    def _set_value(key, dic, default):
        """ Get value for key in dict if exists or return default. """
        return dic[key] if key in dic.keys() else default

    if dct is not None:
        assert isinstance(dct, dict)
        info = deepcopy(dct)
    else:
        info = {"problem": {}, "fe": {}, "fe_coeff": {},
                "expansion": {}, "sampling": {}}
    info["problem"]["name"] = "darcy"
    info["fe"]["degree"] = _set_value("degree", kwargs, 1)
    info["fe"]["mesh"] = _set_value("mesh", kwargs, 8)
    info["fe"]["geometry"] = _set_value("geometry", kwargs, "UnitSquare")
    info["fe_coeff"]["degree"] = _set_value("coeff_degree", kwargs, 1)
    info["fe_coeff"]["space"] = _set_value("coeff_space", kwargs, "CG")
    info["expansion"]["size"] = _set_value("M", kwargs, 20)
    info["expansion"]["mean"] = _set_value("mean", kwargs, 0.0)
    info["expansion"]["decay rate"] = _set_value("decay", kwargs, 2)
    info["expansion"]["rho"] = _set_value("rho", kwargs, 1)
    # NOTE: This vartheta is not the Doerfler marking threshold but the
    #       rescaling parameter for the Hermite basis in the lognormal case.
    info["expansion"]["vartheta"] = _set_value("vartheta", kwargs, 0.1)
    info["expansion"]["scale"] = _set_value("scale", kwargs, 6/np.pi**2)
    info["sampling"]["distribution"] = _set_value("dist", kwargs, "uniform")
    info["sampling"]["use_scaled_base"] = _set_value(
        "use_scaled_base", kwargs, False)
    info["sampling"]["expfield"] = _set_value("exp", kwargs, False)
    info["sampling"]["batch size"] = _set_value("batch", kwargs, 40)

    if info["fe_coeff"]["degree"] == 0:
        assert info["fe_coeff"]["space"] != "CG"
    if info["sampling"]["distribution"] == "uniform":
        assert info["sampling"]["expfield"] is False
    elif info["sampling"]["distribution"] == "normal":
        assert info["sampling"]["expfield"] is True
    if info["sampling"]["use_scaled_base"] is True:
        assert info["sampling"]["distribution"] == "normal"

    # generate mesh
    if isinstance(info["fe"]["mesh"], int):
        mesh = fem.generate_mesh(info["fe"]["geometry"], info["fe"]["mesh"])
        info["fe"]["mesh"] = mesh

    return info


if __name__ == "__main__":

    order = 1
    decay = 4
    n_refs = 10
    sdim = [25]
    # sdim = [5, 3]  # end-dimensions from lognormal paper
    # sdim = [8, 4, 2]  # higher deg/ more modes than here do not improve error
    assert len(sdim) <= order
    N_VMC = 9_000
    N_test = 1_000

    info = info_dict(degree=1, coeff_degree=1, decay=decay,
                     mean=0.0, exp=True, dist="normal",
                     use_scaled_base=False, geometry="UnitSquare",
                     mesh=n_refs, M=order)
    p = Problem(info)

    def get_stiffness_matrix(fs):
        import scipy.sparse as sps
        from dolfin import parameters, TrialFunction, TestFunction, inner, grad, dx, assemble, as_backend_type
        backend = parameters['linear_algebra_backend']
        parameters['linear_algebra_backend'] = "Eigen"
        u = TrialFunction(fs)
        v = TestFunction(fs)
        stiffness = inner(grad(u), grad(v)) * dx
        stiffness = assemble(stiffness)

        S = sps.csr_matrix(as_backend_type(stiffness).sparray())
        parameters['linear_algebra_backend'] = backend
        return S

    # MAMA = fem.mass_matrix(space=p.space)
    # np.save("mama.npy", MAMA)
    # STIMA = get_stiffness_matrix(p.space)
    # np.save("stima.npy", STIMA)
    # exit()

    log(f"generate {N_VMC+N_test} samples")
    try:
        data = np.load("./samples.npz")
        ys = data["ys"]
        ws = data["ws"]
        us = data["us"]
        kappas = data["kappas"]
        fs = data["fs"]
        assert len(ws) >= N_VMC+N_test
    except (IOError, AssertionError):
        ys, ws, us, kappas, fs = generate_samples(N_VMC+N_test, p)
        np.savez("samples.npz", ys=ys, ws=ws, us=us, kappas=kappas, fs=fs)

    if len(sdim) == 1:
        ls_residual(sdim[0], ys[:N_VMC, :1], us[:N_VMC], ws[:N_VMC])

    # reconstruct in orthonormal basis
    log(f"reconstruct u with {N_VMC} samples")
    u_reco = tt.reconstruct(dims=sdim, nodes=ys[:N_VMC, :len(sdim)].T,
                            weights=ws[:N_VMC], values=us[:N_VMC],
                            dist=p.info["sampling"]["distribution"],
                            init=None, label="", verbose=False)

    abs_err = femTT.compute_L2H1_error(
        u_reco, ys[N_VMC:], ws[N_VMC:], us[N_VMC:], p.space, "normal")
    rel_err = abs_err / femTT.compute_L2H1_norm(
        ys[N_VMC:], us[N_VMC:], p.space, ws=ws[N_VMC:])
    log(f"Dimensions: {u_reco.dimensions}")
    log(f"Ranks: {u_reco.ranks()}")
    log(f"L2-H1 error (abs/rel): {abs_err:4.2e}/{rel_err:4.2e}")
    log(f"Residuum: {residuum(u_reco, ys[N_VMC:, :len(sdim)], us[N_VMC:], ws[N_VMC:]):.2e}")
