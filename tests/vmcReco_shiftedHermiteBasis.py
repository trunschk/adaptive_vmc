# -*- coding: utf-8 -*-
import sys
import os
import time
import numpy as np
import matplotlib.pyplot as plt
from IPython import embed

from fenics import *
import xerus as xe

from context import src
from src.misc import Logger, log, timeit, transpose
from src.misc import factorial, compute_scaling_constant
import src.utils.tt_tools as tt
import src.utils.fem_tools as fem
import src.utils.femTT_tools as femTT
from src.utils.darcy_affine_coeff import eval_diff_coeff
from src.problem.equation.darcy import Problem
from measurement_utils.measures import MeasurementList, BasisMeasure
from measurement_utils.bases import LegendrePolynomials, HermitePolynomials


def eval_approx_sH(reco, yss, info):
    """
    Eval approx for scaled HermiteE polynomials.
    """
    u = xe.Tensor.random([reco.dimensions[0]])
    us = np.empty((yss.shape[0], reco.dimensions[0]))
    x_idx, y_idx = xe.indices(2)
    for i, ys in enumerate(yss):
        meas = rk1_measure_sH(ys, reco.dimensions[1:], info)
        u(x_idx) << reco(x_idx, y_idx & 1) * meas(y_idx & 0)
        us[i] = np.array(u)
    return us


def rescale_samples(ys, info):
    """ Dupe of avmc_utils function. """
    assert info["sampling"]["distribution"] == "normal"

    M = info["expansion"]["size"]
    decay = info["expansion"]["decay rate"]
    rho = info["expansion"]["rho"]
    vartheta = info["expansion"]["vartheta"]

    c = np.array([
        compute_scaling_constant(m+1, decay, rho*vartheta) for m in range(M)
    ]).reshape(1, -1)
    return c*ys


def rk1_measure_sH(ys, dims, info):
    """
    Rank-1 measure TTTensor for scaled Hermite polynomials.
    """
    tensor = xe.Tensor.from_buffer
    ns = [1 / np.sqrt(factorial(n)) for n in np.arange(max(dims))]

    def modal_measure(y, dim, s):
        def hermite(deg):
            return np.polynomial.hermite_e.HermiteE.basis(deg)

        def sHermite(deg):
            return hermite(deg)(np.polynomial.Polynomial((0, s)))
        arr = np.empty((1, dim, 1), dtype=np.float_)
        for i in range(dim):
            arr[0, i, 0] = ns[i]*sHermite(i)(y)
        return tensor(arr)

    tt = xe.TTTensor.random(dims, [1]*(len(dims)-1))
    for j, (y, d) in enumerate(zip(ys, dims)):
        s = 1/compute_scale(j+1, info)
        tt.set_component(j, modal_measure(y, d, s))
    return tt


def compute_scale(m, info):
    rho = info["expansion"]["rho"]
    vartheta = info["expansion"]["vartheta"]
    decay = info["expansion"]["decay rate"]
    return compute_scaling_constant(m, decay, rho*vartheta, gamma=0.9)


def compute_error(val1, val2, ws, space):
    """
    Compute L2-L2 norm of approximation error.
    """
    mass_mat = fem.mass_matrix(space=space)
    assert val1.shape == val2.shape
    diff = val1 - val2
    err_sq = np.einsum('n,ni,in', ws, diff, mass_mat@diff.T)
    return np.sqrt(err_sq)


def test_error_computation(info):
    p = Problem(info)
    y_dim, deg = info["expansion"]["size"], 2
    n_samples = info["sampling"]["n_samples"]
    decay = info["expansion"]["decay rate"]

    log(f"generate {n_samples} training samples")
    # ys, ws, cs = fem.sample_diffusion_coeff(info, n_samples)
    dofs = fem.dof_coords(p.space_coeff, p.mesh)
    ys = np.random.normal(0, 1, [n_samples, y_dim])
    ws = np.ones(ys.shape[0]) / ys.shape[0]
    cs = eval_diff_coeff(dofs, ys, decay, mean=0.0)
    # ys_scaled = rescale_samples(ys, info)
    # cs_scaled = eval_diff_coeff(dofs, ys_scaled, decay, mean=0.0)

    # reconstruct with VMC
    log("reconstruct")
    sdim = [deg]*y_dim
    reco = tt.reconstruct(
        dims=sdim, nodes=ys.T, weights=ws, values=cs, dist="normal",
        init=None, label="a", verbose=True
    )

    # apply change of basis
    log("apply change of basis")
    decay = info["expansion"]["decay rate"]
    rho, vartheta = info["expansion"]["rho"], info["expansion"]["vartheta"]
    reco_nb = femTT.change_basis_to_scaled_hermite(reco, decay, rho, vartheta)

    # generate training data
    n_samples_test = 1000
    log(f"generater {n_samples_test} test samples")
    ys_test = np.random.normal(0, 1, [n_samples_test, y_dim])
    ws_test = np.ones(ys_test.shape[0]) / ys_test.shape[0]
    cs_test = eval_diff_coeff(dofs, ys_test, decay, mean=0.0)

    # evaluate reco w.r.t. reco basis
    log("evaluate reco in test samples (w.r.t. original basis)")
    reco_test = tt.eval_approx(reco, ys_test, dist="normal")

    # evaluate reco_nb w.r.t. new basis
    log("evaluate base-changed reco in test samples (w.r.t. new basis)")
    reco_nb_test = eval_approx_sH(reco_nb, ys_test, info)

    log("compute error")
    e1 = np.linalg.norm(reco_test-reco_nb_test)
    log(f"  entry-wise error reco-reco_nb: {e1}")

    e2 = compute_error(cs_test, reco_test, ws_test, space=p.space_coeff)
    log(f"  L2-L2 error smpl-reco:         {e2}")

    e3 = compute_error(cs_test, reco_nb_test, ws_test, space=p.space_coeff)
    log(f"  L2-L2 error smpl-reco_nb:      {e3}")

    e4 = compute_error(reco_nb_test, reco_test, ws_test, space=p.space_coeff)
    log(f"  L2-L2 error reco-reco_nb:      {e4}")


def test_reconstruction(info):
    p = Problem(info)
    M, deg = info["expansion"]["size"], 2
    dims = [deg]*M
    n_samples = info["sampling"]["n_samples"]
    dist = info["sampling"]["distribution"]
    decay = info["expansion"]["decay rate"]
    rho = info["expansion"]["rho"]
    vartheta = info["expansion"]["vartheta"]

    log(f"generate {n_samples} training samples")
    N = 1000
    dofs = fem.dof_coords(p.space_coeff, p.mesh)
    ys = np.random.normal(0, 1, [N, M])
    ys_scaled = rescale_samples(ys, info)
    ws = np.ones(ys.shape[0]) / ys.shape[0]
    cs = eval_diff_coeff(dofs, ys, decay, mean=0.0)
    cs_scaled = eval_diff_coeff(dofs, ys_scaled, decay, mean=0.0)

    # Object that can be evaluated as
    #         ml(ys[j]) = [bm(dim)(ys[j,0]), ..., bm(dim)(ys[j,-1])]
    ml = MeasurementList(get_basis_measure(dims, dist, info=info))

    # # generate eval tensor
    # ys_test = rescale_samples(np.random.normal(0, 1, [int(1e+7), M]), info)
    # bms = get_basis_measure([5]*M, dist, info=info)
    # p_evals = [bms[m](ys_test[:, m])[0] for m in range(M)]
    # for m, vals in enumerate(p_evals):  # for each stochastic dimension m
    #     N, d = vals.shape
    #     gramian = vals.reshape(N, d, 1) * vals.reshape(N, 1, d)
    #     gramian = np.sum(gramian, axis=0) / N
    #     np.set_printoptions(suppress=True)
    #     print(np.linalg.norm(gramian-np.eye(gramian.shape[0])))

    log("prep and run uq_ra_adf for scaled basis")
    assert ml.dimensions == tuple(dims), f"{ml.dimensions} vs {dims}"
    weights = np.ones(cs.shape[0])
    vals = [xe.Tensor.from_buffer(w * val)
            for w, val in zip(weights, cs_scaled)]
    meas = transpose(ml(ys_scaled[:, :len(dims)].T))
    meas = [[xe.Tensor.from_buffer(w * np.array(cmp_m)) for cmp_m in m]
            for w, m in zip(weights, meas)]
    reco = xe.uq_ra_adf(meas, vals, (cs.shape[1],) + ml.dimensions,
                        targeteps=1e-8, maxitr=300)
    log(f"  dims:  {reco.dimensions}")
    log(f"  ranks: {reco.ranks()}")

    log("run uq_ra_adf for normal basis")
    reco2 = tt.reconstruct(dims, ys[:, :len(dims)].T, ws, cs, dist)
    reco3 = femTT.change_basis_to_scaled_hermite(reco2, decay, rho, vartheta)
    log(f"  dims:  {reco2.dimensions}")
    log(f"  ranks: {reco2.ranks()}")

    ys_test = np.random.normal(0, 1, [1000, M])
    ws_test = np.ones(ys_test.shape[0]) / ys_test.shape[0]
    cs_test = eval_diff_coeff(dofs, ys_test, decay, mean=0.0)

    # error of standard reconstruction
    abs_err = femTT.compute_L2L2_error(reco2, ys_test, ws_test, cs_test,
                                       p.space_coeff, dist)
    norm = femTT.compute_L2L2_norm(ys_test, cs_test, p.space_coeff, ws=ws_test)
    rel_err = abs_err / norm
    log(f"L2-L2 Herm original:     {abs_err:4.2e} / {rel_err:4.2e}")

    # error of rescaled coefficient after change to sHerm basis
    abs_err = femTT.compute_L2L2_error(reco3, ys_test, ws_test, cs_test,
                                       p.space_coeff, dist, info=info)
    norm = femTT.compute_L2L2_norm(ys_test, cs_test, p.space_coeff, ws=ws_test)
    rel_err = abs_err / norm
    log(f"L2-L2 Herm scaled after: {abs_err:4.2e} / {rel_err:4.2e}")

    # error of reconstruction with scaled Herm poly
    abs_err = femTT.compute_L2L2_error(reco, ys_test, ws_test, cs_test,
                                       p.space_coeff, dist, info=info)
    norm = femTT.compute_L2L2_norm(ys_test, cs_test, p.space_coeff, ws=ws_test)
    rel_err = abs_err / norm
    log(f"L2-L2 scaled Herm reco:  {abs_err:4.2e} / {rel_err:4.2e}")


def get_basis_measure(dims, dist, info=None):
    def bm(dim, mean=0.0, var=1.0):
        """
        Parameters
        ----------
        dim : int
            Dimension of univariate polynomial space, i.e. 0, ..., dim-1.
        mean : float
        var : float

        Returns
        -------
        Function that computes: ys -> [P_0(ys), ..., P_{dim-1}(ys)]
        """
        return BasisMeasure(HermitePolynomials(dim-1, mean=mean, variance=var))
    assert dist == "normal"
    if info is None or info["sampling"]["use_scaled_base"] is False:
        bms = [bm(dim, mean=0.0, var=1.0) for dim in dims]
    else:
        bms = []
        decay = info["expansion"]["decay rate"]
        rho = info["expansion"]["rho"]
        vartheta = info["expansion"]["vartheta"]
        for m in range(info["expansion"]["size"]):
            std = compute_scaling_constant(m+1, decay, rho*vartheta, gamma=0.9)
            bms.append(bm(dims[m], mean=0.0, var=std**2))
    return bms


# ------------------------------------------------------------------------------
if __name__ == "__main__":
    print()

    info = {
        "problem": {"name": "darcy"},
        "fe": {"degree": 1, "mesh": 8},
        "fe_coeff": {"degree": 0, "space": "DG"},
        "expansion": {"size": 10, "mean": 0.0, "decay rate": 2.0,
                      "rho": 1, "vartheta": 0.1, "scale": 6 / np.pi**2},
        "sampling": {
            "distribution": "normal", "expfield": True,
            "use_scaled_base": True,
            "batch size": 40, "n_samples": 1000
        }
    }

    # test_error_computation(info)
    test_reconstruction(info)

    print()
