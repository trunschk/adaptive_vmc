"""
A copy of adaptive_vmc/.
"""
from math import factorial
from decimal import Decimal
import itertools

import numpy as np
from numpy.polynomial.hermite_e import hermeval
import xerus as xe  # TODO: just for add

from context import src
import src.utils.tt_tools as tt
import src.estimator.intrusive.tt_tail_product as tt_tail


def combine(array, indices):
    """ 
    Subfunction of mult()
    """
    for idcs in indices:
        # all indices are consecutive
        assert np.all(idcs == np.arange(idcs[0], idcs[-1]+1))
    idcs = indices[0]
    for next_idcs in indices[1:]:
        # all indices are sorted and disjoint
        assert idcs[-1] < next_idcs[0]
    for idcs in reversed(indices):
        new_dimension = np.prod(array.shape[idcs[0]:idcs[-1]+1])
        new_shape = array.shape[:idcs[0]] + \
            (new_dimension,) + array.shape[idcs[-1]+1:]
        array = array.reshape(new_shape)
    return array


def mult(left, right, triple_product):
    """ Same as tt.compute_product_coeff_tensor()
    """
    assert len(left) == len(right)

    new_components = []
    for pos in range(len(left)):
        l, r = left[pos].shape[1], right[pos].shape[1]
        assert np.allclose(triple_product[:l, (l+r):, :r], 0)
        middle = triple_product[:l, :(l+r), :r]
        assert middle.shape == (l, l+r, r)

        new_component = np.einsum(
            'aix, ikj, bjy -> abkxy', left[pos], middle, right[pos])
        new_component = combine(new_component, [(0, 1), (3, 4)])
        new_components.append(new_component)

        # l_core = left[lia]
        # r_core = right[lia]
        # comp = np.einsum('ijk, lmj->ilmk', right[lia], mid)
        # comp = np.einsum('ijk, ajmc->aimck', left[lia], comp)
        # comp = comp.reshape(l_core.shape[0]*r_core.shape[0], comp.shape[2], l_core.shape[2]*r_core.shape[2], order="F")
        # new_components.append(comp)

    return new_components


def evaluate(tt, basis, points):
    """ 
    Evaluate TTTensor in points with specified basis.
    """
    assert points.ndim <= 2
    if points.ndim == 1:
        points = points.reshape(-1, len(points))
    M = len(tt)
    assert points.shape[1] == M

    d = max(cmp.shape[1] for cmp in tt)
    N = len(points)
    if basis == "hermite":
        factorials = np.array([factorial(k) for k in range(d)], dtype=object)
        factors = np.sqrt((1/factorials).astype(float))
        measures = hermeval(points, np.diag(factors)).T
    else:
        raise ValueError(f"Unknown basis '{basis}'")
    assert measures.shape == (M, N, d)

    ret = np.ones((N, 1))
    for pos in range(M):
        ret = np.einsum('nl, ler, ne -> nr', ret, tt[pos], measures[pos])
    return ret.reshape(N)


def add(left, right):
    """ 
    Add two TTTensrs.
    """
    ttl = xe.TTTensor([cmp.shape[1] for cmp in left])
    ttr = xe.TTTensor([cmp.shape[1] for cmp in right])
    for m in range(len(left)):
        d = max(left[m].shape[1], right[m].shape[1])
        l, e, r = left[m].shape
        ttl.set_component(m, xe.Tensor.from_buffer(
            np.concatenate([left[m], np.zeros((l, d-e, r))], axis=1)))
        l, e, r = right[m].shape
        ttr.set_component(m, xe.Tensor.from_buffer(
            np.concatenate([right[m], np.zeros((l, d-e, r))], axis=1)))
    s = ttl + ttr
    return [np.asarray(s.get_component(m)) for m in range(len(left))]


def dimensions(tt): return [cmp.shape[1] for cmp in tt]
def ranks(tt): return [cmp.shape[0] for cmp in tt[1:]]


def components(xett): return [np.asarray(
    xett.get_component(m)) for m in range(xett.order())]


def round(tt, threshold=None):
    """
    Round TTTensor to threshold and return as new object.
    """
    xett = xe.TTTensor(dimensions(tt))
    for m in range(len(tt)):
        xett.set_component(m, xe.Tensor.from_buffer(tt[m]))
    xett.canonicalize_left()
    if threshold is not None:
        xett.round(threshold)
    return components(xett)


# order = 5
# dims = [2]*order
# rks = [1] + [2]*(order-1) + [1]
# tt1 = [np.random.randn(rks[i], dims[i], rks[i+1]) for i in range(order)]

# order = 5
# tt1 = [np.array([0,1]).reshape(1,2,1) for i in range(order)]

order = 5
# \sum_{m=1}^5 10/m*x_m
tt1 = [np.zeros((1, 2, 1))]*order
e0 = np.reshape([1, 0], (1, 2, 1))
e1 = np.reshape([0, 1], (1, 2, 1))
for m in range(order):
    # NOTE: ttm is P_{e_m}/(m): [1,0] x ... x [1,0] x [0, 1/(m+1)] x [1,0] x ... x [1,0]
    ttm = [e0]*order
    # ttm[m] = e1/(2*(m+1))
    ttm[m] = e1/(m+1)
    # ttm[m] = 2*e1/(m+1)
    tt1 = add(tt1, ttm)
tt1 = round(tt1, 1)

tt2 = tt1
# triple_product = triple_product_tensor(max(dimensions(tt1)), max(dimensions(tt2)), "hermite")
triple_product = tt.triple_product_tensor(
    max(dimensions(tt1)),
    max(dimensions(tt1))+max(dimensions(tt2)),
    max(dimensions(tt2)), "normal")
# tt3 = mult(tt1, tt2, triple_product)
tt3 = tt.compute_product_coeff_tensor(tt1, tt2, "normal")

y = np.random.randn(10000, order)
# y = 2.0*np.random.rand(10000, order)-1.0
# y = 3.0*np.random.rand(10000, order)-1.5
# y = 4.0*np.random.rand(10000, order)-2.0
# y = 6.0*np.random.rand(10000, order)-3.0

print(np.linalg.norm(evaluate(tt1, "hermite", y) *
                     evaluate(tt2, "hermite", y) - evaluate(tt3, "hermite", y)))

tt4 = add(tt1, tt3)
print(np.linalg.norm(evaluate(tt1, "hermite", y) +
                     evaluate(tt3, "hermite", y) - evaluate(tt4, "hermite", y)))

# NOTE: This is the Taylor expansion of exp(y). We use h: R^M -> R to compute
#       the expansion around the point y = h(x).


def approx_exp(tt, order):
    """
    Approximate exp of function by power series up to [order] terms:
        exp(x) = sum_{k=0}^{order} x^k / k!
    """
    ret = [np.ones((1, 1, 1))]*len(tt)  # k = 0
    xk = tt  # k = 1
    for k in range(1, order):
        # ret += x^k / k!
        xkfac = list(xk)
        xkfac[0] = xkfac[0] / factorial(k)
        ret = add(ret, xkfac)
        if k < order-1:
            dim = max(cmp.shape[1] for cmp in xk)
            triple_product = tt.triple_product_tensor(
                dim, 2*dim, dim, "normal")
            xk = mult(xk, tt, triple_product)
            # xk = tt.compute_product_coeff_tensor(xk, tt, "normal")
    return ret


for order in range(21):
    exp_tt = round(approx_exp(tt1, order), 1)
    print(f"Taylor order:       {order}")
    print(f"dimensions(exp_tt): {dimensions(exp_tt)}")
    print(f"ranks(exp_tt):      {ranks(exp_tt)}")
    rel_err = np.linalg.norm(np.exp(evaluate(tt1, 'hermite', y)) - evaluate(
        exp_tt, 'hermite', y))/np.linalg.norm(np.exp(evaluate(tt1, 'hermite', y)))
    print(f"relative error:     {rel_err}")
    if rel_err < 1e-6:
        break

ett = xe.TTTensor(dimensions(exp_tt))
for m in range(len(exp_tt)):
    ett.set_component(m, xe.Tensor.from_buffer(exp_tt[m]))
ett = xe.dyadic_product([xe.TTTensor.ones([1]), ett])

N = 10_000
d = max(dimensions(exp_tt))
M = len(exp_tt)

points = np.random.randn(N, M)
# points = 3*np.random.rand(N, M)-1.5
# points = 6*np.random.rand(N, M)-3
values = np.exp(evaluate(tt1, 'hermite', points)).reshape(N, 1)
values = [xe.Tensor.from_buffer(val) for val in values]

assert [d]*M == dimensions(exp_tt)
factorials = np.array([factorial(k) for k in range(d)], dtype=object)
factors = np.sqrt((1/factorials).astype(float))
measures = hermeval(points, np.diag(factors)).T
assert measures.shape == (M, N, d)
measures = np.transpose(measures, axes=(1, 0, 2))
assert measures.shape == (N, M, d)
measures = [[xe.Tensor.from_buffer(cmp_m) for cmp_m in m] for m in measures]

ett = xe.uq_ra_adf(ett, measures, values, [1]+[d]*M, targeteps=1e-8, maxitr=50)
exp_tt = components(ett)
exp_tt[1] *= exp_tt[0][0, 0, 0]
exp_tt = exp_tt[1:]
print(f"dimensions(exp_tt): {dimensions(exp_tt)}")
print(f"ranks(exp_tt):      {ranks(exp_tt)}")
rel_err = np.linalg.norm(np.exp(evaluate(tt1, 'hermite', y)) - evaluate(
    exp_tt, 'hermite', y))/np.linalg.norm(np.exp(evaluate(tt1, 'hermite', y)))
print(f"relative error:     {rel_err}")

ett = xe.uq_ra_adf(measures, values, [1]+[d]*M, targeteps=1e-8, maxitr=50)
exp_tt = components(ett)
exp_tt[1] *= exp_tt[0][0, 0, 0]
exp_tt = exp_tt[1:]
print(f"dimensions(exp_tt): {dimensions(exp_tt)}")
print(f"ranks(exp_tt):      {ranks(exp_tt)}")
rel_err = np.linalg.norm(np.exp(evaluate(tt1, 'hermite', y)) - evaluate(
    exp_tt, 'hermite', y))/np.linalg.norm(np.exp(evaluate(tt1, 'hermite', y)))
print(f"relative error:     {rel_err}")
