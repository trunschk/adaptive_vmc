# -*- coding: utf-8 -*-
import sys, os, time
import numpy as np
import matplotlib.pyplot as plt

from fenics import *

from context import src
from src.misc import Logger, log, timeit
import src.utils.tt_tools as tt
from src.utils.fem_tools import mass_matrix
from src.problem.equation.darcy import Problem

# ------------------------------------------------------------------------------
# helper functions

def plot_fenics_function(axis, val, space, mesh=None, cmap=None):
    u = Function(space)
    u.vector().set_local(val)
    
    plt.sca(axis)
    if cmap is not None: plt.set_cmap(cmap)
    im = plot(u)
    if mesh is not None: plot(mesh)
    plt.colorbar(im)
    if cmap is not None: plt.set_cmap("viridis")
    return None

def plot_approximation(V, val_f, val_reco, title, file_name):
    fig, ax = plt.subplots(figsize=(15,4), nrows=1, ncols=3)
    fig.suptitle(title)
    
    plot_fenics_function(ax[0], val_f, V)
    ax[0].set_title("function")
    plot_fenics_function(ax[1], val_reco, V)
    ax[1].set_title("reconstruction")
    plot_fenics_function(ax[2], np.abs(val_f-val_reco), V)
    ax[2].set_title("difference (abs)")

    plt.savefig(file_name)

def get_eval_approx(reco, ys, dist, use_exp=False):
    val = tt.eval_approx(reco, ys, dist)
    if use_exp: val = np.exp(val)
    return val

def compute_approximation_error(reco, ys, ws, vals, space, dist, use_exp=False):
    """
    Compute L2-L2 norm of approximation error.
    """
    mass_mat = mass_matrix(space=space)
    approx = get_eval_approx(reco, ys, dist, use_exp)
    assert vals.shape == approx.shape
    diff = vals - approx
    err_sq = np.einsum('n,ni,in', ws, diff, mass_mat@diff.T)
    return np.sqrt(err_sq)

# ------------------------------------------------------------------------------
# tests

def test_func(ys, V, use_exp=False):  # type: (List[float], FunctionSpace) -> Function
    """
    f(x,y) = sum_{m=1}^M s**m * c(m) * y_j

    c(m) = sin( 2*pi*(m+1)*x_1 ) * sin( 2*pi*(m+1)*x_2 )
    """
    s = 0.9
    M = ys.size
    c = Expression('sin(2*pi*F*x[0]) * sin(2*pi*F*x[1])', F=0, degree=10)
    def summand(m): # this is: cos(1\pi\beta1(m) x_1) cos(2\pi\beta2(m) x_2)
        c.F = m
        return interpolate(c, V).vector().get_local()
    def linear_part(ys, M): # this is: sum_{m=1}^M y_m * a_m(x)
        x = Function(V).vector().get_local()  # zero
        for m in range(M):
            x += ys[m] * s**(m+1) * summand(m+1)
        return x
    val = linear_part(ys, M)
    if use_exp: val = np.exp(val)
    f = Function(V)
    f.vector().set_local(val)
    return f

def func(ys, V, use_exp):
    """
    Generate multiple samples from test_func.
    """
    val = np.zeros((ys.shape[0], V.dim()))
    for j,y in enumerate(ys):
        f = test_func(y,V, use_exp=use_exp)
        val[j] = f.vector().get_local()
    return val

def test(title, case, ys_test=None, y_dim=None, deg=None, n_samples=None):
    log(title)
    if case == "affine":
        use_exp_reco = False
        use_exp_test = False
    elif case == "exp":
        use_exp_reco = True
        use_exp_test = True
    elif case == "log":
        use_exp_reco = False
        use_exp_test = True
    else: 
        raise AssertionError(f"unknown case: {case}")
    use_exp_eval = True if case == "log" else False
        
    mesh = UnitSquareMesh(10, 10)
    V = FunctionSpace(mesh, "CG", 1)
    x_dim = V.dim()
    if y_dim is None: y_dim = 5
    if deg is None: deg = 2
    if n_samples is None: n_samples = 1000
    
    # evaluate function
    log(f"Generate {n_samples} samples")
    ys = np.random.normal(0,1, (n_samples, y_dim))
    ws = np.ones(n_samples)/n_samples
    vals = func(ys, V, use_exp=use_exp_reco)
    
    # reconstruct with VMC
    log("Reconstruct")
    sdim = [deg]*y_dim
    reco = tt.reconstruct(
            dims=sdim, nodes=ys.T, weights=ws, values=vals, dist="normal",
            init=None, label="func", verbose=True
            ) # dimensions: [mesh_cells]+[2]*(expansion length)
    
    # test approximation error
    log("Evaluate approximation")
    n_samples_test = 1000
    if ys_test is None: 
        ys_test = np.random.normal(0,1, (n_samples_test, y_dim))
    ws_test = np.ones(n_samples_test)/n_samples_test
    vals_test = func(ys_test, V, use_exp=use_exp_test)
    
    reco_test = get_eval_approx(reco, ys_test, dist="normal", use_exp=use_exp_eval)
    
    log("Compute approximation error")
    err = compute_approximation_error(
            reco, ys_test, ws_test, vals_test, V, dist="normal", use_exp=use_exp_eval
            )
    log("  error L2: {:<4.2e}".format(err))

    return V, ys_test, vals_test, reco_test

if __name__ == "__main__":
    # NOTE: Test reconstruction of lognormal function with VMC.
    print()

    # --------------------------------------------------------------------------
    # Test 1: reconstruction of affine function with normal variables
    if False:
        title = "TEST 1 -- affine function"
        file_name = "./img/test_HermiteVmcReco_affineCase.png"
        V, ys, val_f, val_reco = test(title, case="affine")
        plot_approximation(V, val_f[0], val_reco[0], title, file_name)
        print()
    
    # --------------------------------------------------------------------------
    # Test 2: reconstruction of lognormal function
    if False:
        title = "TEST 2 -- lognormal function [direct]"
        file_name = "./img/test_HermiteVmcReco_expCase.png"
        V, _, val_f, val_reco = test(title, case="exp", ys_test=ys)
        plot_approximation(V, val_f[0], val_reco[0], title, file_name)
        print()

    # --------------------------------------------------------------------------
    # Test 3: reconstruction of log of lognormal function, then taking exp()
    if False:
        title = "TEST 3 -- lognormal function [via log(f)]"
        file_name = "./img/test_HermiteVmcReco_logCase.png"
        V, _, val_f, val_reco = test(title, case="log", ys_test=ys)
        plot_approximation(V, val_f[0], val_reco[0], title, file_name)
        print()
    
    # --------------------------------------------------------------------------
    # Test 4: reconstruction of lognormal function with larger poly deg
    if False:
        title = "TEST 4 -- lognormal function [direct] -- larger poly deg"
        file_name = "./img/test_HermiteVmcReco_expCase_largerPolyDeg.png"
        V, _, val_f, val_reco = test(title, case="exp", ys_test=ys, deg=15)
        plot_approximation(V, val_f[0], val_reco[0], title, file_name)
        print()
    
    # --------------------------------------------------------------------------
    # Test 5: reconstruction of lognormal function with more samples
    if False:
        title = "TEST 5 -- lognormal function [direct] -- more samples"
        file_name = "./img/test_HermiteVmcReco_expCase_moreSamples.png"
        V, _, val_f, val_reco = test(title, case="exp", ys_test=ys, n_samples=int(4e+04))
        plot_approximation(V, val_f[0], val_reco[0], title, file_name)
        print()
    
