# -*- coding: utf-8 -*-
import sys
import os
import time
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt

from fenics import *
import xerus as xe

from context import src, avmc_intrusive
from src.misc import Logger, log, timeit
from src.utils.fem_tools import sample_solution
from src.utils.tt_tools import reconstruct, eval_approx, get_initial_guess
from src.utils.femTT_tools import compute_L2L2_error
from src.problem.equation.darcy import Problem
from avmc_intrusive import compute_approx_error_sol

print()
plt.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}', r'\usepackage{amssymb}']

# ------------------------------------------------------------------------------


def fem_int2dof(inits, info):
    dofs = np.zeros(len(inits))
    info_tmp = deepcopy(info)
    for i, init in enumerate(inits):
        info_tmp["fe"]["mesh"] = int(init)
        p = Problem(info_tmp)
        dofs[i] = p.space_coeff.dim()
    return dofs

# ------------------------------------------------------------------------------


def get_reconstruction(infos, sdim, u_reco_old=None, u_reco_old_space=None):
    if u_reco_old is not None:
        assert u_reco_old_space is not None
    info = deepcopy(infos)
    p = Problem(info)  # problem.equation.darcy.Problem class
    n_samples = info["sampling"]["n_samples"]
    dist = info["sampling"]["distribution"]

    log("  generate initial samples")
    log(f"    generate training data for {n_samples} samples ...")

    ys, ws, us = sample_solution(info, n_samples)

    init = None
    if u_reco_old is not None:
        dim = [p.space.dim()] + sdim
        init = get_initial_guess(dim, u_reco_old, u_reco_old_space, p.space)

    u_reco = reconstruct(
        dims=sdim, nodes=ys[:, :len(sdim)].T, weights=ws, values=us, init=init,
        dist=dist, label="solution u", verbose=True
    )  # dimensions: [mesh_cells]+[2]*(expansion length)

    return u_reco

# ------------------------------------------------------------------------------


def get_approximation_error(infos, u_reco):
    info = deepcopy(infos)
    dist = info["sampling"]["distribution"]
    expfield = info["sampling"]["expfield"]

    log("  generate initial samples")
    log(f"    generate test data for {n_samples_test} samples ...")
    ys_test, ws_test, us_test = sample_solution(info, n_samples_test)

    log("  evaluate reco in test samples ...")
    with timeit("  ... {:4.2f} s"):
        u_reco_val = get_eval_approx(
            u_reco, ys_test, dist=dist, expfield=expfield)

    # stochastic L^2 norm
    log("  compute approximation error ...")
    space = Problem(info).space
    dist = info["sampling"]["distribution"]
    with timeit("  ... {:4.2f} s"):
        error_L2 = compute_L2L2_error(
            u_reco, ys_test, ws_test, us_test, space, dist)

    return error_L2

# ------------------------------------------------------------------------------


def get_eval_approx(reco, ys, dist, expfield=False):
    val = eval_approx(reco, ys, dist)
    # if expfield: val = np.exp(val) TODO enable, if reconstructing log-field
    return val

# ------------------------------------------------------------------------------


def plot_difference(info, val1, val2, file_name="../img/default.png", title=""):
    p = Problem(info)
    V = p.space_coeff
    a = Function(V)

    fig, ax = plt.subplots(figsize=(15, 4), nrows=1, ncols=3)

    plt.sca(ax[0])
    a.vector().set_local(val1)
    im = plot(a)
    fig.colorbar(im)
    ax[0].set_title("exact solution")

    plt.sca(ax[1])
    a.vector().set_local(val2)
    im = plot(a)
    fig.colorbar(im)
    ax[1].set_title("reco")

    plt.sca(ax[2])
    a.vector().set_local(np.abs(val1-val2))
    im = plot(a)
    fig.colorbar(im)
    ax[2].set_title("error")

    fig.suptitle(title)
    plt.savefig(file_name)

# ------------------------------------------------------------------------------


def plot_approx_error(info, x_axis, val,
                      file_name="../img/default.png", title="", x_label="", loglog=True):

    p = Problem(info)
    mean = np.mean(val, axis=0)
    median = np.median(val, axis=0)

    def nnz(y):
        idx = np.where(y > 1e-15)[0]
        return idx, y[idx]

    plt.figure(figsize=(10, 5))

    idx, mean = nnz(mean)
    if loglog:
        plt.loglog(x_axis[idx], median[idx], "o-",
                   label="error L2", c="teal", lw=2)
    else:
        plt.semilogy(x_axis[idx], median[idx], "o-",
                     label="error L2", c="teal", lw=2)

    for y in val:
        idx, f = nnz(y)
        if loglog:
            plt.loglog(x_axis[idx], f, alpha=0.2, c="teal")
        else:
            plt.semilogy(x_axis[idx], f, alpha=0.2, c="teal")

    plt.legend()
    plt.xlabel(x_label)
    plt.title(title)
    plt.savefig(file_name)
    plt.close()


# ------------------------------------------------------------------------------
if __name__ == "__main__":
    sys.stdout = Logger(fileName="output.log", filePath="./log/")

    # __________________________________________________________________________
    # normal case
    if True:
        log("NORMAL CASE")
        data = {}

        n_samples = 1000
        n_samples_test = 1000
        N_RUNS = 1

        info = {
            "problem": {"name": "darcy"},
            "fe": {"degree": 1, "mesh": 20},
            "fe_coeff": {"degree": 0, "space": "DG"},
            "expansion": {"size": 10, "mean": 3.0, "decay rate": 4.0,
                          "scale": 6 / np.pi**2},
            "sampling": {
                "distribution": "normal", "expfield": False,
                "batch size": 40, "n_samples": n_samples
            }
        }
        data["info"] = deepcopy(info)
        p = Problem(info)

        title = (f"expansion length L = {info['expansion']['size']}, "
                 + f" decay = {info['expansion']['decay rate']}, "
                 + f" n_samples = {info['sampling']['n_samples']}, "
                 + f" FEM space = CG-{info['fe']['degree']}, "
                 + f" FEM dofs = {p.space.dim()}"
                 )
        title_conv = "Approximation error of solution\n" + title

        x_axis = np.array([1000])
        x_label = "number of samples"
        sdim = [2]

        data["x_axis"] = x_axis

        error_L2 = np.zeros((N_RUNS, len(x_axis)))
        for i_run in range(N_RUNS):
            if N_RUNS > 1:
                print()
                log(f"RUN {i_run+1}")
            for i, x in enumerate(x_axis):
                if len(x_axis) > 1:
                    log(f"-- {x_label} = {x} --")

                # info["expansion"]["size"] = 5
                # info["sampling"]["n_samples"] = x
                if i == 0:
                    u_reco = get_reconstruction(info, sdim)
                    eL2 = get_approximation_error(info, u_reco)
                    space = Problem(info).space
                else:
                    u_reco = get_reconstruction(info, sdim, u_reco, space)
                    eL2 = get_approximation_error(info, u_reco)
                    space = Problem(info).space

                error_L2[i_run, i] = eL2
                log("  Approximation error: {:4.2e}".format(eL2))
                # plot_approx_error(info, x_axis, error_L2[:i_run+1], title=title_conv,
                #         loglog=False, x_label=x_label,
                #         file_name="../img/test_coeff_approx_lognormal_conv.png")

                data["error_L2"] = error_L2[:i_run+1]
                os.makedirs("./data/", exist_ok=True)
                np.save("./data/test_solution_approx_normal.npy", data)

        ys_test, ws_test, us_test = sample_solution(info, 1)
        u_reco_val = get_eval_approx(
            u_reco, ys_test, info["sampling"]["distribution"],
            expfield=info["sampling"]["expfield"]
        )
        plot_difference(info, us_test[0], u_reco_val[0], title=title,
                        file_name="../img/test_solution_approx_normal_expl.png")

        print()
