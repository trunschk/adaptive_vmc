# Refinement

  iter  0:                                   -> sdim = [2]
  iter  1: stoch - add mode          [1]     -> sdim = [2,2]
  iter  2: stoch - increase mode     [0]     -> sdim = [3,2]
  iter  3: stoch - add mode          [2]     -> sdim = [3,2,2]
  iter  4: stoch - add/increase mode [3, 0]  -> sdim = [4,2,2,2]
  iter  5: stoch - add mode          [4]     -> sdim = [4,2,2,2,2]
x iter  6: stoch - add mode          [5]     -> sdim = [4,2,2,2,2,2]
  iter  7: stoch - add/increase mode [6, 1]  -> sdim = [4,3,2,2,2,2,2]
x iter  8: stoch - add/increase mode [7, 0]  -> sdim = [5,3,2,2,2,2,2,2]
  iter  9: det   - bisect 6 cells            -> dofs: 625 -> 781
x iter 10: stoch - add mode          [8]     -> sdim = [5,3,2,2,2,2,2,2,2]
x iter 11: stoch - add mode          [9]     -> sdim = [5,3,2,2,2,2,2,2,2,2]
x iter 12: stoch - add/increase mode [10, 0] -> sdim = [6,3,2,2,2,2,2,2,2,2,2]
x iter 13: stoch - add mode          [11]    -> sdim = [6,3,2,2,2,2,2,2,2,2,2,2]
  iter 14: det   - bisect 25 cells           -> dofs: 781 -> 1393
  iter 15: stoch - break

# Error Decrease

iter  1:       -> 1.2562e-02
iter  2: down  -> 9.1611e-03
iter  3: down  -> 5.5210e-03
iter  4: down  -> 3.5777e-03
iter  5: down  -> 2.3560e-03
iter  6: down  -> 1.8808e-03
iter  7: const -> 1.8808e-03
iter  8: down  -> 1.3046e-03
iter  9: const -> 1.3046e-03
iter 10: down  -> 9.0806e-04
iter 11: const -> 9.0806e-04
iter 12: const -> 9.0806e-04
iter 13: const -> 9.0806e-04
iter 14: const -> 9.0806e-04
iter 15: down  -> 5.8891e-04

# Ranks

iter  1:    (   ->  2)
iter  2: +  ( 2 ->  4)
iter  3: +  ( 4 ->  6)
iter  4: +  ( 6 -> 11)
iter  5: +  (11 -> 19)
iter  6: +  (19 -> 22)
iter  7: =  (22 -> 22)
iter  8: +  (22 -> 25)
iter  9: =  (25 -> 25)
iter 10: -  (25 -> 21)
iter 11: =  (21 -> 21)
iter 12: =  (21 -> 21)
iter 13: =  (21 -> 21)
iter 14: =  (21 -> 21)
iter 15: -  (21 -> 20)
           
