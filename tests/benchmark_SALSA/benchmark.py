import os
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

from fenics import *
import xerus as xe

from context import src, avmc_intrusive
import src.estimator.intrusive.intrusive_estimator as EST
from src.problem.equation.darcy import Problem
from src.misc import timeit, log, transpose
from src.utils.tt_tools import get_initial_guess, eval_approx, get_basis_measure, dofs
from src.measurement_utils.measures import MeasurementList
from src.utils.fem_tools import refine_mesh, prolongate_x_2, sample_solution, stiffness_matrix
from avmc_intrusive import compute_approx_error_coeff

# ------------------------------------------------------------------------------


def load_data(level, file_name):
    full_data = np.load(file_name, allow_pickle=True).item()
    assert level < len(full_data["info_dict_level"]), "Level does not exist."

    info = full_data["info_dict_level"][level]
    u_reco = full_data["u_reco_level"][level]
    a_reco = full_data["a_reco_level"][level]
    sdim = full_data["sdim_level"][level]

    info["sampling"]["expfield"] = False

    return info, u_reco, a_reco, sdim, full_data

# ------------------------------------------------------------------------------


def get_samples(n_samples=0, sample_new=False, info=None):
    file_name = "./samples_bm.npy"
    if not os.path.isfile(file_name):
        sample_new = True

    if sample_new:
        # generate new samples and save
        assert n_samples > 0 and info is not None
        with timeit("  ... ({:4.2f} s)"):
            ys, ws, us, cs, fs = generate_samples(n_samples, info)
        np.save(file_name, {"ys": ys, "ws": ws, "us": us, "cs": cs, "fs": fs})
        log(f"  save samples to: {file_name}")
    else:
        log(f"  load samples: {file_name}")
        data = np.load(file_name, allow_pickle=True).item()
        if n_samples < 1:
            n_samples = data["ys"].shape[0]
        if n_samples > data["ys"].shape[0]:
            log(
                f"  not enough samples in file: {data['ys'].shape[0]} < {n_samples}")
            with timeit("  ... ({:4.2f} s)"):
                delta = n_samples - data['ys'].shape[0]
                ys_new, ws_new, us_new, cs_new, fs_new = generate_samples(
                    delta, info)
            data["ys"] = np.concatenate([data["ys"], ys_new], axis=0)
            data["ws"] = np.concatenate([data["ws"], ws_new], axis=0)
            data["us"] = np.concatenate([data["us"], us_new], axis=0)
            data["cs"] = np.concatenate([data["cs"], cs_new], axis=0)
            data["fs"] = np.concatenate([data["fs"], fs_new], axis=0)
            np.save(file_name, data)
            log(f"  update file: {file_name}")
        idx = np.random.choice(data["ys"].shape[0], n_samples, replace=False)
        ys, ws = data["ys"][idx], data["ws"][idx]
        us, cs, fs = data["us"][idx], data["cs"][idx], data["fs"][idx]
        log(f"  loaded {ys.shape[0]} sampling points")

    return ys, ws, us, cs, fs

# ------------------------------------------------------------------------------


def print_table(header, leader, data):
    width = int(np.max([len(h) for h in header]+[len(l) for l in leader]))+2
    header_format = "{:>13}" * (len(header)+1)
    row_format = "{:>13}" + "{:>13.2e}"*len(header)
    print(header_format.format("", *header))
    print("  "+"-"*(13*(len(header)+1)-2))
    for case, row in zip(leader, data):
        print(row_format.format(case, *row))

# ------------------------------------------------------------------------------


def print_est(est_data, iters):
    header = [f"iter {iters[0]}", f"iter {iters[1]}", f"iter {iters[1]} new"]
    leader = ["est_det", "est_stoch", "est_disc", "est_total", "error"]
    print_table(header, leader, est_data)

# ------------------------------------------------------------------------------


def compute_error_single(u_reco, info):
    n_samples_ref = 1000

    info_ref = deepcopy(info)
    p_ref = Problem(info_ref)

    log("  uniform refine last FE mesh ...")

    num_elements = p_ref.mesh.num_cells()
    info_ref = refine_mesh(range(num_elements), info_ref)  # refine uniform
    p_ref = Problem(info_ref)  # update mesh for problem

    log(f"  generate data for {n_samples_ref} samples ...")
    with timeit("  ... ({:4.2f} s)"):
        ys, ws, us = sample_solution(info_ref, n_samples_ref)

    log("  compute error ...")
    stiffn = stiffness_matrix(Problem(info_ref).space)
    p_level = Problem(info)
    u_hs_coarse = eval_approx(u_reco, ys, info["sampling"]["distribution"])
    u_hs = prolongate_x_2(u_hs_coarse, p_level.space, p_ref.space)
    diff = us - u_hs
    err_sq = np.einsum('n,ni,in', ws, diff, stiffn@diff.T)
    error = np.sqrt(err_sq)

    return error

# ------------------------------------------------------------------------------


def reconstruct_new(dims, nodes, weights, values, init=None, label="", verbose=False,
                    dist="uniform"):
    x_dim = values.shape[1]  # physical dimension
    tensor = xe.Tensor.from_buffer
    vals = [tensor(val) for val in values]

    bm = get_basis_measure(dist)
    ml = MeasurementList([bm(dim) for dim in dims])
    assert ml.dimensions == tuple(dims), f"{ml.dimensions} vs {dims}"

    meas = transpose(ml(nodes))
    meas = [[tensor(np.array(cmp_m)) for cmp_m in m] for m in meas]
    # There is a bug in `transpose` that causes `cmp_m` to have wrong strides.
    # Creating a new array fixes the strides.

    log(f"  reconstruct {label} ...")
    with timeit("  ... ({:4.2f} s)"):
        if init is None:
            # TODO replace xe.uq_ra_adf() by something else
            reco = xe.uq_ra_adf(meas, vals, (x_dim,) + ml.dimensions,
                                targeteps=1e-8, maxitr=300)
        else:
            # TODO replace xe.uq_ra_adf() by something else
            start = xe.TTTensor(init)
            reco = xe.uq_ra_adf(start, meas, vals, (x_dim,) + ml.dimensions,
                                targeteps=1e-8, maxitr=300)
        if verbose:
            log(f"      dimensions: {reco.dimensions}")
            log(f"      ranks:      {reco.ranks()}")
            log(f"      tt-dofs:    {dofs(reco)}")

    return reco


# ------------------------------------------------------------------------------
if __name__ == "__main__":
    print()
    file_name = "./full_data_bm.npy"
    iters = [5, 6]  # iterations 6 and 7
    sample_new = False

    # __________________________________________________________________________
    # iteration 5
    log(f"Load data from iteration {iters[0]}")

    # load data (_c = coarse)
    info_c, u_reco_c, a_reco_c, sdim_c, full_data = load_data(
        iters[0], file_name)
    p_c = Problem(info_c)
    est_det = [full_data["est_det"][iters[0]]]
    est_stoch = [full_data["est_stoch"][iters[0]]]
    est_disc = [full_data["est_disc"][iters[0]]]
    est_total = [full_data["est_total"][iters[0]]]
    error = [full_data["error"][iters[0]]]

    # diagnostics
    # _ = compute_approx_error_coeff(a_reco_c, info_c, verbose=True)

    # __________________________________________________________________________
    # iteration 6 (old/reference)
    log(f"Load data from iteration {iters[1]+1} (reference solution)")

    # load data (_f = fine)
    info_f, u_reco_f, a_reco_f, sdim_f, _ = load_data(iters[1], file_name)
    p_f = Problem(info_f)
    est_det += [full_data["est_det"][iters[1]]]
    est_stoch += [full_data["est_stoch"][iters[1]]]
    est_disc += [full_data["est_disc"][iters[1]]]
    est_total += [full_data["est_total"][iters[1]]]
    error += [full_data["error"][iters[1]]]

    # __________________________________________________________________________
    # iteration 6 (new)
    log(f"Iteration {iters[1]+1} -- new computation")

    # load data (_n = new)
    info_n, _, _, sdim_n, _ = load_data(iters[1], file_name)
    p_n = Problem(info_n)
    n_samples = full_data["n_samples"][iters[1]]
    n_samples = 1000  # NOTE only for debug (faster this way)

    # get samples
    ys, ws, us, cs, fs = get_samples(n_samples, sample_new, info_n)

    # generate initial guess and reconstruct u
    init = get_initial_guess(
        [p_n.dofs()]+sdim_n, u_reco_c, p_c.space, p_n.space)
    u_reco_n = reconstruct_new(
        dims=sdim_n,
        nodes=ys[:, :len(sdim_n)].T, weights=ws, values=us,
        init=init, label="solution u", verbose=True
    )

    # generate initial guess and reconstruct a
    a_dim = [2 for j in range(info_n["expansion"]["size"])]
    init = get_initial_guess(
        [p_n.space_coeff.dim()]+a_dim, a_reco_c, p_c.space_coeff, p_n.space_coeff)
    a_reco_n = reconstruct_new(
        dims=a_dim,
        nodes=ys.T, weights=ws, values=cs, init=init,
        label="coefficient a", verbose=True
    )

    # reconstruct rhs f
    f_reco_n = reconstruct_new(
        sdim_n,
        nodes=ys[:, :len(sdim_n)].T, weights=ws, values=fs, init=None,
        label="forcing f", verbose=False
    )

    # compute approximation errors
    # _ = compute_approx_error_sol(u_reco_n, info_n, verbose=True)
    # _ = compute_approx_error_coeff(a_reco_n, info_n, verbose=True)
    # _ = compute_approx_error_rhs(f_reco_n, info_n, verbose=True)

    # compute error estimator contributions
    log("  Compute error estimator parts ...")
    est_det_T_sq, est_modes_sq, est_disc_sq = EST.compute_estimator_terms(
        f_reco_n, a_reco_n, u_reco_n, p_n, t=full_data["L"])

    est_det += [np.sqrt(np.sum(np.abs(est_det_T_sq)))]
    est_stoch += [full_data["c_stoch"]*np.sum(np.sqrt(np.abs(est_modes_sq)))]
    est_disc += [full_data["c_disc"]*np.sqrt(np.abs(est_disc_sq))]
    est_total += [np.sqrt((est_det[-1] + est_stoch[-1] +
                           est_disc[-1])**2 + est_disc[-1]**2)]

    # compute error
    log("Compute error ...")
    error += [compute_error_single(u_reco_n, info_n)]

    # print est data
    log("Estimator Values")
    est_data = np.array([est_det, est_stoch, est_disc, est_total, error])
    print_est(est_data, iters)

    print()
