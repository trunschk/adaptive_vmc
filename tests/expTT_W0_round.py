import sys
import os
import numpy as np
from scipy.special import zeta
import matplotlib.pyplot as plt
import xerus as xe
from tqdm import trange
from copy import deepcopy
from fenics import *

from context import src
from src.misc import timeit, log, transpose, Logger
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt
import src.utils.femTT_tools as femTT
import src.utils.tt_exp as expTT
from src.estimator.intrusive.tt_tail_product import compute_product_coeff_tensor
from src.parallel import Parallel, ParallelizableProblem

# import autoPDB

# ==============================================================================


class TestExponent():
    def __init__(self, decay, M, mean=0.0, scale=1.0, nT=20, fem_deg=0, element="DG", useDarcyCoeff=True):
        """
        Affine function similar to Darcy diffusion coefficient, i.e.
            h(x,y) = scale * ( mean + sum_{m=1}^M h_m(x) y_m )
        where
            h_m(x) = 0.9 / \zeta(decay) * m**(-decay) * cos(2\pi B1(m) x_1) * cos(2\pi B2(m) x_2).
        Here, B1 = B2 = 2 for the test case and 
            B1(m) = m - k(m)*( k(m)+1 ) / 2      and     B2(m) = k(m) - B1(m)
        for 
            k(m) = floor[ -1/2 + \sqrt{ 1/4 + 2m } ].
        """
        self.decay = decay
        self.M = M
        self.scale = scale
        self.det_coeff = Expression(
            "C * (cos(2*pi*B1*x[0]) * cos(2*pi*B2*x[1]))", C=1, B1=2, B2=2, degree=10)
        self.mean = mean
        self.useDarcyCoeff = useDarcyCoeff

        self.mesh = Mesh(UnitSquareMesh(nT, nT))
        if element == "DG":
            assert fem_deg == 0  # does not work for DG-n wit n > 0
        self.fem_deg = fem_deg
        self.space = FunctionSpace(self.mesh, element, self.fem_deg)

    # NOTE: used to mimic Darcy lognormal coefficient
    def k(self, m): return np.floor(-0.5+np.sqrt(0.25 + 2*m))
    def beta1(self, m): return m - self.k(m) * (self.k(m)+1) / 2
    def beta2(self, m): return self.k(m) - self.beta1(m)

    def realisation(self, y, exp=False):
        """ FEM function for one specific parameter realization. """
        det_coeff = self.det_coeff

        def interpolate_det_coeff(C, B1, B2):
            det_coeff.C, det_coeff.B1, det_coeff.B2 = C, B1, B2
            return interpolate(det_coeff, self.space).vector().get_local()

        def linear(y):
            u = Function(self.space).vector().get_local()
            for m in range(self.M):
                C = 0.9/zeta(self.decay) * (m+1)**(-self.decay)
                if self.useDarcyCoeff == True:
                    B1, B2 = self.beta1(m+1), self.beta2(m+1)
                else:
                    B1, B2 = 2, 2
                u += interpolate_det_coeff(C, B1, B2) * y[m]
            return u
        val = self.scale * (self.mean + linear(y))
        if exp == True:
            val = np.exp(val)
        ret = Function(self.space)
        ret.vector().set_local(val)
        return ret


class Problem(ParallelizableProblem):
    def __init__(self, info):
        self.info = info
        self.decay = info["decay"]
        self.M = info["M"]
        self.mean = info["mean"]
        self.scale = info["scale"]
        self.nT = info["nT"]
        self.useDarcyCoeff = info["useDarcyCoeff"]
        assert element in ["CG", "DG"]
        self.element = info["element"]
        self.fem_deg = info["fem_deg"]

        self.field = TestExponent(self.decay, self.M, mean=self.mean,
                                  scale=self.scale, nT=self.nT, element=self.element,
                                  fem_deg=self.fem_deg, useDarcyCoeff=self.useDarcyCoeff)

    def evaluate(self, y):
        """ FEM coefficiens for one specific parameter realization. """
        kappa = self.field.realisation(y, exp=False)
        return kappa.vector().get_local(), y

    def evaluate_exp(self, y):
        """ FEM coefficiens for one specific parameter realization. """
        kappa = self.field.realisation(y, exp=True)
        return kappa.vector().get_local(), y

    def evaluate_in_points(self, x, y, exp=False):
        """ Evaluate h(x,y) / exp(h(x,y)). """
        if x.ndim == 1:
            x.shape = 1, -1
        if y.ndim == 1:
            y.shape = 1, -1
        assert x.ndim == 2 and y.ndim == 2
        assert x.shape[0] == 1 or y.shape[0] == 1
        coeffs = self.get_coefficients(y, exp=exp)
        if x.shape[0] == self.field.space.dim():
            dofs = fem.dof_coords(self.field.space, self.field.mesh)

        # if x are Lagrange points, then evaluation values are the coefficients
        if np.allclose(x, dofs):
            return np.squeeze(coeffs)

        # evaluate coefficients in arbitrary (non-Lagrange) points
        ret = np.zeros((y.shape[0], x.shape[0]))
        for i, coeff in enumerate(coeffs):
            ret[i] = fem.eval_fencis_function(x, coeff, self.field.space)
        ret = np.squeeze(ret)
        if ret.ndim == 0:
            ret = ret.item()  # make 0 dim array to float

        return np.array(ret)

    def get_coefficients(self, y, exp=False):
        """ 
        Compute the FEM coefficients of the affine-normal exponent of the
        diffusion coefficient h for given parameter samples.
        """
        if y.ndim == 1:
            y.shape = 1, -1
        assert y.ndim == 2
        if exp == True:
            coeffs = Parallel(self.evaluate_exp)
        else:
            coeffs = Parallel(self.evaluate)
        ret = coeffs(y)
        c, y = transpose(ret)
        return np.array(c)

    def generate_samples_Darcy_fast(self, ys):
        M = self.info["M"]
        b1 = np.array([self.field.beta1(m+1) for m in range(M)]).reshape(1, -1)
        b2 = np.asarray([self.field.beta2(m+1)
                         for m in range(M)]).reshape(1, -1)
        C = np.array(
            [0.9/zeta(self.info["decay"]) * (m+1)**(-self.info["decay"])
             for m in range(M)]).reshape(1, -1)
        assert ys.ndim == 2 and ys.shape[1] == M
        xs = fem.dof_coords(self.field.space, self.field.mesh)
        cos1 = np.cos(2*np.pi*b1*xs[:, 0].reshape(-1, 1))
        cos2 = np.cos(2*np.pi*b2*xs[:, 1].reshape(-1, 1))
        assert cos1.shape[0] == cos2.shape[0] == self.field.space.dim()
        assert cos1.shape[1] == cos2.shape[1] == M
        terms = np.reshape(C*cos1*cos2, (1, self.field.space.dim(), M))
        ys = np.reshape(ys, (ys.shape[0], 1, M))
        # ret = np.sum(terms*ys, axis=2)
        buffers = list(
            np.unique(list(range(0, ys.shape[0], 1000)) + [ys.shape[0]]))
        ret = np.empty((ys.shape[0], self.field.space.dim()))
        for j in range(len(buffers[:-1])):
            bL, bR = buffers[j], buffers[j+1]
            ret[bL:bR] = np.sum(terms*ys[bL:bR], axis=2)
        return self.info["scale"]*(self.info["mean"]+ret)


def compute_L2L2_error(ys, us, reco, space):
    """
    compute the L2-L2 error of a tt approximation and the exact h (or exp h)
    over the domain.
    """
    ws = np.ones(ys.shape[0])/ys.shape[0]
    abs_err = femTT.compute_L2L2_error(reco, ys, ws, us, space, "normal")

    mass_mat = tt.mass_matrix(space)
    err_sq = np.einsum('n,ni,in', ws, us, mass_mat@us.T)
    rel_err = abs_err / np.sqrt(err_sq)

    return abs_err, rel_err


# ==============================================================================
if __name__ == "__main__":
    sys.stdout = Logger(fileName="expTT_W0_round", filePath="./log/")
    log("setup problem")
    # --------------------------------------------------------------------------
    # setup

    # FEM stuff
    decay, mean = 2, 0.0
    NTs = [112, 112, 112, 100, 142,  71, 159, 224]  # number of triangles
    FDs = [1,   0,   2,   0,   1,   2,   0,   1]  # finite element degree
    Ms = [20,  20,  20,  20,  20,  20,  20,  20]  # number of Parameters
    UP = [0,   0,   0,   0,   0,   0,   0,   1]  # use prolongation

    # NTs = [ 20,  20, 13] # number of triangles
    # FDs = [  1,   1,  1] # finite element degree
    # Ms  = [ 10,  10, 20] # number of Parameters
    # UP  = [  0,   0,  0] # use prolongation

    assert len(NTs) == len(FDs) == len(Ms)

    os.makedirs("./data", exist_ok=True)
    os.makedirs("./data/W0_round/", exist_ok=True)

    for j in range(len(Ms)):
        nT, fem_deg, M = NTs[j], FDs[j], Ms[j]
        log("__"*40)
        log("")
        log(f"RUN: {j+1}/{len(Ms)}    --    nT={nT}, fem_deg={fem_deg}, M={M}")
        dims = [6]*M
        element = "DG" if fem_deg == 0 else "CG"

        APPENDIX = f"{element}{fem_deg}-{nT}_decay-{decay}_M{M}"
        h_reco_file = f"./data/h_recos/h_reco_{APPENDIX}.npy"
        W0_file = f"./data/W0_round/W0_{APPENDIX}.npy"

        if os.path.isfile(W0_file):
            log(f"file {W0_file} already exists")
            continue
        if not os.path.isfile(h_reco_file):
            log(f"file {h_reco_file} does not exist, skip iteration")
            continue

        info = {"M": M, "decay": decay, "mean": mean, "scale": 1, "nT": nT,
                "element": element, "fem_deg": fem_deg, "useDarcyCoeff": True,
                "ITER": 4}
        p = Problem(info)

        # IC
        x0 = fem.dof_coords(p.field.space, p.field.mesh)  # fem-dofs
        y0 = np.zeros((1, M))
        exph0 = p.evaluate_in_points(x0, y0, exp=True)
        exph0_scaled = p.evaluate_in_points(
            x0, y0*2**(-p.info["ITER"]), exp=True)

        log(f"FE-DoFs: {p.field.space.dim()}")

        # h_reco
        log(f"load h_reco")
        h_reco = tt.load(h_reco_file)

        # scale affine exponent h
        log("scale exponent h")
        h_reco_scaled = xe.TTTensor(h_reco)*2**(-p.info["ITER"])

        # build global operator and rhs
        log("build operators W0, b ...")
        with timeit("... {:4.2f} s"):
            W0, b = expTT.build_system_inexactFEMIC(
                dims, dims, h_reco_scaled, x0, y0*2**(-p.info["ITER"]),
                exph0_scaled, p.field.space, mesh=p.field.mesh, weight=1)
        log(f"  W0 ranks: {W0.ranks()}")
        EPS = 1e-12
        log(f"round operators W0 to {EPS:4.0e}...")
        with timeit("... {:4.2f} s"):
            W0.round(EPS)
        log(f"  W0 ranks: {W0.ranks()}")
        log(f"save W0 to: {W0_file}")
        tt.save(W0_file, W0)
