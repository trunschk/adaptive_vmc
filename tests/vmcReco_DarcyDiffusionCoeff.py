# -*- coding: utf-8 -*-
import sys, os, time
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt

from fenics import *
import xerus as xe

from context import src, avmc_intrusive
from src.misc import Logger, log, timeit
from src.utils.fem_tools import sample_diffusion_coeff
from src.utils.tt_tools import reconstruct, eval_approx, get_initial_guess
from src.problem.equation.darcy import Problem
from avmc_intrusive import compute_approx_error_coeff

print()
plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}', r'\usepackage{amssymb}']

# ------------------------------------------------------------------------------
def fem_int2dof(inits, info):
    dofs = np.zeros(len(inits))
    info_tmp = deepcopy(info)
    for i,init in enumerate(inits):
        info_tmp["fe"]["mesh"] = int(init)
        p = Problem(info_tmp)
        dofs[i] = p.space_coeff.dim()
    return dofs

# ------------------------------------------------------------------------------
def get_reconstruction(infos, sdim, a_reco_old=None, a_reco_old_space=None):
    if a_reco_old is not None: assert a_reco_old_space is not None
    info = deepcopy(infos)
    p = Problem(info) # problem.equation.darcy.Problem class
    n_samples = info["sampling"]["n_samples"]
    dist = info["sampling"]["distribution"]
    
    log("  generate initial samples")
    log(f"    generate training data for {n_samples} samples ...")

    ys, ws, cs = sample_diffusion_coeff(info, n_samples)

    init = None
    if a_reco_old is not None:
        dim = [p.space_coeff.dim()] + sdim
        init = get_initial_guess(dim, a_reco_old, a_reco_old_space, p.space_coeff)
    
    a_reco = reconstruct(
            dims=sdim, nodes=ys.T, weights=ws, values=cs, init=init, 
            dist=dist, label="diff. coeff. a", verbose=True
            ) # dimensions: [mesh_cells]+[2]*(expansion length)
    
    return a_reco
    
# ------------------------------------------------------------------------------
def get_approximation_error(infos, a_reco):
    info = deepcopy(infos)
    dist = info["sampling"]["distribution"]
    expfield = info["sampling"]["expfield"]
    
    log("  generate initial samples")
    log(f"    generate test data for {n_samples_test} samples ...")
    ys_test, ws_test, cs_test = sample_diffusion_coeff(info, n_samples_test)
    
    log("  evaluate reco in test samples ...")
    with timeit("  ... {:4.2f} s"): 
        a_reco_val = get_eval_approx(a_reco, ys_test, dist=dist, expfield=expfield)
    
    # stochastic L^2 norm
    error_L2 = compute_approx_error_coeff(a_reco, info, verbose=False)

    return error_L2

# ------------------------------------------------------------------------------
def get_eval_approx(reco, ys, dist, expfield=False):
    val = eval_approx(reco, ys, dist)
    if expfield: val = np.exp(val)
    return val

# ------------------------------------------------------------------------------
def plot_difference(info, val1, val2, file_name="../img/default.png", title=""):
    p = Problem(info)
    V = p.space_coeff
    a = Function(V)

    fig, ax = plt.subplots(figsize=(15,4), nrows=1, ncols=3)
    
    plt.sca(ax[0])
    a.vector().set_local(val1)
    im = plot(a)
    fig.colorbar(im)
    ax[0].set_title("exact coeff")

    plt.sca(ax[1])
    a.vector().set_local(val2)
    im = plot(a)
    fig.colorbar(im)
    ax[1].set_title("reco")
    
    plt.sca(ax[2])
    a.vector().set_local(np.abs(val1-val2))
    im = plot(a)
    fig.colorbar(im)
    ax[2].set_title("error")
    
    fig.suptitle(title)
    plt.savefig(file_name)

# ------------------------------------------------------------------------------
def plot_approx_error(info, x_axis, val, 
        file_name="../img/default.png", title="", x_label="", loglog=True):
    
    p = Problem(info)
    mean = np.mean(val, axis=0)
    median = np.median(val, axis=0)
    
    def nnz(y):
        idx = np.where(y > 1e-15)[0]
        return idx, y[idx]
    
    plt.figure(figsize=(10,5))

    idx, mean = nnz(mean)
    if loglog:
        plt.loglog(x_axis[idx], median[idx], "o-", label="error L2", c="teal", lw=2)
    else:
        plt.semilogy(x_axis[idx], median[idx], "o-", label="error L2", c="teal", lw=2)
    
    for y in val: 
        idx, f = nnz(y)
        if loglog:
            plt.loglog(x_axis[idx], f, alpha=0.2, c="teal")
        else:
            plt.semilogy(x_axis[idx], f, alpha=0.2, c="teal")
    
    plt.legend()
    plt.xlabel(x_label)
    plt.title(title)
    plt.savefig(file_name)
    plt.close()

# ------------------------------------------------------------------------------
if __name__ == "__main__":
    # NOTE: The how good the diffusion coefficient a of the darcy problem is
    #       reconstructed by VMC for
    #       - affine case
    #       - log-normal case
    
    sys.stdout = Logger(fileName = "output.log", filePath = "./log/")
    
    # __________________________________________________________________________
    # affine case
    if False:
        log("AFFINE CASE")
        data = {}
    
        n_samples_test = 100
        N_RUNS = 1
    
        info = {
            "problem": {"name": "darcy"},
            "fe": { "degree": 3, "mesh": 10 },
            "fe_coeff": { "degree": 2, "space":"DG" },
            "expansion": { "size": 5, "mean": 1.0, "decay rate": 2.0,
                "scale": 6 / np.pi**2  # scale <= mean * 6 / np.pi**2 for decay == 2
            },
            "sampling": { "distribution": "uniform", "batch size": 40, 
                "expfield":False, "n_samples": 1000 },
        }
        data["info"] = deepcopy(info)
        p = Problem(info)
        
        title = ( f"expansion length L = {info['expansion']['size']}, "
                + f" decay = {info['expansion']['decay rate']}, "
                + f" n_samples = {info['sampling']['n_samples']}, "
                + f" FEM space = {info['fe_coeff']['space']}-{info['fe_coeff']['degree']}, "
                + f" FEM dofs = {p.space_coeff.dim()}"
                )
        title_conv = "Approximation error of diffusion coefficient\n" + title
    
        # x_axis = np.arange(5, 51, 5)
        # x_axis2 = fem_int2dof(x_axis, info)
        x_axis = np.arange(5,8)
        # x_axis = np.array([100, 1000, 10000, 20000, 30000, 40000, 50000, 70000])
        data["x_axis"] = x_axis
        x_label = "expansion length L"
    
        error_L2 = np.zeros((N_RUNS,len(x_axis)))
        for i_run in range(N_RUNS):
            if N_RUNS > 1:
                print()
                log(f"RUN {i_run+1} / {N_RUNS}")
            for i,x in enumerate(x_axis):
                if len(x_axis) > 1: log(f"-- {x_label} = {x} --")
                # info["sampling"]["n_samples"] = x
                info["expansion"]["size"] = x
                
                if i == 0:
                    a_reco = get_reconstruction(info, sdim)
                    eL2 = get_approximation_error(info, a_reco)
                    space = Problem(info).space_coeff
                else:
                    a_reco = get_reconstruction(info, sdim, a_reco, space)
                    eL2 = get_approximation_error(info, a_reco)
                    space = Problem(info).space_coeff
                    
                error_L2[i_run,i] = eL2
                plot_approx_error(info, x_axis, error_L2[:i_run+1], title=title_conv,
                        loglog=False, x_label=x_label, 
                        file_name="../img/test_coeff_approx_affine_conv.png")
                
                data["error_L2"] = error_L2[:i_run+1]
                os.makedirs("./data/", exist_ok=True)
                np.save("./data/test_coeff_approx_affine.npy",data)
                
        ys_test, ws_test, coeffs_test = sample_diffusion_coeff(info, 1)
        a_reco_val = eval_approx(a_reco, ys_test, info["sampling"]["distribution"])
        plot_difference(info, coeffs_test[0], a_reco_val[0], title=title, 
                file_name="../img/test_coeff_approx_affine_expl.png")
        
        print()
        
    # __________________________________________________________________________
    # log-normal case
    if True:
        log("LOG-NORMAL CASE")
        data = {}
        
        n_samples = 1000
        n_samples_test = 1000
        N_RUNS = 1
        
        info = {
            "problem": {"name": "darcy"},
            "fe": { "degree": 1, "mesh": 20 },
            "fe_coeff": { "degree": 0, "space":"DG" },
            "expansion": {"size": 5, "mean": 0.0, "decay rate": 4.0, 
                "scale": 6 / np.pi**2 },
            "sampling": { 
                "distribution": "normal", "expfield": True, 
                "batch size": 40, "n_samples": n_samples 
                }
        }
        data["info"] = deepcopy(info)
        p = Problem(info)
        
        title = ( f"expansion length L = {info['expansion']['size']}, "
                + f" decay = {info['expansion']['decay rate']}, "
                + f" n_samples = {info['sampling']['n_samples']}, "
                + f" FEM space = {info['fe_coeff']['space']}-{info['fe_coeff']['degree']}, "
                + f" FEM dofs = {p.space_coeff.dim()}"
                )
        title_conv = "Approximation error of diffusion coefficient\n" + title
        
        # x_axis = np.unique(sorted(list(range(5,21,2))+[9,10,11,12,13]+list(range(10,51,10))))
        # x_axis = np.arange(1,11)
        # x_label = "expansion length L"
        
        x_axis = np.array([1000])
        x_label = "number of samples"
        sdim = [20]*info["expansion"]["size"]
        
        data["x_axis"] = x_axis
        
        error_L2 = np.zeros((N_RUNS,len(x_axis)))
        for i_run in range(N_RUNS):
            if N_RUNS > 1:
                print()
                log(f"RUN {i_run+1}")
            for i,x in enumerate(x_axis):
                if len(x_axis) > 1: log(f"-- {x_label} = {x} --")
                
                # info["expansion"]["size"] = 5
                # info["sampling"]["n_samples"] = x
                if i == 0:
                    a_reco = get_reconstruction(info, sdim)
                    eL2 = get_approximation_error(info, a_reco)
                    space = Problem(info).space_coeff
                else:
                    a_reco = get_reconstruction(info, sdim, a_reco, space)
                    eL2 = get_approximation_error(info, a_reco)
                    space = Problem(info).space_coeff
                    
                error_L2[i_run,i] = eL2
                log("  Approximation error: {:4.2e}".format(eL2))
                # plot_approx_error(info, x_axis, error_L2[:i_run+1], title=title_conv,
                #         loglog=False, x_label=x_label, 
                #         file_name="../img/test_coeff_approx_lognormal_conv.png")
                
                data["error_L2"] = error_L2[:i_run+1]
                os.makedirs("./data/", exist_ok=True)
                np.save("./data/test_coeff_approx_lognormal.npy",data)
         
        ys_test, ws_test, coeffs_test = sample_diffusion_coeff(info, 1)
        a_reco_val = get_eval_approx(
                a_reco, ys_test, info["sampling"]["distribution"], 
                expfield=not info["sampling"]["expfield"]
                )
        plot_difference(info, coeffs_test[0], a_reco_val[0], title=title, 
                file_name="../img/test_coeff_approx_lognormal_expl.png")
        
        print()
    
