
Misc Module
===========

Utility functions for logging and timing.
Triple products.

.. automodule:: misc
   :members:
   :show-inheritance:
   :special-members: __getitem__, __call__

.. You can also use :undoc-members:
