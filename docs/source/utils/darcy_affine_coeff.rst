
Darcy_affine_coef Module
========================

This module implements the affine diffusion coefficient.
An exact TT representation can be computed or the diffusion coefficient can be evaluated for a given parameter.

.. automodule:: utils.darcy_affine_coeff
   :members:
   :show-inheritance:

.. You can also use :undoc-members:
