
Utils Package
=============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   darcy_affine_coeff

.. fem_tools -- Utility functions for FEM like stiffness/mass matrices, solution, marking, refinement and prolongation.
.. tt_tools -- reconstruct (wrapper for UQRAADF), evalutation of TTs, slicing, cutting, dofs,  projection and prolongation. convert
.. femTT_tools -- prolongation with a FEM component, error norms
.. tt_exp -- Operators, RHS and utility functions to apply expTT.
