.. Adaptive VMC documentation master file, created by
   sphinx-quickstart on Fri Jun 11 12:44:05 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Adaptive VMC's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   misc
   legacy
   H1_reco
   utils/index.rst
   estimator/index.rst


The project also includes additional files that are briefly described in the following.

- `problem`: Description of the PDE and the diffusion coefficient.
- `parallel`: Utilities for parallel evalutation of PDEs.
- `autoPDB`: In case of an uncought exception, automatically start PDB instead of terminating.
- `context`: Set the path variable to allow for relative imports without installation.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
