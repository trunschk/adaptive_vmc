Notes
=====

To run the code in this repository, you need to provide an environment which
can run [fenics](https://fenicsproject.org/) and
[xerus](https://www.libxerus.org/) at the same time.

**Install mshr (for Lshape):** `conda install -c conda-forge mshr`

Brief Code Overview
===================

The code for the computation of the exponential in TT format is located in
`src/utils/`. The computation of the error estimator and the individual contributions
can be found under `src/estimator/intrusive/`.

Numerical Experiments
=====================

ExpTT
-----

To run the experiments from the Exp-TT paper[^expTT], use the `app/expTT_*`
files. In particular we have

| File | Section | Description |
| --- | --- | --- |
| expTT_logNormal_covariance.py | 4.1 | exponentiation of KLE of random field with Gaussian covariance kernel for different correlation lengths|
| expTT_logNormal_darcy.py | 4.2 | exponentiation of affine Darcy diffusion coefficient |
| expTT_logLikelihood_gauss_correlated.py | 4.3 | exponentiation of Gaussian log-pdf with strongly correlated covariance |
| expTT_logLikelihood_darcy.py | 4.4 | exponentiation of Bayesian log-likelihood with solution of Darcy problem for lognormal diffusion coefficient as forward model |
| expTT_logLikelihood_gauss_concentrated.py | - | exponentiation of concentrated Gaussian log-pdf |

aVMC
----

To run the experiments from the aVMC paper[^aVMC], use the `app/avmc_*` files.
In particular we have

| File | Description |
| --- | --- |
| avmc_affine.py | experiments for the affine diffusion coefficient $\gamma$ |
| avmc_lognormal.py | experiments for the lognormal diffusion coefficient $\kappa$|

The respective decay rates, domains and number of expansion terms of the
diffusion coefficient can be specified in the scripts.

The `plot_avmc_convergence.py` script plots the convergence graphs from the data
saved by the experimental scripts. Similarily, `plot_avmc_mesh.py` plots the
underlying FE mesh and the means of the solution and diffusion coefficient for
the respective case.

[^expTT]: [Efficient approximation of high-dimensional exponentials by tensornetworks](https://arxiv.org/abs/2105.09064)
[^aVMC]: [Adaptive non-intrusive reconstruction of solutions to high-dimensional parametric PDEs](https://arxiv.org/abs/2112.01285)
