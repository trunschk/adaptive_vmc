# -*- coding: utf-8 -*-
from __future__ import division, print_function
from src.parallel import ParallelizableProblem
from ..field.testfield import TestField_noStoch, TestField_constDet  # NOTE: for debug
from ..field.testfield import TestField, TestField2, TestField3
from fenics import *
set_log_level(LogLevel.WARNING)


class Problem(ParallelizableProblem):
    def __init__(self, info):
        assert info['problem']['name'] == "darcy"
        self.info = info

        # setup fe space
        self.space = FunctionSpace(self.mesh, 'CG', self.degree)
        coeff_space = self.info["fe_coeff"]["space"]
        coeff_deg = self.info["fe_coeff"]["degree"]
        self.space_coeff = FunctionSpace(self.mesh, coeff_space, coeff_deg)

        # setup random field
        M = self.info['expansion']['size']
        mean = self.info['expansion']['mean']
        scale = self.info['expansion']['scale']
        decay = -self.info['expansion']['decay rate']
        assert "expfield" in self.info["sampling"].keys()  # XXX only for debug
        expfield = self.info["sampling"]["expfield"]
        if "field_type" in self.info["fe_coeff"]:
            field_type = self.info["fe_coeff"]["field_type"]
        else:
            field_type = 2

        if field_type == 1:
            # Philipp's version
            self.field = TestField(
                M, mean=mean, scale=scale, decay=decay, expfield=expfield)
        elif field_type == 2:
            # [EGSZ13] coefficient
            self.field = TestField2(
                M, decay=-decay, mean=mean, expfield=expfield)
        elif field_type == 3:
            # easy affine coefficient
            self.field = TestField3(
                M, decay=-decay, mean=mean, expfield=expfield)
        else:
            raise ValueError(
                f"unknown value '{field_type}' for key 'field_type'")

        # define forcing term
        self.forcing = Constant(1)

        # define boundary condition
        self.bc = DirichletBC(self.space, Constant(0.0), 'on_boundary')

    def solution(self, y):
        """
        Return solution of Darcy problem for given parameter realization y.

        Parameter
        ---------
        y   :   array_like
                Sample for realization of the problem.

        Returns
        -------
        u   :   solution vector (numpy array)
        y   :   input sample
        """
        V = self.space
        f = self.forcing
        bc = self.bc

        kappa = self.field.realisation(y, self.space_coeff)

        u = TrialFunction(V)
        v = TestFunction(V)
        a = kappa * inner(grad(u), grad(v)) * dx
        L = f * v * dx

        u = Function(V)
        solve(a == L, u, bc)

        return u.vector().get_local(), y

    def diffusion_coefficient(self, y):
        """
        Return diffusion coefficient of Darcy problem for given parameter
        realization y.

        Parameter
        ---------
        y   :   array_like
                Sample for realization of the problem.

        Returns
        -------
        k   :   diffusion coefficient vector (numpy array)
        y   :   input sample
        """
        kappa = self.field.realisation(y, self.space_coeff)
        return kappa.vector().get_local(), y

    def rhs(self, y):
        DG0_FS = FunctionSpace(self.mesh, "DG", 0)  # P_0(Tcal)
        rhs = interpolate(self.forcing, DG0_FS)
        return rhs.vector().get_local(), y

    def application(self, y_u):
        y, u_vec = y_u
        M = self.info['expansion']['size']
        assert y.shape == (M,)

        V = self.space
        f = self.forcing
        kappa = self.field.realisation(y, self.space_coeff)

        u = TrialFunction(V)
        v = TestFunction(V)
        a = kappa * inner(grad(u), grad(v)) * dx
        L = f * v * dx

        bc = DirichletBC(V, Constant(0.0), 'on_boundary')

        A, b = assemble_system(a, L, bc)
        u = Function(V).vector()
        u[:] = u_vec
        return (A*u).get_local(), y_u

    def residuum(self, y_u):
        y, u_vec = y_u
        M = self.info['expansion']['size']
        assert y.shape == (M,)

        V = self.space
        f = self.forcing
        kappa = self.field.realisation(y, self.space_coeff)

        u = TrialFunction(V)
        v = TestFunction(V)
        a = kappa * inner(grad(u), grad(v)) * dx
        L = f * v * dx

        bc = DirichletBC(V, Constant(0.0), 'on_boundary')

        A, b = assemble_system(a, L, bc)
        u = Function(V).vector()
        u[:] = u_vec
        res = (A*u - b).get_local()
        assert res.shape == u_vec.shape
        return res, y_u

    def P1_residual_estimator(self, y_u):
        V = self.space
        mesh = V.mesh()
        try:
            h = CellDiameter(V.mesh())
        except NameError:
            h = CellSize(V.mesh())
        DG0 = FunctionSpace(mesh, 'DG', 0)
        dg0 = TestFunction(DG0)

        y, u_vec = y_u
        u = Function(V)
        u.vector().set_local(u_vec)
        f = self.forcing
        kappa = self.field.realisation(y, self.space_coeff)

        R_T = -(f + div(kappa * grad(u)))
        R_dT = kappa * grad(u)
        J = jump(R_dT)
        indicator = h**2 * (1 / kappa) * R_T**2 * dg0 * dx \
            + avg(h) * avg(1 / kappa) * J**2 * avg(dg0) * 2*dS

        eta_res_local = assemble(indicator, form_compiler_parameters={
                                 'quadrature_degree': -1})
        return eta_res_local.get_local(), y_u

    def refine_mesh(self, marked_cells):
        marker = MeshFunction("bool", self.mesh, self.mesh.topology().dim())
        marker.set_all(False)
        # for idx in marked_cells: marker[idx] = True
        marker.array()[marked_cells] = True
        self.mesh = refine(self.mesh, marker)
        self.space = FunctionSpace(self.mesh, 'CG', self.degree)
