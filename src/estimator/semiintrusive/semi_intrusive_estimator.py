
import numpy as np
import itertools
import scipy.sparse as sps
from scipy.sparse.linalg import spsolve

from fenics import *
import xerus as xe

from .context import src # add src to python path and import src
import src.estimator.intrusive_estimator as INT_EST
# TODO use as much code from intrusive_estimmator.py as possible

