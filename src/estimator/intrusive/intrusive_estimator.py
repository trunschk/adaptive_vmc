import numpy as np

import xerus as xe

from .context import src  # add src to python path and import src
from src.misc import timeit
from src.estimator.intrusive.est_det import compute_physical_estimator_volume_terms
from src.estimator.intrusive.est_det import compute_physical_estimator_jump_terms
from src.estimator.intrusive.est_stoch import compute_stochastic_estimator_terms
from src.estimator.intrusive.est_disc import compute_discretization_estimator_terms


def compute_estimator_terms(f_tt, a_tt, u_tt, p, t=1):
    """ Compute local estimator contributions.

    Paramters
    ---------
    f_tt : xe.TTTensor
        TTTensor of right-hand side.
    a_tt : xe.TTTensor
        TTTensor of diffusion coefficient a.
    u_tt : xe.TTTensor
        TTTensor of solution u.
    p : src.problem.equation.darcy.Problem
    t : int, default = 1
        Look-ahead

    Returns
    -------
    est_det_T_sq : list of float
        Squared deterministic estimator contributions on each triangle.
    est_stoch_sq
        Squared stochastic estimator contributions for each mode.
    est_disc_sq
        Squared algebraic estimator contribution.
    """

    # det volume terms
    est_det_vol_sq = compute_physical_estimator_volume_terms(
        f_tt, a_tt, u_tt, p)

    # det jump terms
    est_det_jump_sq = compute_physical_estimator_jump_terms(a_tt, u_tt, p)
    est_det_T_sq = est_det_vol_sq + est_det_jump_sq

    # stoch terms
    est_stoch_sq = compute_stochastic_estimator_terms(a_tt, u_tt, p, t=t)

    # disc terms
    est_disc_sq = compute_discretization_estimator_terms(f_tt, a_tt, u_tt, p)

    return est_det_T_sq, est_stoch_sq, est_disc_sq
