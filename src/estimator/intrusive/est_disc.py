import numpy as np
import itertools
import scipy.sparse as sps
from scipy.sparse.linalg import spsolve

from fenics import *
import xerus as xe

from .context import src  # add src to python path and import src
import src.utils.fem_tools as fem
import src.utils.tt_tools as tt


def compute_discretization_estimator_terms(f_tt, a_tt, u_tt, p):
    """ Compute the discretization (algebraic) estimator

    Compute the discretization (algebraic) estimator given by

        est_disc = || (AW-F) H^{-1/2} ||_2,

    where the 2-norm ist the Frobenius norm, A is the equation operator, W is the
    coefficient tensor of the discrete solution, F ist the right-hand side and
    H is the base change tensor given by

        H = STIMA \otimes I \otimes ... \otimes I.

    Parameters
    ----------
    f_tt : TTTensor
        TTTensor representation of right-hand side (this is F). f_tt should
        have the same stochastic dimensions as u_tt.
    a_tt : TTTensor
        TTTensor representation of the diffusion coefficient.
    u_tt : TTTensor
        TTTensor representation of the discrete solution (this is W).
    p : problem class
    """
    dist = p.info["sampling"]["distribution"]
    weight_type = None
    if dist == "normal" and p.info["sampling"]["expfield"]:
        weight_type = "lognormal"
        decay = p.info["expansion"]["decay rate"]
        rho = p.info["expansion"]["rho"]
        vartheta = p.info["expansion"]["vartheta"]

    f0_core = tt.convert(f_tt)[0]  # shape (1, |T|, 1)
    f_tt_tail = tt.convert(f_tt)[1:]

    # check if rhs is rank-1 tensor with core[0,:,0] = e_1 for each stochastic core
    assert f_tt.dimensions[1:] == u_tt.dimensions[1:]
    for r in f_tt.ranks():
        assert r == 1
    for core in f_tt_tail:
        e_1 = np.zeros(core.shape[1])
        e_1[0] = 1
        assert np.allclose(core[0, :, 0], e_1)

    # construct operator A
    A, B = assemble_operator(p, a_tt, u_tt, f_tt)

    # compute AW
    AW_tt = apply_operator(A, u_tt)

    # compute AW-F
    dims = [core.shape[1] for core in B]
    ranks = [core.shape[0] for core in B[1:]]
    F_tt = xe.TTTensor.random(dims, ranks)
    for j, core in enumerate(B):
        F_tt.set_component(j, xe.Tensor.from_buffer(core))
    # TODO use TTTensor.__sub__() here or do this manually for component lists,
    #      since TTTensor.__sub__() changes ranks?
    AWF_tt = F_tt - AW_tt
    AWF_tt.canonicalize_right()  # NOTE seems irrelevant

    # construct H
    H = [fem.stiffness_matrix(p.space, p.bc)]
    if weight_type is None:
        H += [np.eye(d) for d in u_tt.dimensions[1:]]
    elif weight_type == "lognormal":
        H += tt.hermite_base_product_operator(
            u_tt.dimensions[1:], z=0, decay=decay,
            s1=vartheta*rho, s2=vartheta*rho, sZeta=vartheta*rho,
            normalized=True)

    # compute H \ (AW-F)
    est_disc_sq = compute_frob_norm(AWF_tt, H, p)

    return est_disc_sq


def assemble_operator(p, a_tt, u_tt, f_tt):
    """ Assemble operator A of discretization estimator.

    Assemble the operator

        A[i,mu,j,nu] = sum( A_0[i,j,k_1] prod_{m=1}^M A_m[k_m, mu_m, nu_m, k_{m+1}] )

    where sum is the sum over k_1=1,...,s_1 to k_M=1,...,s_M, and

     - A_0[i,j,k_1] = int_D a_0[k_1](x) inner( grad(phi_i(x)), grad(phi_j(x)) ) dx

     - for m = 1, ..., M-1:
        A_m[k_m, mu_m, nu_m, k_{m+1}] = sum_{eta_m=1}^{2d_m-1} a_m[k_m,eta_m,k_{m+1}]*kappa_{mu_m,nu_m,eta_m}

     - A_M[k_M, mu_M, nu_M, 1] = sum_{eta_M=1}^{2d_M-1} sum_{k_{M+1},...,k_L=1}^{s_{M+1},..,s_L}
            a_M[k_M,eta_M,k_{M+1}] kappa_{mu_M,nu_M,eta_M} prod_{l=M+1}^L a_l[k_l,0,k_{l+1}]
    """

    dist = p.info["sampling"]["distribution"]
    a0_core = tt.convert(a_tt)[0]  # shape (1, Na, s1)
    f0_core = tt.convert(f_tt)[0]  # shape (1, Nf, 1)
    a_tt_tail = tt.convert(a_tt)[1:]
    f_tt_tail = tt.convert(f_tt)[1:]
    M, L = u_tt.order()-1, len(a_tt_tail)

    # assemble A_M
    A_M = np.array([[1]])
    for l in reversed(range(M, L)):  # for l = L, ..., M+1
        # shape (s_l, q_l, s_{l+1})
        core = np.ascontiguousarray(a_tt_tail[l])
        # shape of A_M (in this iteration) is (s_{l+1}, 1)
        # new shape (s_l, 1)
        A_M = np.einsum("ij,jk->ik", core[:, 0, :], A_M)
    # A_M shape is now (s_{M+1}, 1)
    # shape (s_M, q_M, s_{M+1})
    core = np.ascontiguousarray(a_tt_tail[M-1])
    # shape (s_M, q_M, 1)
    A_M = np.einsum("ijk,kl->ijl", core, A_M)
    # NOTE: Reconstruction uses normalized polynomials, hence kappe needs
    #       to be computed for normalized polynomials
    kappa = tt.triple_product_tensor(  # shape (d_M, q_M, d_M)
        u_tt.dimensions[-1], A_M.shape[1], u_tt.dimensions[-1],
        dist, normalized=True)
    # shape (s_M, d_M, d_M, 1)
    A_M = np.einsum("iqj,kql->iklj", A_M, kappa)

    # assemble A_1, ..., A_{M-1}
    A_ms = []
    for m in range(M-1):  # for m = 1, ..., M-1
        # shape (s_m, q_m s_{m+1})
        a_m = np.ascontiguousarray(a_tt_tail[m])
        # NOTE: Reconstruction uses normalized polynomials, hence kappe needs
        #       to be computed for normalized polynomials
        kappa = tt.triple_product_tensor(  # shape (d_m, q_m, d_m)
            u_tt.dimensions[m+1], a_m.shape[1], u_tt.dimensions[m + 1],
            dist, normalized=True)
        # shape (s_m, d_m, d_m, s_{m+1})
        A_m = np.einsum("iqj,kql->iklj", a_m, kappa)
        A_ms.append(A_m)

    # assemble A_0
    A_0, A_0_bc, B_0 = compute_FE_cores(p, a0_core, f0_core)
    assert len(A_0) == a_tt.ranks()[0]
    if M > 1:
        # only if "intermediate" cores exist
        assert len(A_0) == A_ms[0].shape[0]
    assert B_0.shape[1] == u_tt.dimensions[0]
    assert B_0.shape[2] == f0_core.shape[2]

    # assemble operator A
    A = [A_0] + A_ms + [A_M]

    # add boundary conditions to A
    # first (FE) core is just append BC-FE matrix S_bc
    A[0].append(A_0_bc)
    # intermediate cores are block diagonal with identity for bc part
    for m in range(1, M):
        sl, d1, d2, sr = A[m].shape
        core = np.zeros((sl+1, d1, d2, sr+1))
        core[:sl, :, :, :sr] = np.array(A[m])
        core[sl:, :, :, sr:] = np.eye(d1, d2).reshape(1, d1, d2, 1)
        A[m] = core
    # last core is just concatenation of cores to ensure rank s_{M+1]} is 1
    sl, d1, d2, sr = A[M].shape
    core = np.zeros((sl+1, d1, d2, sr))
    core[:sl, :, :, :sr] = np.array(A[M])
    core[sl:, :, :, :sr] = np.eye(d1, d2).reshape(1, d1, d2, 1)
    A[M] = core

    # assemble linear form (rhs)
    # NOTE difference to f_tt should only be application of BC in first
    #      component.
    B = [B_0]
    for core in f_tt_tail:
        B.append(np.ascontiguousarray(core))

    return A, B


def compute_FE_cores(p, coeff, rhs):
    """ Compute operator FE core.

    Compute the operator matrix A_{ij} given by
         A_{ij} = int_D  a * inner(grad(phi_i), grad(phi_j)  dx
    and the rhs given by
         b_{j} = int_D  f * phi_j dx.
    Also applies boundary conditions. Only works for DG0 rhs.

    Parameters
    ----------
    p : problem class
    coeff : array_like with shape (1, p.space_coeff.dofs(), r_1)
    rhs : array_like with shape (1, p.mesh.num_cells(), t_1=1)
    """
    S, B = [], []

    V, W = p.space, p.space_coeff
    DG0_FS = FunctionSpace(p.mesh, "DG", 0)  # P_0(Tcal)

    u, v = TrialFunction(V), TestFunction(V)
    a = Function(W)
    f = Function(DG0_FS)

    for j in range(rhs.shape[2]):
        f.vector().set_local(rhs[0, :, j])
        b = assemble(f*v*dx)
        p.bc.apply(b)
        B.append(b.get_local())
    B = np.ascontiguousarray(np.expand_dims(np.array(B).T, axis=0))
    # B has shape (1, N, r_1)

    # generate list of sparse STIMAs
    lab = parameters['linear_algebra_backend']
    parameters['linear_algebra_backend'] = "Eigen"
    for j in range(coeff.shape[2]):
        a.vector().set_local(coeff[0, :, j])
        stiffness = a * inner(grad(u), grad(v)) * dx
        stiffness = assemble(stiffness)
        p.bc.apply(stiffness)
        S.append(sps.csr_matrix(as_backend_type(stiffness).sparray()))
        # stiffness matrix shape [r_1, (N,N)]
    parameters['linear_algebra_backend'] = lab

    bc_dofs = list(p.bc.get_boundary_values().keys())
    S_bc = sps.csr_matrix(
        (np.ones(len(bc_dofs)), (bc_dofs, bc_dofs)), S[0].get_shape())

    return S, S_bc, B


def apply_operator(A, u_tt):
    """ Compute A(u).

    For a given TTOperator A constructed by assemble_operator() and TTTensor
    u_tt, compute the application A(u_tt).
    """
    u0_core = tt.convert(u_tt)[0]  # shape (1, Nu, r1)
    u_tt_tail = tt.convert(u_tt)[1:]

    AW = []
    for m, A_m in enumerate(A):
        if m == 0:
            AW_0 = []
            for k1 in range(u0_core.shape[2]):
                # shape (Nu,)
                W_0 = u0_core[0, :, k1]
                for l1 in range(len(A_m)):
                    # AW_0 shape (Nu,)
                    AW_0.append(A_m[l1].dot(W_0))
            AW_0 = np.array(AW_0).T.reshape(  # shape (1, Nu, s1*r1)
                1, u0_core.shape[1], len(A_m)*u0_core.shape[2])
            AW.append(AW_0)
        elif m > 0:
            W_m = u_tt_tail[m-1]
            r1, d, r2 = W_m.shape
            s1, d1, d2, s2 = A_m.shape
            assert d == d1 == d2
            # shape (s_m, r_m, d_m, s_{m+1}, r_{m+1})
            AW_m = np.einsum("iabj,kbl->ikajl", A_m, W_m)
            # shape (s_m*r_m, d_m, s_{m+1}*r_{m+1})
            AW_m = np.reshape(AW_m, (s1*r1, d, s2*r2), order="F")
            AW.append(AW_m)

    # NOTE never canonicalize!
    ranks = [core.shape[0] for core in AW[1:]]
    AW_tt = xe.TTTensor.random(u_tt.dimensions, ranks)
    for j, core in enumerate(AW):
        tmp = np.array(list(core))  # need for compatible strides
        AW_tt.set_component(j, xe.Tensor.from_buffer(tmp))

    return AW_tt


def compute_frob_norm(AWF, H, p):
    """ Compute the Frobenius norm || (AW-F) H^{-1/2} ||_F^2.

    Parameters
    ---------
    AWF : TTTensor
        TTTensor representing AW-F.
    H : list of array_like
        List of basis matrices.
        For affine case, H = STIMA x I ... x I.
        For lognormal case, H = STIMA x Z_1 ... x Z_M (see article for detail).
    p : problem class
    """
    M = AWF.order()
    AWF_cores = tt.convert(AWF)
    assert M == len(H)
    # shape (1, 1)
    core = np.array([[1]])
    for m in reversed(range(1, M)):
        # core shape is (r_{m+1}, r{m+1})
        AWF_core = AWF_cores[m]  # shape (r_m, d_m, r_{m+1})
        # shape (d_m, d_m)
        mat = np.ascontiguousarray(H[m])

        # shape (r_m, d_m, r_{m+1})
        core = np.einsum("ijk,kl->ijl", AWF_core, core)
        r1, d, r2 = core.shape
        # shape (d_m, r_m*r_{m+1})
        core = np.reshape(np.swapaxes(core, 0, 1), (d, r1*r2))
        # shape (d_m, r_m*r_{m+1})
        core = np.linalg.solve(mat, core)
        # shape (r_m, d_m, r_{m+1})
        core = np.swapaxes(np.reshape(core, (d, r1, r2)), 0, 1)
        # shape (r_m, r_m)
        core = np.einsum('ijk, mjk->im', core, AWF_core)

    # core shape is (r_1, r_1)
    # AWF_0_core shape (1, N, r_1)
    AWF_0_core = np.ascontiguousarray(AWF_cores[0])
    core = np.einsum("ij,jk->ik", AWF_0_core[0], core)  # shape (N, r_1)
    retval = 0
    for k1 in range(AWF_0_core.shape[2]):
        retval += np.dot(core[:, k1], spsolve(H[0], AWF_0_core[0, :, k1]))
    return retval
