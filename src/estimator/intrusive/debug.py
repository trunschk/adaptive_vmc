import numpy as np
import matplotlib.pyplot as plt

from fenics import *

# ==============================================================================

# ------------------------------------------------------------------------------
def print_dims_ranks(tt_tail,name="tt_tail"):
    dims = [core.shape[1] for core in tt_tail]
    ranks = [core.shape[0] for core in tt_tail] + [tt_tail[-1].shape[2]]
    print(name+":")
    print("  order:", len(dims))
    print("  dims :",dims)
    print("  ranks:",ranks)

# ------------------------------------------------------------------------------
def get_matrix_entries(arr, name=""):
    print("Non-Zero entries of "+name)
    msg = ""
    for i in range(arr.shape[0]):
        if i == 0: msg += "["
        else: msg += " "
        for j in range(arr.shape[1]):
            if j == 0: msg += "["
            if np.abs(arr[i,j]) > 1e-14: msg += "* "
            else: msg += "  "
            if j == arr.shape[1]-1 and i < arr.shape[0]-1: msg += "]\n"
            elif j == arr.shape[1]-1 and i == arr.shape[0]-1: msg += "]]"
    return msg
    
# ------------------------------------------------------------------------------
def plot_function(vec, space, path, mesh=None):
    f = Function(space)
    f.vector().set_local(vec)
    plt.figure()
    c = plot(f)
    if mesh is not None: plot(mesh)
    plt.colorbar(c)
    plt.savefig(path)
    
# ------------------------------------------------------------------------------
def print_info(arr, name="arr"):
    print("--- "+name+" ---")
    print("  len:",len(arr))
    print("  sum:", np.sum(arr))
    print("  max:", np.max(arr))

# ------------------------------------------------------------------------------
def get_entry(tt_tail, mu):
    val = np.array([[1]])
    for j in reversed(range(len(tt_tail))):
        mat = tt_tail[j][:,mu[j],:]
        val = np.dot(mat,val)
    return val

# ==============================================================================
if __name__ == "__main__":
    pass
