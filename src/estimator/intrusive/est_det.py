import numpy as np
import itertools

from fenics import *
import xerus as xe

from .context import src  # add src to python path and import src
import src.utils.tt_tools as tt


def compute_physical_estimator_volume_terms(f_tt, a_tt, u_tt, p):
    """ Compute the deterministic estimator volume parts

    The deterministic estimator volume parts read
        est_{det,T}(u_h)^2 = h_T^2 || R_Lambda(u_h) ||_T^2,  for all T.

    Parameters
    ----------
    f_tt : TTTensor
        Right-hand side f in TTTensor format.
    a_tt : TTTensor
        Diffusion coefficient a in TTTensor format.
    u_tt : TTTensor
        Discrete solution u_h in TTTensor format.
    p : problem class
    """
    # Assume
    # f_tt
    #     ranks: (1, rf_1, ..., rf_M, 1)
    #     dimensions: (|T|, df_1, ..., df_M)
    # u_tt
    #     ranks: (1, r_1, ..., r_M, 1)
    #     dimensions: (Nu, d_1, ..., d_M)
    # a_tt
    #     ranks: (1, s_1, ..., s_M, ..., s_L, 1)
    #     dimensions: (Na, q_1, ..., q_M, ..., q_L)

    # NOTE: || R_Lambda(u_h) ||_T^2
    #       = || f ||_T^2 + || div r_Lambda(u_h) ||_T^2 + 2 < f, div r_Lambda(u_h) >_T
    #       = est_f       + est_r                       + est_mixed

    dist = p.info["sampling"]["distribution"]

    weight_type = None
    if dist == "normal" and p.info["sampling"]["expfield"]:
        weight_type = "lognormal"
        decay = p.info["expansion"]["decay rate"]
        rho = p.info["expansion"]["rho"]
        vartheta = p.info["expansion"]["vartheta"]

    a0_core = tt.convert(a_tt)[0]  # shape (1, Na, s1)
    u0_core = tt.convert(u_tt)[0]  # shape (1, Nu, r1)
    f0_core = tt.convert(f_tt)[0]  # shape (1, |T|, f1)
    a_tt_tail = tt.convert(a_tt)[1:]
    u_tt_tail = tt.convert(u_tt)[1:]
    f_tt_tail = tt.convert(f_tt)[1:]

    r1, s1, f1 = u0_core.shape[2], a0_core.shape[2], f0_core.shape[2]

    num_cells = p.mesh.num_cells()
    est_f = np.zeros(num_cells)
    est_sq = np.zeros(num_cells)
    est_mixed = np.zeros(num_cells)

    # TODO check if this is actually correct
    # define base_product matrix weight and apply to f_tt
    if weight_type is None:
        H_f = [np.eye(d) for d in f_tt.dimensions[1:]]
    elif weight_type == "lognormal":
        H_f = tt.hermite_base_product_operator(
            f_tt.dimensions[1:], z=2, decay=decay,
            s1=vartheta*rho, s2=vartheta*rho, sZeta=vartheta*rho,
            normalized=True)
    Hf_tt_tail = tt.apply_basis_product_operator(f_tt_tail, H_f)

    # compute est_f = || f ||_{L^2(T)}^2
    # NOTE: always assume f is DG0 function
    res_f = tt.inner_TT_stoch(f_tt_tail, Hf_tt_tail, dist)
    # shape (|T|, rf_1)
    res_f = np.dot(f0_core[0], res_f)
    for k1, k2 in itertools.product(range(f1), range(f1)):
        # shape (|T|,)
        est_f += compute_zero_core_rhs(p, f0_core[0, :, k1], res_f[:, k2])

    if p.space.ufl_element().degree() > 1 or p.space_coeff.ufl_element().degree() > 0:
        # NOTE: We have est_r = est_mixed = 0 if u_h is CG1 and a is DG0. In
        #       this case, we skip the explicit computation.

        res = tt.compute_product_coeff_tensor(
            a_tt_tail, u_tt_tail, dist)  # length L
        dims = u_tt.dimensions[1:] + [1]*(a_tt.order()-u_tt.order())
        res = tt.cut(res, dims)

        # TODO check if this is actually correct
        # define base_product matrix weight and apply to f_tt
        if weight_type is None:
            H = [np.eye(d) for d in dims]
        elif weight_type == "lognormal":
            H = tt.hermite_base_product_operator(
                dims, z=2, decay=decay,
                s1=vartheta*rho, s2=vartheta*rho, sZeta=vartheta*rho,
                normalized=True)
        Hres = tt.apply_basis_product_operator(res, H)

        # compute est_sq = || div r_mu ||_{L^2(T)}^2
        # shape (s_1*r_1, s_1*r_1)
        res_sq = tt.inner_TT_stoch(res, Hres, dist)
        res_sq = res_sq.reshape(s1, r1, s1, r1, order="F")
        # new shape (Na, r_1, s_1, r_1)
        res_sq = np.einsum('ik, kabc->iabc', a0_core[0, :, :], res_sq)
        est_det_vol_sq = np.zeros([r1, s1, r1, p.mesh.num_cells()])
        for k1, l2, k2 in itertools.product(range(r1), range(s1), range(r1)):
            sol1, sol2 = u0_core[0, :, k1], u0_core[0, :, k2]  # shape (Nu,)
            # shape (Na,)
            coeff1 = res_sq[:, k1, l2, k2]
            # shape (Na,)
            coeff2 = a0_core[0, :, l2]
            est_det_vol_sq[k1, l2, k2] = compute_zero_core_squared(  # shape (|T|,)
                p, sol1, sol2, coeff1, coeff2)
        est_det_vol_sq = est_det_vol_sq.reshape(  # new shape (r1*s1*r1, |T|)
            (r1*s1*r1, -1), order="F")
        # shape (|T|,)
        est_sq = np.sum(est_det_vol_sq, axis=0)

        # compute est_mixed = 2*< f, div r_mu >_{L^2(T)}
        # shape (rf_1, s_1*r_1)
        res_mixed = tt.inner_TT_stoch(f_tt_tail, Hres, dist)
        res_mixed = res_mixed.reshape((f1, s1, r1), order="F")
        # new shape (|T|, s_1, r_1)
        res_mixed = np.einsum("jf,fsr->jsr", f0_core[0, :, :], res_mixed)
        est_det_vol_mixed = np.zeros([s1, r1, p.mesh.num_cells()])
        for l1, k1 in itertools.product(range(s1), range(r1)):
            # shape (Nu,)
            sol1 = u0_core[0, :, k1]
            # shape (Na,)
            coeff1 = a0_core[0, :, l1]
            # shape (|T|,)
            rhs1 = res_mixed[:, l1, k1]
            est_det_vol_mixed[l1, k1] = compute_zero_core_mixed(  # shape (|T|,)
                p, rhs1, sol1, coeff1)
        # new shape (s1*r1, |T|)
        est_det_vol_mixed = est_det_vol_mixed.reshape(s1*r1, -1, order="F")
        est_mixed = np.sum(est_det_vol_mixed, axis=0)  # shape (|T|,)

    return est_f + est_sq + est_mixed


def compute_zero_core_rhs(p, rhs_1, rhs_2):
    """ Compute the mixed volume terms est_f = || f ||_{T}^2.

    Parameters
    ----------
    p : problem class
    rhs1 : array_like with shape (p.mesh.num_cells(),)
    rhs2 : array_like with shape (p.mesh.num_cells(),)
    """

    mesh = p.mesh
    fem_deg = p.space.ufl_element().degree()  # degree of FE space of solution
    assert fem_deg > 0
    DG0_FS = FunctionSpace(mesh, "DG", 0)  # P_0(Tcal)
    h = CellDiameter(mesh)
    dg0_tf = TestFunction(DG0_FS)  # P_0 test function

    f1, f2 = Function(DG0_FS), Function(DG0_FS)

    f1.vector().set_local(rhs_1)
    f2.vector().set_local(rhs_2)

    vec = h**2 * fem_deg**(-2) * inner(f1, f2) * dg0_tf * dx
    vec = assemble(vec, form_compiler_parameters={'quadrature_degree': -1})
    vec = vec.get_local()

    return vec


def compute_zero_core_mixed(p, rhs, sol, coeff):
    """ Compute mixed volume terms.

    The mixed volume terms read
                2 int_T delta_{mu,0} f(x) div r_mu(x) dx.

    Parameters
    ----------
    p : problem class
    rhs : array_like with shape (p.mesh.num_cells(),)
    sol : array_like with shape (p.space.dim(),)
    coeff : array_like with shape (p.space_coeff.dim(),)
    """

    V, W = p.space, p.space_coeff
    mesh = p.mesh
    fem_deg = V.ufl_element().degree()  # degree of FE space of solution
    assert fem_deg > 0
    DG0_FS = FunctionSpace(mesh, "DG", 0)  # P_0(Tcal)
    h = CellDiameter(mesh)
    dg0_tf = TestFunction(DG0_FS)  # P_0 test function

    u = Function(V)
    a = Function(W)
    f = Function(DG0_FS)

    u.vector().set_local(sol)
    a.vector().set_local(coeff)
    f.vector().set_local(rhs)

    vec = 2*h**2 * fem_deg**(-2) * inner(f, div(a*grad(u))) * dg0_tf * dx
    vec = assemble(vec, form_compiler_parameters={'quadrature_degree': -1})
    vec = vec.get_local()

    return vec


def compute_zero_core_squared(p, sol1, sol2, coeff1, coeff2):
    """ Compute squared volume terms.

    The squared volume terms are || div r_mu ||_{T}^2.

    Parameters
    ---------.
    p : problem class
    sol1 : array_like with shape (p.space.dim(),)
    sol2 : array_like with shape (p.space.dim(),)
    coeff1 : array_like with shape (p.space_coeff.dim(),)
    coeff2 : array_like with shape (p.space_coeff.dim(),)
    """

    V, W = p.space, p.space_coeff
    mesh = p.mesh
    fem_deg = V.ufl_element().degree()  # degree of FE space of solution
    assert fem_deg > 0
    DG0_FS = FunctionSpace(mesh, "DG", 0)  # P_0(Tcal)
    h = CellDiameter(mesh)
    dg0_tf = TestFunction(DG0_FS)  # P_0 test function

    u1, u2 = Function(V), Function(V)
    a1, a2 = Function(W), Function(W)

    u1.vector().set_local(sol1)
    u2.vector().set_local(sol2)
    a1.vector().set_local(coeff1)
    a2.vector().set_local(coeff2)

    vec = h**2 * fem_deg**(-2) * inner(div(a1*grad(u1)),
                                       div(a2*grad(u2))) * dg0_tf * dx
    vec = assemble(vec, form_compiler_parameters={'quadrature_degree': -1})
    vec = vec.get_local()

    return vec


def compute_physical_estimator_jump_terms(a_tt, u_tt, p):
    """ Compute deterministic estimator jump parts.

    The jump parts read
        est_{det,E}(u_h)^2 = h_E || [R_Lambda(u_h)]_E ||_E^2,  for all E.

    Parameters
    ----------
    a_tt : TTTensor
        Diffusion coefficient a in TTTensor format.
    u_tt : TTTensor
        Discrete solution u_h in TTTensor format.
    p : problem class
    """
    # Assume
    # u_tt
    #     ranks: (1, r_1, ..., r_M, 1)
    #     dimensions: (|N|, d_1, ..., d_M)
    # a_tt
    #     ranks: (1, s_1, ..., s_M, ..., s_L, 1)
    #     dimensions: (|T|, q_1, ..., q_M, ..., q_L)

    dist = p.info["sampling"]["distribution"]

    weight_type = None
    if dist == "normal" and p.info["sampling"]["expfield"]:
        weight_type = "lognormal"
        decay = p.info["expansion"]["decay rate"]
        rho = p.info["expansion"]["rho"]
        vartheta = p.info["expansion"]["vartheta"]

    a0_core = tt.convert(a_tt)[0]  # shape (1, |T|, s1)
    u0_core = tt.convert(u_tt)[0]  # shape (1, |N|, r1)
    a_tt_tail = tt.convert(a_tt)[1:]
    u_tt_tail = tt.convert(u_tt)[1:]

    r1, s1 = u0_core.shape[2], a0_core.shape[2]
    num_cells = p.mesh.num_cells()

    res = tt.compute_product_coeff_tensor(a_tt_tail, u_tt_tail, dist)  # len L
    dims = u_tt.dimensions[1:] + [1]*(a_tt.order()-u_tt.order())
    res = tt.cut(res, dims)

    # TODO check if this is actually correct
    # define base_product matrix weight and apply to f_tt
    if weight_type is None:
        H = [np.eye(d) for d in dims]
    elif weight_type == "lognormal":
        H = tt.hermite_base_product_operator(
            dims, z=2, decay=decay,
            s1=vartheta*rho, s2=vartheta*rho, sZeta=vartheta*rho,
            normalized=True)
    Hres = tt.apply_basis_product_operator(res, H)

    # compute (res, res)_L^2
    # shape (s_1*r_1, s_1*r_1)
    res = tt.inner_TT_stoch(res, Hres, dist)
    res = res.reshape(s1, r1, s1, r1, order="F")
    # new shape (|T|, r_1, s_1, r_1)
    res = np.einsum('ik, kabc->iabc', a0_core[0, :, :], res)

    est_det_jump = np.empty([r1, s1, r1, num_cells])
    for k1, l2, k2 in itertools.product(range(r1), range(s1), range(r1)):
        sol1, sol2 = u0_core[0, :, k1], u0_core[0, :, k2]  # shape (|N|,)
        # shape (|T|,)
        coeff1 = res[:, k1, l2, k2]
        # shape (|T|,)
        coeff2 = a0_core[0, :, l2]
        est_det_jump[k1, l2, k2] = compute_zero_core_jumps(  # shape (|T|,)
            p, sol1, sol2, coeff1, coeff2)
    est_det_jump = est_det_jump.reshape(  # new shape (r1*s1*r1, |T|)
        r1*s1*r1, num_cells, order="F")
    return np.sum(est_det_jump, axis=0)  # shape (|T|,)


def compute_zero_core_jumps(p, sol1, sol2, coeff1, coeff2):
    """ Compute jump terms of FE cores.

    Compute the jump terms
            int_E 2 h_E [a1 grad(u1)]_E [a2 grad(u2)]_E dS.

    Parameters
    ----------
    p : problem class
    sol1 : array_like with shape (p.space.dim(),)
    sol2 : array_like with shape (p.space.dim(),)
    coeff1 : array_like with shape (p.space_coeff.dim(),)
    coeff2 : array_like with shape (p.space_coeff.dim(),)
    """

    V, W = p.space, p.space_coeff
    mesh = p.mesh
    fem_deg = V.ufl_element().degree()  # degree of FE space of solution
    assert fem_deg > 0
    DG0_FS = FunctionSpace(mesh, "DG", 0)  # P_0(Tcal)
    h = CellDiameter(mesh)
    nu = FacetNormal(mesh)
    dg0_tf = TestFunction(DG0_FS)  # P_0 test function

    u1, u2 = Function(V), Function(V)
    a1, a2 = Function(W), Function(W)

    u1.vector().set_local(sol1)
    u2.vector().set_local(sol2)
    a1.vector().set_local(coeff1)
    a2.vector().set_local(coeff2)

    R_dT = inner(jump(a1*grad(u1), nu), jump(a2*grad(u2), nu))
    vec = 2 * avg(h) * fem_deg**(-1) * R_dT * avg(dg0_tf) * dS
    vec = assemble(vec, form_compiler_parameters={'quadrature_degree': -1})
    vec = vec.get_local()

    return vec
