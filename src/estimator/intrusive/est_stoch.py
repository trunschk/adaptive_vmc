import numpy as np
import itertools

from fenics import *
import xerus as xe

from .context import src  # add src to python path and import src
import src.utils.tt_tools as tt


def compute_stochastic_estimator_terms(a_tt, u_tt, p, t=1):
    """ Compute the stochastic estimator parts

    Stochastic estimator parts read
        est_{sto,j}(u_h)^2 = || sum_{mu in Xi_{j}} r_mu(u_h) ||_D^2,
    for Xi_{j} boundary index set for j-th mode, i.e.
          Xi_{j} = [d1] x ... x [d_{j-1}] x [d_{j}+1, d_{j}+t] x [d_{j+1}] x ... x [d_M], j = 1, ..., M
        Xi_{M+1} = [d1] x ... x [d_M] x [t]
    where t in {1,2,3,...} is the look-ahead.

    Parameters
    ----------
    a_tt : xe.TTTensor
        Diffusion coefficient a in TTTensor format.
    u_tt : xe.TTTensor
        Discrete solution u_h in TTTensor format.
    p : problem class
    t : int
        Look-ahead.
    """
    # Assume
    # u_tt
    #     ranks: (1, r_1, ..., r_M, 1)
    #     dimensions: (Nu, d_1, ..., d_M)
    # a_tt
    #     ranks: (1, s_1, ..., s_M, ..., s_L, 1)
    #     dimensions: (Na, q_1, ..., q_M, ..., q_L)

    dist = p.info["sampling"]["distribution"]
    a0_core = tt.convert(a_tt)[0]  # shape (1, Na, s1)
    u0_core = tt.convert(u_tt)[0]  # shape (1, Nu, r1)
    a_tt_tail = tt.convert(a_tt)[1:]
    u_tt_tail = tt.convert(u_tt)[1:]

    assert t <= len(a_tt_tail)
    for core in a_tt_tail:
        assert t <= core.shape[1]

    est_stoch = np.zeros(len(u_tt_tail)+min(t, a_tt.order()-u_tt.order()))

    res = tt.compute_product_coeff_tensor(a_tt_tail, u_tt_tail, dist)  # len L

    # set dims = [d_1, ..., d_M, 1, ..., 1]
    dims = u_tt.dimensions[1:] + [1]*(a_tt.order()-u_tt.order())
    res_inner = tt.cut(res, dims)
    res_outer, total, padded_inner = create_outer_tt_own_sum(res_inner, res)

    for j in range(len(est_stoch)):
        # For lookahead t, j = 1, ..., M+t and d_j = 1 if j > M, define the
        # boundary set Xi_j by
        #   Xi_j = [d_1] x ... x [d_{j-1}] x [d_j+t] x [d_{j+1}] x ... x [d_{M+t}]
        dims = [1]*len(res_outer)
        for k, core in enumerate(u_tt_tail):
            dims[k] = core.shape[1]
        dims[j] = dims[j] + t
        res_j = tt.cut(res_outer, dims)
        est_stoch[j] = helper_stochastic_estimator_terms(
            res_j, u0_core, a0_core, p)

    return est_stoch  # shape (M+t,)


def helper_stochastic_estimator_terms(res, sol, coeff, p):
    """ Utility function for deterministic integration of stochastic parts.

    Helper function to call computation of physical integral of stochastic
    estimator part for sliced residual, i.e. different boundary sets Xi_j.
    """
    # TODO: this could be done more efficiently since for different sets Xi_j,
    #       the same products are comouted
    dist = p.info["sampling"]["distribution"]
    r1, s1 = sol.shape[2], coeff.shape[2]

    weight_type = None
    if dist == "normal" and p.info["sampling"]["expfield"]:
        weight_type = "lognormal"
        decay = p.info["expansion"]["decay rate"]
        rho = p.info["expansion"]["rho"]
        vartheta = p.info["expansion"]["vartheta"]

    # TODO check if this is actually correct
    # define base_product matrix weight and apply to f_tt
    res_dims = [core.shape[1] for core in res]
    if weight_type is None:
        H = [np.eye(d) for d in res_dims]
    elif weight_type == "lognormal":
        H = tt.hermite_base_product_operator(
            res_dims, z=2, decay=decay,
            s1=vartheta*rho, s2=vartheta*rho, sZeta=vartheta*rho,
            normalized=True)
    Hres = tt.apply_basis_product_operator(res, H)

    # compute (res, res)_L^2
    # shape (s_1*r_1, s_1*r_1)
    res = tt.inner_TT_stoch(res, Hres, dist)
    res = res.reshape(s1, r1, s1, r1, order="F")
    # new shape (|T|, r_1, s_1, r_1)
    res = np.einsum('ik, kabc->iabc', coeff[0, :, :], res)

    est_stoch = np.empty([r1, s1, r1])
    for k1, l2, k2 in itertools.product(range(r1), range(s1), range(r1)):
        sol1, sol2 = sol[0, :, k1], sol[0, :, k2]  # shape (|N|,)
        coeff1 = res[:, k1, l2, k2]  # shape (|T|,)
        coeff2 = coeff[0, :, l2]  # shape (|T|,)
        est_stoch[k1, l2, k2] = compute_zero_core_stoch(  # shape (1,)
            p, sol1, sol2, coeff1, coeff2)
    return np.sum(est_stoch)  # shape (1,)


def compute_zero_core_stoch(p, sol1, sol2, coeff1, coeff2):
    """ Compute the volume terms int_D inner( a1 grad(u1), a2 grad(u2) ) dx.

    Parameters
    ----------
    p : problem class
    sol1 : array_like with shape (p.space.dim(),)
    sol2 : array_like with shape (p.space.dim(),)
    coeff1 : array_like with shape (p.space.num_cells(),)
    coeff2 : array_like with shape (p.space.num_cells(),)
    """

    V, W = p.space, p.space_coeff
    mesh = p.mesh

    u1, u2 = Function(V), Function(V)
    a1, a2 = Function(W), Function(W)

    u1.vector().set_local(sol1)
    u2.vector().set_local(sol2)
    a1.vector().set_local(coeff1)
    a2.vector().set_local(coeff2)

    vec = inner(a1*grad(u1), a2*grad(u2)) * dx
    vec = assemble(vec, form_compiler_parameters={'quadrature_degree': -1})
    return vec


def create_outer_tt_own_sum(inner, total):
    """ Legacy code from Manuel for sum of two TTTensors.

    Parameters
    ----------
    inner : list of xerus.TTTensor.get_component() or numpy.array
        TT-core list on active set padded with zeros.
    total : list of xerus.TTTensor.get_component() or numpy.array
        TT-core list of total set.
    """

    padded_inner = []
    for _lia in range(len(total) - 1, -1, -1):  # for each (large) component
        # NOTE pad inner core by zero in components [:, d_l:(d_l+q_l), :]
        component = np.zeros(
            (inner[_lia].shape[0], total[_lia].shape[1], inner[_lia].shape[2]))
        component[:, :inner[_lia].shape[1], :] = inner[_lia]
        # shape (r_l, d_l+q_l, r_{m+1})
        padded_inner.append(component)
    padded_inner = list(reversed(padded_inner))

    # NOTE padded_inner should have same length as total, last core should be
    #      shape (1,1,1)
    assert (len(padded_inner) == len(total))
    for j, core in enumerate(padded_inner):
        assert core.shape[1] == total[j].shape[1]

    return own_tt_sum(total, padded_inner), total, padded_inner


def own_tt_sum(ten1, ten2):
    """ Legacy code from Manuel for sum of two TTTensors.

    Required for create_outer_tt_own_sum().
    """
    # Assume k-th cores have shape (k=1,...,M)
    #     (r1_k, q_k, r1_{k+1}) for ten1
    #     (r2_k, q_k, r2_{k+1}) for ten2

    _retval = []
    assert len(ten1) == len(ten2)
    p = -1 if len(ten1) % 2 == 0 else 1  # don't ask, this seems to work...
    core = None

    for k in range(len(ten1)):
        if k == len(ten1) - 1:  # NOTE last core
            core = np.zeros(  # shape (r2_M+r1_M, q_M, 1)
                (ten1[k].shape[0]+ten2[k].shape[0], ten1[k].shape[1], 1))
            # fill the first r2_M rows of core
            core[:ten1[k].shape[0], :, :] = ten1[k]
            # fill rows r2_M to r2_M+r1_M with -1*ten2
            core[ten1[k].shape[0]:, :, :] = -ten2[k]
            if len(ten1) > 1:
                _retval.append(core)
                continue

        if len(ten1) > 1:  # NOTE intermediate cores
            core = np.zeros((  # shape (r2_k+r1_k, q_k, r2_{k+1}+r1_{k+1})
                ten1[k].shape[0] + ten2[k].shape[0],
                ten1[k].shape[1],
                ten1[k].shape[2] + ten2[k].shape[2]
            ))
            # fill first r2_k rows with ten1-core
            core[:ten1[k].shape[0], :, :ten1[k].shape[2]] = ten1[k]
            # fill rows r2_k to r2_k+r1_k with -1*ten2-core
            core[ten1[k].shape[0]:, :, ten1[k].shape[2]:] = -ten2[k]

        if k == 0:  # NOTE first core, only here actual addition happens
            # shape (n, r2_1+r1_1) for n = ceil( (r2_1+r1_1) / 2 )
            core_of_ones = np.zeros(
                (int(np.math.ceil(core.shape[0]*0.5)), core.shape[0]))

            # NOTE: this assumes ten1[0].shape[0] == int(np.math.ceil(core.shape[0]*0.5)), i.e.
            #       r1_1 = ceil( (r2_1+r1_1)/2 ) = n   =>   r1_1 = r2_1
            core_of_ones[:, :ten1[0].shape[0]] = np.eye(
                int(np.math.ceil(core.shape[0]*0.5)),
                int(np.math.ceil(core.shape[0]*0.5))
            )
            core_of_ones[:, ten1[0].shape[0]:] = p*np.eye(
                int(np.math.ceil(core.shape[0]*0.5)),
                int(np.math.ceil(core.shape[0]*0.5))
            )
            # NOTE: core_of_ones is [I, p*I] for I identity in n x n

            # NOTE Instead of reshaping, one could use
            #      np.einsum("ij,jkl->ikl", core_of_ones, core)
            _k, _l = core.shape[1], core.shape[2]
            core = np.reshape(core, (core.shape[0], _k * _l), order='C')
            core = np.dot(core_of_ones, core)
            core = np.reshape(core, (core_of_ones.shape[0], _k, _l), order='C')

        _retval.append(core)

    return _retval
