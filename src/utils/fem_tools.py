# standard imports
import numpy as np
import scipy.sparse as sps
import matplotlib.pyplot as plt
from copy import deepcopy

# third party / non-standard imports
from fenics import *
from dolfin import Mesh, UnitSquareMesh
try:
    import mshr
except ModuleNotFoundError:
    print("WARNING: Could not load 'mshr'.")
import xerus as xe

# local imports
from .context import src  # add src to python path and import src
from src.misc import log, transpose, timeit, get_y_samples
from src.problem.equation.darcy import Problem
from src.parallel import Parallel


def sample_solution(info, n_samples=500, samples=None, get_eta=False):
    """ Sample solution of Darcy problem specified in info.

    Sample solution u of darcy problem for different realizations of the
    stochastic variables. Also return the samples and weights used to compute
    the solutions. Possibly also returns samples of the error estimator values
    for the different sample points.

    Paramter:
    ---------
    info        : dict
        Information dictionary for the darcy problem.
    n_samples   : int
        Number of samples that should be used.
    samples     : array like
        If None, new y samples are drawn, otherwise the given samples must have
        shape (n_samples, info['expansion']['size']). Samples of solution and
        estimator are then computed for these samples.
    get_eta     : bool
        If true, return samples of error estimator as well
    """

    ys = get_y_samples(info, n_samples, samples)
    ws = np.full(n_samples, 1/n_samples)  # uniform weights

    problem = Problem(info)
    solutions = Parallel(problem.solution)
    results = solutions(ys)
    us, ys = transpose(results)
    us = np.array(us)
    ys = np.array(ys)
    assert us.shape[0] == n_samples

    if get_eta:
        estimators = Parallel(problem.P1_residual_estimator)
        results = estimators(list(zip(ys, us)))
        etas = np.array(transpose(results)[0])
        assert etas.shape[0] == n_samples
        return ys, ws, us, etas

    return ys, ws, us


def sample_diffusion_coeff(info, n_samples, samples=None):
    """ Sample diffusion coefficient of Darcy problem as specified in info.

    Sample diffusion coefficient a of darcy problem for different realizations
    of the stochastic variables. Also return the samples and weights used to
    compute the solutions.

    Paramter:
    ---------
    info : dict
        Information dictionary for the darcy problem.
    n_samples : int
        Number of samples that should be used.
    samples : array like
        If None, new y samples are drawn, otherwise the given samples must have
        shape (n_samples, info['expansion']['size']). Samples of diffusion
        coefficient are then computed for these samples.
    """

    ys = get_y_samples(info, n_samples, samples)
    ws = np.full(n_samples, 1/n_samples)  # uniform weights

    problem = Problem(info)
    coeffs = Parallel(problem.diffusion_coefficient)
    results = coeffs(ys)
    cs, ys = transpose(results)
    cs = np.array(cs)
    ys = np.array(ys)
    assert cs.shape[0] == n_samples
    return ys, ws, cs


def sample_rhs(info, n_samples, samples=None):
    """ Sample right-hand side of Darcy problem as specified in info.

    Sample right-hand side f of darcy problem for different realizations
    of the stochastic variables. Also return the samples and weights used to
    compute the solutions.

    Paramter:
    ---------
    info        : dict
        Information dictionary for the darcy problem.
    n_samples   : int
        Number of samples that should be used.
    samples     : array like
        If None, new y samples are drawn, otherwise the given samples must have
        shape (n_samples, info['expansion']['size']). Samples of right-hand
        side are then computed for these samples.
    """
    ys = get_y_samples(info, n_samples, samples)
    ws = np.full(n_samples, 1/n_samples)  # uniform weights

    problem = Problem(info)
    rhs = Parallel(problem.rhs)
    results = rhs(ys)
    fs, ys = transpose(results)
    fs = np.array(fs)
    ys = np.array(ys)
    assert fs.shape[0] == n_samples
    return ys, ws, fs


def interpolation_matrix(info_coarse, info_fine):
    """ Compute interpolation matrx from coarse to fine FE mesh.
    """
    u_coarse = Function(Problem(info_coarse).space)
    u_fine = Function(Problem(info_fine).space)
    op_shape = (u_fine.vector().size(), u_coarse.vector().size())
    # TODO just transpose if this is not the case ...
    assert op_shape[0] >= op_shape[1]
    data = []
    rows = []
    cols = []
    for j in range(op_shape[1]):
        e = np.zeros(Problem(info_coarse).space.dim())
        e[j] = 1
        u_coarse.vector().set_local(e)
        u_fine.interpolate(u_coarse)
        arr = u_fine.vector().get_local()
        arr_rows = (abs(arr) > 1e-12).nonzero()[0]
        data.extend(arr[arr_rows])
        rows.extend(arr_rows)
        cols.extend([j]*len(arr_rows))
    op = sps.coo_matrix((data, (rows, cols)), shape=op_shape)
    return op.tocsr()


def compute_interpolation_tensor(dim):
    """ Assemble sparse FEM triple product interpolation tensor.

    Compute the order-3 tensor that interpolates the product of two FE basis
    functions in the same basis, i.e. we approximate
                phi_i(x) * phi_j(x) = sum_{k=1}^N w_{i,j,k} phi_k(x),
    where the weights w_{i,j,k} are chosen such that the interpolation in the
    dofs is exact. For DG-0 and CG-p (p > 0) elements we have
                w_{i,j,k} = delta_{i,j,k}
    for Kronecker delta.

    Parameters
    ----------
    dim : int
        Dimension of FE-space.
    """

    return xe.Tensor.kronecker([dim, dim, dim])


def mass_matrix(space):
    """ Compute mass matrx of FE space.
    """
    lab = parameters['linear_algebra_backend']
    parameters['linear_algebra_backend'] = "Eigen"

    u = TrialFunction(space)
    v = TestFunction(space)
    mass = inner(u, v) * dx
    mass = assemble(mass)

    M = sps.csr_matrix(as_backend_type(mass).sparray())
    parameters['linear_algebra_backend'] = lab
    return M


def stiffness_matrix(space, bc=None):
    """ Compute stiffness matrx of FE space.
    """
    lab = parameters['linear_algebra_backend']
    parameters['linear_algebra_backend'] = "Eigen"
    u = TrialFunction(space)
    v = TestFunction(space)
    stiffness = inner(grad(u), grad(v)) * dx
    stiffness = assemble(stiffness)
    if bc is not None:
        bc.apply(stiffness)

    S = sps.csr_matrix(as_backend_type(stiffness).sparray())
    parameters['linear_algebra_backend'] = lab
    return S


def prolongate_x(u_vec, info_coarse, info_fine):
    """ Prolongate coefficient vector from coarse to refined mesh.
    """
    # TODO do this only for one vector and not for an entire array?
    interp = interpolation_matrix(info_coarse, info_fine)
    assert interp.shape[0] >= interp.shape[1]
    assert u_vec.shape[1] == interp.shape[1]
    return u_vec @ interp.T  # nc,cf -> nf


def prolongate_x_2(u_vec, coarse_space, fine_space):
    """ Prolongate coefficient vector from coarse to refined mesh.

    NOTE: This uses fenics interpolation instead of assembling an interpolation
    matrix explicitly (as in prolongate_x()). Same result, but this version is
    faster for relatively low amount of samples with a large FE space
    dimension.
    """
    u1 = Function(coarse_space)
    interp = np.zeros([u_vec.shape[0], fine_space.dim()])
    for i, val in enumerate(u_vec):
        u1.vector().set_local(val)
        u2 = interpolate(u1, fine_space)
        interp[i] = u2.vector().get_local()
    return interp


def marking(vec_est, theta):
    """ Doerfler marking.
    """
    order = np.argsort(-vec_est)  # descending order
    vec_est = vec_est[order]
    assert np.all(vec_est[:-1] >= vec_est[1:])
    vec_est = np.cumsum(vec_est)
    idx = min(np.searchsorted(vec_est, theta*vec_est[-1])+1, len(vec_est))
    assert 1 <= idx <= len(vec_est)
    return order[:idx]


def generate_mesh(geometry, initial_refinement=2):
    """ Generate mesh for specified geometry.

    Parameters
    ----------
    geometry : str, ("UnitSquare", "Lshape")
        Description of computational domain.
    initial_refinement : int, default=2
        Number of initial refinements of the triangulation.

    Returns
    -------
    mesh : dolfin.cpp.mesh.Mesh
        Mesh for domain.
    """

    if geometry == "UnitSquare":
        # mesh = Mesh(UnitSquareMesh(initial_refinement, initial_refinement))
        square = mshr.Rectangle(Point(0, 0), Point(1, 1))
        mesh = mshr.generate_mesh(square, initial_refinement)
    elif geometry == "Lshape":
        big_square = mshr.Rectangle(Point(0, 0), Point(1, 1))
        small_square = mshr.Rectangle(Point(0.5, 0.5), Point(1, 1))
        l_shape = big_square - small_square
        # NOTE: mshr.generate_mesh() does not yield right isosceles triangles.
        mesh = mshr.generate_mesh(l_shape, initial_refinement)
    else:
        raise NotImplementedError(f"Unknown geometry '{geometry}'")

    return mesh


def refine_mesh(marked_cells, info):
    """ Refine physical mesh of problem.
    """
    local_info = deepcopy(info)
    local_info["expansion"]["size"] = 1
    problem = Problem(local_info)
    problem.refine_mesh(marked_cells)
    info['fe']['mesh'] = problem.mesh
    return info


def triangle_centers(mesh):
    """ Return centers of gravity (midpoints) of triangles in mesh.
    """
    coordinates = mesh.coordinates()
    cells = mesh.cells()
    mid = np.sum(coordinates[cells], axis=1)/3
    return mid


def dof_coords(space, mesh):
    """ Return coordinates of the degrees of freedom of a FE space.
    """
    ret = np.zeros([space.dim(), 2])
    element = space.element()
    dofmap = space.dofmap()
    for cell in cells(mesh):
        dofs = dofmap.cell_dofs(cell.index())
        coords = element.tabulate_dof_coordinates(cell)
        ret[dofs] = coords
    return ret


def eval_fencis_function(x, coeff, space):
    """ Evaluate FE-function coefficients.

    Evaluate a fenics function given by it`s coefficients (and space) at the
    coordinates x.

    Parameter
    ---------
    x     : array like of shape (#points, 2)
        Coordinates of points the function is evaluated in
    coeff : array like of shape (space.dim(),)
        Coefficient vector of function w.r.t. space
    space : fenics FunctionSpace
        Finite element space
    """
    if x.ndim == 1:
        x.shape = 1, -1
    assert x.ndim == 2
    assert coeff.ndim == 1 and coeff.shape[0] == space.dim()
    f = Function(space)
    f.vector().set_local(coeff)
    ret = np.zeros(x.shape[0])
    for j, xj in enumerate(x):
        ret[j] = f(xj)
    if x.shape[0] == 1:
        ret = ret[0]
    return ret


def plot_fenics_function(axis, val, space, mesh=None, cmap=None, cbar=True):
    """ Plot FE-function with fenics plot.

    Plot a fenics function in specified axis of figure.
    NOTE: Fenics plot() cannot display DG-p functions for p > 0.

    Parameter
    ---------
    axis : pyplot axis
        Axis the function is displayed in.
    val : array like
        Coefficients of function w.r.t. space.
    space : fenics FunctionSpace
        Finite element space.
    mesh : fenics Mesh
        Underlying triangulation. If None (default), mesh is not displayed.
    cmap : matplotlib color map
        Colormap for plot. Default is viridis.
    cbar : bool, default = True
        Plot color bar.
    """
    u = Function(space)
    u.vector().set_local(val)

    plt.sca(axis)
    if cmap is not None:
        plt.set_cmap(cmap)
    im = plot(u)
    if mesh is not None:
        plot(mesh)
    if cbar is True:
        plt.colorbar(im)
    if cmap is not None:
        plt.set_cmap("viridis")
    return None
