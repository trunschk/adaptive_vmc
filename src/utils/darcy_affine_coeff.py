""" Evaluation and TT representation of affine diffusion coefficient. """

import numpy as np
from scipy.special import zeta

import xerus as xe

import src.utils.fem_tools as fem
import src.utils.tt_tools as tt


def _k(m):
    """ Frequency k(m) = floor{ -1/2  + \\sqrt(1/4 + 2*m) }.

    Parameters
    ----------
    m : int
        Expansion coefficient index.
    """
    return np.floor(-0.5 + np.sqrt(0.25 + 2*m))


def _beta1(m):
    """ Coefficient $\\beta_1(m) = m - k(m) [k(m)+1] / 2$.

    Parameters
    ----------
    m : int
        Expansion coefficient index.
    """
    return m - _k(m) * (_k(m)+1) / 2


def _beta2(m):
    """ Coefficient $\\beta_2(m) = k(m) - \\beta1(m)$.

    Parameters
    ----------
    m : int
        Expansion coefficient index.
    """
    return _k(m) - _beta1(m)


def _diff_coeff_0(x, const):
    """ Deterministic coefficient a_0(x) = const.

    Parameters
    -------
    x : array_like of shape (..., 2)
        Spatial points the coefficients are evaluated in.
    const : float
        Constant value of 0th expansion coefficient.
    """
    assert x.shape[-1] == 2
    return const * np.ones(x.shape[0])


def _diff_coeff_m(x, m, decay):
    """ Deterministic coefficient a_m(x).

    Compute the deterministic coefficient

        a_m(x) = c m^{-decay} cos(2*pi*beta_1(m) x_1) cos(2*pi*beta_2(m) x_2)

    where c = 0.9 / zeta(decay) for Riemann zeta function.

    Parameters
    -------
    x : array_like of shape (..., 2)
        Spatial points the coefficients are evaluated in.
    m : int
        Number of expansion coefficient.
    decay : float
        Decay of affine coefficient.
    """
    assert m > 0  # only for m = 1, ..., M
    gamma = 0.9
    c = gamma / zeta(decay)
    cos1 = np.cos(2 * np.pi * _beta1(m) * x[:, 0])
    cos2 = np.cos(2 * np.pi * _beta2(m) * x[:, 1])
    return c * m**(-decay) * cos1 * cos2


def _eval_diff_coeff_spatial(x, M, decay, mean):
    """ Assemble spatial core for exact TT representation.

    The first core is given by
        core_0 = [a_0(x), a_1(x), ..., a_M(x)]
    where x are supposed to be the FE dof coordinates of the spatial domain.

    Parameters
    ----------
    x : array_like of shape (..., 2)
        Spatial points the coefficients are evaluated in.
    M : int
        Expansion length.
    decay : float
        Decay of affine coefficient.
    mean : float
        Constant value of 0th expansion coefficient.

    Returns
    -------
    ret : array_like of shape (space.dim(), M+1)
        Deterministic coefficients [a_0(x), ... , a_M(x)] in DoFs.
    """
    ret = np.zeros([x.shape[0], M+1])
    ret[:, 0] = _diff_coeff_0(x, mean)
    for m in range(1, M+1):
        ret[:, m] = _diff_coeff_m(x, m, decay)
    return ret


def _assemble_stoch_core_m(m, dim, M, dist):
    """ Assemble mth (m < M) stochastic core of affine diffusion coefficient.

    Compute the stochastic core w.r.t. normalized Legendre or Hermite basis.
    Stochastic core has shape

        core[j, :, j] = (1, 0, ...)              for m != j = 1, ..., M
        core[m, :, m] = (0, c, 0, ...)
        core[j, :, k] = (0, 0, ...)              for j != k = 1, ..., M

    for normalization constant c = 1 for normalized Hermite polynomials and
    c = 1 / sqrt(3) for normalized Legendre polynomials.

    Parameters
    ----------
    m : int
        Core number.
    dim : int > 2
        Dimension of stochastic core.
    M : int
        Expansion length of affine diffusion coefficient.
    dist : str, ('uniform' or 'normal')
        Distribution of stochastic parameters for expansion basis.
    """
    assert dim >= 2
    assert 0 < m < M
    ret = np.zeros((M+1, dim, M+1))
    assert dist in ["uniform", "normal"]
    norm = 1 if dist == "normal" else 1/np.sqrt(3)
    for j in range(M+1):
        if j == m:
            ret[j, :, j] = norm * np.eye(1, dim, k=1)[0]  # (0, c, 0, ...)
        else:
            ret[j, :, j] = np.eye(1, dim, k=0)[0]  # (1, 0, 0, ...)
    return ret


def _assemble_stoch_core_last(dim, M, dist):
    """ Assemble Mth (last) stochastic core of affine diffusion coefficient.

    Compute the stochastic core w.r.t. normalized Legendre or Hermite basis.
    Stochastic core has the shape

        core[j, :, 1] = (1, 0, ...)         for j = 1, ..., M-1
        core[M, :, 1] = (0, c, 0, ...)

    for normalization constant c = 1 for normalized Hermite polynomials and
    c = 1 / sqrt(3) for normalized Legendre polynomials.

    Parameters
    ----------
    dim : int > 2
        Dimension of stochastic core.
    M : int
        Expansion length of affine diffusion coefficient.
    dist : str, ('uniform' or 'normal')
        Distribution of stochastic parameters for expansion basis.
    """
    assert dim >= 2
    ret = np.zeros((M+1, dim, 1))
    assert dist in ["uniform", "normal"]
    norm = 1 if dist == "normal" else 1/np.sqrt(3)
    for j in range(M+1):
        if j < M:
            ret[j, :, 0] = np.eye(1, dim, k=0)[0]  # (1, 0, 0, ...)
        else:
            ret[j, :, 0] = norm * np.eye(1, dim, k=1)[0]  # (0, c, 0, ...)
    return ret


def eval_diff_coeff(x, y, decay, mean):
    """ Evaluate a(x,y) = a_0(x) + sum_{m=1}^M a_m(x) y_m.

    Parameters
    ----------
    x : array_like of shape (..., 2)
        Spatial points the coefficients are evaluated in.
    y : array_like of shape (..., M)
        Stochastic points the coefficients are evaluated in.
    decay : float
        Decay of affine coefficient.
    mean : float
        Constant value of 0th expansion coefficient.

    Returns
    -------
    ret : array_like of shape (y.shape[0], x.shape[0])
    """
    M = y.shape[1]
    val_det = _eval_diff_coeff_spatial(x, M, decay, mean)
    val_stoch = np.concatenate([np.ones([y.shape[0], 1]), y], axis=1)
    ret = np.einsum("im,jm->ij", val_stoch, val_det)
    return ret


def exact_TT_diff_coeff(space, mesh, M, decay, mean, dist, dim=2):
    """ Build exact TT representation of affine diffusion coefficient.

    The exact TT representation is build in the following way:

        1. To obtain the values of the FE expansion coefficients (0th core),
           the spatial coefficient a_m(x) (m=0, ..., M) are evaluated in the
           Lagrange points of the FE space. The first core then is

                [a_0(x), a_1(x), ..., a_M(x)]   with shape (space.dim(), M+1).

        2. The stochastic cores for m=1, ..., M-1 have the shape

                       | 1                       |
                       |   ...                   |
                       |       1                 |
              core_m = |          y_m            |  with shape (M+1, 2, M+1).
                       |               1         |
                       |                  ...    |
                       |                       1 |

           Here 1 = P_0(y_m) and y_m = c*P_1(y_m) can be expanded into
           polynomials and represented by the coefficients

                    1 = (1, 0)    and    y_m = (0, c).

            The normalization constant is c = 1 for normalized Hermite
            polynomials and c = 1 / sqrt(3) for normalized Legendre
            polynomials.

        3. Similar to 2., the last stochastic core has the shape

                       |  1  |
              core_m = | ... |  with shape (M+1, 2, 1).
                       |  1  |
                       | y_M |

    Parameters
    ----------
    space : fenics.FunctionSpace
        Finite element space.
    mesh : fenics.Mesh
        Finite element mesh.
    M : int
        Expansion length.
    decay : float
        Decay of expansion coefficients.
    mean : float
        Mean of affine diffusion coefficient.
    dist : str, ('uniform' or 'normal')
        Distribution of stochastic parameters for expansion basis.
    dim : int, default=2
        Stochastic dimensions.

    Returns
    -------
    ret : xe.TTTensor
    """

    dofs = fem.dof_coords(space, mesh)
    val_det = _eval_diff_coeff_spatial(dofs, M, decay, mean)
    cores = [np.expand_dims(val_det, 0)]
    for m in range(1, M+1):
        if m < M:
            core = _assemble_stoch_core_m(m, dim, M, dist)
        else:
            core = _assemble_stoch_core_last(dim, M, dist)
        cores.append(core)
    return tt.convert(cores)
