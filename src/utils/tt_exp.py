import numpy as np
import xerus as xe
import itertools

from .context import src  # add src to python path and import src
from src.misc import log, leg_triple_product, herm_triple_product, factorial
import src.utils.tt_tools as tt
import src.utils.fem_tools as fem


class InternalSolver:
    def __init__(self, A, x, b):
        """
        Standard ALS copied from xerus examples page. (only added different
        verbosity levels.)
        """
        self.A = A
        self.b = b
        self.x = x
        self.d = x.order()
        self.solutionsNorm = b.frob_norm()

        self.leftAStack = [xe.Tensor.ones([1, 1, 1])]
        self.leftBStack = [xe.Tensor.ones([1, 1])]
        self.rightAStack = [xe.Tensor.ones([1, 1, 1])]
        self.rightBStack = [xe.Tensor.ones([1, 1])]

        self.maxIterations = 1000

    def push_left_stack(self, pos):
        i1, i2, i3, j1, j2, j3, k1, k2 = xe.indices(8)
        Ai = self.A.get_component(pos)
        xi = self.x.get_component(pos)
        bi = self.b.get_component(pos)

        tmpA = xe.Tensor()
        tmpB = xe.Tensor()
        tmpA(i1, i2, i3) << self.leftAStack[-1](j1, j2, j3)\
            * xi(j1, k1, i1)*Ai(j2, k1, k2, i2)*xi(j3, k2, i3)
        self.leftAStack.append(tmpA)
        tmpB(i1, i2) << self.leftBStack[-1](j1, j2)\
            * xi(j1, k1, i1)*bi(j2, k1, i2)
        self.leftBStack.append(tmpB)

    def push_right_stack(self, pos):
        i1, i2, i3, j1, j2, j3, k1, k2 = xe.indices(8)
        Ai = self.A.get_component(pos)
        xi = self.x.get_component(pos)
        bi = self.b.get_component(pos)

        tmpA = xe.Tensor()
        tmpB = xe.Tensor()
        tmpA(j1, j2, j3) << xi(j1, k1, i1)*Ai(j2, k1, k2, i2)*xi(j3, k2, i3) \
            * self.rightAStack[-1](i1, i2, i3)
        self.rightAStack.append(tmpA)
        tmpB(j1, j2) << xi(j1, k1, i1)*bi(j2, k1, i2) \
            * self.rightBStack[-1](i1, i2)
        self.rightBStack.append(tmpB)

    def calc_residual_norm(self):
        i, j = xe.indices(2)
        ret = xe.TTTensor()
        # ret(i&0) << self.A(i/2, j/2)*self.x(j&0) - self.b(i&0)
        ret(i&0) << self.A(i/2, j/2)*self.x(j&0)
        ret = ret - self.b
        return xe.frob_norm(ret) / self.solutionsNorm

    def solve(self, verbose=False, fmttr=print):
        """
        Run the ALS.

        Parameter
        ---------
        verbose : int
            Verbosity of output print. Supported are 1,2,3.
        fmttr : function
            Formatter of output function (default is print function).
        """
        # build right stack
        self.x.move_core(0, True)
        for pos in reversed(range(1, self.d)):
            self.push_right_stack(pos)

        i1, i2, i3, j1, j2, j3, k1, k2 = xe.indices(8)
        residuals = [np.inf]*10

        for itr in range(self.maxIterations):
            residuals.append(self.calc_residual_norm())
            if residuals[-1] < 1e-8:
                if int(verbose) > 0:
                    fmttr(f"Done! Residual decreased "
                          + f"from [{residuals[10]:4.2e}] "
                          + f"to [{residuals[-1]:4.2e}] "
                          + f"in [{len(residuals)-10}] sweeps")
                return
            # if residuals[-1]/residuals[-6] > 0.99:
            last_res = residuals[-6:-1]
            if residuals[-1] > 0.5*np.mean(last_res):
                if int(verbose) > 0:
                    fmttr(f"Done! Residual decreased "
                          + f"from [{residuals[10]:4.2e}] "
                          + f"to [{residuals[-1]:4.2e}] "
                          + f"in [{len(residuals)-10}] sweeps")
                return

            if int(verbose) > 1:
                fmttr(f"  Iteration: {itr+1}, Residual: {residuals[-1]:4.2e}")

            # sweep left -> right
            for pos in range(self.d):
                op = xe.Tensor()
                rhs = xe.Tensor()

                Ai = self.A.get_component(pos)
                bi = self.b.get_component(pos)

                op(i1, i2, i3, j1, j2, j3) << self.leftAStack[-1](i1, k1, j1) \
                    * Ai(k1, i2, j2, k2) * self.rightAStack[-1](i3, k2, j3)
                rhs(i1, i2, i3) << self.leftBStack[-1](i1, k1) \
                    * bi(k1, i2, k2) * self.rightBStack[-1](i3, k2)

                if int(verbose) > 2:
                    fmttr(f"    op dims:  {op.dimensions}")
                    fmttr(f"    op ranks: {op.dimensions}")
                # XXX
                # l1, l2, d, q, r1, r2 = op.dimensions
                # tmp = np.asarray(op).reshape(l1*l2*d, q*r1*r2)
                # l1, d, q, r1 = Ai.dimensions
                # tmp = np.asarray(Ai).reshape(l1*d, q*r1)
                # print(f"cond for pos {pos}: {np.linalg.cond(tmp):4.2e}")
                # XXX

                tmp = xe.Tensor()
                tmp(i1&0) << rhs(j1&0) / op(j1/2, i1/2)
                self.x.set_component(pos, tmp)

                if pos+1 < self.d:
                    self.x.move_core(pos+1, True)
                    self.push_left_stack(pos)
                    self.rightAStack.pop()
                    self.rightBStack.pop()

            # right -> left, only move core and update stack
            self.x.move_core(0, True)
            for pos in reversed(range(1, self.d)):
                self.push_right_stack(pos)
                self.leftAStack.pop()
                self.leftBStack.pop()


def run_simpleALS(A, u_start, b, verbose=False, fmttr=log):
    """ Run standard SPD ALS. For detail see xerus example page.

    Parameters
    ----------
    A :         xe.TTOperator
    u_start :   xe.TTTensor
    b :         xe.TTTensor
    verbose :   int
        Verbosity level of ALS. Supported are 0,1,2.
    """
    solver = InternalSolver(A, xe.TTTensor(u_start), b)
    solver.solve(verbose, fmttr=fmttr)
    return solver.x


def compute_D(da, dt, normalized=True):
    """ Assemble univariate differentiation operator D.

    Discretized differentiation operator for (normalized, probabilists) Hermite
    polynomials.

    Parameters
    ----------
    da : int
        Dimension of the ansatz space.
    dt : int
        Dimension of the test space.
    normalized : bool
        If False, normalize w.r.t. standard Gaussian in the compuation.
    """
    assert da >= 0 and dt >= 0
    d = max(da, dt)
    if not normalized:
        def c(n): return 1 / np.sqrt(factorial(n))
        norm = np.array([1/c(j) for j in range(d+1)])
    else:
        norm = np.ones(d+1)
    val = np.sqrt(np.arange(1, d+1))*norm[:-1]*norm[1:]
    return np.diag(val, k=1)[:dt, :da]


def compute_H(da, dt, h, use_diff=True, kappa=None, normalized=True):
    """ Assemble univariate multiplication operator H.

    Discretized multiplication operator for (normalized, probabilists) Hermite
    polynomials.

    Parameters
    ----------
    da : int
        Dimension of the ansatz space.
    dt : int
        Dimension of the test space.
    h : np.ndarray
        Core of TTTensor representation of h.
    use_diff : bool
        If True, differentiate h.
    kappa : np.ndarray
        1D triple product tensor. If None (default), it is computed
        automaically.
    normalized : bool
        If False, normalize w.r.t. standard Gaussian in the compuation.
    """
    assert da >= 0 and dt >= 0
    dh = h.shape[1]
    if kappa is None:
        kappa = tt.triple_product_tensor(da, dh, dt, normalized=normalized)
    if use_diff:
        D = compute_D(dh, dh, normalized=normalized)
    else:
        D = np.eye(dh)
    return np.einsum("gn,rns,agt->rtas", D, h, kappa)


def build_B_parts(da, dt, h, kappa=None, normalized=True):
    """ Helper function to assemble operators $B_m$

    Compute the order-4 tensor subcores used to build the operators B_{m}
    for m = 1, ..., M. See paper for more detailed description.

    Parameter
    ---------
    da : int
        Dimensions of ansatz space.
    dt : int
        Dimensions of test space.
    h : numpy array
        Core of TTTensor h.
    kappa : numpy array
        Order-3 numpy array of the triple product. The correct shape of
        kappa is required. If None (default) the triple product of normalized
        standard HermiteE polynomials is used.
    normalized : bool
        Coefficients h given w.r.t. normalized HermiteE polynomials.
    """
    assert h.ndim == 3
    r, dh, l = h.shape
    if kappa is None:
        kappa = tt.triple_product_tensor(da, dh, dt, normalized=normalized)

    D = compute_D(da, dt, normalized=normalized).reshape(1, dt, da, 1)
    # shape (r, dt, da, l)
    H = compute_H(da, dt, h, use_diff=True, kappa=kappa)

    G = np.eye(dt, da).reshape(1, dt, da, 1)
    # shape (r, dt, da, l)
    tildeV = compute_H(da, dt, h, use_diff=False, kappa=kappa)

    return D, H, G, tildeV


def build_B_cores(da, dt, h, normalized=True):
    """
    Build the cores B[m,m] and V[m] given respectively by
        B[1,1] = [[ D[1,1]  -H[1,1] ]]
        B[m,m] = [[ D[m,m]        0 ],
                  [       0  H[m,m] ]]
        B[M,M] = [[ D[M,M] ],
                  [ H[M,M] ]]
    and
        V[1] = [[ G[1]  -tildeV[1] ]]
        V[m] = [[ G[m]         0   ],
                [    0   tildeV[m] ]]
        V[M] = [[ G[M]      ],
                [ tildeV[M] ]].
    NOTE: This only works for 0th component of h = [[[1]]].

    Parameters
    ----------
    da : list of int
        Dimensions of ansatz space.
    dt : list of int
        Dimensions of test space.
    h : TTTensor
        Coefficients of h given in same basis as test space
    normalized : bool
        Coefficients h given w.r.t. normalized HermiteE polynomials.
    """
    h.move_core(1)  # normalize 0th component of h to [[[1]]]
    M = h.order()-1
    dh = h.dimensions[1:]
    assert len(da) == len(dt) == len(dh) == M
    kappa = tt.triple_product_tensor(max(da), max(dh), max(dt))
    B, V = [], []
    h_cores = tt.convert(h)
    assert np.allclose(h_cores[0], np.array([[[1]]]))

    for j, h_core in enumerate(h_cores[1:]):  # drop 0th core, this is FEM dim
        D, H, G, tildeV = build_B_parts(
            da[j], dt[j], h_core, kappa=kappa[:da[j], :dh[j], :dt[j]],
            normalized=normalized
        )

        if M == 1:  # 1D case: return B = [B_{1,1}] and V = [None]
            assert H.shape[0] == H.shape[3] == 1
            Bjj = D - H
            B.append(Bjj)
            V.append(None)
        else:
            # Bjj  ->   B_{j,j} = D_{j,j} - H_{j,j}
            # Vj   ->   V_{j} = D_{m,j} - H_{m,j} = G_j - \hat V_j for any j != m
            # ranks(h) = (r_1, ..., r_{M+1})
            if j == 0:
                # shape (1, d_{t,1}, d_{a,1}, r_{2}+1)
                Bjj = np.concatenate([D, -H], axis=3)
                # shape (1, d_{t,1}, d_{a,1}, r_{2}+1)
                Vj = np.concatenate([G, -tildeV], axis=3)
            elif 0 < j < M-1:
                # shape (1,   d_{t,m}, d_{a,m}, r_{m+1})
                O1 = np.zeros((D.shape[0], dt[j], da[j], H.shape[3]))
                # shape (r_{m}, d_{t,m}, d_{a,m}, 1)
                O2 = np.zeros((H.shape[0], dt[j], da[j], D.shape[3]))
                Bjj = np.concatenate([                                          # shape (r_{m}+1, d_{t,m}, d_{a,m}, r_{m+1}+1)
                    np.concatenate([D,  O1], axis=3),
                    np.concatenate([O2,  H], axis=3)
                ], axis=0)
                Vj = np.concatenate([                                           # shape (r_{m}+1, d_{t,m}, d_{a,m}, r_{m+1}+1)
                    np.concatenate([G,      O1], axis=3),
                    np.concatenate([O2, tildeV], axis=3)
                ], axis=0)
            elif j == M-1:
                # shape (r_{M}+1, d_{t,M}, d_{a,M}, 1)
                Bjj = np.concatenate([D, H], axis=0)
                # shape (r_{M}+1, d_{t,M}, d_{a,M}, 1)
                Vj = np.concatenate([G, tildeV], axis=0)

            B.append(Bjj)
            V.append(Vj)

    return B, V


def build_f_cores(h, normalized=True):
    """
    Build the cores f[m,m] and g[m] given respectively by
        f[m,m] = D h_m     and      g[m] = h[m]     for m = 1, ..., M.
    NOTE: This only works for 0th component of h = [[[1]]].

    Parameters
    ----------
    h : TTTensor
        Coefficients of h given in same basis as test space
    normalized : bool
        Coefficients h given w.r.t. normalized HermiteE polynomials.
    """
    h.move_core(1)  # normalize 0th component of h to [[[1]]]
    M = h.order()-1
    dh = h.dimensions[1:]
    g = tt.convert(h)
    f = [np.array(g[0])]
    for j, h_core in enumerate(g[1:]):  # drop 0th core, this is FEM dim
        D = compute_D(dh[j], dh[j], normalized=True)
        fm = np.einsum("ij,ljr->lir", D, h_core)
        f.append(fm)

    return f, g


def build_operator_W(B, V):
    """
    Build global operator
        W = sum_{j=1,...,M} B_j.T @ B_j
    as rank-2*(r_{h}+1)**2 TTTensor operator, where r_{h} is the rank of h. 
    This is possible, since the operator W has the form
        W =  B[1,1].T @ B[1,1] *   V[2].T @ V[2]   * ... *   V[M].T @ V[M]
           +   V[1].T @ V[1]   * B[2,2].T @ B[2,2] * ... *   V[M].T @ V[M]
           ...
           +   V[1].T @ V[1]   *   V[2].T @ V[2]   * ... * B[M,M].T @ B[M,M].

    Parameters
    ----------
    B : list of array_like
        Diagonal cores of the TTOperators B_j, i.e. B[1,1], ..., B[M,M].
    V : list of array_like
        Off-diagonal cores of the TTOperators B_j, i.e. V[1], ..., V[M].
    """
    # TODO make this more general with handling 0th component of h
    # rank: (r_{h}+1)**2
    BTB = [tt.symmetrize_TTOperator_core(core) for core in B]
    if len(B) == 1:
        VTV = [None]
    else:
        # rank: (r_{h}+1)**2
        VTV = [tt.symmetrize_TTOperator_core(core) for core in V]
    ret = tt.convert(tt.low_rank_add_operator(BTB, VTV, VTV))
    return ret


def build_rhs_b(B, V, f, g, exph0):
    """
    Build global right-hand side
        b = sum_{m=1,...,M} B_m.T @ f_m
    as rank-2*r*(r_{h}+1) TTTensor, where r_{h} is the rank of h.
    This is possible, since the right-hand side b has the form
        b =  B[1,1].T @ f[1,1] *   V[2].T @ g[2]   * ... *   V[M].T @ g[M]
           +   V[1].T @ g[1]   * B[2,2].T @ f[2,2] * ... *   V[M].T @ g[M]
           ...
           +   V[1].T @ g[1]   *   V[2].T @ g[2]   * ... * B[M,M].T @ f[M,M].

    Parameters
    ----------
    B : list of array_like
        Diagonal cores of the TTOperators B_m, i.e. B[1,1], ..., B[M,M].
    V : list of array_like
        Off-diagonal cores of the TTOperators B_m, i.e. V[1], ..., V[M].
    f : list of array_like
        Diagonal cores of the right-hand side f_m, i.e. f[1,1], ..., f[M,M].
    g : list of array_like
        Off-diagonal cores of the right-hand side f_m, i.e. g[1], ..., g[M].
    exph0 : float or array_like
        Precomputed value of $\\exp h(x0)$.
    """
    # TODO make this more general with handling 0th component of h
    BT = [tt.transpose_TTOperator_core(core) for core in B]
    BTf = []
    for j, B_jj in enumerate(BT):
        l1, q, p, r1 = B_jj.shape
        l2, p2, r2 = f[j+1].shape
        p = min(p, p2)
        core = np.einsum("iqpj,lpr->ilqjr", B_jj[:, :, :p], f[j+1][:, :p])
        BTf.append(core.reshape(l1*l2, q, r1*r2))

    if len(B) == 1:
        VTg = [None]
    else:
        VT = [tt.transpose_TTOperator_core(core) for core in V]
        VTg = []
        for j, V_j in enumerate(VT):
            l1, q, p, r1 = V_j.shape
            l2, p2, r2 = g[j+1].shape
            p = min(p, p2)
            core = np.einsum("iqpj,lpr->ilqjr", V_j[:, :, :p], g[j+1][:, :p])
            VTg.append(core.reshape(l1*l2, q, r1*r2))

    if isinstance(exph0, np.ndarray):
        exph0 = exph0.flatten()[0]
    ret = tt.convert(tt.low_rank_add_operator(BTf, VTg, VTg))
    return exph0*ret


def build_operator_W_naive(da, dt, h, normalized=True):
    """ DEPRECIATED: Only for test purposes.

    Build global operator
        W = sum_{j=1,...,M} S_j.T @ S_j
    as rank-M*(r_{h}+1)**2 TTTensor operator in direct way. This is a very
    inefficient way that should only be used for comparison. For efficiency,
    use 'build_operator_W()' instead.

    Parameters
    ----------
    da : list of int
        Dimensions of ansatz space.
    dt : list of int
        Dimensions of test space.
    h : TTTensor
        Coefficients of h given in same basis as test space
    normalized : bool
        Coefficients h given w.r.t. normalized HermiteE polynomials.
    """
    ret = xe.TTOperator(da + da)
    M = h.order()-1
    assert len(da) == len(dt) == M
    assert np.all(np.asarray(da) <= np.asarray(dt))
    dh = h.dimensions[1:]
    Ds, Hs, Gs, tildeVs = [], [], [], []
    h_cores = tt.convert(h)[1:]  # drop 0th core, this is not stochastic
    kappa = tt.triple_product_tensor(max(da), max(dh), max(dt))

    # build all 1D components
    for j in range(M):
        D, H, G, tildeV = build_B_parts(
            da[j], dt[j], h_cores[j], kappa=kappa[:da[j], :dh[j], :dt[j]],
            normalized=normalized
        )
        Ds.append(D)
        Hs.append(H)
        Gs.append(G)
        tildeVs.append(tildeV)

    # build the (global) operators S_j
    for j in range(M):
        if M == 1:
            assert Hs[0].shape[0] == Hs[0].shape[3] == 1
            Bj = Ds[0] - Hs[0]
            ret.set_component(0, xe.Tensor.from_buffer(Bj))
        else:
            # build (global) operators D_j and H_j
            Dj, Hj = xe.TTOperator(dt + da), xe.TTOperator(dt + da)
            for m in range(M):
                if m == j:
                    Dj.set_component(m, xe.Tensor.from_buffer(Ds[m]))
                    Hj.set_component(m, xe.Tensor.from_buffer(Hs[m]))
                else:
                    Dj.set_component(m, xe.Tensor.from_buffer(Gs[m]))
                    Hj.set_component(m, xe.Tensor.from_buffer(tildeVs[m]))

            Bj = Dj - Hj
            Bj = tt.symmetrize_TTOperator(Bj)  # B_{j}.T @ B_{j}
            ret = ret + Bj

    return ret


def generate_IC_components(x0, h, da, exph0=None, scale=1):
    """ Prepare initial conditions for expTT.

    Compute the basis evaluation TTTensor P(x0) and the rhs exp( h(x0) ).
    Here, the polynomials P are multivariate hermite polynomials normalized
    w.r.t.  the standard Gaussian density.

    Parameters
    ----------
    x0 : array_like
        Parameter values for initial condition.
    h : xerus.TTTensor
        Coefficients of basis representation of exponent.
    da : list of int
        Dimensions of ansatz space for each parameter component.
    exph0 : float or array_like, default=None
        Precomputed value of $\\exp h(x0)$.
    scale : fload, default=1
        Rescaling of initial conditions.

    Returns
    -------
    P : xerus.TTTensor
        TTTensor representation of P(x0)
    b : array_like
        Right-hand side $\\exp h(x0)$.
    """
    if x0.ndim == 1:
        x0.shape = (1, -1)
    assert x0.shape[1] == len(da)
    def c(n): return 1 / np.sqrt(factorial(n))  # normalizing const
    if exph0 is None:
        exp_h = np.exp(tt.eval_approx(h, x0, dist="normal"))
    else:  # reshape to be compatible with shape of eval_approx()
        exp_h = np.array(exph0)
        if np.array(exp_h).ndim < 2:
            exp_h = np.array(exp_h).reshape(1, -1)
    P = xe.TTTensor(da)
    for m, d in enumerate(da):
        cm = np.diag([c(i) for i in range(d)])
        Pm = np.polynomial.hermite_e.hermeval(x0[0, m], cm)
        P.set_component(m, xe.Tensor.from_buffer(Pm.reshape(1, -1, 1)))
    return scale*P, scale*exp_h


def add_inexactIC_to_operator(W, h, x0, da, exph0=None, weight=1):
    """ Add inexact initial conditions to operator W.

    Build the global operator W_0 including the minimization term of the IC, i.e.
        W_0 = PoP + W = P(x0) \otimes P(x0) + sum_{m=1}^{M} S_{m}.T \\otimes S_{m}
    and the (non-zero) right-hand side
        b = exp_h * P(x0).
    The operator W_0 is of (maximal) rank-(2*(r_h+1)**2 + 1), since PoP is
    rank-1 and W is (maximal) rank-(2*(r_h+1)**2) and we have
    W_0 = W_0[1] * ... * W_0[M] for
        W_0[1] = [ PoP[1], W[1] ]
        W_0[m] = [[ PoP[m]     0 ],
                  [      0  W[m] ]
        W_0[M] = [[ PoP[M] ],
                  [   W[M] ]].

    Parameters
    ----------
    W : xe.TTOperator
        Operator without initial condition.
    h : array_like or xe.TTTensor
        Coefficients of h w.r.t. normalized standard HermiteE polynomials.
    x0 : array_like
        Point of initial condition.
    da : list of int
        Dimensions of ansatz space.
    exph0 : float or array_like, default=None
        Precomputed value of $\\exp h(x0)$.
    weight : float, default=1
        Weight of initial condition.

    Returns
    -------
    W_0 : xerus.TTOperator
        Operator wit added initial condition.
    b : xerus.TTTensor
        Right-hand side.
    """
    P, exp_h = generate_IC_components(x0, h, da, exph0=exph0, scale=weight)

    # build rhs b
    b = exp_h[0, 0]*P

    # build operator PoP = P(x0) \otimes P(x0)
    PoP = tt.rank_one_outer_product(P)

    # build W_0
    W_0 = W + PoP

    W_0.canonicalize_left()
    b.canonicalize_left()

    return W_0, b


def build_operator_Bf(da, dt, h, exph0):
    """ Assemble operator B and right-hand side f.

    Wrapper to comfortably build the operator B and rhs f.

    Parametes
    ---------
    da : list of int
        Dimensions of ansatz space.
    dt : list of int
        Dimensions of test space.
    h : array_like or xe.TTTensor
        Coefficients of h w.r.t. normalized standard HermiteE polynomials.
    exph0 : float or array_like
        Precomputed value of $\\exp h(x0)$. If None, use h for computation.
    """

    assert isinstance(h, xe.TTTensor)
    assert len(da) == len(dt) == h.order()-1
    assert np.all(np.asarray(da) <= np.asarray(dt))
    if isinstance(exph0, np.ndarray):
        exph0 = exph0.flatten()[0]
    Bmm, Vm = build_B_cores(da, dt, h, normalized=True)  # ranks: r_{h}+1
    fmm, gm = build_f_cores(h, normalized=True)  # ranks: r_{h}

    B = tt.convert(tt.low_rank_add_operator(Bmm, Vm, Vm))
    f = exph0 * tt.convert(tt.low_rank_add_operator(fmm, gm, gm))

    return B, f


def build_system_inexactIC(da, dt, h, x0, exph0=None, homogeneous=False):
    """ Assemble system (W, b) and add initial condition.

    Wrapper to comfortably build the operator W_0 and rhs b. Here the IC are
    added to the operator W, i.e the IC will also be minimized and not
    satisfied exactly. If homogeneous is true, the right-hand side is
            b = sum_{m=1}^M B_m.T@f_m   instead of     b = exph0*P.T.

    Parametes
    ---------
    da : list of int
        Dimensions of ansatz space.
    dt : list of int
        Dimensions of test space.
    h : array_like or xe.TTTensor
        Coefficients of h w.r.t. normalized standard HermiteE polynomials.
    x0 : array_like
        Point of initial condition.
    exph0 : float or array_like, default = None
        Precomputed value of $\\exp h(x0)$. If None, use h for computation.
    homogeneous : bool, default = False
        Assemble homogeneous system.
    """

    assert isinstance(h, xe.TTTensor)
    assert len(da) == len(dt) == h.order()-1
    assert np.all(np.asarray(da) <= np.asarray(dt))
    if exph0 is None:
        exph0 = np.exp(tt.eval_approx(h, x0, dist="normal"))
    B, V = build_B_cores(da, dt, h, normalized=True)  # ranks: r_{h}+1
    W = build_operator_W(B, V)
    W_0, b = add_inexactIC_to_operator(W, h, x0, da, exph0=exph0)

    if homogeneous:
        f, g = build_f_cores(h, normalized=True)  # ranks: r_{h}
        b = build_rhs_b(B, V, f, g, exph0)

    return W_0, b


def expTT_reconstruction(da, dt, h, x0, verbose=False):
    """ Reconstruct exp(h) with the inexact expTT approach.

    Parameters
    ----------
    da : list of int
        Dimensions of ansatz space.
    dt : list of int
        Dimensions of test space.
    h : array_like or xe.TTTensor
        Coefficients of h given in standard normalized HermiteE polynomials.
    x0 : array_like
        Initial Condition value.
    verbose : int
        Verbosity level of ALS.

    Returns
    -------
    ret : xerus.TTTensor
        Exp-TT Galerkin approximation of exp(h).
    """

    M = len(da)

    # build operator and rhs
    W0, b = build_system_inexactIC(da, dt, h, x0)

    # random initialization
    u_start = xe.TTTensor.random(da, [2]*(M-1) if M > 1 else [])
    u_start = u_start / xe.frob_norm(u_start)

    # run ALS
    ret = run_simpleALS(A=W0, u_start=u_start, b=b, verbose=verbose)
    ret.round(1e-12)  # round to get unneccesary large ranks down

    ret = tt.add_dimension_left(ret)  # add deterministic component

    return ret


def compute_residual_old(u, B, f):
    """ Compute || Bu - f ||_2.

    Parameters
    ----------
    u : xe.TTTensor
    B : xe.TTOperator
    f : xe.TTTensor
    """
    M = u.order()
    assert M == B.order()//2 == f.order()-1
    assert np.array_equal(u.dimensions, B.dimensions[:M])
    i, j = xe.indices(2)
    ret = xe.TTTensor()
    ret(i&0) << B(i/2, j/2) * u(j&0)
    ret = tt.add_dimension_left(ret)
    dims = [max(ret.dimensions[j], f.dimensions[j]) for j in range(f.order())]
    ret = tt.prolong_tt(ret, dims) - tt.prolong_tt(f, dims)
    return xe.frob_norm(ret)


def compute_residual(u, h, exph0, dt=None, normalized=True):
    """ Compute sqrt( sum_{m=1}^M || B_m u - f_m||_2^2 ).

    Notes
    -----
    This can be done more efficiently by pre-computing the core contractions.
    """
    # TODO(Nando) make this more efficient by pre-computing core contractions.
    da = u.dimensions
    if dt is None:
        dt = da
    Bmm, Vm = build_B_cores(da, dt, h, normalized=normalized)
    fmm, gm = build_f_cores(h, normalized=normalized)
    i, j = xe.indices(2)
    ret = []
    for m in range(u.order()):
        Bm = tt.convert([Bmm[k] if k == m else Vm[k]
                         for k in range(u.order())])
        fm = [gm[0]] + [fmm[k+1] if k == m else gm[k+1]
                        for k in range(u.order())]
        if exph0.ndim < 3:
            exph0.shape = 1, -1, 1
        assert exph0.shape[1] == fm[0].shape[1]
        fm[0] = fm[0] * exph0
        fm = tt.convert(fm)

        BmU = xe.TTTensor()
        BmU(i&0) << Bm(i/2, j/2) * u(j&0)
        BmU = tt.add_dimension_left(BmU)
        dims = [max(BmU.dimensions[k], fm.dimensions[k])
                for k in range(fm.order())]
        BmU_Fm = tt.prolong_tt(BmU, dims) - tt.prolong_tt(fm, dims)
        noise = 1e-16 * xe.TTTensor.random(
            BmU_Fm.dimensions, [1]*(BmU_Fm.order()-1))
        BmU_Fm += noise  # circumvent nan if BmU-Fm is too small
        ret.append(xe.frob_norm(BmU_Fm)**2)
    return np.sqrt(np.sum(ret))


# ==============================================================================
# Handling FEM components as initial condition (IC)


def low_rank_add_operator_femIC(B, L, R):
    """
    Compute the sum of M-1 TTOperators where each individual term
    has the form
        T_j = L[1] * ... * L[j-1] * B[j] * R[j+1] * ... * R[M], j = 2,...,M,
    such that the resulting TT object has rank 2r, if r is the rank of the T_j.
    This is done by constructing the cores of the sum
        W = sum_{j=1,...,M} T_j = W[1] * ... * W[M]
    to be
        W[1] = [[ L[1] L[1] ]]
        W[2] = [[ B[2,2]    0 ],
                [      0 L[2] ]]
        W[m] = [[   R[m]    0 ],
                [ B[m,m] L[m] ]]
        W[M] = [[   R[M] ],
                [ B[M,M] ]].

    Parameter
    ---------
    B : list of array_like, first entry may be xe.Tensor
        Diagonal cores of the TTOperators T_j, i.e. B[1], ..., B[M].
    L : list of array_like, first entry may be xe.Tensor
        Left cores of the TTOperators T_j, i.e. L[1], ..., L[M]. Note that L[M]
        is never used.
    R : list of array_like, first entry may be xe.Tensor
        Right cores of the TTOperators T_j, i.e. R[1], ..., R[M]. Note that
        R[1] is never used.
    """
    assert isinstance(B, list) and isinstance(L, list) and isinstance(R, list)
    assert len(B) == len(L) == len(R) and len(L) > 1
    ret = []
    for j in range(len(B)):
        if len(B) == 2:
            if isinstance(L[0], xe.Tensor):
                assert L[0].dimensions[0] == 1
            else:
                assert L[0].shape[0] == 1
            assert B[1].shape[-1] == 1
            ret = [L[0], B[1]]
        else:
            if j == 0:
                if isinstance(L[0], xe.Tensor):
                    # shape ( 1, d_1, q_1, 2*r_2 )
                    core = tt.concatenate_tensor(L[0], L[0], axis=-1)
                else:
                    # shape ( 1, d_1, q_1, 2*r_2 )
                    core = np.concatenate([L[0], L[0]], axis=-1)
            elif j == 1:
                O = np.zeros((*B[j].shape[:2], *L[j].shape[2:]))
                OT = np.zeros((*L[j].shape[:2], *B[j].shape[2:]))
                core = np.concatenate([  # shape ( 2*r_j, d_j, q_j, 2*r_{j+1} )
                    np.concatenate([B[j],    O], axis=-1),
                    np.concatenate([OT,  L[j]], axis=-1)
                ], axis=0)
            elif 1 < j < len(B)-1:
                O = np.zeros((*R[j].shape[:2], *L[j].shape[2:]))
                core = np.concatenate([  # shape ( 2*r_j, d_j, q_j, 2*r_{j+1} )
                    np.concatenate([R[j],    O], axis=-1),
                    np.concatenate([B[j], L[j]], axis=-1)
                ], axis=0)
            elif j == len(B)-1:
                # shape ( 2*r_M, d_M, q_M, 1 )
                core = np.concatenate([R[-1], B[-1]], axis=0)
            ret.append(core)
    return ret


def build_B_femIC_parts(space_dim, h):
    """ Build component matrices of B for FEM core.

    Build the parts of the FE-core S_0 = G - \tilde{V}. When considering the
    FE component as IC, there is no differentiation w.r.t. the FE component.
    Hence we only need to compute G = identity and \tilde{V} = h*K, where
    K is the order 3 Kronecker tensor (see fem.compute_interpolation_tensor
    for more information.

    Parameters
    ----------
    space_dim : int
        Finite element space dimension.
    h         : xerus Tensor
        FE component of exponent.
    """
    assert h.order() == 3
    assert h.dimensions[0] == 1 and h.dimensions[1] == space_dim
    K = fem.compute_interpolation_tensor(space_dim)

    G = xe.Tensor.identity([space_dim, space_dim])
    G.reinterpret_dimensions([1, space_dim, space_dim, 1])

    tildeV = xe.Tensor()
    it, jt, kt, lt, rt = xe.indices(5)
    tildeV(lt, jt, kt, rt) << h(lt, it, rt) * K(it, jt, kt)

    return G, tildeV


def build_B_femIC_cores(da, dt, h, fem_space_dim, normalized=True):
    """ Build operator lists [B_mm] and [V_m].

    Build the operator cores
       B_mm = D_m - H_m     and    V_m = G_m - \tilde{V}_m,    m = 0, ..., M
    for FE component as initial condition (IC). The zeroth component B_0 is not
    differentiated in this case, hence B_0 is set to None.

    Parameters
    ----------
    da            : list of int
        Dimensions for (stochastic) ansatz space.
    dt            : list of int
        Dimensions for (stochastic) test space.
    h             : xerus TTTensor
        TTTensor representation of exponent h.
    fem_space_dim : int
        Finite element space dimensions.
    normalized  : bool
        Use normalized (stochastic) polynomials. Default is True.

    Returns
    -------
    B : list of xe.Tensor/np.ndarray
        List of cores [B_mm] with first component (FEM) as sparse xe.Tensor.
    V : list of xe.Tensor/np.ndarray
        List of cores [V_m] with first component (FEM) as sparse xe.Tensor.
    """
    M = h.order()-1
    dh = h.dimensions[1:]
    assert len(da) == len(dt) == len(dh) == M
    assert fem_space_dim == h.dimensions[0]
    kappa = tt.triple_product_tensor(max(da), max(dh), max(dt))
    B, V = [], []
    h_cores = tt.convert(h)

    for j, h_core in enumerate(h_cores):
        if j == 0:  # fem component
            # NOTE: These cores are xe.Tensor objects
            G, tildeV = build_B_femIC_parts(
                fem_space_dim, h.get_component(0)
            )
            # V0 -> V_{0} = G_0 - \hat V_0
            # shape (1, N, N, r_{1}+1)
            V0 = tt.concatenate_tensor(G, -1*tildeV, axis=3)
            V.append(V0)
            B.append(None)
        else:
            # NOTE: These cores are np.array objects
            D, H, G, tildeV = build_B_parts(
                da[j-1], dt[j-1], h_core,
                kappa=kappa[:da[j-1], :dh[j-1], :dt[j-1]],
                normalized=normalized
            )

            # Bjj  ->   S_{j,j} = D_{j,j} - H_{j,j}
            # Vj   ->   V_{j} = D_{m,j} - H_{m,j} = G_j - \hat V_j for any j != m
            # ranks(h) = (1, r_1, ..., r_{M}, 1)
            if j < M:
                # shape (1,   d_{t,m}, d_{a,m}, r_{m+1})
                O1 = np.zeros((D.shape[0], dt[j-1], da[j-1], H.shape[3]))
                # shape (r_{m}, d_{t,m}, d_{a,m}, 1)
                O2 = np.zeros((H.shape[0], dt[j-1], da[j-1], D.shape[3]))
                Bjj = np.concatenate([                                          # shape (r_{m}+1, d_{t,m}, d_{a,m}, r_{m+1}+1)
                    np.concatenate([D,  O1], axis=3),
                    np.concatenate([O2,  H], axis=3)
                ], axis=0)
                Vj = np.concatenate([                                           # shape (r_{m}+1, d_{t,m}, d_{a,m}, r_{m+1}+1)
                    np.concatenate([G,      O1], axis=3),
                    np.concatenate([O2, tildeV], axis=3)
                ], axis=0)
            elif j == M:
                # shape (r_{M}+1, d_{t,M}, d_{a,M}, 1)
                Bjj = np.concatenate([D, H], axis=0)
                # shape (r_{M}+1, d_{t,M}, d_{a,M}, 1)
                Vj = np.concatenate([G, tildeV], axis=0)

            B.append(Bjj)
            V.append(Vj)

    return B, V


def build_f_femIC_cores(h, normalized=True):
    """
    Build the cores f[m,m] and g[m] given respectively by
        f[m,m] = D h_m     and      g[m] = h[m]     for m = 1, ..., M
    for FE component as initial condition (IC). The zeroth component f_0
    is not differentiated in this case, hence f_0 is set to None.

    Parameters
    ----------
    h : TTTensor
        Coefficients of h given in same basis as test space
    normalized : bool
        Coefficients h given w.r.t. normalized HermiteE polynomials.
    """
    M = h.order()-1
    dh = h.dimensions[1:]
    g = tt.convert(h)
    f = [None]
    for j, h_core in enumerate(g[1:]):  # drop 0th core, this is FEM dim
        D = compute_D(dh[j], dh[j], normalized=True)
        fm = np.einsum("ij,ljr->lir", D, h_core)
        f.append(fm)

    return f, g


def generate_fem_IC_components(x0, y0, exph0, da, space, scale=1):
    """ Generate IC components for FEM-TTs.

    This method handles arbitrary deterministic points x0 but is slow in
    assembling the basis evaluation matrix for the FEM component.

    Parameters
    ----------
    x0      : array like of shape (space.dim(), 2)
        Deterministic initial condition points.
    y0      : array like of shape (1, len(da))
        Stochastic initial condition points.
    exph0   : array_like of shape (space.dim(),)
        Value of exp h(x0,y0).
    da      : list of int
        Dimensions of (stochastic) ansatz space.
    space   : fenics FunctionSpace
        Finite element space.
    scale   : float
        Weighting for the initial condition for least-squares minimization. 
        Default is 1.
    """
    if x0.ndim == 1:
        x0.shape = (1, -1)
    if y0.ndim == 1:
        y0.shape = (1, -1)
    assert y0.shape[0] == 1
    assert x0.shape[1] == 2 and y0.shape[1] == len(da)

    N = space.dim()
    def c(n): return 1 / np.sqrt(factorial(n))  # normalizing const.
    P = xe.TTOperator([x0.shape[0]]+[1]*len(da) + [N]+da)
    z = xe.TTTensor([x0.shape[0]]+[1]*len(da))

    # fem component
    phi = Function(space)
    core = xe.Tensor([1, x0.shape[0], N, 1])
    for j in range(N):  # iter over basis functions
        phi.vector().set_local(np.eye(1, N, k=j).flatten())
        for i in range(x0.shape[0]):  # iter over IC
            val = phi(x0[i])
            if np.abs(val) > 1e-16:
                core[0, i, j, 0] = val
    P.set_component(0, core)
    if exph0.ndim == 1:
        exph0.shape = 1, -1, 1
    assert exph0.shape == (1, x0.shape[0], 1)
    z.set_component(0, xe.Tensor.from_buffer(exph0))

    # stochastic components
    for m, d in enumerate(da):
        cm = np.diag([c(i) for i in range(d)])
        Pm = np.polynomial.hermite_e.hermeval(y0[0, m], cm)
        P.set_component(m+1, xe.Tensor.from_buffer(Pm.reshape(1, 1, -1, 1)))
        z.set_component(m+1, xe.Tensor.from_buffer(np.ones([1, 1, 1])))

    return scale*P, scale*z


def generate_femDof_IC_components(y0, exph0, da, space_dim, scale=1):
    """ Generate IC components for FEM-TTs.

    Use the Lagrange interpolation points for the FE-space as x0,
    hence the basis evaluation matrix for the FEM component is simply the
    identity. This is fast in assembling but only works for DG-0 and arbitrary
    CG function spaces.

    Parameters
    ----------
    y0        : array like of shape (1, len(da))
        Stochastic initial condition points.
    exph0     : array_like of shape (space.dim(),)
        Value of exp h(x0,y0).
    da        : list of int
        Dimensions of (stochastic) ansatz space.
    space_dim : int
        Finite element space dimension.
    scale     : float
        Weighting for the initial condition for least-squares minimization. 
        Default is 1.
    """
    if y0.ndim == 1:
        y0.shape = (1, -1)
    assert y0.shape[0] == 1 and y0.shape[1] == len(da)

    def c(n): return 1 / np.sqrt(factorial(n))  # normalizing const.
    P = xe.TTOperator([space_dim]+[1]*len(da) + [space_dim]+da)
    z = xe.TTTensor([space_dim]+[1]*len(da))

    # fem component
    core = xe.Tensor.identity([space_dim, space_dim])
    core.reinterpret_dimensions([1, space_dim, space_dim, 1])
    P.set_component(0, core)
    if exph0.ndim == 1:
        exph0.shape = 1, -1, 1
    assert exph0.shape == (1, space_dim, 1)
    z.set_component(0, xe.Tensor.from_buffer(exph0))

    # stochastic components
    for m, d in enumerate(da):
        cm = np.diag([c(i) for i in range(d)])
        Pm = np.polynomial.hermite_e.hermeval(y0[0, m], cm)
        P.set_component(m+1, xe.Tensor.from_buffer(Pm.reshape(1, 1, -1, 1)))
        z.set_component(m+1, xe.Tensor.from_buffer(np.ones([1, 1, 1])))

    return scale*P, scale*z


def build_operator_W_femIC(B, V):
    """ Assemble operator W with FEM component as initial condition.

    Build global operator
        W = sum_{j=2,...,M} S_j.T @ S_j
    as rank-2*(r_{h}+1)**2 TTTensor operator, where r_{h} is the rank of h.
    This is possible, since the operator W has the form
        W =  V[1].T @ V[1]   * B[2,2].T @ B[2,2] * ... *   V[M].T @ V[M]
           ...
           + V[1].T @ V[1]   *   V[2].T @ V[2]   * ... * B[M,M].T @ B[M,M].

    Parameters
    ----------
    B : list of array_like
        Diagonal cores of the TTOperators S_j, i.e. B[1,1], ..., B[M,M].
    V : list of array_like
        Off-diagonal cores of the TTOperators S_j, i.e. V[1], ..., V[M].
    """
    BTB = [tt.symmetrize_TTOperator_core(
        core) for core in B]                   # rank: (r_{h}+1)**2
    # rank: (r_{h}+1)**2
    VTV = [tt.symmetrize_TTOperator_core(core) for core in V]
    ret = tt.convert(low_rank_add_operator_femIC(BTB, VTV, VTV))
    return ret


def build_operator_Bf_femIC(da, dt, h, exph0):
    """ Assemble operator B and right-hand side f.

    Wrapper to comfortably build the operator B and rhs f.

    Parametes
    ---------
    da : list of int
        Dimensions of ansatz space.
    dt : list of int
        Dimensions of test space.
    h : array_like or xe.TTTensor
        Coefficients of h w.r.t. normalized standard HermiteE polynomials.
    exph0 : array_like of shape (h.dimensions[0],)
        Value of exp h(y0).
    """

    assert isinstance(h, xe.TTTensor)
    fem_dim, dh = h.dimensions[0], h.dimensions[1:]
    assert len(da) == len(dt) == len(dh)
    assert np.all(np.asarray(da) <= np.asarray(dt))
    Bmm, Vm = build_B_femIC_cores(da, dt, h, fem_dim, normalized=True)
    fmm, gm = build_f_cores(h, normalized=True)

    B = tt.convert(low_rank_add_operator_femIC(Bmm, Vm, Vm))

    f = low_rank_add_operator_femIC(fmm, gm, gm)
    if exph0.ndim < 3:
        exph0.shape = 1, -1, 1
    assert exph0.shape[1] == f[0].shape[1]
    f[0] = f[0] * exph0
    f = tt.convert(f)

    return B, f


def add_inexact_femIC_to_operator(W, P, exp_h, da):
    """ Add initial condition to operator W.

    Generate operator for minimizing FE initial conditions and add that to the
    stochastic differentiation operator W.

    Parameters
    ----------
    W       : xerus TTOperator
        Operator W = sum_m S_m of stochastic differentiation parts.
    P       : xerus TTOperator
        Rank-1 basis evaluation operator in (x0, y0).
    exp_h   : xerus TTTensor
        Rank-1 tensor of exp(h(x0,y0)).
    da : list of int
        Dimensions of ansatz space.
    """
    # build rhs b = exp_h * P.T and PoP = P.T * P
    # dimensions (N, da, N, da)
    PoP = xe.TTOperator(P.dimensions[len(da)+1:] + P.dimensions[len(da)+1:])
    # dimensions (N, da)
    b = xe.TTTensor(P.dimensions[len(da)+1:])
    l1, l2, i, j, k, r1, r2 = xe.indices(7)
    for jj in range(exp_h.order()):
        Pm = P.get_component(jj)
        rhs = exp_h.get_component(jj)

        PoP_core = xe.Tensor()
        PoP_core(l1, l2, j, k, r1, r2) << Pm(l1, i, j, r1) * Pm(l2, i, k, r2)
        PoP_core.reinterpret_dimensions(
            [1, Pm.dimensions[2], Pm.dimensions[2], 1]
        )
        PoP.set_component(jj, PoP_core)

        b_core = xe.Tensor()
        b_core(l1, l2, j, r1, r2) << Pm(l1, i, j, r1) * rhs(l2, i, r2)
        b_core.reinterpret_dimensions([1, Pm.dimensions[2], 1])
        b.set_component(jj, b_core)

    # build W_0
    W_0 = W + PoP

    # NOTE: cannonicalize_left() makes first component not sparse anymore!
    # W_0.canonicalize_left()
    # b.canonicalize_left()

    return W_0, b


def build_system_inexactFEMIC(da, dt, h, x0, y0, exph0, space, mesh=None,
                              weight=1):
    """ Wrapper to comfortably build the operator W_0 and rhs b for the FE case.

    The FE component is completely handeled as IC and not differentiated.
    The IC are added to the operator W, i.e the IC will also be minimized
    and not satisfied exactly.

    Parametes
    ---------
    da : list of int
        Dimensions of ansatz space.
    dt : list of int
        Dimensions of test space.
    h : xerus TTTensor
        TTTensor representation of exponent h.
    x0      : array like of shape (space.dim(), 2)
        Deterministic initial condition points.
    y0      : array like of shape (1, len(da))
        Stochastic initial condition points.
    exph0   : array_like of shape (space.dim(),)
        Value of exp h(x0,y0).
    space   : fenics FunctionSpace
        Finite element space.
    mesh    : fenics Mesh
        Finite element mesh underlying the space.
    weight  : float
        Weighting for the initial condition for least-squares minimization.
        Default is 1.
    """
    # build operator W (without IC)
    B, V = build_B_femIC_cores(da, dt, h, space.dim(), normalized=True)
    W = build_operator_W_femIC(B, V)

    # build operator for IC and rhs
    fem_dofs = fem.dof_coords(space, mesh) if mesh is not None else None
    if fem_dofs is not None and np.allclose(fem_dofs, x0):
        P, exp_h = generate_femDof_IC_components(
            y0, exph0, da, space.dim(), scale=weight)
    else:
        P, exp_h = generate_fem_IC_components(
            x0, y0, exph0, da, space, scale=weight)

    # add IC to operator W
    W0, b = add_inexact_femIC_to_operator(W, P, exp_h, da)

    return W0, b


def compute_residual_femIC_old(u, B, f):
    """ Compute || Bu - f ||_2 with

    This computes the Frobenious norm also over the FE-component!

    Parameters
    ----------
    u : xe.TTTensor
    B : xe.TTOperator
    f : xe.TTTensor
    """
    # TODO: This is basically the same as compute_residual(). The only
    # difference is the order of the operators u, B, f. This should unified
    # for 1d and FEM case!
    assert u.order() == B.order()//2
    assert np.array_equal(u.dimensions, B.dimensions[:u.order()])
    i, j = xe.indices(2)
    ret = xe.TTTensor()
    ret(i&0) << B(i/2, j/2) * u(j&0)

    assert u.order() == f.order()
    assert u.dimensions[0] == f.dimensions[0]
    dims = [max(ret.dimensions[j], f.dimensions[j]) for j in range(f.order())]
    ret = tt.prolong_tt(ret, dims) - tt.prolong_tt(f, dims)

    return xe.frob_norm(ret)


def compute_residual_femIC(u, h, exph0, dt=None, normalized=True):
    """ Compute sqrt( sum_{m=1}^M || B_m u - f_m||_2^2 ).

    Notes
    -----
    This can be done more efficiently by pre-computing the core contractions.
    """
    # TODO(Nando) make this more efficient by pre-computing core contractions.
    # TODO(Nando) This is basically the same as compute_residual(). The only
    # difference is the order of the operators u, B, f. This should unified
    # for 1d and FEM case!
    fem_space_dim = u.dimensions[0]
    da = u.dimensions[1:]
    if dt is None:
        dt = da
    Bmm, Vm = build_B_femIC_cores(da, dt, h, fem_space_dim, normalized)
    fmm, gm = build_f_femIC_cores(h, normalized)
    i, j = xe.indices(2)
    ret = []
    for m in range(1, u.order()):
        Bm = tt.convert([Bmm[k] if k == m else Vm[k]
                         for k in range(u.order())])
        fm = [fmm[k] if k == m else gm[k] for k in range(u.order())]
        if exph0.ndim < 3:
            exph0.shape = 1, -1, 1
        assert exph0.shape[1] == fm[0].shape[1]
        fm[0] = fm[0] * exph0
        fm = tt.convert(fm)

        BmU = xe.TTTensor()
        BmU(i&0) << Bm(i/2, j/2) * u(j&0)
        assert BmU.order() == fm.order()
        assert BmU.dimensions[0] == fm.dimensions[0]
        dims = [max(BmU.dimensions[k], fm.dimensions[k])
                for k in range(fm.order())]
        BmU_Fm = tt.prolong_tt(BmU, dims) - tt.prolong_tt(fm, dims)
        noise = 1e-16 * xe.TTTensor.random(
            BmU_Fm.dimensions, [1]*(BmU_Fm.order()-1))
        BmU_Fm += noise  # circumvent nan if BmU-Fm is too small
        ret.append(xe.frob_norm(BmU_Fm)**2)

    return np.sqrt(np.sum(ret))
