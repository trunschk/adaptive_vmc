# standard imports
import numpy as np
from numpy.polynomial.legendre import Legendre
from numpy.polynomial.hermite_e import HermiteE
from copy import deepcopy
import itertools

# third party / non-standard imports
from fenics import *
import xerus as xe

# local imports
from src.misc import log, timeit, transpose, factorial
from src.misc import herm_triple_product, leg_triple_product
from src.misc import compute_scaling_constant, hermite_base_product_matrix
from src.utils.fem_tools import prolongate_x_2, mass_matrix
from src.problem.equation.darcy import Problem
from src.parallel import Parallel
from measurement_utils.measures import MeasurementList, BasisMeasure
from measurement_utils.bases import LegendrePolynomials, HermitePolynomials


def get_initial_guess(dims, init=None, coarse_space=None, fine_space=None):
    """ Prolongate FEM-TTTensor from coarse FE space to fine FE space.

    Generate an initial guess for the uq_ra_adf algorithm based on a
    reconstruction in a previous iteration. This is done by prolongation of
    stochastic components and/or interpolation of FE components if necessary.

    Parameters
    ----------
    dims : list
        Dimensions of the initial guess TTTensor. First entry is FE dimension.
    init : TTTensor
        Reconstruction of previous iteration that is prolongated. If None
        (default), return a random TTTensor with dimensions dims.
    coarse_space : dolfin.function.functionspace.FunctionSpace
        FE space on coarser grid (previous iteration). Must not be None if FE
        spaces of previous and current iteration are different.
    fine_space : dolfin.function.functionspace.FunctionSpace
        FE space on fine grid (current iteration). Must not be None if FE
        spaces of previous and current iteration are different.
    """

    # return random initial tensor if init not specified
    if init is None:
        return xe.TTTensor.random(dims, [1]*(len(dims)-1))

    assert init.order() <= len(dims)
    for j, d in enumerate(init.dimensions):
        assert d <= dims[j]
    initial_guess = prolong_tt(init, [init.dimensions[0]] + dims[1:])

    if init.dimensions[0] < dims[0]:  # interpolate FEM space
        assert coarse_space is not None and fine_space is not None
        # shape (1, N, r1)
        init_core = to_ndarray(init.get_component(0))
        # shape (r1, \hat N)
        init_core = prolongate_x_2(init_core[0].T, coarse_space, fine_space)
        # shape (1, \hat N, r1)
        init_core = np.ascontiguousarray(np.expand_dims(init_core.T, axis=0))
        initial_guess.set_component(0, xe.Tensor.from_buffer(init_core))
    initial_guess.canonicalize_left()

    return initial_guess


def reconstruct(dims, nodes, weights, values, dist, scale=0, decay=-1,
                init=None, label="", verbose=False):
    """ Reconstruct TTTensor of function from samples (uq_ra_adf).

    The reconstruction of the coefficient TTTensor is done w.r.t. the specified
    distribution. In case of normally distributed random variables, it is
    possible to compute the reconstruction w.r.t. either standard or scaled
    normalized Hermite polynomials.

    Parameters
    ----------
    dims : list of int
        Stochastic reconstruction dimensions.
    nodes : array_like
        Parameter samples.
    weights : array_like
        Reconstruct weights.
    values : array_like
        Function samples.
    dist : string
        Distribution of parameters. ('normal' or 'uniform')
    scale : float, default = 0
        Scaling constant for scaled Hermite basis.
    decay : float, default = -1
        Decay rate of affine diffusion coefficient. Needs to be positive if
        used for computation.
    init : None or TTTensor, default = None
        Initial guess for the uq_ra_adf().
    label : string, default = ''
        Description of what is reconstructed.
    verbose : bool or int, default = 0
        Verbosity level.

    Returns
    -------
    reco : xe.TTTensor
        Coefficient tensor w.r.t. specified basis.
    """

    x_dim = values.shape[1]  # physical dimension
    tensor = xe.Tensor.from_buffer
    if scale != 0:
        assert decay > 0
        assert dist == "normal"

    # weight constant for WLS sampling
    if np.all(weights == weights[0]):
        cs = np.ones(values.shape[0])
    else:
        cs = np.sqrt(np.abs(weights))

    vals = [tensor(c * val) for c, val in zip(cs, values)]

    ml = MeasurementList(get_basis_measures(
        dims, dist, scale=scale, decay=decay))
    assert ml.dimensions == tuple(dims), f"{ml.dimensions} vs {dims}"
    meas = transpose(ml(nodes))
    meas = [[tensor(c * np.array(cmp_m)) for cmp_m in m]
            for c, m in zip(cs, meas)]
    # There is a bug in `transpose` that causes `cmp_m` to have wrong strides.
    # Creating a new array fixes the strides.

    with timeit("  ... ({:4.2f} s)", verbose=verbose):
        if init is None:
            reco = xe.uq_ra_adf(meas, vals, (x_dim,) + ml.dimensions,
                                targeteps=1e-8, maxitr=300)
        else:
            start = xe.TTTensor(init)
            reco = xe.uq_ra_adf(start, meas, vals, (x_dim,) + ml.dimensions,
                                targeteps=1e-8, maxitr=300)

    return reco


def get_basis_measures(dims, dist, scale=0, decay=-1):
    """ Get BasisMeasure list dependend on distribution type.

    This is a helper function for reconstruct() that returns a list of basis
    polynomial functions for each stochastic dimension.  Each entry of the list
    consists of normalized polynomials (Legendre, Hermite or scaled Hermite) up
    to degree dims[m]-1.

    Parameters
    ----------
    dims : list of int
        List of stoachstic dimensions.
    dist : string, either ['uniform', 'normal']
        Distribution type.
    scale : float
        Scaling constant for Hermite basis, e.g. vartheta*rho.
    decay : float
        Decay rate of affine coefficient.

    Returns
    -------
    basis_measures : list of measurement_utils.BasisMeasure
    """
    if dist == "uniform":
        # NOTE: In uniform case, use the same Legendre polynomials for each component.
        dom = [-1, 1]
        def bm(dim): return BasisMeasure(LegendrePolynomials(dom, dim-1))
        basis_measures = [bm(dim) for dim in dims]
    elif dist == "normal":
        def bm(dim, mean=0.0, var=1.0):
            """ Return BasisMeasure of scaled Hermite polynomials. """
            poly = HermitePolynomials(dim-1, mean=mean, variance=var)
            return BasisMeasure(poly)
        if scale == 0:
            # NOTE: In unscaled case, use standard Hermite polynomials in for
            #       each component.
            basis_measures = [bm(dim, mean=0.0, var=1.0) for dim in dims]
        else:
            # NOTE: In scaled case, use a differently scaled Hermite basis for
            #       each stochastic component.
            assert decay > 0
            basis_measures = []
            for m, dim in enumerate(dims):
                std = compute_scaling_constant(m+1, decay, scale, gamma=0.9)
                basis_measures.append(bm(dim, mean=0.0, var=std**2))
    else:
        raise AssertionError(f"unknown dist type: {dist}")
    return basis_measures


def triple_product_tensor(d1, d2, d3, dist="normal", normalized=True):
    """ Compute triple product tensor.

    Compute the order-3 tensor of 1D polynomial triple products given by
        Kappa_{i,k,j} = int_{D} P_i P_j P_k dx
    for i=0,...,d1-1, j=0,...,d2-1, k=0,...,d3-1. Supported are Legendre and
    Hermite polynomials both normalized and unnormalized.
    """

    if dist == "uniform":
        triple_product = leg_triple_product
    elif dist == "normal":
        triple_product = herm_triple_product
    else:
        raise AssertionError(f"unsupported distribution: {dist}")
    kappa = np.zeros([d1, d2, d3])

    for i, j, k in itertools.product(range(d1), range(d2), range(d3)):
        kappa[i, j, k] = triple_product(i, j, k, normalized=normalized)
    return kappa


def eval_approx(reco, yss, dist, scale=0, decay=-1):
    """ Evaluate TTTensor in samples.

    For all samples y in ys, return u(y) = sum_alpha( u_alpha P_alpha(y) ).

    Parameters
    ----------
    reco :  TTTensor
        Coefficient tensor of approximaiton.
    yss :  array_like, shape (n_samples, y_dim)
        Evaluation samples.
    dist :  string
        Parameter distribution ('normal','uniform').
    scale : float, default = 0
        Scaling for scaled Hermite basis the coefficients are given in.
        Defaults to no scalining, i.e. standard Hermite basis.
        NOTE: This is only relevant for dist = 'normal'.
    decay : float, default = -1
        Decay rate of affine diffusion coefficient. If scale != 0, decay has
        to be greater than zero.
        NOTE: This is only relevant for dist = 'normal'.
    """
    u = xe.Tensor.random([reco.dimensions[0]])
    us = np.empty((yss.shape[0], reco.dimensions[0]))
    x_idx, y_idx = xe.indices(2)
    meas = rk1_measure(yss, reco.dimensions[1:], dist,
                       scale=scale, decay=decay)
    for i in range(yss.shape[0]):
        u(x_idx) << reco(x_idx, y_idx & 1) * meas[i](y_idx & 0)
        us[i] = to_ndarray(u)
    return us


def rk1_measure(yss, dims, dist, scale=0, decay=-1):
    """ Generate rank-1 basis evaluation tensor.

    Helper function for eval_approx(). Uses either Legendre, Hermite or
    scaled Hermite basis polynomials, where the latter need to be specified
    in the info dict (scaling etc.).

    Parameters
    ----------
    yss : array_like
        Samples basis is evaluated in. Shape is (N, len(dims)).
    dims : list of int
        Stochastic dimensions.
    dist : string, ("uniform", "normal")
        Identifier for basis type.
    scale : float, default = 0
        Scaling for scaled Hermite basis the coefficients are given in.
        Defaults to no scalining, i.e. standard Hermite basis.
    decay : float, default = -1
        Decay rate of affine diffusion coefficient. If scale != 0, decay has
        to be greater than zero.

    Returns
    -------
    tt_list : list of xerus.TTTensor()
        List of TTTensors, where each entry corresponds to the evaluation of
        the basis polynomials in one sample.
    """

    def _compute_scale(m, s, decay):
        """
        Compute rescaling for Hermite polynomials. Return 1 for Legendre
        polynomials or if standard Hermite polynomials are used.
        """
        assert decay > 0
        return 1 / compute_scaling_constant(m, decay, s, gamma=0.9)

    def _sHermite(deg, _s):
        """ Unnormalized scaled Hermite polynomial. """
        return HermiteE.basis(deg)(np.polynomial.Polynomial((0, _s)))

    M = len(dims)
    if scale == 0:
        scales = [1.0 for m in range(1, M+1)]
    else:
        scales = [_compute_scale(m, scale, decay) for m in range(1, M+1)]
    # normalization constant
    if dist == "uniform":
        ns = np.sqrt(1+2*np.arange(max(dims)))
    elif dist == "normal":
        ns = [1 / np.sqrt(factorial(n)) for n in np.arange(max(dims))]

    # evaluate basis polynomials in samples yss
    # NOTE: vals is list of length M where each entry is an array with
    #               vals[m].shape = (dims[m], yss.shape[0])
    vals = [np.zeros((d, yss.shape[0])) for d in dims]
    for m, dim in enumerate(dims):
        for d in range(dim):
            if dist == "uniform":
                vals[m][d] = ns[d] * Legendre.basis(d)(yss[:, m])
            elif dist == "normal":
                vals[m][d] = ns[d] * _sHermite(d, scales[m])(yss[:, m])

    tt_list = []
    for j in range(yss.shape[0]):
        arr_list = [vals[m][:, j].reshape(1, -1, 1) for m in range(M)]
        tt_list.append(convert(arr_list))

    return tt_list


def slice(TT, *args):
    """ Cut-out TTTensor.

    Slice TTTensor in each component from lower to (not including) upper
    dimension.
    """
    la = len(args)
    if la == 1:
        upper = args[0]
        lower = [0]*len(upper)
    elif la == 2:
        lower, upper = args
    else:
        if la < 1:
            raise TypeError(f"slice expected at least 1 argument, got {la}")
        else:
            raise TypeError(f"slice expected at most 2 arguments, got {la}")
    assert len(lower) == len(upper) == len(TT.dimensions)
    lower, upper = np.ascontiguousarray(lower), np.ascontiguousarray(upper)
    assert np.all(lower < upper)
    assert np.all(upper <= TT.dimensions)
    diff = (upper-lower).tolist()

    # TODO there should be a more elegant way to slice TTTensors!
    tmp = xe.TTTensor(TT)
    for pos in range(TT.order()):
        tmp.move_core(pos)
        cmp = np.ascontiguousarray(tmp.get_component(pos))
        cmp[:, :lower[pos], :] = 0
        cmp[:, upper[pos]:, :] = 0
        tmp.set_component(pos, xe.Tensor.from_buffer(cmp))
    tmp.move_core(0)

    TT_slice = xe.TTTensor.random(diff, tmp.ranks())
    for pos in range(TT.order()):
        cmp = np.ascontiguousarray(tmp.get_component(pos))
        cmp = np.array(cmp[:, lower[pos]:upper[pos], :])
        TT_slice.set_component(pos, xe.Tensor.from_buffer(cmp))

    return TT_slice


def cut(core_list, dims):
    """ Same as project, but for list of numpy arrays.

    Cut a TTTensor to lower dimension. Does not handle any exceptions.
    """
    sliced = []
    for k, core in enumerate(core_list):
        assert dims[k] <= core.shape[1]
        sliced.append(core[:, :dims[k], :])
    return sliced


def project(TT, dims):
    """ Project a TTTensor to lower dimension.

    NOTE: slice_TT throws an error if dims as entries greater then
          TT.dimensions.
    """
    for j, d in enumerate(TT.dimensions):
        if d < dims[j]:
            dims[j] = d
    return slice(TT, dims)


def add_dimension_left(TT):
    """ Add 1D core to the left of TT.

    Insert [[[1]]] as the 0th component of a TTTensor.
    """
    ret = xe.TTTensor([1]+TT.dimensions)
    ret.set_component(0, xe.Tensor.from_buffer(np.array([[[1]]])))
    for j in range(len(TT.dimensions)):
        ret.set_component(j+1, TT.get_component(j))
    return ret


def dofs(TT):
    """ Wrapper to get TT-DoFs of TTTensor. """
    return TT.degrees_of_freedom(TT.dimensions, TT.ranks())


def dofs_nnz(TT, EPS=1e-08):
    """ Compute number of effective non-zero entries of TTTensor. """
    cores = [np.abs(core)/xe.frob_norm(TT) for core in convert(TT)]
    return np.sum([np.count_nonzero(core[core >= EPS]) for core in cores])


def prolong_tt(TT, dims):
    """ Add more stochastic modes and larger mode dimensions to TTTensor.
    """
    ret = xe.TTTensor(dims)
    for idx in range(TT.order()):
        cmp = TT.get_component(idx)
        cmp = prolong_tensor(cmp, dims[idx])
        ret.set_component(idx, cmp)
    for idx in range(TT.order(), len(dims)):
        cmp = xe.Tensor.dirac([1, dims[idx], 1], [0, 0, 0])
        ret.set_component(idx, cmp)
    return ret


def prolong_tensor(T, dim):
    """ Increase dimension of TTTensor core.

    Parameters
    ----------
    T : xerus.Tensor
        Core of xe.TTTensor.
    dim : int
        Dimension to increase core to.
    """
    assert len(T.dimensions) == 3
    assert dim >= T.dimensions[1]
    arr = to_ndarray(T)
    ret_arr = np.zeros((arr.shape[0], dim, arr.shape[2]))
    ret_arr[:, :T.dimensions[1], :] = arr
    return xe.Tensor.from_buffer(ret_arr)


def constant(const, dims):
    """Assemble constant TTTensor with [[[1]]] for all stochastic components.

    Parameters
    ----------
    const : float or array_like
        Constant for the first TT component.
    dims : list of int
        Stochastic dimensions.

    Returns
    -------
    ret : xe.TTTensor
    """
    if isinstance(const, float):
        const = np.array([const])
    const.shape = 1, -1, 1
    ret = [const]
    for j, d in enumerate(dims):
        core = np.eye(d, 1, k=0).reshape(1, -1, 1)
        ret.append(core)
    return convert(ret)


def to_ndarray(tensor):
    """ Convert xerus Tensor to numpy ndarray.

    Wrapper to handle conversion of tensors to numpy arrays for xerus legacy
    versons.

    Parameters
    ----------
    tensor : xe.Tensor
    """
    if isinstance(tensor, xe.Tensor):
        try:
            ret = tensor.to_ndarray()
        except AttributeError:
            ret = np.asarray(tensor)
    elif isinstance(tensor, np.ndarray):
        ret = tensor
    else:
        raise TypeError(f"unsupported type '{type(tensor)}'.")
    return ret


def convert(TT):
    """ Convert list of numpy cores to TTTensor/TTOperator and vice versa.

    Parameters
    ----------
    TT : xe.TTTensor or xe.TTOperator or list of array_like
    """
    if isinstance(TT, (xe.TTOperator, xe.TTTensor)):
        d = len(TT.ranks())+1
        # ret = [np.asarray(TT.get_component(j)) for j in range(d)]
        ret = [np.ascontiguousarray(to_ndarray(TT.get_component(j)))
               for j in range(d)]
    else:
        ndim = [c.order() if isinstance(c, xe.Tensor) else c.ndim for c in TT]
        for j in range(len(ndim)):
            assert ndim[j] == ndim[0]
        d = [c.dimensions[1] if isinstance(c, xe.Tensor) else c.shape[1]
             for c in TT]
        if ndim[0] == 4:
            q = [c.dimensions[2] if isinstance(c, xe.Tensor) else c.shape[2]
                 for c in TT]
        ret = xe.TTOperator(d+q) if ndim[0] == 4 else xe.TTTensor(d)
        for j, c in enumerate(TT):
            if isinstance(c, np.ndarray):
                core = xe.Tensor.from_buffer(np.ascontiguousarray(c))
            else:
                core = c
            ret.set_component(j, core)
    return ret


def concatenate_tensor(A, B, axis=0):
    """ Same as np.concatenate([A,B], axis=axis) for xe.Tensor objects A and B.
    """
    assert isinstance(A, xe.Tensor) and isinstance(B, xe.Tensor)
    assert A.order() == B.order()
    M = A.order()
    axis = axis % M
    assert all(A.dimensions[m] == B.dimensions[m] for m in range(M)
               if m != axis)
    ABdim = list(A.dimensions)
    ABdim[axis] += B.dimensions[axis]
    AB = xe.Tensor(ABdim)
    offset = [0]*M
    AB.offset_add(A, offset)
    offset[axis] = A.dimensions[axis]
    AB.offset_add(B, offset)
    return AB


def inner_TT_stoch(TT1, TT2, dist):
    """ Inner product of two TTTensors.

    Compute inner product (over stochastic dimensions) of two TTTensors in
    Legendre/Hermite basis, i.e.
                            < TT1, TT2 >_{L^2(Gamma)}.
    Note that for f(y) = g(y)*h(y) = sum_m f_m P_m(y), we get
        < g, h > = sum_m f_m E[P_m] = f_0.
    For more detail, see compute_product_coeff().
    """
    # NOTE: Since we use compute_product_coeff() for the computation of the
    #       inner product (in which no triple products of polynomial appear),
    #       we need to work around the base change tensor kappa. We are using,
    #       that
    #           int P_a*P_b dy = int P_a*P_b*1 dy = int P_a*P_b*L_0 dx
    #       where L_0 is the unnormalized Legendre polynomial of deg 0.
    normalized = [True, True, True]
    mu = [0]*max(len(TT1), len(TT2))
    return compute_product_coeff(TT1, TT2, mu, dist, normalized)


def compute_product_coeff(TT1, TT2, mu, dist, normalized):
    """ Compute mean of product of two TTTensors.

    Compute product coefficient of (the stochastic dimensions of) two
    TTTensors, i.e. for f=g*h, compute
        f_mu = sum_{a,b} g_a * h_b * int_{-1}^{1} P_a P_b P_mu dy
             = sum_{a,b} g_a * h_b * kappa_{a,b,mu}
    for (normalized) Legendre/Hermite polynomials P_n of degree n.

    Parameters
    ----------
    TT1 : list of length L with components as TTTensor.get_component() or array
        Each component has shape (s_l, q_l, s_{l+1}) for l = 1, ..., L 
        and s_L = 1.
    TT2 : list of length L with components as TTTensor.get_component() or array
        Each component has shape (r_m, d_m, r_{m+1}) for m = 1, ..., M 
        and r_M = 1.
    mu : list, length is max{len(TT1), len(TT2)}
        list with polynomial degrees to integrate (i.e. kappa[:,:,mu[j]]).
        If None, default is set to [0, ..., 0].
    dist : string
        String describing the distribution type of the stochastic variables.
        If 'uniform', Legendre polynomials are used, if 'normal', HermiteE
        polynomials are used.
    normalized : list of bool, length is 3
        List to specify which components of the triple product tensor are
        for normalized polynomials and which are not.

    Returns
    -------
    ret : array_like, shape (s_1, r_1)
    """
    # NOTE: Assume w.l.o.g. (within the comments), that L > M, i.e.
    # TT1
    #     ranks (s_1, ..., s_M, ..., s_L, 1)
    #     dimensions (q_1, ..., q_M, ..., q_L)
    # TT2
    #     ranks (r_1, ..., r_M, 1)
    #     dimensions (d_1, ..., d_M)

    # check which TTTensor has more components
    if len(TT1) >= len(TT2):
        long_tt = [np.ascontiguousarray(cmp) for cmp in TT1]
        short_tt = [np.ascontiguousarray(cmp) for cmp in TT2]
    else:
        long_tt = [np.ascontiguousarray(cmp) for cmp in TT2]
        short_tt = [np.ascontiguousarray(cmp) for cmp in TT1]
    if mu is None:
        mu = [0]*len(long_tt)
    assert len(mu) == len(long_tt)

    ret = np.array([[1]])  # initial shape is (s_{L+1}, 1) = (1, 1)

    # compute inner product over higher order components (only long_tt contributions)
    for l in reversed(range(len(short_tt), len(long_tt))):  # for l = L, ..., M+1
        # shape (s_l, q_l, s_{l+1})
        core = np.ascontiguousarray(long_tt[l])
        if l == len(long_tt)-1:  # if l = L
            # shape (s_L, 1)
            ret = core[:, mu[l], :].reshape(-1, 1, order="F")
            assert ret.shape[0] == core.shape[0]
            assert ret.shape[1] == 1
            continue  # jump to next iteration
        # shape of core (in this iteration) is (s_l, q_l, s_{l+1})
        # shape of ret (in this iteration) is (s_{l+1}, 1)
        # new shape (s_l, 1)
        ret = np.einsum('ij, jk->ik', core[:, mu[l], :], ret)
    # ret shape is now (s_{M+1},1)

    # compute inner product over lower order components (both tt have contributions)
    for m in reversed(range(len(short_tt))):  # for m = M, ..., 1
        # ret shape is (s_{m+1}, r_{m+1})
        # shape (s_m, q_m, s_{m+1})
        long_core = np.ascontiguousarray(long_tt[m])
        # shape (r_m, d_m, r_{m+1})
        short_core = np.ascontiguousarray(short_tt[m])
        assert short_core.shape[2] == ret.shape[1]

        # shape (s_m, q_m, r_{m+1})
        r_core = np.einsum('ijk, kl->ijl', long_core, ret)

        # NOTE: Reconstruction uses normalized polynomials, hence kappe needs
        #       to be computed for normalized polynomials
        kappa = triple_product_tensor(  # shape (d_m, n_m, q_m)
            short_core.shape[1], mu[m] + 1, long_core.shape[1],
            dist, normalized=normalized)
        assert kappa.shape[0] == short_core.shape[1]
        assert kappa.shape[2] == long_core.shape[1]

        # shape (s_m, d_m, n_m, r_{m+1})
        cmp = np.einsum('ijk, lmj->ilmk', r_core, kappa)
        # shape (s_m, r_m, n_m)
        cmp = np.einsum('ijk, ajlk->ail', short_core, cmp)
        # new shape (s_m, r_m)
        ret = cmp[:, :, mu[m]]
    # ret shape is now (s_1, r_1)

    if len(TT1) < len(TT2):
        ret = ret.T
    return ret


def compute_product_coeff_tensor(TT1, TT2, dist, normalized=[True, True, True],
                                 project_dims=None):
    """ Compute full product tensor of two TTTensor.

    Compute product coefficient tensor of two TTTensors, i.e. compute F for
        F[mu] = f_mu given by compute_product_coeff(TT1, TT2, mu).
    This is for given stochastic dimension cores of TTTensors.

    Parameters
    ----------
    TT1 : list of length L with components as TTTensor.get_component() or array
        Each component has shape (s_l, q_l, s_{l+1}) for l = 1, ..., L
        and s_L = 1.
    TT2 : list of length L with components as TTTensor.get_component() or array
        Each component has shape (r_m, d_m, r_{m+1}) for m = 1, ..., M
        and r_M = 1.
    dist : string
        String describing the distribution type of the stochastic variables.
        If 'uniform', Legendre polynomials are used, if 'normal', HermiteE
        polynomials are used.

    Returns
    -------
    ret  : list of length L with components as arrays
        Each component has shape (r_l*s_l, d_l+q_l, r_{l+1}*s_{l+1}), with
        r_l = 1 and d_l = 0 for l > M.
    """
    # NOTE: Assume w.l.o.g. (within the comments), that L > M, i.e.
    # TT1
    #     ranks (s_1, ..., s_M, ..., s_L, 1)
    #     dimensions (q_1, ..., q_M, ..., q_L)
    # TT2
    #     ranks (r_1, ..., r_M, 1)
    #     dimensions (d_1, ..., d_M)

    # check which tttensor has more components
    if len(TT1) >= len(TT2):
        long_tt = [np.ascontiguousarray(cmp) for cmp in TT1]
        short_tt = [np.ascontiguousarray(cmp) for cmp in TT2]
    else:
        long_tt = [np.ascontiguousarray(cmp) for cmp in TT2]
        short_tt = [np.ascontiguousarray(cmp) for cmp in TT1]

    cmps_list = []

    max_ltt = np.max([core.shape[1] for core in long_tt])
    max_stt = np.max([core.shape[1] for core in short_tt])
    KAPPA = triple_product_tensor(max_stt, max_stt+max_ltt-1, max_ltt,
                                  dist, normalized=normalized)

    # compute inner product over higher order components (only long_tt contributions)
    for l in reversed(range(len(short_tt), len(long_tt))):  # for l = L, ..., M+1
        # each of these components has shape (s_l, q_l, s_{l+1})
        cmps_list.append(np.ascontiguousarray(long_tt[l]))

    # compute inner product over lower order components (both tt have contributions)
    for m in reversed(range(len(short_tt))):  # for m = M, ..., 1
        # ret shape is (s_{m+1}, r_{m+1})
        # shape (s_m, q_m, s_{m+1})
        long_core = np.ascontiguousarray(long_tt[m])
        # shape (r_m, d_m, r_{m+1})
        short_core = np.ascontiguousarray(short_tt[m])
        d_m, q_m = short_core.shape[1], long_core.shape[1]
        # NOTE: Reconstruction uses normalized polynomials, hence kappa needs
        #       to be computed for normalized polynomials
        dim = project_dims[m] if project_dims is not None else d_m+q_m-1
        # shape (d_m, d_m+q_m, q_m) or (d_m, project_dims[m], q_m)
        kappa = KAPPA[:d_m, :dim, :q_m]
        assert kappa.shape[0] == short_core.shape[1]
        assert kappa.shape[2] == long_core.shape[1]

        lc_l, lc_d, lc_r = long_core.shape
        sc_l, sc_d, sc_r = short_core.shape
        k_d1, k_d2, k_d3 = kappa.shape
        assert k_d3 == lc_d and k_d1 == sc_d

        # this is faster than np.einsum("pqd,idj->ipqj", kappa, long_core)
        K = np.reshape(kappa, (k_d1*k_d2, k_d3))
        LC = np.reshape(np.swapaxes(long_core, 0, 1), (lc_d, lc_l*lc_r))
        cmp = np.matmul(K, LC)
        cmp = np.moveaxis(np.reshape(cmp, (k_d1, k_d2, lc_l, lc_r)), 2, 0)

        # this is faster than np.einsum("kpl,ipqj->ikqjl", short_core, cmp)
        SC = np.reshape(np.swapaxes(short_core, 1, 2), (sc_l*sc_r, sc_d))
        cmp = np.reshape(np.swapaxes(cmp, 0, 1), (k_d1, lc_l*k_d2*lc_r))
        cmp = np.matmul(SC, cmp)
        cmp = np.moveaxis(np.reshape(cmp, (sc_l, sc_r, lc_l, k_d2, lc_r)),
                          [0, 1, 2, 3, 4], [1, 4, 0, 2, 3])

        # shape (s_m*r_m, d_m+q_m, s_{m+1}*r_{m+1})
        cmp = np.reshape(cmp, (lc_l*sc_l, k_d2, lc_r*sc_r), order="F")

        # each of these components has shape (s_m*r_m, q_m+d_m, s_{m+1}*r_{m+1})
        cmps_list.append(cmp)

    cmps_list = list(reversed(cmps_list))  # reorder components

    # check if components have correct shape
    for j, cmp in enumerate(cmps_list):
        r_left, r_right = long_tt[j].shape[0], long_tt[j].shape[2]
        d = long_tt[j].shape[1]
        if j < len(short_tt):
            r_left *= short_tt[j].shape[0]
            r_right *= short_tt[j].shape[2]
            d += short_tt[j].shape[1]-1
        if project_dims is not None:
            d = min(d, project_dims[j])
        assert cmp.shape[0] == r_left
        assert cmp.shape[1] == d
        assert cmp.shape[2] == r_right

    return cmps_list


def low_rank_add_operator(S, L, R):
    """ Add TTOperators with rank independent of the number of operators.

    Compute the sum of M TTOperators where each individual term
    has the form
        T_j = L[1] * ... * L[j-1] * S[j] * R[j+1] * ... * R[M],
    such that the resulting TT object has rank 2r, if r is the rank of the T_j.
    This is done by constructing the cores of the sum
        W = sum_{j=1,...,M} T_j = W[1] * ... * W[M]
    to be
        W[1] = [[ S[1,1] L[1] ]]
        W[m] = [[   R[m]    0 ],
                [ S[m,m] L[m] ]]
        W[M] = [[   R[M] ],
                [ S[M,M] ]].

    Parameters
    ----------
    S : list of array_like, first entry may be xe.Tensor
        Diagonal cores of the TTOperators T_j, i.e. S[1], ..., S[M].
    L : list of array_like, first entry may be xe.Tensor
        Left cores of the TTOperators T_j, i.e. L[1], ..., L[M]. Note that L[M]
        is never used.
    R : list of array_like, first entry may be xe.Tensor
        Right cores of the TTOperators T_j, i.e. R[1], ..., R[M]. Note that R[1]
        is never used.
    """
    assert isinstance(S, list) and isinstance(L, list) and isinstance(R, list)
    assert len(S) == len(L) == len(R)
    ret = []
    for j in range(len(S)):
        if len(S) == 1:
            if isinstance(S[0], xe.Tensor):
                assert S[0].dimensions[0] == S[0].dimensions[3] == 1
            else:
                assert S[0].shape[0] == S[0].shape[3] == 1
            ret.append(S[0])
        else:
            if j == 0:
                if isinstance(S[0], xe.Tensor) and isinstance(L[0], xe.Tensor):
                    # shape ( 1, d_1, q_1, 2*r_2 )
                    core = concatenate_tensor(S[0], L[0], axis=-1)
                else:
                    # shape ( 1, d_1, q_1, 2*r_2 )
                    core = np.concatenate([S[0], L[0]], axis=-1)
            elif 0 < j < len(S)-1:
                O = np.zeros((*R[j].shape[:2], *L[j].shape[2:]))
                core = np.concatenate([                                         # shape ( 2*r_j, d_j, q_j, 2*r_{j+1} )
                    np.concatenate([R[j],    O], axis=-1),
                    np.concatenate([S[j], L[j]], axis=-1)
                ], axis=0)
            elif j == len(S)-1:
                # shape ( 2*r_M, d_M, q_M, 1 )
                core = np.concatenate([R[-1], S[-1]], axis=0)
            ret.append(core)
    return ret


def rank_one_outer_product(TT, TT2=None):
    """ Rank-1 outer product of TTTensors.

    Given two (rank-1) TTTensors TT and TT2, return the rank-1 TTOperator
        TT2 \otimes TT.
    If TT2 is None (default) use TT2 = TT, i.e. compute the outer
    product (TT \otimes TT) and if TT2 = 1, compute the outer product of TT
    with the constant 1 TTTensor, i.e. transform TT to a TTOperator
    type with output dimension 1.
    """
    if TT2 is None:
        TT2 = xe.TTTensor(TT)
    if TT2 == 1:
        TT2 = xe.TTTensor([1]*TT.order())
        for j in range(TT2.order()):
            TT2.set_component(j, xe.Tensor.from_buffer(np.array([[[1]]])))
    assert TT.order() == TT2.order()
    assert np.all(np.asarray(TT.ranks()) == 1)
    assert np.all(np.asarray(TT2.ranks()) == 1)

    TT_cores = [TT.get_component(j) for j in range(TT.order())]
    TT2_cores = [TT2.get_component(j) for j in range(TT.order())]

    OP = xe.TTOperator(TT2.dimensions+TT.dimensions)
    i, k, l1, l2, r1, r2 = xe.indices(6)
    for j in range(TT.order()):
        ret = xe.Tensor()
        ret(l1, l2, i, k, r1, r2) << TT_cores[j](
            l1, i, r1) * TT2_cores[j](l2, k, r2)
        ret.reinterpret_dimensions([1, TT2.dimensions[j], TT.dimensions[j], 1])
        OP.set_component(j, ret)

    return OP


def transpose_TTOperator_core(core):
    """Compute core.T.

    Parameters
    ----------
    core : np.ndarray or xe.Tensor
        Order-4 tensor (core) for transposing.
    """
    if isinstance(core, np.ndarray):
        assert core.ndim == 4
        ret = np.swapaxes(core, 1, 2)
    elif isinstance(core, xe.Tensor):  # TODO: is this working?
        ret = xe.Tensor()
        r, i, j, l = xe.indices(4)
        ret(r, i, j, l) << core(r, j, i, l)
    return ret


def symmetrize_TTOperator_core(core):
    """ Compute core.T @ core.

    Symmetrize a TTOperator core with shape (L,I,J,R), i.e. compute
        (C.T @ C)[l3,j,k,r3] = sum_{i} C[l1,i,j,r1] * C[l2,i,k,r2],
    where l3 = l1*L + l2 and r3 = r1*R + r2.
    NOTE: Returns same type as input.

    Parameters
    ----------
    core : np.ndarray or xe.Tensor of shape (L,I,J,R)
        Order-4 tensor (core) for symmetrization.
    """
    if isinstance(core, np.ndarray):
        r, q, d, l = core.shape
        ret = np.einsum("igaj,kgbl->ikabjl", core, core)
        ret = ret.reshape(r**2, d, d, l**2)
    elif isinstance(core, xe.Tensor):
        dims = core.dimensions
        i, j, k, l1, l2, r1, r2 = xe.indices(7)
        ret = xe.Tensor()
        ret(l1, l2, j, k, r1, r2) << core(l1, i, j, r1) * core(l2, i, k, r2)
        ret.reinterpret_dimensions([dims[0]**2, dims[2], dims[2], dims[3]**2])
    elif core is None:
        ret = None
    return ret


def symmetrize_TTOperator(TT, use_xerus=False):
    """ Compute TT.T @ TT.

    For a given TTOperator TT, compute TT.T @ TT. If 'use_xerus',
    symmetrization is done with xerus, otherwise it is done manually with
    python.
    """
    if use_xerus:
        M = TT.order()//2
        dim = TT.dimensions[M:]*2
        ret = xe.TTOperator(dim)
        i, j, k = xe.indices(3)
        ret(i/2, j/2) << TT(k/2, i/2) * TT(k/2, j/2)
    else:
        TT_cores = convert(TT)
        ret_cores = [symmetrize_TTOperator_core(core) for core in TT_cores]
        ret = convert(ret_cores)
    return ret


def hermite_base_product_operator(dims, z, decay, s1, s2, sZeta,
                                  normalized=True):
    """ Compute a list of scaled Hermite base product matrices."""
    H_list = []
    for m, d in enumerate(dims):
        H_list.append(hermite_base_product_matrix(
            d-1, s1=s1, s2=s2, z=z, m=m+1, decay=decay, sZeta=sZeta,
            normalized=normalized))
    return H_list


def apply_basis_product_operator(TT_cores, rk1_cmp_list):
    """ Contract TT with rank-1 basis eval TTTensor.

    Apply a rank one basis product tensor operator component wise to a given tt
    tensor.

    Parameters
    ----------
    TT_cores     : list of array_like
        Stochastic components of the TTTensor the basis product tensor operator
        is applied to.
    rk1_cmp_list :
        Components of the basis product tensor operator.
    """
    assert len(TT_cores) == len(rk1_cmp_list)
    new_cores = deepcopy(TT_cores)
    for j, core in enumerate(TT_cores):
        assert core.shape[1] == rk1_cmp_list[j].shape[0]

    for j, core in enumerate(TT_cores):
        new_cores[j] = np.ascontiguousarray(
            np.einsum("ij,rjs->ris", rk1_cmp_list[j], core)
        )

    return new_cores


def save(file_name, TT):
    """ Save the numpy-array cores of a xe.TTTensor to a file.

    Parameter
    ---------
    file_name : string
        Filename the data are saved to.
    TT : xe.TTTensor or list of array_like
        TTTensor that is saved.
    """
    if isinstance(TT, (xe.TTTensor, xe.TTOperator)):
        TT_list = convert(TT)
    np.save(file_name, np.array(TT_list, dtype=object), allow_pickle=True)


def load(file_name):
    """ Load a TTTensor from numpy file created with tt_tools.save().
    """
    TT_list = list(np.load(file_name, allow_pickle=True))
    return convert(TT_list)
