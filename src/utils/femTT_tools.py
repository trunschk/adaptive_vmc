import numpy as np
import scipy.sparse
from scipy.sparse.linalg import eigsh

from fenics import *
import xerus as xe

import src.utils.fem_tools as fem
import src.utils.tt_tools as tt

from measurement_utils.measures import MeasurementList, BasisMeasure


def get_initial_guess(dims, init=None, coarse_space=None, fine_space=None):
    """ Prolongate FEM-TTTensor from coarse FE space to fine FE space.

    Generate an initial guess for the uq_ra_adf algorithm based on a
    reconstruction in a previous iteration. This is done by prolongation of
    stochastic components and/or interpolation of FE components if necessary.

    Parameters
    ----------
    dims : list
        Dimensions of the initial guess TTTensor. First entry is FE dimension.
    init : TTTensor
        Reconstruction of previous iteration that is prolongated. If None
        (default), return a random TTTensor with dimensions dims.
    coarse_space : dolfin.function.functionspace.FunctionSpace
        FE space on coarser grid (previous iteration). Must not be None if FE
        spaces of previous and current iteration are different.
    fine_space : dolfin.function.functionspace.FunctionSpace
        FE space on fine grid (current iteration). Must not be None if FE
        spaces of previous and current iteration are different.
    """

    # return random initial tensor if init not specified
    if init is None:
        return xe.TTTensor.random(dims, [1]*(len(dims)-1))

    assert init.order() <= len(dims)
    for j, d in enumerate(init.dimensions):
        assert d <= dims[j]
    initial_guess = prolong_tt(init, [init.dimensions[0]] + dims[1:])

    if init.dimensions[0] < dims[0]:  # interpolate FEM space
        assert coarse_space is not None and fine_space is not None
        init_core = np.ascontiguousarray(
            init.get_component(0))                 # shape (1, N, r1)
        init_core = fem.prolongate_x_2(
            init_core[0].T, coarse_space, fine_space)    # shape (r1, \hat N)
        init_core = np.ascontiguousarray(np.expand_dims(
            init_core.T, axis=0))   # shape (1, \hat N, r1)
        initial_guess.set_component(0, xe.Tensor.from_buffer(init_core))
    initial_guess.canonicalize_left()

    return initial_guess


def multiply(TT1, TT2, project_dims=None):
    """ Compute the product of two FEM-TTTensors and clip dimensions.
    """
    assert TT1.dimensions[0] == TT2.dimensions[0]
    ret = tt.convert(TT1)
    TT1_fem, TT1_tail = ret[0], ret[1:]
    ret = tt.convert(TT2)
    TT2_fem, TT2_tail = ret[0], ret[1:]

    K = fem.compute_interpolation_tensor(TT1.dimensions[0])

    # det. components
    assert TT1_fem.shape[0] == 1 and TT2_fem.shape[0] == 1
    ret_fem = xe.Tensor()
    i, j, k, l, r = xe.indices(5)
    l1, N, r1 = TT1_fem.shape
    l2, N, r2 = TT2_fem.shape
    TT1_fem = xe.Tensor.from_buffer(TT1_fem.reshape(l1*N, r1))
    TT2_fem = xe.Tensor.from_buffer(TT2_fem.reshape(l2*N, r2))
    ret_fem(k, l, r) << TT1_fem(i, l) * TT2_fem(j, r) * K(i, j, k)
    ret_fem = [tt.to_ndarray(ret_fem).reshape(l1*l2, N, r1*r2)]

    # stoch. components
    tail = tt.compute_product_coeff_tensor(TT1_tail, TT2_tail, "normal",
                                           project_dims=project_dims)

    # join det. + stoch. cmp together
    assert tail[0].shape[0] == r1*r2
    ret = tt.convert(ret_fem + tail)

    return ret


def power(TT1, n, eps=None, project_dims=None):
    """ Compute the n-th power of a FEM-TTTensor.

    Parameters
    ----------
    TT1 : xerus TTTensor
        TTTensor to potentiate.
    n   : int
        Power the TTTensor.
    eps : float
        Round power of TTTensor in each step to precision eps. Default is None.
    """
    n = int(n)
    assert n > 0
    ret = xe.TTTensor(TT1)
    for j in range(1, n):
        assert all(not np.any(np.isnan(tt.to_ndarray(ret.get_component(pos))))
                   for pos in range(ret.order()))
        ret = multiply(ret, TT1, project_dims=project_dims)
        assert all(not np.any(np.isnan(tt.to_ndarray(ret.get_component(pos))))
                   for pos in range(ret.order()))
        if eps is not None:
            ret.round(eps)
        assert all(not np.any(np.isnan(tt.to_ndarray(ret.get_component(pos))))
                   for pos in range(ret.order()))
    return ret


def compute_L2L2_norm(ys, vals, space, ws=None):
    """L2-L2 norm.

    Compute the L2-L2 norm of a set of FE-coefficient samples.

    Parameters
    ----------
    ys : array_like
        Parameter samples.
    vals : array_like
        Function samples (FE coefficients).
    space : fenics.FunctionSpace
        FE space.
    ws : array_like, default = None
        MC integral weights. If None, use uniform weights.

    Returns
    -------
    ret : float
        L2-L2 norm.

    """
    assert vals.shape[1] == space.dim()
    if ws is None:
        ws = np.ones(vals.shape[0]) / vals.shape[0]
    assert ws.ndim == 1 and ws.size == vals.shape[0] == ys.shape[0]
    mass_mat = fem.mass_matrix(space=space)
    ret = np.einsum('n,ni,in', ws, vals, mass_mat@vals.T)
    return np.sqrt(ret)


def compute_L2Linf_norm(ys, vals, ws=None):
    """L2-Linf norm.

    Compute the L2-Linf norm of a set of FE-coefficient samples. This is done
    by simply taking the largest coefficient value. Hence, this method is exact
    for DG-0 and CG-1 space, but only an approximation of the deterministic
    Linf norm for higher CG elements.

    Parameters
    ----------
    ys : array_like
        Parameter samples.
    vals : array_like
        Function samples (FE coefficients).
    space : fenics.FunctionSpace
        FE space.
    ws : array_like, default = None
        MC integral weights. If None, use uniform weights.

    Returns
    -------
    ret : float
        L2-Linf norm.
    """
    if ws is None:
        ws = np.ones(vals.shape[0]) / vals.shape[0]
    assert ws.ndim == 1 and ws.size == vals.shape[0] == ys.shape[0]
    ret = np.sum(ws*np.max(np.abs(vals), axis=1).flatten())
    return ret


def compute_L2H1_norm(ys, vals, space, ws=None, bc=None):
    """L2-L2 norm.

    Compute the L2-L2 norm of a set of FE-coefficient samples.

    Parameters
    ----------
    ys : array_like
        Parameter samples.
    vals : array_like
        Function samples (FE coefficients).
    space : fenics.FunctionSpace
        FE space.
    ws : array_like, default = None
        MC integral weights. If None, use uniform weights.
    bc : fenics.BoundaryCondition, default = None
        Boundary conditions for the stiffness matrix.

    Returns
    -------
    ret : float
        L2-H1 norm.

    """
    assert vals.shape[1] == space.dim()
    if ws is None:
        ws = np.ones(vals.shape[0]) / vals.shape[0]
    assert ws.ndim == 1 and ws.size == vals.shape[0] == ys.shape[0]
    stiff_mat = fem.stiffness_matrix(space=space, bc=bc)
    ret = np.einsum('n,ni,in', ws, vals, stiff_mat@vals.T)
    return np.sqrt(ret)


def compute_L2L2_error(reco, ys, ws, vals, space, dist, scale=0, decay=-1):
    """ Compute L2-L2 norm of approximation error.

    Parameters
    ----------
    reco : TTTensor
        TTTensor of approximation.
    ys : array_like
        Parameter samples.
    ws : array_like
        MC integral weights.
    vals : array_like
        Function samples (FE coefficients).
    space : fenics.FunctionSpace
        FE space.
    dist : string
        Parameter distribution, ('normal', 'uniform')
    scale : float, default = 0
        Scaling for scaled Hermite basis the coefficients are given in.
        Defaults to no scalining, i.e. standard Hermite basis.
    decay : float, default = -1
        Decay rate of affine diffusion coefficient. If scale != 0, decay has
        to be greater than zero.
    """

    if scale != 0:
        assert decay > 0
    approx = tt.eval_approx(reco, ys, dist, scale=scale, decay=decay)
    assert vals.shape == approx.shape
    diff = vals - approx
    return compute_L2L2_norm(ys, diff, space, ws=ws)


def compute_L2Linf_error(reco, ys, ws, vals, dist, scale=0, decay=-1):
    """ Compute L2-Linf norm of approximation error.

    Parameters
    ----------
    reco : TTTensor
        TTTensor of approximation.
    ys : array_like
        Parameter samples.
    ws : array_like
        MC integral weights.
    vals : array_like
        Function samples (FE coefficients).
    dist : string
        Parameter distribution, ('normal', 'uniform')
    scale : float, default = 0
        Scaling for scaled Hermite basis the coefficients are given in.
        Defaults to no scalining, i.e. standard Hermite basis.
    decay : float, default = -1
        Decay rate of affine diffusion coefficient. If scale != 0, decay has
        to be greater than zero.
    """
    assert ys.shape[0] == vals.shape[0]
    if scale != 0:
        assert decay > 0
    approx = tt.eval_approx(reco, ys, dist, scale=scale, decay=decay)
    assert vals.shape == approx.shape
    diff = vals - approx
    return compute_L2Linf_norm(ys, diff, ws=ws)


def compute_L2H1_error(reco, ys, ws, vals, space, dist, bc=None, scale=0,
                       decay=-1):
    """ Compute L2-L2 norm of approximation error.

    Parameters
    ----------
    reco : TTTensor
        TTTensor of approximation.
    ys : array_like
        Parameter samples.
    ws : array_like
        MC integral weights.
    vals : array_like
        Function samples (FE coefficients).
    space : fenics.FunctionSpace
        FE space.
    dist : string
        Parameter distribution, ('normal', 'uniform')
    bc : fenics.BoundaryCondition, default = None
        Boundary conditions for the stiffness matrix.
    scale : float, default = 0
        Scaling for scaled Hermite basis the coefficients are given in.
        Defaults to no scalining, i.e. standard Hermite basis.
    decay : float, default = -1
        Decay rate of affine diffusion coefficient. If scale != 0, decay has
        to be greater than zero.
    """

    if scale != 0:
        assert decay > 0
    approx = tt.eval_approx(reco, ys, dist, scale=scale, decay=decay)
    assert vals.shape == approx.shape
    diff = vals - approx
    return compute_L2H1_norm(ys, diff, space, ws=ws, bc=bc)


def prolongate(TT, coarse_space, fine_space):
    """ Prolongate spatial component of femTT.

    Use fem_tools.prolongate_x_2() to prolongate spatial component of TTTensor.

    Parameters
    ----------
    TT : xe.TTTensor
        TTTensor with FE component in first core.
    space_old : fenics.FunctionSpace
        Space of TT FE-core.
    space_new : fenics.FunctionSpace
        New space for TT FE-core.

    Returns
    -------
    TTnew : xe.TTTensor
        TTTensor with prolongated first core.
    """
    tt_cores = tt.convert(TT)
    u_vec = tt_cores[0][0].T  # shape (r_1, N)
    u_vec = fem.prolongate_x_2(u_vec, coarse_space, fine_space).T
    tt_cores[0] = np.expand_dims(u_vec, axis=0)  # shape (1, N_new, r_1)
    return tt.convert(tt_cores)


def round(TT, EPS=1e-16):
    """Round FEM TTTensor/TTOperator.

    Round all stochastic cores using truncated SVD but keep first (FEM)
    component untouched to maintain sparsity.

    Parameters
    ----------
    TT : TTTensor or TTOperator
        TTTensor/TTOperator with FEM component.
    EPS : float, default = 1e-16
        Precision for truncated SVD.

    Returns
    -------
    ret : TTperator or TTTensor
    """
    if isinstance(TT, xe.TTTensor):
        ret = xe.TTTensor(TT.dimensions)
        order = TT.order()
    elif isinstance(TT, xe.TTOperator):
        ret = xe.TTOperator(TT.dimensions)
        order = len(TT.ranks())+1
    else:
        raise NotImplementedError(f"unknown input type: {type(TT)}")

    # do not touch first component
    ret.set_component(0, TT.get_component(0))

    # round all but the first component
    rounded_cores = []
    cores = [np.asarray(TT.get_component(j)) for j in range(1, order)]
    for m, core in enumerate(reversed(cores)):
        if m > 0:  # contract U@A to core (except for last component)
            if isinstance(TT, xe.TTOperator):
                core = np.einsum("lpqr,rs,s->lpqs", core, U[:, :idx], A[:idx])
            else:
                core = np.einsum("lpr,rs,s->lps", core, U[:, :idx], A[:idx])
        if m == len(cores)-1:  # no SVD for the second core
            rounded_cores.append(core)
        else:  # compute truncated SVD for core
            s = core.shape
            mat = core.reshape(s[0], np.prod(s[1:]))
            U, A, VT = np.linalg.svd(mat)
            idx = np.where(A <= EPS)[0][0]  # truncation index
            rounded_cores.append(VT[:idx].reshape(idx, *s[1:]))

    for j, core in enumerate(reversed(rounded_cores)):
        core = xe.Tensor.from_buffer(np.ascontiguousarray(core))
        ret.set_component(j+1, core)
    return ret


def change_basis_to_scaled_hermite(TT, decay, rho=1, vartheta=0.1, s=0):
    """ Change standard Hermite basis to scaled Hermite basis.

    Compute TT-coefficient w.r.t. the scaled Hermite polynomial basis required
    for the lognormal case.

    Parameters
    ----------
    TT : TTTensor
        TTTensor for the coefficients of a function in standard HermiteE basis.
    decay : float
        Decay rate of the diffusion coefficient a.
    rho : float
        Scaling measure. See lognormal paper for description.
    vartheta : float
        Convex-combination parameter. See lognormal paper for description.
    s : float, default = 0
        Scaling constant for Hermite basis that is changed to the vartheta*rho
        Hermite basis. Defaults to standard Hermite basis.
    """
    assert s >= 0
    TT_cores = tt.convert(TT)
    H_list = tt.hermite_base_product_operator(
        TT.dimensions[1:], z=1, decay=decay, s1=vartheta*rho, s2=s,
        sZeta=vartheta*rho, normalized=True)
    new_cores_tail = tt.apply_basis_product_operator(TT_cores[1:], H_list)
    TT_cores_new = [TT_cores[0]] + new_cores_tail
    return tt.convert(TT_cores_new)


def compute_gramian_eigenvalues(nodes, TT, dist, scale=0, decay=-1):
    """ Compute eigenvalues of empirical Gramian.

    Given multivariate samples and a TTTensor, compute the empirical
    (approximation of the) Gramian TT for each core component and return
    a list of eigenvalue arrays of those Gramians.

    Parameters
    ----------
    nodes : array_like
        Random y samples.
    TT: xe.TTTensor
        TTTensor with FE component the Gramian is computed with.
    dist : string ("uniform" or "normal")
        Distribution of the nodes.
    scale : float, default = 0
        Scaling of the basis polynomials (required for lognormal case).
    decay : float, default = -1
        Decay of affine diffusion coefficient (required for lognormal case).
        Needs to be positive if scale not 0, else irrelevant.

    Returns
    -------
    eigenvalues : list of ndarray
        List of Gramian eigenvalue vectors.
    """

    assert TT.order() > 1
    _TT = xe.TTTensor(TT)
    _TT.move_core(0)

    assert nodes.shape[0] == _TT.order()-1
    xdim, dims = _TT.dimensions[0], _TT.dimensions[1:]
    order = _TT.order()
    n_samples = nodes.shape[1]
    eigenvalues = []

    # generate basis evaluation list
    ml = MeasurementList(tt.get_basis_measures(
        dims, dist, scale=scale, decay=decay))
    assert ml.dimensions == tuple(dims), f"{ml.dimensions} vs {dims}"
    measures = ml(nodes)

    # initiate left stack and build right stack
    assert len(measures) == order - 1
    l_stack = [np.ones((n_samples, 1, 1))]
    r_stack = [np.ones((n_samples, 1))]
    for m in reversed(range(1, order)):
        # NOTE: r_stacks are ( P(y) @ u )_{m:M} with shape (#smpl, r_{m+1}).
        #       This is more storage-efficient then assembling the full stack.
        core = tt.to_ndarray(_TT.get_component(m))
        r_stack.append(np.einsum("ler, ne, nr -> nl",
                                 core, measures[m-1], r_stack[-1]))
    assert order == len(r_stack)

    # compute Gramian for FE component
    # NOTE: left stack is only ones aka left out
    G = np.einsum("nr, nk -> rk", r_stack[-1], r_stack[-1])
    eye = scipy.sparse.csr_matrix(np.eye(xdim))
    G = scipy.sparse.kron(eye, G) / n_samples
    # compute k closest eigenvalues to sigma=0 aka smallest eigenvalue
    ev, _ = eigsh(G, k=3, sigma=0)
    min_ev = np.min(np.abs(ev))
    eigenvalues.append(min_ev)

    # set l_stack for FE component
    # NOTE: Fist core of left stack is #smpl-times (core.T @ core). Since
    #       0th core is left orthogonal after core move, core.T @ core is
    #       the identity.
    _TT.move_core(1)
    _ = r_stack.pop()
    core = tt.to_ndarray(_TT.get_component(0))
    # NOTE: this is n-times identity by left-orthogonality of move_core(1)
    l_stack.append(np.einsum("nli, ldr, idk-> nrk", l_stack[-1], core, core))
    # NOTE: check if l_stack is identity in each component
    assert np.allclose(l_stack[-1] - np.eye(core.shape[2])[None], 0)
    assert len(l_stack) == 2 and len(r_stack) + 1 == order

    # assemble remaining Gramians and compute eigenvalues
    for m in range(1, order):
        assert m + 1 == len(l_stack) and len(r_stack) + m == order
        G = np.einsum("nli, ne, nd, nr, nk -> leridk",
                      l_stack[-1], measures[m-1], measures[m-1], r_stack[-1],
                      r_stack[-1])
        G = G.reshape(np.prod(G.shape[:3]), -1) / n_samples
        eigenvalues.append(np.linalg.eigvalsh(G))
        if m + 1 < order:  # update left and right stack
            _TT.move_core(m+1)
            _ = r_stack.pop()
            core = tt.to_ndarray(_TT.get_component(m))
            # NOTE: Left stack is full, i.e. contracts with stack and stack.T.
            stack = np.einsum("ler, ne -> nlr", core, measures[m-1])
            l_stack.append(np.einsum("nli, nlr, nik -> nrk",
                                     l_stack[-1], stack, stack))

    return eigenvalues
