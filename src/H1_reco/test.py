import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from fenics import Function, Constant, Mesh


mesh_size = 30
ord = 10


print("Loading problem ...")
from problem.darcy import Problem
info = {
    "problem": {"name": "darcy"},
    "fe": {
        "mesh": mesh_size,
        "degree": 1
    },
    "expansion": {
        "mean": 1.0,
        "scale": 6/np.pi**2,  # == 1/sum(1/m**2 for m in range(1, np.inf))
        "size": ord,
        "decay rate": 2.0
    },
    "sampling": {
        "distribution": "uniform" #TODO uniform? wenn unten lognormal steht?
        hier
    }
}
problem = Problem(info)


def mesh2triang(mesh):
    xy = mesh.coordinates()
    return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())


def fe_plot(obj):
    plt.gca().set_aspect('equal')
    if isinstance(obj, Function):
        mesh = obj.function_space().mesh()
        if (mesh.geometry().dim() != 2):
            raise(AttributeError)
        if obj.vector().size() == mesh.num_cells():
            C = obj.vector().array()
            plt.tripcolor(mesh2triang(mesh), C)
        else:
            C = obj.compute_vertex_values(mesh)
            plt.tripcolor(mesh2triang(mesh), C, shading='gouraud')
    elif isinstance(obj, Mesh):
        if (obj.geometry().dim() != 2):
            raise(AttributeError)
        plt.triplot(mesh2triang(obj), color='k')


def plot(x, name='sol.png'):
    f = plt.figure()
    u = Function(problem.space)
    u.vector()[:] = x
    fe_plot(u)
    plt.colorbar()
    plt.savefig(name)
    plt.close(f)


z = np.load('vmc_lognormal.0.npz')

plot(z['values'][400], 'value.400.png')
plot(z['approximation_h1'][-1][400], 'approx_h1.400.png')
