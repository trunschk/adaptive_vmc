# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
from coloring import set_BIMoS_style, alerted_text_fg, example_text_fg
set_BIMoS_style()
from glob import glob
import argparse


descr = ""
parser = argparse.ArgumentParser(description=descr)
parser.add_argument('PATH', help='path to the directory containing the `.npz` files with the paths of multiple optimization runs')
parser.add_argument('--Xnorm', default=2, help='the norm in X')
parser.add_argument('--Ynorm', default=2, help='the norm in Y')
args = parser.parse_args()
path = args.PATH
file_name = "{}/{{}}.npz".format(path)
to_num = lambda s: np.inf if s == "inf" else int(s)
NORM_X = to_num(args.Xnorm)
NORM_Y = to_num(args.Ynorm)
latex_inf = lambda p: str(p).replace("inf", r"\infty")


def error(values, reco_values, norm_x=2, norm_y=np.inf): # l2 in x and linf in y
    assert 1 <= norm_x < np.inf
    error = np.linalg.norm(reco_values-values, ord=norm_x, axis=1)**norm_x
    error = np.linalg.norm(error, ord=norm_y)**(1/norm_x)
    norm = np.linalg.norm(values, ord=norm_x, axis=1)**norm_x
    norm = np.linalg.norm(norm, ord=norm_y)**(1/norm_x)
    return error / norm


def plot_quantiles(values, errors, ax=None, num_quantiles=4, linewidth_fan=0, **kwargs):
    assert values.ndim == 1
    assert len(values) == errors.shape[1]
    if ax is None:
        ax = plt.gca()

    ps = np.linspace(0,1,2*num_quantiles+1)
    alphas = np.empty(num_quantiles)
    alphas[0] = 2*ps[0]
    for i in range(1, num_quantiles):
        alphas[i] = 2*(ps[i] - ps[i-1])/(1 - 2*ps[i-1])
    alphas *= kwargs.get('alpha', 1)

    qs = np.quantile(errors, ps, axis=0)
    zorder = kwargs.pop('zorder', 1)
    base_line, = ax.plot(values, qs[num_quantiles], zorder=zorder+num_quantiles, **kwargs)
    for e in range(num_quantiles):
        ax.fill_between(values, qs[e], qs[-1-e], color=base_line.get_color(), alpha=alphas[e], zorder=zorder+e, linewidth=linewidth_fan)


ns = None
errs_l2 = []
errs_h1 = []
for file in glob(file_name.format("*")):
    print("Loading: {}".format(file))
    z = np.load(file)
    if ns is None:
        ns = z['samples']
    else:
        assert np.all(ns == z['samples'])
    values = z['values']
    errs_l2.append([error(values, appr, NORM_X, NORM_Y) for appr in  z['approximation_l2']])
    errs_h1.append([error(values, appr, NORM_X, NORM_Y) for appr in  z['approximation_h1']])
errs_l2 = np.array(errs_l2)
errs_h1 = np.array(errs_h1)

f = plt.figure()

num_quantiles = 80
plot_quantiles(ns, errs_l2, num_quantiles=num_quantiles, label="$L^2(\mathcal{X}) \otimes L^2(\mathcal{Y})$-Loss", color=alerted_text_fg)
plot_quantiles(ns, errs_h1, num_quantiles=num_quantiles, label="$L^2(\mathcal{X}) \otimes H^1(\mathcal{Y})$-Loss", color=example_text_fg)

plt.xlim(ns[0], ns[-1])
plt.yscale('log')
plt.legend()
plt.title("$L^{}(\mathcal{{X}}) \otimes L^{}(\mathcal{{Y}})$-Error".format(NORM_X, latex_inf(NORM_Y)))
plt.tight_layout()
file_name = "{}/errors_L{}X_L{}Y.png".format(path, NORM_X, NORM_Y)
print("Saving: '{}'".format(file_name))
plt.savefig(file_name, dpi=100, facecolor=fig.get_facecolor(), edgecolor='none')
plt.close(f)
