import numpy as np
import matplotlib as mpl


def mix(*color_value_list):
    colorvec = lambda c: np.array(mpl.colors.to_rgba(c))
    assert len(color_value_list) > 0
    c1 = colorvec(color_value_list[0])
    if len(color_value_list) == 1:
        return c1
    v = color_value_list[1]/100
    assert isinstance(v, float)
    if len(color_value_list) > 2:
        c2 = colorvec(color_value_list[2])
    else:
        c2 = 1
    c3 = v*c1 + (1-v)*c2
    return mix(c3, *color_value_list[3:])


# \definecolor{mDarkTeal}{HTML}{23373B}
# \definecolor{bimosred}{rgb}{.65,0,0}
# \definecolor{bimosyellow}{HTML}{FCD667}
bimosblack = "#23373B"
bimosred = (.65,0,0)
bimosyellow = "#FCD667"
# \setbeamercolor{normal text}{fg=mDarkTeal, bg=bimosyellow!50!bimosred!8}
# \setbeamercolor{alerted text}{fg=bimosred!80!bimosyellow}
# \setbeamercolor{example text}{fg=bimosred!50!bimosyellow!80}
normal_text_fg = bimosblack
normal_text_bg = mix(bimosyellow, 50, bimosred, 8)
alerted_text_fg = mix(bimosred, 80, bimosyellow)
example_text_fg = mix(bimosred, 50, bimosyellow, 80)
# \setbeamercolor{block title}{fg=normal text.fg, bg=normal text.bg!80!fg}
# \setbeamercolor{block body}{bg=block title.bg!50!normal text.bg}
block_title_bg = mix(normal_text_bg, 80, bimosblack)
block_body_bg = mix(block_title_bg, 50, normal_text_bg)


def set_BIMoS_style():
    mpl.rc('figure', facecolor=normal_text_bg, edgecolor=normal_text_bg)
    mpl.rc('axes', facecolor=block_body_bg, edgecolor=bimosblack, labelcolor=normal_text_fg)
    mpl.rc('xtick', color=normal_text_fg)
    mpl.rc('ytick', color=normal_text_fg)


# BIMoSmap = mpl.colors.LinearSegmentedColormap.from_list('mycolors',[normal_text_fg, alerted_text_fg, example_text_fg])
BIMoSmap = mpl.colors.LinearSegmentedColormap.from_list('BIMoS', [normal_text_fg, bimosred, example_text_fg])
