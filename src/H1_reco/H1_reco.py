# coding: utf-8
from __future__ import division, print_function
#TODO: Note: for the embedding of W^{k,2} into L^\infty in n dimensions you need k > n/2.
#      That means you need to minimize in the H^{n/2} norm.

import numpy as np
import xerus as xe

from parallel import Parallel
from measurement_utils.samplers import constant, Uniform, CartesianProductSampler
from measurement_utils.bases import LegendrePolynomials, gramian, scipy_integral, L2inner, BasisFunction
from measurement_utils.measures import BasisMeasure, MeasurementList, IdentityMeasure
from functools import partial

import matplotlib.pyplot as plt
import matplotlib.tri as tri
from matplotlib.colors import SymLogNorm

import os, time
from contextlib import contextmanager
from glob import glob

print_old = print
ts = lambda: time.strftime("[%Y-%m-%d %H:%M:%S]", time.localtime())
print = lambda *args, **kwargs: print_old(ts(), *args, **kwargs)
transpose = lambda ls: list(zip(*ls))

@contextmanager
def timeit(title):
    try:
        t0 = time.process_time()
        yield
    finally:
        print(title.format(time.process_time()-t0))


mesh_size = 30
dim = 10
ord = 10
n_samples = 1600
n_test_samples = 1000

basis = LegendrePolynomials((-1,1), dim-1)
assert len(basis) == dim
unif = partial(scipy_integral, density=constant(0.5))
# i_l2 = L2inner(unif)
# deriv = lambda bf: BasisFunction(bf.function.deriv(), bf.domain)

def Hkinner(k, integral):
    assert isinstance(k, int) and k >= 0
    l2i = L2inner(integral)
    deriv = lambda bf: BasisFunction(bf.function.deriv(), bf.domain)
    def inner(b1, b2):
        ret = l2i(b1, b2)
        for j in range(k):
            b1 = deriv(b1)
            b2 = deriv(b2)
            ret += l2i(b1, b2)
        return ret
    return inner

# i_h10 = lambda f,g: i_l2(deriv(f), deriv(g))
# i_h1 = lambda f,g: i_h10(f,g) + i_l2(f,g)

i_l2 = Hkinner(0, unif)
i_h1 = Hkinner(1, unif)
i_h2 = Hkinner(2, unif)
i_h5 = Hkinner(5, unif)

g_l2 = gramian(basis, i_l2)
assert np.allclose(g_l2, np.eye(dim))
# g_h10 = gramian(basis, i_h10)
g_h1 = gramian(basis, i_h1)
g_h2 = gramian(basis, i_h2)
g_h5 = gramian(basis, i_h5)


#TODO: andere basen zu verwenden reicht nicht! du willst ja |.|_L2 + |.|_H10 minimieren
ks = [0,1,2,5]

f, ax = plt.subplots(2,2)
ax[0,0].matshow(g_l2, cmap='magma_r', norm=SymLogNorm(1))
ax[0,0].set_xticks([]); ax[0,0].set_yticks([])
ax[0,0].set_title("$L^2$")
# ax[0,1].matshow(g_h10, cmap='magma_r', norm=SymLogNorm(1))
# ax[0,1].set_xticks([]); ax[1].set_yticks([])
# ax[0,1].set_title("$H^1_0$")
ax[0,1].matshow(g_h1, cmap='magma_r', norm=SymLogNorm(1))
ax[0,1].set_xticks([]); ax[0,1].set_yticks([])
ax[0,1].set_title("$H^1$")
ax[1,0].matshow(g_h2, cmap='magma_r', norm=SymLogNorm(1))
ax[1,0].set_xticks([]); ax[1,0].set_yticks([])
ax[1,0].set_title("$H^2$")
ax[1,1].matshow(g_h5, cmap='magma_r', norm=SymLogNorm(1))
ax[1,1].set_xticks([]); ax[1,1].set_yticks([])
ax[1,1].set_title("$H^5$")
plt.tight_layout()
plt.savefig("gramians.png", dpi=100)
plt.close(f)


xs = np.linspace(*basis.domain, num=1000)
f, ax = plt.subplots(2,2)
ax.shape = -1
inners = [i_l2, i_h1, i_h2, i_h5]
labels = ["$L^2$", "$H^1$", "$H^2$", "$H^5$"]
orthos = []
for e in range(len(inners)):
    b = basis.orthonormalize(inners[e])  #TODO: allow to pass gramian as well
    for bi in b:
        ax[e].plot(xs, b(xs))
    ax[e].set_title("{}-orthonormal basis".format(labels[e]))
    orthos.append(b)
plt.tight_layout()
plt.savefig("bases.png", dpi=300)
plt.close(f)

def samples(n_samples, solution, CM=False):
    print('Creating samples ...')
    sampler = CartesianProductSampler([Uniform((-1,1))]*ord)
    ys = sampler.sample(n_samples)

    print('Computing values ...')
    solutions = Parallel(solution)
    us,ys = transpose(solutions(ys))
    us = np.array(us)
    ys = np.array(ys)

    assert ys.shape == (n_samples, ord)
    assert us.shape[0] == n_samples

    nodes = ys.T
    values = us
    print("Computing weights ...")
    weights = np.full(n_samples, 1/n_samples)
    return nodes, values, weights


def mesh2triang(mesh):
    xy = mesh.coordinates()
    return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())

def fe_plot(obj):
    from fenics import Function, Mesh
    plt.gca().set_aspect('equal')
    if isinstance(obj, Function):
        mesh = obj.function_space().mesh()
        if (mesh.geometry().dim() != 2):
            raise(AttributeError)
        if obj.vector().size() == mesh.num_cells():
            C = obj.vector().array()
            plt.tripcolor(mesh2triang(mesh), C)
        else:
            C = obj.compute_vertex_values(mesh)
            plt.tripcolor(mesh2triang(mesh), C, shading='gouraud')
    elif isinstance(obj, Mesh):
        if (obj.geometry().dim() != 2):
            raise(AttributeError)
        plt.triplot(mesh2triang(obj), color='k')

def plot_mean_and_variance(nodes, values, weights, problem):  #TODO: see plot/plot_solution.py
    from fenics import Function  #, plot as fe_plot
    us = values
    assert us.shape[0] == n_samples
    x_dim = us.shape[1]
    mean = np.mean(us, axis=0)
    var = np.var(us, axis=0)
    m = Function(problem.space)
    v = Function(problem.space)
    m.vector()[:] = mean
    v.vector()[:] = var
    plt.subplot(1,2,1)
    fe_plot(m)
    plt.colorbar()
    plt.subplot(1,2,2)
    fe_plot(v)
    plt.colorbar()
    plt.savefig("mean_and_var.png", dpi=300)
    plt.close(f)


def reconstruct(nodes, values, weights, basis, title):
    x_dim = values.shape[1]
    n_samples = len(weights)
    assert nodes.shape == (ord, n_samples)
    assert values.shape == (n_samples, x_dim)
    assert weights.shape == (n_samples,)

    print("Creating measures ...")
    tensor = lambda b: xe.Tensor.from_buffer(np.array(b))
    values = [tensor(val) for val in values]
    ml = MeasurementList([BasisMeasure(basis)]*ord)
    measures = transpose(ml(nodes))
    measures = [[tensor(cmp_m) for cmp_m in m] for m in measures]

    print('Reconstruct ...')
    with timeit("Reconstruction time [ {} ]: {{:.2f}} s".format(title)):
        reco = xe.uq_ra_adf(
                measures, values, weights, 
                (x_dim,) + ml.dimensions, targeteps=1e-10, maxitr=300)

    return reco


def evaluate(tensor, basis, nodes):
    # x_dim = values.shape[1]
    assert np.shape(nodes)[1] == n_test_samples
    id = np.eye(x_dim)
    ml = MeasurementList([IdentityMeasure((x_dim, x_dim))] + [BasisMeasure(basis)]*ord)
    with timeit("Evaluation time: {:.2f} s"):
        values = ml.evaluate(tensor, [[id]*n_test_samples]+list(nodes))
    return [np.array(val) for val in values]


def error(values, reco_values):
    l2_error = np.linalg.norm(reco_values - values, axis=1)**2
    l2_norm = np.linalg.norm(values, axis=1)**2
    # linf_error = np.linalg.norm(reco_values - values, ord=np.inf, axis=1)
    # linf_norm = np.linalg.norm(values, ord=np.inf, axis=1)**2
    # return np.max(linf_error / linf_norm)
    return np.max(l2_error / l2_norm)  # we currently optimize in L2



xs = np.linspace(-1, 1, 1000)
f, ax = plt.subplots(1,2)
for e,b in enumerate(orthos[1]):
    ax[0].plot(xs, b(xs), label='{}'.format(e))
    ax[0].set_title("Basis")
for e,b in enumerate(orthos[1].deriv()):
    ax[1].plot(xs, b(xs), label='{}'.format(e))
    ax[1].set_title("Differentiated basis")
plt.tight_layout()
plt.savefig("diff_bases.png", dpi=300)
plt.close(f)


print("Loading problem ...")
from problem.equation.darcy import Problem
info = {
    "problem": {"name": "darcy"},
    "fe": {
        "mesh": mesh_size,
        "degree": 1
    },
    "expansion": {
        "mean": 1.0,
        "scale": 6/np.pi**2,  # == 1/sum(1/m**2 for m in range(1, np.inf))
        "size": ord,
        "decay rate": 2.0
    },
    "sampling": {
        "distribution": "uniform"
    }
}
problem = Problem(info)

from fenics import Function, Constant
def plot(x, name='sol.png'):
    f = plt.figure()
    u = Function(problem.space)
    u.vector()[:] = x
    fe_plot(u)
    plt.colorbar()
    plt.savefig(name)
    plt.close(f)

# y = 2*np.random.rand(ord)-1
# u = problem.solution(y)[0]
# plot(u, 'u.png')


# # TODO: Diese Klasse sollte eigentlich funktionieren!!! Warum tut sie es nicht?
# class Differential(object):
#     def __init__(self, k):
#         assert k < problem.field.M
#         self.k = k
#         self.__name__ = "Differential({})".format(k)
#     def __call__(self, y):
#         k = self.k
#         u = problem.solution(y)[0]
#         problem.field.k = [k]
#         forcing = Function(problem.space)
#         forcing.vector()[:] = problem.application((y, u))[0]
#         problem.field.k = []
#         problem.forcing = forcing
#         du = -problem.solution(y)[0]
#         problem.forcing = Constant(1)
#         return du, y

#TODO: workaround
class Differential(object):
    def __init__(self, k, h=1e-8):
        assert k < problem.field.M
        self.h = h
        self.dy = dy = np.zeros(problem.field.M)
        dy[k] = h
        self.__name__ = "Differential({})".format(k)
    def __call__(self, y):
        h = self.h
        dy = self.dy
        u_1 = problem.solution(y+dy)[0]
        u_2 = problem.solution(y)[0]
        du = (u_1 - u_2) / h
        return du, y

# y = np.zeros(ord)
# for k in range(ord):
#     du = Differential(k)(y)[0]
#     plot(du, 'du_{}.png'.format(k))

#TODO: Does not approximate the mixed terms d_i d_j!
#      But approximating all ord**2 terms would require a lot of samples...
#      --> directly use directional derivatives
class Differential2(object):
    def __init__(self, k, h=1e-8):
        assert k < problem.field.M
        self.h = h
        self.dy = dy = np.zeros(problem.field.M)
        dy[k] = h
        self.__name__ = "Differential({})".format(k)
    def __call__(self, y):
        h = self.h
        dy = self.dy
        u_1 = problem.solution(y-dy)[0]
        u_2 = problem.solution(y)[0]
        u_3 = problem.solution(y+dy)[0]
        ddu = (u_1 - 2*u_2 + u_3) / h**2
        return ddu, y

# y = np.zeros(ord)
# for k in range(ord):
#     ddu = Differential2(k)(y)[0]
#     plot(ddu, 'ddu_{}.png'.format(k))


tensor = lambda b: xe.Tensor.from_buffer(np.array(b))
def create_measures(nodes, basis, deriv=None):
    basis_list = [BasisMeasure(basis)]*ord
    if isinstance(deriv, int):
        basis_list[deriv] = BasisMeasure(basis.deriv())
    ml = MeasurementList(basis_list)
    measures = transpose(ml(nodes))
    return [[tensor(cmp_m) for cmp_m in m] for m in measures]


def test_differential(func, diff, dim, idx):
    errs = []
    for h_exp in range(3,21):
        h_idx = 2**(-h_exp)
        err = 0
        for x_idx in np.linspace(-1,1-h_idx,10):
            x,h = np.zeros((2,dim))
            x[idx] = x_idx
            h[idx] = h_idx
            db_1 = (func(x+h)- func(x))/h_idx
            db_2 = diff(x)
            err = max(err, np.linalg.norm(db_1 - db_2))
        errs.append(err)
        print("2**-{}: {:.2e}".format(h_exp, err))
    rates = np.diff(np.log2(errs))
    avg_rate = rates[0]
    for rate in rates[1:]:
        avg_rate = 0.5*avg_rate + 0.5*rate
    print("Rate: {:.2f}".format(avg_rate))

# print_old("\nTest basis differential ...")
# test_differential(orthos[1], orthos[1].deriv(), 1, 0)

# print_old("\nTest operator differential ...")
# fst = lambda f: lambda x: f(x)[0]
# test_differential(fst(problem.solution), fst(Differential(0)), 10, 0)


# TODO: vergleiche auch mit einer Reco ohne orthonomalisierung!
# dann bekommt man für H1 nur
# [2019-05-15 08:53:43] Reconstruction time [ H1 | 400 ]: 27.76 s
# [2019-05-15 08:53:43] Dimensions [ H1 | 400 ]: [961, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
# [2019-05-15 08:53:43] Ranks [ H1 | 400 ]: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# [2019-05-15 08:56:13] Error [ H1 | 400 ]: 6.63e-03
# statt
# [2019-05-15 08:58:48] Reconstruction time [ H1 | 400 ]: 255.26 s
# [2019-05-15 08:58:48] Dimensions [ H1 | 400 ]: [961, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]
# [2019-05-15 08:58:48] Ranks [ H1 | 400 ]: [19, 13, 10, 7, 6, 5, 4, 3, 2, 1]
# [2019-05-15 09:00:46] Evaluation time: 30.56 s
# [2019-05-15 09:00:46] Error [ H1 | 400 ]: 3.73e-05




print_old("\nCreate test set ...")
test_nodes, test_values, test_weights = samples(n_test_samples, problem.solution)

x_dim = test_values.shape[1]


def reconstruct_l2(n_samples, basis):
    title = "L2 | {}".format(n_samples)
    print_old("\nCreate training set ...")
    nodes, values, weights = samples(n_samples, problem.solution) #TODO: CM=True
    measures = create_measures(nodes, basis)

    values = [tensor(val) for val in values]
    weights = weights.tolist()

    print('Reconstruct ...')
    with timeit("Reconstruction time [ {} ]: {{:.2f}} s".format(title)):
        r_l2 = xe.uq_ra_adf(measures, values, weights, (x_dim,) + (dim,)*ord, targeteps=1e-8, maxitr=300)
    print("Dimensions [ {} ]:".format(title), r_l2.dimensions)
    print("Ranks [ {} ]:".format(title), r_l2.ranks())

    return r_l2


def reconstruct_h1(n_samples_total, basis):
    title = "H1 | {}".format(n_samples_total)
    n_samples_diff = n_samples_total // (1+ord)
    n_samples_l2 = n_samples_total - ord*n_samples_diff

    print_old("\nCreate training set ...")
    nodes, values, weights = samples(n_samples_l2, problem.solution) #TODO: CM=True
    measures = create_measures(nodes, basis)

    #TODO: define measurements where the kth component gets differentiated
    # i.e. coef_vec @ basis(x) == U(x)
    # i.e. coef_vec @ dervi_basis(x) == DU(x)
    # since DU(x) = coef_vec @ D basis(x) you have deriv_basis = D basis
    # new measurements:
    for k in range(ord):
        ns, vs, ws = samples(n_samples_diff, Differential(k))  #TODO: CM=True
        nodes = np.concatenate([nodes, ns], axis=1)
        values = np.concatenate([values, vs])
        weights = np.concatenate([weights, ws])
        ms = create_measures(ns, basis, deriv=k)
        measures = measures + ms

    values = [tensor(val) for val in values]
    weights = weights.tolist()

    print('Reconstruct ...')
    with timeit("Reconstruction time [ {} ]: {{:.2f}} s".format(title)):
        r_h1 = xe.uq_ra_adf(measures, values, weights, (x_dim,) + (dim,)*ord, targeteps=1e-8, maxitr=300)
    print("Dimensions [ {} ]:".format(title), r_h1.dimensions)
    print("Ranks [ {} ]:".format(title), r_h1.ranks())

    return r_h1

dir_name = "vmc"
if not os.path.isdir(dir_name):
    os.makedirs(dir_name)

assert n_samples % 400 == 0
reco_l2 = []
reco_h1 = []
ns = np.arange(400, n_samples+1, 400)
for k in ns:
    print_old("\n" + "="*38 + " H1 | {} ".format(k) + "="*38)
    r_h1 = reconstruct_h1(k, orthos[1])
    reco_h1.append(evaluate(r_h1, orthos[1], test_nodes))
    print("Error [ H1 | {} ]: {:.2e}".format(k, error(test_values, reco_h1[-1])))

    print_old("\n" + "="*38 + " L2 | {} ".format(k) + "="*38)
    r_l2 = reconstruct_l2(k, orthos[0])
    reco_l2.append(evaluate(r_l2, orthos[0], test_nodes))
    print("Error [ L2 | {} ]: {:.2e}".format(k, error(test_values, reco_l2[-1])))

file_name = "{}/{{}}.npz".format(dir_name)
num_files = len(glob(file_name.format("*")))
file_name = file_name.format(num_files)
print("Saving: '{}'".format(file_name))
np.savez_compressed(file_name, samples=ns, values=test_values, approximation_l2=reco_l2, approximation_h1=reco_h1)
