# coding: utf-8
from __future__ import division, print_function
#TODO: Note: for the embedding of W^{k,2} into L^\infty in n dimensions you need k > n/2.
#      That means you need to minimize in the H^{n/2} norm.

import numpy as np
import xerus as xe

from parallel import Parallel
from samplers import constant, Uniform, CartesianProductSampler
from bases import LegendrePolynomials, gramian, scipy_integral, L2inner, BasisFunction
from measures import BasisMeasure, MeasurementList, IdentityMeasure
from functools import partial

import time
from contextlib import contextmanager
from glob import glob

print_old = print
ts = lambda: time.strftime("[%Y-%m-%d %H:%M:%S]", time.localtime())
print = lambda *args, **kwargs: print_old(ts(), *args, **kwargs)
transpose = lambda ls: list(zip(*ls))

@contextmanager
def timeit(title):
    try:
        t0 = time.process_time()
        yield
    finally:
        print(title.format(time.process_time()-t0))


mesh_size = 30
dim = 10
ord = 10
n_samples = 1600
n_test_samples = 1000

basis = LegendrePolynomials((-1,1), dim-1)
assert len(basis) == dim
unif = partial(scipy_integral, density=constant(0.5))


def Hkinner(k, integral):
    assert isinstance(k, int) and k >= 0
    l2i = L2inner(integral)
    deriv = lambda bf: BasisFunction(bf.function.deriv(), bf.domain)
    def inner(b1, b2):
        ret = l2i(b1, b2)
        for j in range(k):
            b1 = deriv(b1)
            b2 = deriv(b2)
            ret += l2i(b1, b2)
        return ret
    return inner


inners = [Hkinner(0, unif), Hkinner(1, unif)]
orthos = [basis.orthonormalize(i) for i in inners]


# sample = CartesianProductSampler([Uniform((-1,1), random=qmc)]*ord).sampler
from qmc.sobol_seq import i4_sobol_generate as qmc
sample = lambda n_samples: 2*qmc(dim_num=ord, n=n_samples)-1

def samples(n_samples, solution, CM=False):
    print('Creating samples ...')
    ys = sample(n_samples)

    print('Computing values ...')
    solutions = Parallel(solution)
    us,ys = transpose(solutions(ys))
    us = np.array(us)
    ys = np.array(ys)

    assert ys.shape == (n_samples, ord)
    assert us.shape[0] == n_samples

    nodes = ys.T
    values = us
    print("Computing weights ...")
    weights = np.full(n_samples, 1/n_samples)
    return nodes, values, weights


def reconstruct(nodes, values, weights, basis, title):
    x_dim = values.shape[1]
    n_samples = len(weights)
    assert nodes.shape == (ord, n_samples)
    assert values.shape == (n_samples, x_dim)
    assert weights.shape == (n_samples,)

    print("Creating measures ...")
    tensor = xe.Tensor.from_ndarray
    values = [tensor(val) for val in values]
    ml = MeasurementList([BasisMeasure(basis)]*ord)
    measures = transpose(ml(nodes))
    measures = [[tensor(cmp_m) for cmp_m in m] for m in measures]

    print('Reconstruct ...')
    with timeit("Reconstruction time [ {} ]: {{:.2f}} s".format(title)):
        reco = xe.uq_ra_adf(measures, values, weights, (x_dim,) + ml.dimensions, targeteps=1e-8, maxitr=300)

    return reco


def evaluate(tensor, basis, nodes):
    # x_dim = values.shape[1]
    assert np.shape(nodes)[1] == n_test_samples
    id = np.eye(x_dim)
    ml = MeasurementList([IdentityMeasure((x_dim, x_dim))] + [BasisMeasure(basis)]*ord)
    with timeit("Evaluation time: {:.2f} s"):
        values = ml.evaluate(tensor, [[id]*n_test_samples]+list(nodes))
    return [val.to_ndarray() for val in values]


def error(values, reco_values):
    l2_error = np.linalg.norm(reco_values - values, axis=1)**2
    l2_norm = np.linalg.norm(values, axis=1)**2
    return np.max(l2_error / l2_norm)  # we currently optimize in L2


print("Loading problem ...")
from problem.darcy import Problem
info = {
    "problem": {"name": "darcy"},
    "fe": {
        "mesh": mesh_size,
        "degree": 1
    },
    "expansion": {
        "mean": 1.0,
        "scale": 6/np.pi**2,  # == 1/sum(1/m**2 for m in range(1, np.inf))
        "size": ord,
        "decay rate": 2.0
    },
    "sampling": {
        "distribution": "uniform"
    }
}
problem = Problem(info)


#TODO: Does not approximate the mixed (directional) derivatives
class Differential(object):
    def __init__(self, k, h=1e-8):
        assert k < problem.field.M
        self.h = h
        self.dy = dy = np.zeros(problem.field.M)
        dy[k] = h
        self.__name__ = "Differential({})".format(k)
    def __call__(self, y):
        h = self.h
        dy = self.dy
        u_1 = problem.solution(y+dy)[0]
        u_2 = problem.solution(y)[0]
        du = (u_1 - u_2) / h
        return du, y


tensor = xe.Tensor.from_ndarray
def create_measures(nodes, basis, deriv=None):
    basis_list = [BasisMeasure(basis)]*ord
    if isinstance(deriv, int):
        basis_list[deriv] = BasisMeasure(basis.deriv())
    ml = MeasurementList(basis_list)
    measures = transpose(ml(nodes))
    return [[tensor(cmp_m) for cmp_m in m] for m in measures]


def test_differential(func, diff, dim, idx):
    errs = []
    for h_exp in range(3,21):
        h_idx = 2**(-h_exp)
        err = 0
        for x_idx in np.linspace(-1,1-h_idx,10):
            x,h = np.zeros((2,dim))
            x[idx] = x_idx
            h[idx] = h_idx
            db_1 = (func(x+h)- func(x))/h_idx
            db_2 = diff(x)
            err = max(err, np.linalg.norm(db_1 - db_2))
        errs.append(err)
        print("2**-{}: {:.2e}".format(h_exp, err))
    rates = np.diff(np.log2(errs))
    avg_rate = rates[0]
    for rate in rates[1:]:
        avg_rate = 0.5*avg_rate + 0.5*rate
    print("Rate: {:.2f}".format(avg_rate))


# print_old("\nTest basis differential ...")
# test_differential(orthos[1], orthos[1].deriv(), 1, 0)

# print_old("\nTest operator differential ...")
# fst = lambda f: lambda x: f(x)[0]
# test_differential(fst(problem.solution), fst(Differential(0)), 10, 0)


print_old("\nCreate test set ...")
test_nodes, test_values, test_weights = samples(n_test_samples, problem.solution)

x_dim = test_values.shape[1]


def reconstruct_l2(n_samples, basis):
    title = "L2 | {}".format(n_samples)
    print_old("\nCreate training set ...")
    nodes, values, weights = samples(n_samples, problem.solution) #TODO: CM=True
    measures = create_measures(nodes, basis)

    values = [tensor(val) for val in values]
    weights = weights.tolist()

    print('Reconstruct ...')
    with timeit("Reconstruction time [ {} ]: {{:.2f}} s".format(title)):
        r_l2 = xe.uq_ra_adf(measures, values, weights, (x_dim,) + (dim,)*ord, targeteps=1e-8, maxitr=300)
    print("Dimensions [ {} ]:".format(title), r_l2.dimensions)
    print("Ranks [ {} ]:".format(title), r_l2.ranks())

    return r_l2


def reconstruct_h1(n_samples_total, basis):
    title = "H1 | {}".format(n_samples_total)
    n_samples_diff = n_samples_total // (1+ord)
    n_samples_l2 = n_samples_total - ord*n_samples_diff

    print_old("\nCreate training set ...")
    nodes, values, weights = samples(n_samples_l2, problem.solution) #TODO: CM=True
    measures = create_measures(nodes, basis)

    for k in range(ord):
        ns, vs, ws = samples(n_samples_diff, Differential(k))  #TODO: CM=True
        nodes = np.concatenate([nodes, ns], axis=1)
        values = np.concatenate([values, vs])
        weights = np.concatenate([weights, ws])
        ms = create_measures(ns, basis, deriv=k)
        measures = measures + ms

    values = [tensor(val) for val in values]
    weights = weights.tolist()

    print('Reconstruct ...')
    with timeit("Reconstruction time [ {} ]: {{:.2f}} s".format(title)):
        r_h1 = xe.uq_ra_adf(measures, values, weights, (x_dim,) + (dim,)*ord, targeteps=1e-8, maxitr=300)
    print("Dimensions [ {} ]:".format(title), r_h1.dimensions)
    print("Ranks [ {} ]:".format(title), r_h1.ranks())

    return r_h1


assert n_samples % 400 == 0
reco_l2 = []
reco_h1 = []
ns = np.arange(400, n_samples+1, 400)
for k in ns:
    print_old("\n" + "="*38 + " H1 | {} ".format(k) + "="*38)
    r_h1 = reconstruct_h1(k, orthos[1])
    reco_h1.append(evaluate(r_h1, orthos[1], test_nodes))
    print("Error [ H1 | {} ]: {:.2e}".format(k, error(test_values, reco_h1[-1])))

    print_old("\n" + "="*38 + " L2 | {} ".format(k) + "="*38)
    r_l2 = reconstruct_l2(k, orthos[0])
    reco_l2.append(evaluate(r_l2, orthos[0], test_nodes))
    print("Error [ L2 | {} ]: {:.2e}".format(k, error(test_values, reco_l2[-1])))

name = "vmc_qmc.{}.npz"
num_files = len(glob(name.format("*")))
name = name.format(num_files)
print("Saving: '{}'".format(name))
np.savez_compressed(name, samples=ns, values=test_values, approximation_l2=reco_l2, approximation_h1=reco_h1)
