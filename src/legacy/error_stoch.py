"""
Error estimate for the stochastic dimensions.
"""
from __future__ import division, print_function
import time
import numpy as np
import xerus as xe
from copy import deepcopy
from measurement_utils.basis import LegendrePolynomials
from measurement_utils.samplers import Uniform, CartesianProductSampler
from measurement_utils.measures import BasisMeasure, MeasurementList, IdentityMeasure
from contextlib import contextmanager

print_old = print
def ts(): return time.strftime("[%Y-%m-%d %H:%M:%S]", time.localtime())


print = lambda *args, **kwargs: print_old(ts(), *args, **kwargs)


@contextmanager
def timeit(title):
    start = time.clock()
    try:
        yield
    finally:
        print(title.format(time.clock()-start))


def transpose(ls):
    """
    Transpose a list similar to an array_like.
    """
    return list(zip(*ls))


def sample_solution(dims, info):
    print("Calling sample_solution")
    print("Loading problem ...")
    from problem.equation.darcy import Problem
    from parallel import Parallel

    print('Creating samples ...')
    sampler = CartesianProductSampler([Uniform((-1, 1))]*len(dims))
    sample = sampler.sample
    def weight(nodes): return np.full(nodes.shape[1], 1/nodes.shape[1])

    print('Sampling ...')
    ys = sample(n_samples)

    local_info = deepcopy(info)
    local_info["expansion"]["size"] = len(dims)
    problem = Problem(local_info)

    print('Computing values ...')
    solutions = Parallel(problem.solution)
    results = solutions(ys)
    us, ys = transpose(results)
    us = np.array(us)
    ys = np.array(ys)

    assert ys.shape == (n_samples, len(dims))
    assert us.shape[0] == n_samples

    return ys.T, weight(ys.T), us


def reconstruct(dims, nodes, weights, values, label):
    print("Calling reconstruct")
    x_dim = values.shape[1]  # physical dimension
    tensor = xe.Tensor.from_buffer
    vals = [tensor(val) for val in values]
    def bm(dim): return BasisMeasure(LegendrePolynomials([-1, 1], dim-1))
    ml = MeasurementList([bm(dim) for dim in dims])
    assert ml.dimensions == tuple(dims), f"{ml.dimensions} vs {dims}"

    meas = transpose(ml(nodes))
    meas = [[tensor(np.array(cmp_m)) for cmp_m in m] for m in meas]
    # There is a bug in `transpose` that causes `cmp_m` to have wrong strides.
    # Creating a new array fixes the strides.

    print('Reconstruct ...')
    with timeit("Reconstruction time [{}]: {{:.2f}} s".format(label)):
        reco = xe.uq_ra_adf(meas, vals, weights, (x_dim,) +
                            ml.dimensions, targeteps=1e-8, maxitr=300)
    print("Dimensions [{}]:".format(label), reco.dimensions)
    print("Ranks [{}]:     ".format(label), reco.ranks())

    return reco


def sample_residuum(reco, dims, info):
    """
    Parameter
    ---------
    reco    :   TTTensor
        reconstruction of solution u on active set
    dims    :   list
        dimensions of active set + boundary
    info    :   dict
        info dict for problem
    """
    print("Calling sample_residuum")
    x_dim = reco.dimensions[0]
    dims_orig = reco.dimensions[1:]
    order = len(dims)
    order_orig = len(dims_orig)
    assert order_orig <= order

    print('Creating samples ...')
    sampler = CartesianProductSampler([Uniform((-1, 1))]*order)
    sample = sampler.sample
    def weight(nodes): return np.full(nodes.shape[1], 1/nodes.shape[1])

    print('Sampling ...')
    ys = sample(n_samples)  # uniform samples, shape is (n_samples, order)
    # uniform samples, shape is (n_samples, order_orig)
    ys_orig = ys[:, :order_orig]
    assert ys.shape == (n_samples, order)
    assert ys_orig.shape == (n_samples, order_orig)

    # evaluate reconstructed approximation of solution on active set
    print('Evaluating reconstruction ...')
    meas = [[np.eye(x_dim)]*n_samples] + list(ys_orig.T)

    Id = IdentityMeasure((x_dim, x_dim))
    def bm(dim): return BasisMeasure(LegendrePolynomials([-1, 1], dim-1))
    ml = MeasurementList([Id] + [bm(dim) for dim in dims_orig])

    us = ml.evaluate(reco, meas)  # evaluate approximation of u
    us = np.array([np.array(u) for u in us])  # shape is (n_samples, x_dim)
    assert us.dtype == np.float64, us.dtype
    assert us.shape == (n_samples, x_dim)

    print("Loading problem ...")
    from problem.equation.darcy import Problem
    from parallel import Parallel

    local_info = deepcopy(info)
    local_info["expansion"]["size"] = order
    problem = Problem(local_info)

    print('Computing values ...')
    residuums = Parallel(problem.residuum)
    results = residuums(list(zip(ys, us)))
    res, yus = transpose(results)
    ys, us = transpose(yus)
    res = np.array(res)
    ys = np.array(ys)

    assert ys.shape == (n_samples, order)
    assert res.ndim == 2
    assert res.shape[0] == n_samples

    return ys.T, weight(ys.T), res


def prolong_tt(t, dims):
    """
    Prolongate TT to TT with more stochastic modes and larger dimensions.
    """
    t.move_core(0)
    assert t.canonicalized and t.corePosition == 0
    t_norm = xe.frob_norm(t)
    ret = xe.TTTensor(dims)
    for idx in range(t.order()):
        cmp = t.get_component(idx)
        cmp = prolong_tensor(cmp, dims[idx])
        ret.set_component(idx, cmp)
    for idx in range(t.order(), len(dims)):
        cmp = xe.Tensor.dirac([1, dims[idx], 1], 0)
        ret.set_component(idx, cmp)
    ret.assume_core_position(0)
    assert t_norm == xe.frob_norm(
        ret), "{} vs {}".format(t_norm, xe.frob_norm(ret))
    return ret


def prolong_tensor(t, dim):
    assert len(t.dimensions) == 3
    assert dim >= t.dimensions[1]
    arr = np.array(t)
    ret_arr = np.zeros((arr.shape[0], dim, arr.shape[2]))
    ret_arr[:, :t.dimensions[1], :] = arr
    return xe.Tensor.from_buffer(ret_arr)


def ONB_residual_estimator(reco, info, maxitr=10):
    def inner(t1, t2):
        assert t1.order() == t2.order()
        I, = xe.indices(1)
        ret = xe.Tensor()
        ret() << t1(I & 0) * t2(I & 0)
        return ret[0]

    x_dim = reco.dimensions[0]
    dims = reco.dimensions[1:]
    inc_dims = [d+1 for d in dims] + [2]

    for it in range(maxitr):
        nodes_orig, weights_orig, res_orig = sample_residuum(reco, dims, info)
        R_lam = reconstruct(dims, nodes_orig, weights_orig,
                            res_orig, label="R_lam")
        R_lam = prolong_tt(R_lam, [x_dim] + inc_dims)

        nodes_new, weights_new, res_new = sample_residuum(reco, inc_dims, info)
        R_tet = reconstruct(inc_dims, nodes_new,
                            weights_new, res_new, label="R_tet")

        if xe.frob_norm(R_lam) <= xe.frob_norm(R_tet):
            break
        print("Iteration {}/{} insufficient".format(it+1, maxitr))
    else:
        raise RuntimeError("Cannot estimate residuum")

    R = R_tet - R_lam
    print("Residuum norm:", xe.frob_norm(R))

    rs = []
    for m in range(len(inc_dims)):
        R_m = xe.TTTensor(R)
        R_m.fix_mode(m+1, inc_dims[m]-1)
        r = inner(R_m, R_m)
        rs.append(r)
    return rs


def normalize(xs): return (xs - np.min(xs))/(np.max(xs) - np.min(xs))


def P1_residual_estimator(dims, info):
    print("Calling P1_residual_estimator")
    print("Loading problem ...")
    from problem.equation.darcy import Problem
    from parallel import Parallel

    print('Creating samples ...')
    sampler = CartesianProductSampler([Uniform((-1, 1))]*len(dims))
    sample = sampler.sample
    def weight(nodes): return np.full(nodes.shape[1], 1/nodes.shape[1])

    print('Sampling ...')
    ys = sample(n_samples)

    local_info = deepcopy(info)
    local_info["expansion"]["size"] = len(dims)
    problem = Problem(local_info)

    print('Computing values ...')
    solutions = Parallel(problem.solution)
    results = solutions(ys)
    us, ys = transpose(results)
    estimators = Parallel(problem.P1_residual_estimator)
    yus = list(zip(ys, us))
    results = estimators(yus)
    ests, uys = transpose(results)

    ests = np.array(ests)
    assert ests.shape[0] == n_samples

    mean_est = np.mean(ests, axis=0)
    # print("Total physical error estimate:", mean_est.sum())
    return mean_est


def marking(vec_est, theta):
    order = np.argsort(vec_est)
    vec_est = vec_est[order]
    sum_est = sum(vec_est)
    cum_est = 0
    for idx, est in enumerate(reversed(vec_est), 1):
        cum_est += est
        if cum_est >= theta*sum_est:
            break
    return order[-idx:]


if __name__ == "__main__":
    # Initial settings
    # mesh = "mesh/reentrant.xml"  # path to file or initial mesh size for UnitSquareMesh
    mesh = 25  # path to file or initial mesh size for UnitSquareMesh
    dims = [2]

    # Refinement options
    max_iterations = 2
    theta = 0.3
    max_dofs = 1e4

    # Reconstruction options
    n_samples = 500

    info = {
        "problem": {"name": "darcy"},
        "fe": {
            "mesh": mesh,
            "degree": 1
        },
        "expansion": {
            "size": None,  # this will be set in the algorithm
            "mean": 1.0,
            "decay rate": 2.0,
            "scale": 6 / np.pi**2  # scale <= mean * 6 / np.pi**2 for decay == 2
        },
        "sampling": {
            "distribution": "uniform"
        }
    }

    def x_dim():
        from problem.equation.darcy import Problem
        local_info = deepcopy(info)
        local_info["expansion"]["size"] = 1
        problem = Problem(local_info)
        return problem.dofs()

    def refine_mesh(marked_cells):
        from problem.equation.darcy import Problem
        global info
        local_info = deepcopy(info)
        local_info["expansion"]["size"] = 1
        problem = Problem(local_info)
        problem.refine_mesh(marked_cells)
        info['fe']['mesh'] = problem.mesh

    for step in range(max_iterations):
        print()
        print("="*80)
        print("Current dimensions:", [x_dim()] + dims)
        print("="*80)
        print()

        # TODO: reverse order of output tuple in darcy (input: (y,u) --> output: ((y,u),res))
        # TODO: do not resample
        # TODO: reuse reco, R_lam, R_tet as initial values for future iterations

        nodes, weights, values = sample_solution(dims, info)
        reco = reconstruct(dims, nodes, weights, values, label="u")

        phys_est = P1_residual_estimator(dims, info)
        tot_phys_est = np.sum(phys_est)
        stoch_est = ONB_residual_estimator(reco, info)
        tot_stoch_est = np.sum(stoch_est)

        print("-"*80)
        print("Total phys. error estimate:  {:.2e}".format(tot_phys_est))
        print("Total stoch. error estimate: {:.2e}".format(tot_stoch_est))

        if tot_phys_est > tot_stoch_est:
            marked_cells = marking(phys_est, theta)
            print("Refining %d cells" % (len(marked_cells)))
            refine_mesh(marked_cells)
        else:
            idx = np.argmax(stoch_est)
            print("Refining mode %d:" % idx)
            if idx < len(dims):
                dims[idx] += 1
            else:
                dims = dims + [2]
