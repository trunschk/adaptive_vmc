# coding: utf-8
import time, numpy as np
from numpy.polynomial.legendre import Legendre
from scipy.integrate import quad
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.ticker import MaxNLocator

# Compute Theta[j,k] = inner(P_j**2, P_k) jor j = 0, ..., dim-1.
# Theta[j,k] != 0 only if k is less than 2*j. Thus let k = 0, ..., 2*dim-1 + offset.
# The offset allows to compute a few  more coefficients to illustrate that these are indeed 0.
dim = 20
prod_dim = 2*dim
offset = 4

b = Legendre.basis
fs = np.sqrt(2*np.arange(prod_dim+offset)+1)

# Legendre triple product (Gaunt's formula) in 1D
def leg_triple_product(a,b,c):
    """
    Compute int_{-1}^{1} P_a P_b P_c dx for 1D Legendre Polynomials P.
    """
    fac = np.math.factorial
    A = lambda n: fac(2*n)/2**n/fac(n)**2
    s = (a+b+c)/2
    fs = lambda n: np.sqrt(2*n+1) # normalization constant
    if (a+b+c) % 2 == 0 and c-b <= a <= c+b:
        gaunt = A(s-a)*A(s-b)*A(s-c)/A(s)/(2*s+1) * fs(a)*fs(b)*fs(c)
        return gaunt
    else: return 0

tic = time.time()
Theta = np.empty((dim, prod_dim+offset))
inner = lambda f,g: (lambda I: (I(1)-I(-1))/2)((f*g).integ())
for j in range(dim):
    for k in range(prod_dim+offset):
        Theta[j,k] = fs[j]**2 * fs[k] * inner(b(j)**2, b(k))
print("Time to compute integral: {} s".format(time.time()-tic))

tic = time.time()
Theta2 = np.empty((dim, prod_dim+offset))
for j in range(dim):
    for k in range(prod_dim+offset):
        Theta2[j,k] = leg_triple_product(j,j,k)
print("Time to compute Gaunt's formula: {} s".format(time.time()-tic))

tic = time.time()
Theta3 = np.empty((dim, prod_dim+offset))
abs_inner = lambda f,g: quad(lambda x: abs(f(x))*g(x), -1, 1)[0]/2
for j in range(dim):
    for k in range(prod_dim+offset):
        Theta3[j,k] = fs[j] * fs[k] * abs_inner(b(j), b(k))
print("Time to compute new integral: {} s".format(time.time()-tic))

assert np.all(Theta[abs(Theta)>1e-12] >= 0)  # The entries of Theta are non-negative (numerically) ...
assert np.all(Theta <= 1+1e-4)  # ... and bounded by 1.
assert np.allclose(Theta, Theta2)
vmin = min(np.min(Theta[abs(Theta)>1e-8]), 1e-4)
vmax = np.max(abs(Theta))

f,ax = plt.subplots(1,1, figsize=(16,9), dpi=300)
im = ax.imshow(np.clip(Theta, vmin, vmax), norm=LogNorm(vmin=vmin, vmax=vmax), cmap="cividis")
ax.set_title(r"$\Theta_{\alpha\beta} := \int_{-1}^1 P_\alpha^2(x) P_\beta(x)\,\frac{\mathrm{d}x}{2}$")
ax.set_xlabel(r"$\beta$")
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.set_ylabel(r"$\alpha$")
ax.yaxis.set_major_locator(MaxNLocator(integer=True))
f.colorbar(im, ax=ax)
plt.tight_layout()
plt.savefig("Theta.png", dpi=300)

f,ax = plt.subplots(1,1, figsize=(16,9), dpi=300)
im = ax.imshow(np.clip(Theta2, vmin, vmax), norm=LogNorm(vmin=vmin, vmax=vmax), cmap="cividis")
ax.set_title(r"$\Theta_{\alpha\beta} := \int_{-1}^1 P_\alpha^2(x) P_\beta(x)\,\frac{\mathrm{d}x}{2}$")
ax.set_xlabel(r"$\beta$")
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.set_ylabel(r"$\alpha$")
ax.yaxis.set_major_locator(MaxNLocator(integer=True))
f.colorbar(im, ax=ax)
plt.tight_layout()
plt.savefig("Theta2.png", dpi=300)

thresh = np.min(abs(Theta3[abs(Theta3)>1e-8]))
Theta3[abs(Theta3) <= thresh] = 0
f,ax = plt.subplots(1,1, figsize=(16,9), dpi=300)
vmax = np.max(abs(Theta3))
vmin = thresh
im = ax.imshow(np.clip(abs(Theta3), vmin, vmax), norm=LogNorm(vmin=vmin, vmax=vmax), cmap="cividis")
ax.set_title(r"$\Theta_{\alpha\beta} := \int_{-1}^1 |P_\alpha(x)| P_\beta(x)\,\frac{\mathrm{d}x}{2}$")
ax.set_xlabel(r"$\beta$")
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.set_ylabel(r"$\alpha$")
ax.yaxis.set_major_locator(MaxNLocator(integer=True))
f.colorbar(im, ax=ax)
plt.tight_layout()
plt.savefig("Theta3.png", dpi=300)
