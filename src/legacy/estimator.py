# """
# Error estimate for the stochastic dimensions.
# """
# import time
# import numpy as np
# import xerus as xe
# from copy import deepcopy
# from measurement_utils.basis import LegendrePolynomials
# from measurement_utils.samplers import Uniform, CartesianProductSampler
# from measurement_utils.measures import BasisMeasure, MeasurementList, IdentityMeasure
# from contextlib import contextmanager

# print_old = print
# ts = lambda: time.strftime("[%Y-%m-%d %H:%M:%S]", time.localtime())
# print = lambda *args, **kwargs: print_old(ts(), *args, **kwargs)

# @contextmanager
# def timeit(title):
#     start = time.clock()
#     try:
#         yield
#     finally:
#         print(title.format(time.clock()-start))

def triple_product(dim1, dim2):
    """
    int P_i P_j P_k
    """
    pass

def P1_product(dim):
    """
    Product of two P1-functions. (u*v)
    """
    return xe.kronecker([dim, dim, dim])  #TODO: this is only the P1-interpolation of the real product

def P1_grad_product(dim):
    """
    Product of the gradients of two P1-functions. (grad(u)*grad(v))
    """
    return ...

def P1_normal_gradient():
    """
    Compute the normal gradient of a function. (grad(u) * nu)
    """
    return ...

def laplace(uh):
    return 0  #TODO: this only works for P1

def product(u, v, P1_product, triple_product):
    assert u.order() == v.order()
    lu,du,ru, do, lv,dv,rv = xe.indices(7)

    components = [None] * u.order()
    for pos in reversed(range(1, u.order())):
        up = u.get_component(pos)
        vp = v.get_component(pos)
        T = triple_product(up.dimensions[1], vp.dimensions[1])
        w(lu,lv,do,ru,rv) = up(lu,du,ru) * T(du,dv,do) * vp(lv,dv,rv)
        l = up.dimensions[0] * vp.dimensions[0]
        r = up.dimensions[2] * vp.dimensions[2]
        o = T.dimensions[2]
        w.reinterpret_dimensions([l,o,r])
        components[pos] = w
    up = u.get_component(pos)
    vp = v.get_component(pos)
    o = up.dimensions[1]
    T = P1_product()
    w(lu,lv,do,ru,rv) = up(lu,du,ru) * T(du,dv,do) * vp(lv,dv,rv)
    r = up.dimensions[2] * vp.dimensions[2]
    w.reinterpret_dimensions([1,o,r])
    components[0] = w

    dims = [w[1] for w in components]
    ranks = [w[2] for w in components[:-1]]
    ret = xe.TTTensor.random(dims, ranks)
    for pos in range(u.order()):
        ret.set_component(pos, components[pos])

    return ret

def estimator(uh, problem):
    V = problem.space
    h = CellDiameter(V.mesh())


    def define(fnc):
        return fnc()

    @define
    def R_T():
        one = product(a, uh, P1_product, triple_product)
        two = product(a, laplace(uh), P1_product, triple_product)
        return f + one + two

    @define
    def R_E():  #TODO: das ist nicht R_E sondern eta_E
        #TODO: mach das auch mit eta_T
        uh0 = np.array(uh.get_component(0))[0]

        DG0 = FunctionSpace(V.mesh(), 'DG', 0)
        dg0 = TestFunction(DG0)
        u = Function(V)
        c = mesh.num_cells()????
        R_E = np.empty((c, uh0.shape[1]))
        for i, ui in enumerate(uh0.T):
            u.vector()[:] = ui
            J = jump(a * grad(u))
            R_E[i] = assemble(avg(h)/2 * J**2 * avg(dg0) * dS)

        ret = xe.TTTensor(uh)
        ret.set_component(0, xe.Tensor.from_buffer(R_E[None]))
        return ret


    y,u_vec = y_u
    u = Function(V)
    u.vector()[:] = u_vec
    f = self.forcing
    kappa = self.field.realisation(y, V)

    R_T = -(f + div(kappa * grad(u)))
    R_dT = kappa * grad(u)
    J = jump(R_dT)
    indicator = h**2 * (1 / kappa) * R_T**2 * dg0 * dx \
                + avg(h) * avg(1 / kappa) * J**2 * avg(dg0) * 2*dS

    eta_res_local = assemble(indicator, form_compiler_parameters={'quadrature_degree': -1})

