# -*- coding: utf-8 -*-
import os
from copy import deepcopy
import time
import numpy as np
import matplotlib.pyplot as plt
from misc import *

plt.rcParams['text.latex.preamble'] = [
    r'\usepackage{amsmath}',
    r'\usepackage{amssymb}']

# ------------------------------------------------------------------------------
# SETUP

max_iterations = 10  # adaptive iterations
sdim = [2]           # start with one stochastic dimension and linear polynomial
threshold = 0.3      # Refinement fraction
max_dofs = 1e4       # max dofs for physical solver
L = 10               # lookahead for solver

# # affine
# info = {
#     "problem": {"name": "darcy"},
#     "fe": {
#         "degree": 1,
#         "mesh": 4
#     },
#     "expansion": {
#         "size": 20,
#         "mean": 1.0,
#         # "decay rate": 2.0,
#         # "scale": 6 / np.pi**2  # scale <= mean * 6 / np.pi**2 for decay == 2
#         "decay rate": 1.0,
#         "scale": 1/np.sum(np.arange(1,21)**(-1.0))  # scale <= mean * 6 / np.pi**2 for decay == 2
#     },
#     "sampling": {
#         "distribution": "uniform"
#     }
# }

# lognormal
info = {
    "problem": {"name": "darcy"},
    "fe": {
        "degree": 1,
        "mesh": 4
    },
    "expansion": {
        "mean": 0.0,
        "scale": 10.0,
        "size": 20,
        # "decay rate": 2.0
        "decay rate": 1.0
    },
    "sampling": {
        "distribution": "normal"
    }
}

p = Problem(info)  # problem.equation.darcy.Problem class


# ------------------------------------------------------------------------------
# LOOP ADAPTIVE SCHEME

global_dofs_u_level = []
ttdofs_u_reco_level = []
est_p_level = []
est_s_level = []
est_global_level = []

u_reco_level = []
info_level = []

# ### GENERATE SAMPLES of solution and extimator
# log("--- GENERATE SAMPLES ---")

# NOTE The less regular the stochastic dimension the more samples you need
n_samples = 1000  # TODO How to choose number of samples sufficiently?

# n_all_samples = 2000  # TODO How to choose number of samples sufficiently?
# log(f"  generate training data for {n_all_samples} samples ...")
# with timeit("  ... ({:4.2f} s)"):
#     try:
#         z = np.load("data/training_data.npz")
#         ys_all = z['ys']
#         if ys_all.shape != (n_all_samples, p.field.M):
#             raise FileNotFoundError()
#         ws_all = z['ws']
#         us_all = z['us']
#         if us_all.shape != (n_all_samples, p.dofs()):
#             raise FileNotFoundError()
#         etas_all = z['etas']
#     except FileNotFoundError:
#         ys_all, ws_all, us_all, etas_all = sample_solution(info, n_all_samples, get_eta=True)
#         assert ys_all.shape == (n_all_samples, p.dofs())
#         os.makedirs("data", exist_ok=True)
#         np.savez_compressed("data/training_data.npz", ys=ys_all, ws=ws_all, us=us_all, etas=etas_all)
# TODO precomputation is not as simple as it seems: you need to take
#      physical refinement into acccount
# log(f"  drawing {n_samples} samples ...")
# with timeit("  ... ({:4.2f} s)"):
#     selection = np.random.choice(n_all_samples, n_samples, replace=False)
#     ys = ys_all[selection]
#     ws = ws_all[selection]
#     us = us_all[selection]
#     etas = etas_all[selection]
# NOTE we need to draw new samples when physic is refined due to different physical dofs


print()
log("--- GENERATE INITIAL SAMPLES ---")
log(f"  generate training data for {n_samples} samples ...")
with timeit("  ... ({:4.2f} s)"):
    ys, ws, us, etas = sample_solution(info, n_samples, get_eta=True)
    # NOTE eta is an estimator of the square of the error
    assert np.all(etas >= 0)
    etas = np.sqrt(etas)

for j in range(max_iterations):
    print()
    log(f"--- STARTING ITERATION {j+1} ---")
    log(f"  current physical dofs: {p.dofs()}")
    log(f"  current stochastic modes: {sdim}")

    ##
    ### SOLVE and ESTIMATE for samples ###
    ##

    log("--- RECONSTRUCT SOLUTION ---")

    # NOTE this is only required for diagnostics !
    u_reco = reconstruct(
        dims=sdim, nodes=ys[:, :len(sdim)].T, weights=ws, values=us, label="u"
    )  # dimensions: [mesh_vertices]+sdim
    ttdofs_u_reco_level.append(tt_dofs(u_reco))
    global_dofs_u_level.append(np.product(u_reco.dimensions))
    u_reco_level.append(u_reco)
    info_level.append(deepcopy(info))

    log("--- RECONSTRUCT ESTIMATOR ---")

    alpha_dim = [d+L for d in sdim] + [L+1]  # Lambda + L
    eta_reco = reconstruct(
        dims=alpha_dim, nodes=ys[:, :len(
            alpha_dim)].T, weights=ws, values=etas, label="eta"
    )  # dimensions: [mesh_cells]+alpha_dim

    # DERIVE ESTIMATOR CONTRIBUTIONS (deterministic and stochastic)
    log("--- DERIVE GLOBAL ESTIMATOR CONTRIBUTIONS ---")

    log("  compute physical/stochastic contributions ...")
    with timeit("  ... ({:4.2f} s)"):
        p_t, s_m, est_global = get_error_contributions(eta_reco, L)

    est_p = np.sum(p_t)
    # the entire stochastic part is est_global-np.sum(p_t) >= np.sum(s_m)
    est_s = est_global - est_p
    # est_s = np.sum(s_m)
    est_p_level.append(est_p)
    est_s_level.append(est_s)
    est_global_level.append(est_global)

    log(f"  total deterministic contribution: {est_p_level[-1]:.2e}")
    log(f"  total stochastic contribution:    {est_s_level[-1]:.2e}")

    ##
    ### MARK and REFINE ###
    ##

    log("--- MARK and REFINE ---")
    if est_p >= est_s:
        log("  marking physical domain ")
        marked_cells = marking(p_t, threshold)
        log(f"  marked {len(marked_cells)} cells")
        log("  refining physical space...")
        with timeit("  ... ({:4.2f} s)"):
            info = refine_mesh(marked_cells, info)
            p = Problem(info)  # update mesh for problem
        log(f"  generate new training data for {n_samples} samples ...")
        with timeit("  ... ({:4.2f} s)"):
            ys, ws, us, etas = sample_solution(info, n_samples, get_eta=True)
            # NOTE eta is an estimator of the square of the error
            assert np.all(etas >= 0)
            etas = np.sqrt(etas)
    else:
        log("  marking stochastic domain")
        idxs = marking(s_m, threshold)
        log(f"  marked {len(idxs)} modes: {idxs}")
        log("  refining stochastic space...")
        with timeit("  ... ({:4.2f} s)"):
            for idx in idxs:
                if idx < len(sdim):
                    sdim[idx] += 1
                else:
                    sdim = sdim + [2]

    # BREAK CONDITION
    if p.dofs() > max_dofs:
        break

print()
log("--- STOPPING ITERATION ---")
log(f"  final physical dofs: {p.dofs()}")
log(f"  final stochastic modes: {sdim}")

# ------------------------------------------------------------------------------
# COMPUTE ERROR
print()
log("--- COMPUTING ERROR ---")
log("  generate reference mesh")
info_ref = deepcopy(p.info)
q = Problem(info_ref)
for j in range(1):
    num_elements = q.mesh.num_cells()
    refine_mesh(range(num_elements), info_ref)  # refine uniform
    q = Problem(info_ref)  # update mesh for problem

log("  generate test data for 1000 samples ...")
with timeit("  ... ({:4.2f} s)"):
    # TODO: store the mesh as well so that you can check that the mesh is still correct...
    # try:
    #     z = np.load("data/test_data.npz")
    #     ys = z['ys']
    #     if ys.shape != (1000, q.field.M):
    #         raise FileNotFoundError()
    #     ws = z['ws']
    #     us = z['us']
    #     if us.shape != (1000, q.dofs()):
    #         raise FileNotFoundError()
    # except FileNotFoundError:
    #     ys, ws, us = sample_solution(info_ref, 1000)
    #     os.makedirs("data", exist_ok=True)
    #     np.savez_compressed("data/test_data.npz", ys=ys, ws=ws, us=us)
    ys, ws, us = sample_solution(info_ref, 1000)

error = []
log("  compute error ...")
with timeit("  ... ({:4.2f} s)"):
    stiffn = stiffness_matrix(info_ref)
    for j, u_reco in enumerate(u_reco_level):
        u_hs = eval_approx(u_reco, ys)
        u_hs = prolongate_x(u_hs, info_level[j], info_ref)
        diff = us - u_hs
        err = np.einsum('n,ni,in', ws, diff, stiffn@diff.T)
        error.append(err)

# ------------------------------------------------------------------------------
# NOTES

# maybe use reconstruction of fine solution to compute energy error.

# use projection instead of interpolation of finer mesh. This could be more
# feasible, since we can precompute samples on fine mesh. Interpolation
# requires a strictly finer mesh.

# ------------------------------------------------------------------------------
# SAVE
print()
log("--- SAVING ---")
os.makedirs("data", exist_ok=True)
np.save("data/error.npy", error)
np.save("data/est_s_level.npy", est_s_level)
np.save("data/est_p_level.npy", est_p_level)
np.save("data/est_global_level.npy", est_global_level)
np.save("data/ttdofs_u_reco_level.npy", ttdofs_u_reco_level)
np.save("data/global_dofs_u_level.npy", global_dofs_u_level)

# ------------------------------------------------------------------------------
# PLOT
print()
log("--- PLOTTING ---")
os.makedirs("img", exist_ok=True)

# PLOT CONVERGENCE
levels = range(len(est_s_level))
plt.figure()
plt.semilogy(gobal_dofs_level, np.sqrt(est_p_level), "o--", label=r"$p$")
plt.semilogy(gobal_dofs_level, np.sqrt(est_s_level), "o--", label=r"$s$")
plt.semilogy(gobal_dofs_level, np.sqrt(est_global_level),
             "o-", label=r"$|| \eta ||_{\mathrm{F}}^2$")
plt.semilogy(gobal_dofs_level, np.sqrt(error), "o-",
             label=r"$|| u - u_h ||_{L^2\otimes H^1_0}^2$")
plt.xlabel("global DoFs ($u$)")
plt.legend()
plt.grid()
plt.savefig("img/convergence.png")

# PLOT MESHES
plt.figure()
plot(p.mesh)
plt.savefig("img/mesh_finest.png")

plt.figure()
plot(q.mesh)
plt.savefig("img/mesh_ref.png")
