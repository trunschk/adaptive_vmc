# standard imports
import sys
import os
import datetime
import time
import shutil
import numpy as np
from numpy.polynomial.legendre import Legendre
from numpy.polynomial.hermite_e import HermiteE
import scipy.integrate
from scipy.special import zeta, loggamma
from contextlib import contextmanager
from copy import deepcopy


class Logger():
    def __init__(self, fileName='logfile.log', filePath='./Log/', overwrite=True):
        """
        Set standart output to terminal and file.
        """
        if fileName[-4:] != '.log':
            now = datetime.datetime.now()
            fn = '{}_{}{:02}{:02}-{:02}{:02}{:02}.log'.format(
                fileName, now.year, now.month, now.day, now.hour,
                now.minute, now.second)
        else:
            fn = fileName
        self.terminal = sys.stdout
        os.makedirs(filePath, exist_ok=True)
        # if file exists and overwrite is true, move file and write in fresh.
        if os.path.isfile(filePath+fn) and overwrite:
            shutil.move(filePath+fn, filePath+fn+".old")
        self.log = open(filePath+fn, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        pass


def ts():
    """Time stamp for logging."""
    return time.strftime("[%Y-%m-%d %H:%M:%S]", time.localtime())


def log(*args, **kwargs):
    """Print with time stamp."""
    print(ts(), *args, **kwargs)


def logf(file, *args, **kwargs):
    """Print with time stamp to terminal and file."""
    path = file[:file.rfind("/")+1] if file is not None else None
    if path is not None and len(path) > 0:  # create dir if path is specified
        os.makedirs(path, exist_ok=True)
    time_stamp = ts()
    if file is not None:
        if "overwrite" in kwargs.keys() and kwargs["overwrite"] is True:
            with open(file, "w") as f:  # clean file
                f.write("")
            _ = kwargs.pop("overwrite", "")
        with open(file, "a") as f:
            if len(args) > 0:
                f.write(time_stamp+" ")
                f.write(*args)
                f.write("\n")
    print(time_stamp, *args, **kwargs)


@contextmanager
def timeit(title, verbose=True):
    """Time functions and print time."""
    # TODO perf_counter vs process_time
    start = time.perf_counter()
    try:
        yield
    finally:
        if verbose:
            log(title.format(time.perf_counter()-start))


def transpose(ls):
    """Transpose list."""
    return list(zip(*ls))


def factorial(n):
    """Compute factorial of a natural number.

    Parameters
    ----------
    n : int
        Number for factorial.

    Returns
    -------
    ret : float
        Factorial n!
    """
    assert n >= 0
    return float(np.math.factorial(n))


def switch_columns(ls):
    """Switch coulumns of (n x 2) list."""
    return list(zip(*reversed(transpose(ls))))


def leg_triple_product(a, b, c, normalized=False):
    """Compute triple product of Legendre polynomials analytically.

    Compute int_{-1}^{1} P_a P_b P_c dx for 1D Legendre Polynomials P by
    Gaunt's formula. If normalized is true, use normalized Legendre polynomials
    in the triple product.
    """
    if not (a+b+c) % 2 == 0:
        return 0
    s = (a+b+c)/2
    if s < a or s < b or s < c:
        return 0

    if normalized == False:
        normalized = [False]*3
    elif normalized == True:
        normalized = [True]*3
    assert len(normalized) == 3
    def A(n): return factorial(2*n)/2**n/factorial(n)**2 if n > 0 else 1
    # NOTE Normalization constant is sqrt(2n+1) for measure d\rho = 1/2 dx
    def fs(n): return np.sqrt((2*n+1))  # normalization constant
    norm_const = np.prod([
        fs(n) if normalized[i] else 1 for i, n in enumerate([a, b, c])])
    # NOTE the 1/2 is due to the measure, i.e. d\rho = 1/2 dx. For
    #      understandability, we thus do not cancel the 1/2 with the 2 in the
    #      next line.
    gaunt = A(s-a)*A(s-b)*A(s-c)/A(s)/(2*s+1) * norm_const * 1/2
    return 2*float(gaunt)


def herm_triple_product(a, b, c, normalized=False):
    """Compute triple product of Hermite polynomials analytically.

    Compute int_{-inf}^{inf} P_a P_b P_c dx for 1D probabilist Hermite
    Polynomials P by formula. If normalized is true, use normalized probabilist
    Hermite polynomials in the triple product.
    """
    # NOTE: We use that n! = Gamma(n+1) and do all computations in logarithmic
    #       space for more stability.
    if not (a+b+c) % 2 == 0:
        return 0
    if (a+b-c < 0) or (a-b+c < 0) or (-a+b+c < 0):
        return 0

    if normalized == False:
        normalized = [False]*3
    elif normalized == True:
        normalized = [True]*3
    assert len(normalized) == 3

    def log_fac(n): return loggamma(n+1)  # log(n!)
    def log_A(i, j, k): return log_fac((i+j+k)/2)  # log( [(i+j+k)/2]! )
    # NOTE - Normalization constant is 1/sqrt(n!)
    #      - measure is d\rho = exp(-x**2/2) / sqrt(2*pi) dx
    def fs(n): return -log_fac(n)/2  # log( normalization constant )
    norm_const = np.sum([
        fs(n) if normalized[i] else 0 for i, n in enumerate([a, b, c])
    ])  # use log(1) = 0 for "else" case
    val = (  # log{ norm_const * a!b!c! / [ A(a,b,-c) A(a,-b,c) A(-a,b,c) ] }
        log_fac(a) + log_fac(b) + log_fac(c)
        - log_A(a, b, -c) - log_A(a, -b, c) - log_A(-a, b, c)
        + norm_const
    )

    return np.exp(val)


def leg_triple_product_slow(a, b, c, normalized=False):
    """Slow triple product due to numerical integration.

    Compute int_{-1}^{1} P_a P_b P_c dx for 1D Legendre Polynomials P by
    integration. If normalized is true, use normalized Legendre polynomials
    in the triple product.
    """
    if not (a+b+c) % 2 == 0:
        return 0
    s = (a+b+c)/2
    if s < a or s < b or s < c:
        return 0
    leg1, leg2, leg3 = Legendre.basis(a), Legendre.basis(b), Legendre.basis(c)
    if normalized:
        def fs(n): return np.sqrt((2*n+1))  # normalization constant
        leg1 = fs(a)*leg1
        leg2 = fs(b)*leg2
        leg3 = fs(c)*leg3

    def rho(y): return 0.5
    def f(y): return leg1(y)*leg2(y)*leg3(y)*rho(y)
    val = scipy.integrate.quad(f, -1, 1)[0]
    return val


def herm_triple_product_slow(a, b, c, normalized=False):
    """Slow triple product due to numerical integration.

    Compute int_{-inf}^{inf} P_a P_b P_c dx for 1D Hermite Polynomials P by
    integration. If normalized is true, use normalized Hermite polynomials
    in the triple product.
    """
    if not (a+b+c) % 2 == 0:
        return 0
    if (a+b-c < 0) or (a-b+c < 0) or (-a+b+c < 0):
        return 0
    herm1, herm2, herm3 = HermiteE.basis(
        a), HermiteE.basis(b), HermiteE.basis(c)
    if normalized:
        # normalization constant
        def fs(n): return 1/np.sqrt(factorial(n))
        herm1 = fs(a)*herm1
        herm2 = fs(b)*herm2
        herm3 = fs(c)*herm3

    def rho(y): return np.exp(-y**2/2)/np.sqrt(2*np.pi)
    def f(y): return herm1(y)*herm2(y)*herm3(y)*rho(y)
    val = scipy.integrate.quad(f, -np.inf, np.inf)[0]
    return val


def hermite_base_product_matrix(d, s1, s2, z, m, decay, sZeta, normalized):
    """Compute :math:`\\int H_1 H_2 \\zeta^z \\,\\mathrm{d}\\pi_0.

    Compute the basis product matrix of (scaled) HermiteE polynomials w.r.t.
    some density, i.e.

    .. math::
        \\int_{\\mathbb{R}} H_{i}^{[1]}(y) H_{j}^{[2]}(y) \\zeta_s(y)^z \\,\\mathrm{d}\\pi_0(y),

    where H^{[1]}, H^{[2]} are scaled HermiteE polynomials and

    .. math::
        \\zeta_s(y) = \\frac{1}{\\sigma(s)} \\exp( (\\frac{1}{2} -\\frac{1}{2\\sigma(s)^2}) y**2 ).

    Here the variance is given by

    .. math:
        \\sigma_m(s) = \\exp(s \\Vert \\gamma \\Vert_{L^\\infty(D)} ),
    where :math:`\\gamma` is the m-th expansion coefficient of the affine
    exponent.

    Parameters
    ----------
    d : int
        Maximum polynomial degree for basis functions.
    s1, s2 : float
        Scalings of the Hermite basis polynomials w.r.t. the standard HermiteE
        basis functions, e.g. :math:`H^{\\vartheta\\rho}`.
    z : int (>= 0)
        Power of :math:`\\zeta_s`.
    m : int (> 0)
        Mode number of stochastic parameter.
    decay : float (> 0)
        Decay rate specified for the problem.
    sZeta: float (> 0)
        Scaling factor :math:`s` for :math:`\\zeta_s`,
        e. g. :math:`\\zeta_{\\vartheta\\rho}`.
    normalized : bool
        Use normalized (scaled) HermiteE polynomials.
    """

    assert m > 0 and decay > 0 and sZeta > 0
    assert s1 >= 0 and s2 >= 0
    sigma1 = compute_scaling_constant(m, decay, s1)
    sigma2 = compute_scaling_constant(m, decay, s2)
    sigmaZeta = compute_scaling_constant(m, decay, sZeta)

    # NOTE: \zeta_s(y)**z * \pi_0(y)
    #       = exp( -1/2 (z/sigma(s)**2 - z + 1) y**2 ) / sqrt(2 pi) / sigma(s)**z
    def density(x, z=z, sigmaZeta=sigmaZeta):
        """ Density \zeta_s(x)*\pi_0(x). """
        val = 1 / (np.sqrt(2*np.pi) * sigmaZeta**z)
        val *= np.exp(-1/2 * (z/sigmaZeta**2 - z + 1) * x**2)
        return val

    c_norm = np.array([1/np.sqrt(factorial(j)) for j in range(d+1)])
    H1 = get_scaled_herm_poly(d, s=1/sigma1, c=c_norm)
    H2 = get_scaled_herm_poly(d, s=1/sigma2, c=c_norm)

    mat = np.zeros((d+1, d+1))
    for i in range(d+1):
        for j in range(d+1):
            def integrand(x):
                return H1[i](x)*H2[j](x)*density(x)
            mat[i, j], _ = scipy.integrate.quad(integrand, -np.inf, np.inf)

    # scale matrix by normalization constants
    # NOTE This is the same as dot( c_norm, dot(mat, c_norm**(-1)) )
    C = np.ones(d+1).reshape(-1, 1) if normalized else c_norm.reshape(-1, 1)
    mat = C * (1/C).reshape(1, -1) * mat
    return mat


def compute_scaling_constant(m, decay, s, gamma=0.9):
    """
    Compute the scaling constant for the stronger measure in the lognormal
    case. This is
          sigma_m = exp( s * beta_m )
    where
          beta_m = || a_m ||_{L^\infty(D)} = gamma / zeta(decay) * m**(-decay).

    Parameters
    ----------
    m : int
        Mode number (number of stochastic parameter)
    decay : float
        Decay rate for the diffusion coefficient expansion terms. Needs to be
        grater than 0.
    s : float
        Scaling factor. Needs to be greater than 0.
    gamma : float
        Problem dependent scaling constant.
    """
    assert decay > 0
    beta_m = gamma / zeta(decay) * m**(-decay)
    return np.exp(s*beta_m)


def get_scaled_herm_poly(d, s=1, c=None):
    """
    Compute scaled Hermite polynomials up to degree. The polynomials are
        P_j(x) = c[j] * H_j(s * x)
    where H_j are the standard Hermite Polynomials. If s is one, directly
    return the standard Hermite polynomials.

    Parameters
    ----------
    d : int
        Maximal polynomial degree of basis functions.
    s : float, default = 0
        Scaling for Hermite Polynomials.
        (Default 1 represents no scaling, i.e. P_j = Q_j.)
    c : float, default = None
        Factor for normalization of Hermite polynomials.
        (Default 1 represents no normalization.)
    """
    if c is None:
        c = np.ones(d+1)

    def hermite(deg):
        return np.polynomial.hermite_e.HermiteE.basis(deg)

    def sHermite(deg, s=s):
        return hermite(deg)(np.polynomial.Polynomial((0, s)))

    if s == 1:
        p = [c[deg]*hermite(deg) for deg in range(d+1)]  # Hermite poly
    else:
        p = [c[deg]*sHermite(deg, s=s) for deg in range(d+1)]  # sHermite poly

    return p


def uniform_samples(n_samples, dimension):
    """Generate uniform samples on [-1,1]."""
    return 2*np.random.rand(n_samples, dimension)-1


def normal_samples(n_samples, dimension):
    """Generate standard normal samples (mean is 0, var is 1)."""
    return np.random.normal(0, 1, (n_samples, dimension))


def get_y_samples(info, n_samples, samples=None):
    """Generate independent parameter samples accroding to distribution."""
    # always use samples with specified coefficient expansion
    if not samples is None:
        assert samples.shape[0] == n_samples
        assert samples.shape[1] == info["expansion"]["size"]
        ys = np.array(samples)
    elif info["sampling"]["distribution"] == "uniform":
        ys = uniform_samples(n_samples, info["expansion"]["size"])
    elif info["sampling"]["distribution"] == "normal":
        ys = normal_samples(n_samples, info["expansion"]["size"])
    else:
        dist = info["sampling"]["distribution"]
        raise ValueError(f"unknown distribution: {dist}")

    return ys
